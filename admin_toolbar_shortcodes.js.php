/*
<?php 
	
	Header("content-type: application/x-javascript");
	require_once('../../../wp-load.php');
    require_once('../../../wp-admin/includes/admin.php');
    require_once('part_phone_infinity_pools.php');
    do_action('admin_init');
    
    if ( ! is_user_logged_in() )
        die('You must be logged in to access this script.');
    
    //if we want to display or filter all active shortcodes
    global $shortcode_tags;
    $ordered_sct = array_keys($shortcode_tags);
    sort($ordered_sct);

    $items_js_template = "{text: '[##sct##]', onclick : function() {
			 		if(tinyMCE.activeEditor.selection.getContent() != ''){
                    	tinyMCE.activeEditor.selection.setContent('[##sct##]' + tinyMCE.activeEditor.selection.getContent() + '[/##sct##]');
	                }
	                else{
	                    tinyMCE.activeEditor.selection.setContent('[##sct##]');
	                }
					}},";
?>
*/



//////////
// BUTTON for main frn_phone shortcode
/////////
(function() {  
    tinymce.create('tinymce.plugins.phoneButton', {  
        init : function(ed, url) {  
            ed.addButton('phoneButton', {  
                title : 'Add Any Phone Number',  
                image : url+'/button-phone.png',
                onclick : function() {  
                     ed.selection.setContent('[frn_phone number=""]');  
                }  
            });  
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('phoneButton', tinymce.plugins.phoneButton);  
})();




//////////
// DROPDOWN FOR OUR FACILITY PHONE SHORTCODES ONLY
/////////
	// This relies on the infinity pool global var to get list of SCs
(function() {

    <?php       $phone_array=""; $i=0;
                global $infinity_pools; 
                foreach($infinity_pools as $pool) {
                    if(!isset($pool['sc'])) $pool['sc']=""; $comma=",";
                    if($i==0) $comma="";
                    if($pool['sc']!=="") $phone_array.=$comma."
                        '[".$pool['sc']."_phone]'"; 
                    $i++;
                }
    ?>

    var frn_phs = [<?=$phone_array."\n";?>
                  ];
    
    tinymce.PluginManager.add( 'frn_phones', function( editor ){
        
        var items = [];

        tinymce.each( frn_phs, function( frn_shortcode ){
            items.push({
                text: frn_shortcode,
                onclick: function(){
                    var content = tinyMCE.activeEditor.selection.getContent();
                    editor.insertContent( frn_shortcode + " " + content );
                }
            });
        });

        editor.addButton( 'frn_phones', {
            type: 'menubutton',
            text: 'Phone Shortcode',
            icon: 'menu',
            menu: items
        });

    });
})();




//////////
// DROPDOWN FOR OUR FACILITY SMS SHORTCODES ONLY
/////////
    // This relies on the infinity pool global var to get list of SCs
(function() {

    <?php       $sms_array=""; $i=0;
                global $infinity_pools; 
                foreach($infinity_pools as $pool) {
                    $comma=",";
                    if(!isset($pool['sc'])) $pool['sc']=""; 
                    if(!isset($pool['sms'])) $pool['sms']=""; 
                    if($i==0) $comma="";
                    if($pool['sc']!=="" && $pool['sms']!=="") {
                        //sms field is only used to determine if a number is provided
                        $sms_array.=$comma."
                        '[".$pool['sc']."_sms]'"; 
                    }
                    $i++;
                }
    ?>

    var frn_sms = [<?=$sms_array."\n";?>
                  ];
    
    tinymce.PluginManager.add( 'frn_sms_shortcodes', function( editor ){
        
        var items = [];

        tinymce.each( frn_sms, function( frn_sms_sc ){
            items.push({
                text: frn_sms_sc,
                onclick: function(){
                    var content = tinyMCE.activeEditor.selection.getContent();
                    editor.insertContent( frn_sms_sc + " " + content );
                }
            });
        });

        editor.addButton( 'frn_sms_shortcodes', {
            type: 'menubutton',
            text: 'SMS Shortcode',
            icon: 'menu',
            menu: items
        });

    });
})();




//////////
// DROPDOWN FOR ALL REGISTERED SHORTCODES IN THE SYSTEM
/////////

(function() {

    <?php       $sc_array=""; $sc_i=0;
                foreach($ordered_sct as $sct):
                    if($sct!=="") {
                        $comma=",";
                        if($sc_i==0) $comma="";
                        //skip listing FRN shortcodes that are not meant for content
                        if($sct!=="frn_related_list" 
                            && $sct!=="frn_footer" 
                            && $sct!=="frn_privacy_url"
                            && $sct!=="lhn_inpage"
                            && stripos($sct,"_phone")===false
                            && stripos($sct,"_sms")===false
                        ) { $sc_i++;
                            $sc_array.=$comma."
                    '".$sct."'"; 
                        }
                        //by this point the plain shortcode already printed (i.e. #1). We're just adding versions with popular attributes.
                        if($sct=="frn_phone") {
                            $attr=""; $i=2; 
                            while($i<3) {
                                //there is really only one version, but just keeping approach similar to the normal loop
                                if($i==2) $attr=" number=\"\"";
                                $sc_array.=$comma."
                    '".$sct.$attr."'"; 
                                $i++;
                            }
                            $sc_i++;
                        }
                        //by this point the plain shortcode already printed (i.e. #1). We're just adding versions with popular attributes.
                        /*
                        elseif($sct=="callout") {
                            $attr=""; $i=2; 
                            while($i<5) {
                                if($i==2) $attr="=\'border\'";
                                elseif($i==3) $attr="=\'inverse\'";
                                elseif($i==4) $attr="=\'secondary\'";
                                $sc_array.=$comma."
                    '".$sct.$attr."'"; 
                                $i++;
                            }
                            $sc_i++;
                        } */
                    }
                endforeach;
    ?>

    var codes = [<?=$sc_array."\n";?>
                ];
    
    tinymce.PluginManager.add( 'frn_listshortcodes', function( editor ){
        
        var items=[];

        tinymce.each( codes, function( code ){
            var code_base="", code_end="", limit=1, i=0, content="";
            if(code=="callout") limit=4; //limit lets us add the callout shortcode 3 more times using different attributes that are commonly used on our sites.
            while(i<limit) {
                code_base=callout(code,i);
                items.push({
                    text: code_base,
                    onclick: function(){
                        content = tinyMCE.activeEditor.selection.getContent();
                        if(content.trim()!=="") {
                            code_end="[/"+code+"]";
                        }
                        editor.insertContent( code_base + content + code_end );
                    }
                });
                i++; 
            }
            limit=1; i=0;
        });

        editor.addButton( 'frn_listshortcodes', {
            type: 'menubutton',
            text: 'Theme Shortcodes',
            icon: 'menu',
            menu: items
        });

    });
})();

function callout(code,i) {
    var attr="";
    if(code=="callout") {
        if(i==1) attr='="border"';
        else if(i==2) attr='="inverse"';
        else if(i==3) attr='="secondary"';
    }
    return "["+code+attr+"]";
}