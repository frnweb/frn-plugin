<?php

// This PHP file adds the TOC options SUB menu page to the main FRN Settings Menu
add_action('admin_menu', 'frn_plugin_subpage_boxes');
function frn_plugin_subpage_boxes() {
	add_submenu_page( 'frn_features',"FRN Automatic Context Boxes", "Auto Context Boxes", 'manage_options', 'frn_boxes', 'frn_boxes_layout');
	add_settings_section('site_personal_code', '', 'frn_boxes_text_before_form', 'frn_boxes');
		add_settings_field('frn_plugin_personal_activate', "<span style='white-space:nowrap;'>Activate All Boxes <a href='javascript:showhide(\"frn_plugin_personal_act_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_personal_radio', 'frn_boxes', 'site_personal_code');
		add_settings_field('frn_plugin_personal_boxes', "", 'frn_personal_boxes', 'frn_boxes', 'site_personal_code');
	add_action('admin_init', 'frn_boxes_variable');
}

function frn_boxes_variable() {
	//registers the variable for the database
	register_setting( 'frn_plugin_boxes', 'frn_personal_boxes','plugin_options_personalboxes'  );
					   // main variable  //specific var for page  //function
}
 



//////////////
//// GLOBAL VARIABLES (DEFAULTS)
/////////////

	//Global enables us to use these variables across functions, but it may mean another theme or plugin might shoose the same variable.
	//In the end, this makes it easier for defaults to be found and changed by us.
	//global $personal_box_var; global $frn_cta_mssg; global $frn_trigger_words;
	$GLOBALS['personal_box_var'] = "frn_personal_boxes"; //don't forget saving function too
	$GLOBALS['message_cta'] = "
					<div class=\"frn_cta_title\">
						Have questions about this %%drug%%? We've helped thousands since 1995 determine what direction they need to go.
					</div>
					<strong class=\"hide-for-small-only\">Confidential. No judgment. We're here to help.</strong>";
	$GLOBALS['trigger_words'] = "rehab, treatment, facility, center, outpatient, inpatient, counsel, intervention, recovery, emergency, get clean, withdrawal";




//////////////
//// MAIN ADMIN FUNCTION
/////////////

function frn_personal_boxes() {
	
	/*

	//Default global variable should be frn_personal_boxes
	//This is the main function that executes the personal boxes settings and pulling from the DB.
	//This new approach relies on an array to build the form and store settings. It allows us to add an infinite number of boxes.
	
	//OUR EXAMPLE ARRAY STRUCTURE SET BELOW FOR BOXES
	$frn_personal_boxes = array(
		[activation] => Deactivate 
		[bckgd] => 
		[font] => 
		[boxes] => Array ( 
			[0] => Array ( 
				[id] => 
				[type] => 
				[activation] => Activate 
				[autoinsert] => words 
				[position] => 
				[excludes] => 
				[chat_id] => 
				[email_image] => 
				[key_pages] => Array ( 
					[0] => Array ( 
						[url] => 
						[anchor] => 
						[hover_text] => 
						) 
					) 
				) 
			) 
		) 
	);

	//Basic way to pull data deep within this array
	echo $frn_personal_boxes[0]['settings']['message'];
	
	// DETECTING TYPE:
	//Since we are using the type as the key (i.e. variable) to house the settings, we just need to check if the key is set. If not, then we check for the next type.
	if(isset($test_array[0]['settings'])) "";
	
	// PULLING DATA IN A LOOP
	When we use a foreach or other type of loop, we store the parent part of the array into a single variable like "cta_settings" in the next example. 
	At that point, all we have to do is use the final variable names to get the data. We don't have to use the entire array brackets.
	$cta_settings['message']
	
	// SAVING DATA INTO OUR ARRAY IN A FORM "NAME"
	The execution of it:
	NAME="<?=$GLOBALS['personal_box_var'];?>[$i][settings][activation]"
		DETAILS:
		$GLOBALS['personal_box_var'] = the main variable where everything is stored. It's global so we can use it in any function but needs to be unique to avoid any other plugin or theme from using it.
		$i = this must be used when saving since we are using cta as a variable and there could be many in our array. If we didn't have the number, then any subsequent cta after the first one would just save over the first.
		"cta" = is what houses all the settings, like activation in this example
		"activation" = is what houses the final data we'll want to display on pages.
	

	*/

	
	?>
	<div id="frn_personal_boxes">
	<?php
	
	
	// Load personal boxes variable
	$personal_boxes_feature = get_option($GLOBALS['personal_box_var']);

	
	
	//checks if there is even a variable stored for the boxes (basically, if this is the first time ever that the form has even been displayed)
	if(!$personal_boxes_feature) { 
	
		?>
		<div id="frn_personal_boxes_group" style="display:block;">
		<?php 
		//if not, then just post defaults
		echo frn_personal_box(0);
		echo frn_personal_box(1,'kps','','','','','','','','','',1,'','',array(
			array('url'=>'','anchor'=>'Can I afford rehab?','hover_text'=>'[use article title]'),
			array('url'=>'','anchor'=>'Do I need rehab?','hover_text'=>'[use article title]'),
			array('url'=>'','anchor'=>'Are there different treatments?','hover_text'=>'[use article title]'),
			array('url'=>'','anchor'=>'How do I tell my family/boss?','hover_text'=>'[use article title]')
		));
		echo frn_personal_box(2,'kps','','','','','','addiction,abuse',$GLOBALS['trigger_words'],'','',2,'','',array(array('url'=>'','anchor'=>'[side effects or how to know]','title'=>'')));
		echo frn_personal_box(3,'kps','','','','','','addiction,abuse',$GLOBALS['trigger_words'],'','',3,'','',array(array('url'=>'','anchor'=>'[do I need treatment or overdose]','title'=>'')));
		echo frn_personal_box(4,'kps','','','','','','overdose',$GLOBALS['trigger_words'],'','',4,'','',array(array('url'=>'','anchor'=>'[side effects or avoid overdose]','title'=>'')));
		echo frn_personal_box(5,'kps','','','','','','overdose',$GLOBALS['trigger_words'],'','',5,'','',array(array('url'=>'','anchor'=>'[am i addicted or how do I stop]','title'=>'')));
		echo frn_personal_box(6,'kps','','','','','','the site drug (include street names) or mental health or geo',$GLOBALS['trigger_words'],'','',6,'','',array(
			array('url'=>'','anchor'=>'[side effects]','hover_text'=>''),
			array('url'=>'','anchor'=>'[street names]','hover_text'=>''),
			array('url'=>'','anchor'=>'[addiction]','hover_text'=>''),
			array('url'=>'','anchor'=>'[other]','hover_text'=>'')
		));
		?>
		</div>
		<?php 
	}
	else {
		if(!isset($personal_boxes_feature['boxes'])) $personal_boxes_feature['boxes']="";
		$boxes=$personal_boxes_feature['boxes'];
		if(is_array($boxes)) {
			//echo frn_personal_box();
			//print_r($boxes);
			//if it's there, it is an array (the following can't happen unless it is)
			//echo "<h2>".count($boxes)."</h2>";
			//echo "<h2>".$personal_boxes_feature['activation']."</h2>";
			if($personal_boxes_feature['activation']=="" || $personal_boxes_feature['activation']=="Deactivate") $display="none";
			else $display="block";
		?>
		<div id="frn_personal_boxes_group" style="display:<?=$display;?>;">
		<?php 
			
			$i=0;
			foreach($boxes as $box_id => $box) {
				//print_r($box);
				//example array structure
				//$frn_personal_boxes[0]['type'];
				//$frn_personal_boxes[0]['settings']['message'];
					
				//make sure these are defined if not so that the form fields show
				if(!isset($box_id)) $box_id="";
				if(!isset($box['type'])) $box['type']="cta";
				if(!isset($box['activation'])) $box['activation']="";
				if(!isset($box['autoinsert'])) $box['autoinsert']="";
				if(!isset($box['position'])) $box['position']="";
				if(!isset($box['sections'])) $box['sections']="";
				if(!isset($box['excludes'])) $box['excludes']="";
				if(!isset($box['triggers'])) $box['triggers']="";
				if(!isset($box['negatives'])) $box['negatives']="";
				if(!isset($box['dominant'])) $box['dominant']="";
				if(!isset($box['message'])) $box['message']="";
				if(!isset($box['chat_id'])) $box['chat_id']="";
				if(!isset($box['email_image'])) $box['email_image']="";
				if(!isset($box['key_pages'])) $box['key_pages']="";
				
				//run the box function. It will run for each box stored in $boxes.
				echo frn_personal_box(
					$box_id,
					$box['type'],
					$box['activation'],
					$box['autoinsert'],
					$box['position'],
					$box['sections'],
					$box['excludes'],
					$box['triggers'],
					$box['negatives'],
					$box['dominant'],
					$box['message'],
					$i,
					$box['chat_id'],
					$box['email_image'],
					$box['key_pages']
					);
				
				$i++;
				
			} 
		?>
		</div>
		<?php 
		} // endif it's an array check
		else echo frn_personal_box();
	}
	
	?>
	</div>
	<?php
	
}








function frn_boxes_layout() {
	?>
	<div class="wrap">
		<h2>FRN Plugin: Automated Context Boxes</h2>
		<div class="intro_text" style="margin-top:50px;">
			<h3>How To Use This:</h3>
			<p>This feature places "boxes" within the content of pages or posts based on keyword triggers in page titles. You can add related posts, a custom callout, the site's typical CTA options, or a list of links that help the person move on to the next stage in our Rehab Decision Journey.</p>
			<p><b>Templates:</b> The system comes with a beginning template of potential link options and keyword triggers based on our typical Rehab Decision Journey. There are no links, so using the keywords provided in the link fields, you'll need to search the site for articles that relate or just delete the links ("[-]"). There is no limit to the number of boxes you can have or links in each box.</p>
			<p><b>Styles: </b>There are two different "recommended for you" box styles depending on how many links you have. One style is for just one or two links. The other is for 3 or more. If you create two boxes and based on the size of the post, if they end up being placed into the same location in the content, they will combine into a new design to avoid things looking like they're a mistake. Unlike our other automated features, there is no starting style for these boxes. You'll have to customize the CSS yourself.</p>
			<p>Click the help icons <img src="<?=$GLOBALS['help_image'];?>" /> to know the options for each section.</p>
			<br />
			<hr />
			<br />
		</div>
		<form action="options.php" method="post" class="frn_styles">
			<?php
				settings_fields( 'frn_plugin_boxes' );
				do_settings_sections( 'frn_boxes' );
			?>
			<table class="form-table">
				<tr>
					<th></th>
					<td>
						<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
					</td>
				</tr>
			</table>
			</div>
		</form>
	</div>
	<?php 

}


function frn_boxes_text_before_form() {
	?>
	<div class="intro_text">
		<p>Individual boxes are automatically inserted if activated. Otherwise, use the shortcode. 
		Use this to create messaging that helps a person move to the next stage in their decision process&#8212;such as an article that answers their next question or, if they are on a page about rehab or treatment, display our contact options (i.e. CTAs).</p>
		
		<h4><a href='javascript:showhide("context_box_help")' ><img src="<?=$GLOBALS['help_image'];?>" /></a> Context Box Shortcode: "<strong id="cta_shortcode" style="cursor:pointer;" onClick="selectText('cta_shortcode')">[frn_boxes id="#"]</strong>"</h4>
		<div id="context_box_help" style="display:none;" class="frn_help_boxes">
		<ol>
			<li>SHORTCODE: 
			<ul>
				<li>You don't have to use a shortcode. Use it only to display on specific pages.
				<li>Using the shortcode without an ID will just display the default CTA box&#8212;no customizations other than background and font styles. You can use this even if you have other boxes saved. It's independant of any settings.
				<li>Use <strong id="cta_shortcode2" style="cursor:pointer;" onClick="selectText('cta_shortcode2')">[frn_boxes id="#"]</strong> to display a specific box on the page. You'll find the ID after "In-Context Message" for each box you add below.
			</ul>
			<li>ACTIVATION: If you deactivate everything (the first option below), it will also keep boxes from showing using the shortcode. If you turn on auto-inserting but a page already has a shortcode in it, the automatic insertion will be deactivated and only the shortcode will work for the page.
			<li>PAGE LIMITS: Personal boxes will not show on category, tag, author, archive listing pages, or the homepage no matter what. We can program exceptions if it becomes important to.
			<li>PAGE LOAD will increase in microseconds the more boxes you add. For every webpage, it has to cycle through all the boxes to see which ones apply to the page loading. So try not to add too many.
		</ol>
		</div>
		<p> </p>
		<!--<div style="float:right;"><a href='javascript:showhide("frn_personal_boxes_group")' >Show/Hide Boxes</a></div>-->
	</div>
	<?php
}



//////////
//// Functions for building specific form fields
///set up form field functions so we don't have to lengthen the code
/// this RADIO function is used both:
///		(1) as the main activation option without any variables
///		(2) and within a box function with variables -- so this needs to work if variables aren't defined
function frn_personal_radio($id,$radio_type="main",$setting="",$i="",$help="") {
	
	//default
	if(is_array($id)) $id="";
		elseif($id!=="") $id="[boxes]".$id;
	$i_id=""; if($i!=="") $i_id="_".($i+1);
	
	if($radio_type=="main") {
		$personal_boxes = get_option($GLOBALS['personal_box_var']);
		$setting=$personal_boxes['activation'];
		$activate_chkd=""; $deactivate_chkd=" checked"; 
		if($setting=="Activate") {$activate_chkd=" checked"; $deactivate_chkd="";}

		if(!isset($personal_boxes['bckgd'])) $personal_boxes['bckgd'] = "";
		if(!isset($personal_boxes['font'])) $personal_boxes['font'] = "";
	} 
	else {
		$activate_chkd=" checked"; $deactivate_chkd=""; 
		if($setting=="Deactivate") {$activate_chkd=""; $deactivate_chkd=" checked";}
	} 
	if($radio_type=="main" ) {
		$help = '
		Activates or deactivates call-to-action boxes based on settings below. 
		IMPORTANT: If you put the shortcode in a category or other listing page (see list below), the box will still not show. 
		It is always disabled since we don\'t want the shortcode processed in the snippets of content like a typical shortcode is (i.e. improves page load time).
		<ol>
			<li>Categories
			<li>Archives
			<li>Search results
			<li>Date archives
			<li>Tags
		</ol>
		';

	}
	else {
		$help = '
			<div id="frn_plugin_personal_act_help'.$i_id.'" class="frn_help_boxes">
				Use this to keep your settings and still deactivate the box.
			</div>
		';
	}
	if($deactivate_chkd!=="") $frn_boxes_style="display:none;";
		else $frn_boxes_style="display:block;";

	$showhide="";
	if($radio_type=="main" ) {
		$showhide = " onClick=\"showhide('frn_boxes_style','block'); showhide('frn_personal_boxes_group','block');\" ";
	}
	
	echo '
	<input id="frn_boxes_activation'.$i_id.'" type="radio" name="'.$GLOBALS['personal_box_var'].$id.'[activation]" value="Activate"'.$activate_chkd.$showhide.' /> Activate <br />
	<input id="frn_boxes_activation'.$i_id.'" type="radio" name="'.$GLOBALS['personal_box_var'].$id.'[activation]" value="Deactivate"'.$deactivate_chkd.$showhide.' /> Deactivate<br />
	';
	if($radio_type=="main" ) {
		echo '
		<div id="frn_boxes_style" style="'.$frn_boxes_style.'" >
			<br />
			<table class="frn_options_table">
			<tr>
				<td><span style="white-space:nowrap;">Background CSS: <a href="javascript:showhide(\'frn_plugin_personal_bckgd_help\');" ><img src="'.$GLOBALS['help_image'].'" /></a></span></td>
				<td>';
				frn_personal_text('','bckgd',$personal_boxes['bckgd']);
				echo '</td>
			<tr>
				<td><span style="white-space:nowrap;">Font CSS: <a href="javascript:showhide(\'frn_plugin_personal_font_help\');" ><img src="'.$GLOBALS['help_image'].'" /></a></span></td>
				<td>';
				frn_personal_text('','font',$box_settings['font']);
				echo '
				</td>
			</tr>
			</table>
		</div>
		';
	}
	echo '<div id="frn_plugin_personal_act_help'.$i_id.'" class="frn_help_boxes">'.$help.'</div>
	'; 
}
/// these two functions are used to build text fields when called for a main personal box variable so the right information is added for the text field function
function frn_personal_bckgd() {
	
// DEACTIVATED 2/15/17 and moved to the prior function in order to hide this field unless the boxes were activated.

	//this is because WP requires us to call a function without variables. So it helps insert our default variables so we don't have to duplicate code.
	//$box_settings = get_option($GLOBALS['personal_box_var']);
	//if(!isset($box_settings['bckgd'])) $box_settings['bckgd'] = "";
	//frn_personal_text('','bckgd',$box_settings['bckgd']);
}
function frn_personal_font() {
	
	// DEACTIVATED 2/15/17 and moved to the prior function in order to hide this field unless the boxes were activated.

	//this is because WP requires us to call a function without variables. So it helps insert our default variables so we don't have to duplicate code.
	//$box_settings = get_option($GLOBALS['personal_box_var']);
	//if(!isset($personal_boxes['font'])) $personal_boxes['font'] = "";
	//frn_personal_text('','font',$box_settings['font']);
	
}


/////////
/// Functions for Building the Boxes
function frn_personal_box($id="", $type="",$box_activate="",$auto="",$position="",$section="",$excludes="",$triggers="",$negatives="",$dominant="",$message="",$i=0,$chat_id="",$email_image="",$key_pages="") {
	/*
	
	This is used to print the set of fields that make up one box.
	The function that triggers this is after this one. It uses a loop to print an instance of this function for every box in the array.
	Even this function calls upon other functions that actually create the fields. 
	So this mostly just determines the settings that are then passed to the perspective form field function (defined prior to this function).
	This approach is used to keep from having to duplicate the same code for first use and subsequent use scenarios--reduces bulk, human error, and speed in the end (once we understand the structure).
	To understand how the array build works and how we pull and save information, see the main function below this one. It includes the notes on how things are structured.
	
	THE PLAN for EACH BOX: 
		select type dropdown (3 options)
		activation radio options
		auto insert:
			activation dropdown (3 options)
			exclude ids text field
			activate trigger words radio options
			trigger words textarea
			position dropdown
		message textarea
		IF CTA:
			chat id text field
			email image text field
		IF KEY PAGES:
			url text field
			linked text text field
			hover text text field
	
		*/
		
		
		
		//defaults
		if($id=="") $id=0; //case happens when it's the first time the admin loads (it's unlikely that id will be blank otherwise)
		$onpage_id=$id;
		$box_id="[".$id."]";
		$field_id = "frn_personal_box";
		
		
	?>
		<div id="<?=$field_id;?>_<?=$i+1;?>" class="<?=$field_id;?>" >
		
		<div class="frn_personal_add_delete_<?=$i;?>" style="float:right;">
			<a style="  font-size: 18px;
			  text-decoration: none;
			  background-color: #fff;
			  border-radius: 5px;
			  padding: 0 6px;
			  border: 1px dashed#ccc;
			  display: inline-block;" href="javascript:add_contact_boxes('<?=$GLOBALS['personal_box_var'];?>','<?=$field_id;?>',<?=$i+1;?>);" title="Add a new box form to the end">+</a>
			<a style="  font-size: 18px;
			  text-decoration: none;
			  background-color: #fff;
			  border-radius: 5px;
			  padding: 0 6px;
			  border: 1px dashed#ccc;
			  display: inline-block;" href="javascript:add_contact_boxes('<?=$GLOBALS['personal_box_var'];?>','<?=$field_id;?>',<?=$i+1;?>,'delete');" title="Remove this box">&#8211;</a>
		</div>
		<h3 style="margin: 0 0 8px;">
			<span class="frn_list_number"><?=$i+1;?></span> In-Context Message (ID:<?=$onpage_id;?>)
			<a href='javascript:showhide("frn_personal_conflicthelp_<?=$i;?>")' style="font-size: 13px; margin-right: 20px; margin-top: 3px; font-weight: normal; float: right;">[box conflicts?]</a>
		</h3>
		<input type="hidden" name="<?=$GLOBALS['personal_box_var'];?>[boxes]<?=$box_id;?>[id]" value="<?=$id;?>" />
		<?php

		
		
		//////////
		// type dropdown
		//////////
		//we use the dropdown to set the array variable (or "key" in an array)
		if($type=="" || is_array($type)) $type="";
		$fieldvar="type"; $type_options="";
		$help = '
		<ol>
			<li>Select the type of box that you want to use.</li>
			<li>Use the round selection options to activate this box or keep the settings if you want to reuse it. 
			<li>IMPORTANT: If you put the shortcode in a category or other listing page PHP, the box will still not show in order to avoid unnecessary processing on the content snippets.
		</ol>
		';
		
		?>
		<div class="frn_personal_type_<?=$i;?>" style="float:left;">
			TYPE OF MESSAGE: <a href='javascript:showhide("frn_plugin_personal_type_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span> 
			<?php 
			frn_personal_dropdown($box_id,$fieldvar,$type,$type_options,$i,$help)
			?>
		</div>
		<?php
		
		
		
		
		//////////
		// activation
		//////////
		?>
		<div class="frn_personal_activation_<?=$i;?>" style="float:left; width:150px; margin-left: 20px;">
			<?php frn_personal_radio($box_id,'',$box_activate,$i); ?>
		</div>
		<?php
		
		
		
		
		
		
		/////////
		// auto insert:
		/////////
	
		////////
		// auto insert activation
		$trigger_options = array(
			'' => "Default (Use trigger words to display this)",
			'words' => 'Use trigger words to display this',
			'all' => 'Place it on every page',
			'shortcode' => 'Don\'t auto-insert (rely on shortcode only)'
		);
		$help = "This dropdown lets you choose if you want to put this box on every page, just ones with certain keywords in the title, or turn off the automatic insertion by relying on the shortcode only.";
		
		//$auto is defined in the call for the function. It's the actual selection stored in the database.
		$fieldvar = "autoinsert"; //the variable where the selected value is stored ?>
		<div class="frn_personal_auto_<?=$i;?>" style="margin: 10px 0; float: left;">
			<!--<span style='white-space:nowrap;'>Auto-Insertion: </span><br />-->
			<a href='javascript:showhide("frn_plugin_personal_autoinsert_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a>
			<?php frn_personal_dropdown($box_id,$fieldvar,$auto,$trigger_options,$i,$help); ?>
		</div>
		
		
		
		<div class="section_break" style="width:100%; float: left;">
		<?php //makes sure position, etc on seperate line
		
		
		
		
		
		//////////
		// position 
		$fieldvar="position";
		//position selected is sent with the function call for $position;
		if($type=="") $dd_type="cta";
			else $dd_type=$type;
		if($type!=="kps") $default_pos="35%";
			else $default_pos="75%";
		$position_options = array(
			'' => strtoupper($dd_type)." Default (".$default_pos.")",
			'.25' => '25%',
			'.35' => '35%',
			'.5' => '50%',
			'.6' => '60%',
			'.75' => '75%',
			'1' => 'At End',
		);
		$help = "
		<ol>
			<li>Selecting the default allows us to change the position across many sites via updating the plugin. So, only select another position if you know it's the right option. 
			<li>The default position is 35% for CTAs and message boxes. 75% for key page lists.
			<li>For example, if there are four sections on the page and you selected 75%, then it'll insert the CTA box just below the third section. 
			<li>Even if the fourth section takes up most of the page, the box will still end up after the third one. So, don't expect this box to be in just the right place. But it will be close.
			<li>If the position for any two boxes are the same, then the system will combine the two into one.
			<li>If there is an image in the first 800 characters (including HTML code) and the position is less than 35%, the position for the box will default to 35% so that it doesn't compete with the image on desktops.
			<li>If the post has fewer than 6 sections, your settings here will be ignored and all boxes will be combined into one box and put at about the middle of the page.
			<li>Use the shortcode if you don't like the placement. It'll automatically disable the auto-insertion.
			<li>Technical Info: 
			<ul>
				<li>This system counts blocks of content in the content section of a page based on &lt;p&gt; or &lt;h#&gt; or &lt;ul&gt;.
				<li>If the position seems off: it could be due to it being after a header or before a list item. It will then increment down one section.
			</ul>
			</li>
		</ol>
		";
		?>
		<div id="frn_personal_<?=$fieldvar;?>_<?=$i;?>" style="float:left;">
			<span style='white-space:nowrap;'>Position in Content: <a href='javascript:showhide("frn_plugin_personal_<?=$fieldvar;?>_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_dropdown($box_id,$fieldvar,$position,$position_options,$i,$help,$type); ?>
		</div>
		<?php
		
		
		
		
		
		//////////
		// section positioning 
		$fieldvar="sections";
		//position selected is sent with the function call for $position;
		if($type=="") $dd_type="cta";
			else $dd_type=$type;
		if($type!=="kps") $default_pos="Anywhere";
			else $default_pos="End of Sections";
		$section_options = array(
			'' => strtoupper($dd_type)." Default (".$default_pos.")",
			'Headers' => 'End of Sections',
			'Anywhere' => 'No Restrictions'
		);
		$help = "
		<ol>
			<li>Defaults:
			<ol>
				<li>CTA or message: No restrictions (since the content needs to interrupt)
				<li>Key Pages: At the end of sections (since they don't need to interrupt as much)
			</ol>
			<li>Choose \"End of Sections\" if you'd like a box to show there.
			<li>Otherwise it'll be placed according to the normal positioning restrictions
			<li>If more than one box is set to display at the same position, this setting for first one in the order of boxes on this page will the the setting used to determine placement.
		</ol>
		";
		?>
		<div id="frn_personal_<?=$fieldvar;?>_<?=$i;?>" style="float:left;margin-left: 15px;">
			<span style='white-space:nowrap;'>Position in Sections: <a href='javascript:showhide("frn_plugin_personal_<?=$fieldvar;?>_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_dropdown($box_id,$fieldvar,$section,$section_options,$i,$help,$type); ?>
		</div>
		<?php
	
	
	
	
		/////////
		// exclude ids 
		$help = "<li>Enter page ID numbers for the pages you DON'T want the CTA to show on--separated by commas (e.g. 235,442,5). 
				<li>No matter what other option you have selected for this box, it will not show on the page with these IDs UNLESS a shortcode is present.
				<li>If you choose to place the box on every page (the dropdown above), you can keep the CTA from showing on specific pages by including their IDs in this field above.";
		?>
		<div class="frn_personal_exclude_<?=$i;?>" style="float:left;margin-left: 15px;">
			<span style='white-space:nowrap;padding-left: 2px;'>Exclude Page IDs: <a href='javascript:showhide("frn_plugin_personal_excludes_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_text($box_id,'excludes',$excludes,$i,$help);?>
		</div>
		
		
		</div>
		<?php  //end position, etc. line
		
		
		/////////
		// trigger words
		$box_type = $type;
		$help = "
			<ol>
				<li>If you want the box only to show on pages using select words in titles, include those here separated by comma.
				<li>If you enter nothing above, the following terms are used by default: <strong id=\"trigger_words\" style=\"cursor:pointer;\" onClick=\"selectText('trigger_words')\">".$GLOBALS['trigger_words']."</strong>.
				<li>These words will be ignored if:
				<ol>
					<li>If you choose to place the box on every page (the dropdown above)
					<li>A page's ID is in your exclusion list above
					<li>Place the shortcode in the content
				</ol>
				<li>This feature uses a simple find in titles. So words like \"counsel\" will work for both counselor or counseling.
				<li>It doesn't have to be just words. You can include multi-word phrases like \"alcohol abuse\" between commas.
				<li>When trigger words for two different boxes are in a title, the system will only show the first box. So choose your trigger words and box order carefully.
			</ol>
		";
		
		?>
		<div class="frn_personal_triggers_<?=$i;?>" style="float: left; width: 100%; margin-top: 10px;">
			<span style='white-space:nowrap;'>Page Title Trigger Words: <a href='javascript:showhide("frn_plugin_personal_triggers_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<div id="frn_personal_triggers_note_<?=$i;?>" style="<?=($type!=="cta")? 'display:none;' : 'display:block;' ;?>"><small>Default for CTA: <?=$GLOBALS['trigger_words'];?></small></div>
			<?php frn_personal_textarea($box_id,'triggers',$triggers,$i,$help);?>
		</div>
		<?php
	
	
	
	
		//////////
		// message
		//////////
		//message var is defined in function call above
		$help = "
			<li>Leave this blank if you want to use the default message in the CTA box. It will be blank for other uses. 
			<li>To remove it altogether for CTAs, insert HTML commenting out tags: &lt;!-- --&gt;.
			<li>Use <strong id=\"box_message\" style=\"cursor:pointer;\" onClick=\"selectText('box_message')\">%%drug%%</strong> to use the drug name found in the title or site name. It'll add \"or\" before it if found.
			<li>If you want to replace the drug with a stand in word or phrase, use <strong id=\"box_message\" style=\"cursor:pointer;\" onClick=\"selectText('box_message')\">%%drug|your words%%</strong>. Replace \"your words\" with the word or phrase you want to replace the drug with. Use this also if you want to remove \"or\" and just leave it blank.
			";
		?>
		<div class="frn_personal_message_<?=$i;?>" style="float:left; width:100%;">
			<span style='white-space:nowrap;'>Message: <a href='javascript:showhide("frn_plugin_personal_message_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_textarea($box_id,'message',$message,$i,$help); ?>
		</div>
		<?php
		
		
		
		
		
		/////////
		// CONFLICT RESOLUTION SECTION
		/////////

		
		/////////
		// negative words & dominant checkbox
		$negatives=trim($negatives);
		?>
		<div style="float:left; width:100%;">
		<div><a href='javascript:showhide("frn_personal_conflicthelp_<?=$i;?>")'>Show box conflict resolution options...</a></div>
		<div id="frn_personal_conflicthelp_<?=$i;?>" style="float: left; width: 97%; margin-top: 10px; border: 2px solid #6DA3EC; padding: 0 10px 10px 10px; border-radius: 7px; background-color: #ddd; <?=($negatives!="" || $dominant=="yes") ? null : "display:none;" ;?>">
			<div class="frn_personal_negatives_<?=$i;?>" style="float: left; width: 100%; margin-top: 10px;">
				<h3 style="margin:0 0 5px 0;">Negative Keywords</h3>
				<div style="margin-bottom:5px;">Add words you do NOT want the box showing for. They are most useful when avoiding box conflicts--where titles use trigger words for two different boxes.</div>
				<?php
				//Negatives textarea
				$fieldvar = "negatives";
				$help = "
					<p>Sometimes a box may show for posts that you don't want it to. Use this option to reduce the chances a box will show when it shouldn't. 
					For example, you may want one box to show for addiction and another for rehab. But there's an article that has \"addiction rehab\" in the title. 
					The addiction box shouldn't show if rehab is in the title, but there is no other way to keep this from happening without using negative keywords.
					So in this case, you would add rehab to this section for the addiction box only.</p>
				";
				?>
				<div style='white-space:nowrap;'>
					<b>Page Title Deactivate Words: </b>
					<a href='javascript:showhide("frn_plugin_personal_<?=$fieldvar;?>_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a>
				</div>
				<?php
				frn_personal_textarea($box_id,$fieldvar,$negatives,$i,$help);
				?>
			</div>
			<?php
				//dominant checkbox
				$fieldvar = "dominant"; //the variable where the selected value is stored
				$value = "yes";
				$label="Dominant Box";
				$help = "<p>This option only applies when more than one box of the same type is showing at the same position.
				Use this option to restrict only one of the boxes to show. If more than one is dominant, then only the first dominant box will show.</p>
				<p>For example: Sometimes because of trigger words, a two key pages boxes may show at the same position. This enables you to choose one of them to \"dominant\" the other and be the only one showing.
				Otherwise, both boxes will just show in a combined box...that doesn't really look that great.</p>";
				$help_btn="<a href='javascript:showhide(\"frn_plugin_personal_".$fieldvar."_help_".$i."\")' ><img src=\"".$GLOBALS['help_image']."\" /></a>";
			?>
			<div class="frn_personal_<?=$fieldvar;?>_<?=$i;?>" style="margin: 5px 0 0 5px; float: left;">
				<b><?php
				frn_personal_checkbox($box_id,$fieldvar,$value,$dominant,$label,$i,$help,$help_btn); 
				?></b>
			</div>
		</div>
		</div>
		
		
		<?php
		
		
		
	
	
		//////////
		// IF CTA:
		//////////
		// chat id
		$help = "If you want a custom chat button, upload and online and offline button to the LiveHelpNow system. It'll give you an ID for the buttons. Enter it here.";
		?>
		<div id="cta_<?=($i);?>" <?=($type!=="cta")? 'style="display:none;"' : 'style="display:block;"' ;?>>
		<h4 style="  float: left;   width: 100%;   margin-bottom: 10px;">CALL TO ACTION BUTTONS</h4>
		<div class="frn_personal_chat_id_<?=$i;?>" style="float:left;">
			<span style='white-space:nowrap;'>Chat Button ID: <a href='javascript:showhide("frn_plugin_personal_chat_id_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_text($box_id,'chat_id',$chat_id,$i,$help);?>
		</div>
		<?php
		
		// email image
		$help = "You can make a custom email button in coordination with the LHN system. Just upload it to this site somehow and then enter the web address to it here.";
		?>
		<div class="frn_personal_email_image_<?=$i;?>" style="float:left;">
			<span style='white-space:nowrap;'>Email Button Image URL: <a href='javascript:showhide("frn_plugin_personal_email_image_help_<?=$i;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></span><br />
			<?php frn_personal_text($box_id,'email_image',$email_image,$i,$help);?>
		</div>
		</div>
		<?php
	
	
		//////////
		// IF KEY PAGES:
		//////////
		
		?>
		<div id="key_pages_<?=($i);?>" style="float:left; width:100%; <?=($type!=="kps")? 'display:none;' : 'display:block;' ;?>">
			<h4 style='white-space:nowrap;margin-top: 20px; margin-bottom: 5px;'>KEY PAGES: <a href='javascript:showhide("frn_plugin_personal_keypages_help_<?=$i+1;?>")' ><img src="<?=$GLOBALS['help_image'];?>" /></a></h4>
			<div id="frn_plugin_personal_keypages_help_<?=$i+1;?>" class="frn_help_boxes">
				<li>Enter the URLs on this site you want to feature, the text you want linked, and the little popup text when hovering over a link (optional) for every top page you think would be valuable to people seeking rehab. 
				<li>If you don't enter linked text, the system will use the URL's title.
				<li>There is no limit to the number of key pages you can add. Just hit any plus sign to add a new one at the end. To remove one, hit the minus sign.
				<li>You can use the hover text to help explain the link more or phrase the anchor text in a question or visa versa. For example, your anchor text could be "How much does rehab cost?" and the hover text could be "Read about the cost of various treatment options".
				<li>If you try to activate this option without filling out a URL or anchor field, it will not let you activate the option.
				<li>If you're trying to add several fields back to back and having trouble with it (e.g. it stops adding fields), you likely did it too quickly and your browser didn't have enough time to finish creating the previous one. Refresh the page and try again leaving about three seconds between them.
				The best kind of pages to link to for rehab pages are:
				<ol>
					<li>How to know when you need treatment</li>
					<li>Helping a loved one get help</li>
					<li>How to tell your family</li>
					<li>What others have been through</li>
					<li>Treatment options for them</li>
					<li>Cost of the various treatment types</li>
					<li>Questions to ask providers</li>
				</ol>
				</p>
			</div>
			<?php $root_id = "frn_personal_keypages_".($i) ;?>
			<div id="<?=$root_id;?>" style="float:left; width:100%;">
				<div id="frn_personal_keypages_headers">
					<div style="display:inline-block; width: 20%; font-size: 12px; margin-left:20px;">URL</div>
					<div style="display:inline-block; width: 20%; font-size: 12px;">LINKED TEXT</div>
					<div style="display:inline-block; width: 40%; font-size: 12px; ">HOVER TEXT (Optional)</div>
				</div>
				<?php 
				$kp_for_saving=0; $kp=1;
				if(!is_array($key_pages)) {
					$key_pages = array(
									array('url'=>'','anchor'=>'','hover_text'=>'')
								);
				}
				foreach($key_pages as $keypage) {
				?>
				<div id="<?=$root_id;?>-<?=$kp;?>">
					<?=$kp;?>. 
					<?php 
					if(!isset($keypage['url'])) $keypage['url']="";
					if($keypage['url']!=="") {
						if(!$title=frn_check_url($keypage['url'])) { ?>
						<span class="frn_exclamation" title="Check this URL. It doesn't point to a page that exists.">!</span>
						<?php }
					}
					else $title="";
					if(trim($keypage['anchor'])!=="") $title=$keypage['anchor'];
					?><input name="<?=$GLOBALS['personal_box_var'];?>[boxes]<?=$box_id;?>[key_pages][<?=$kp_for_saving;?>][url]" type="text" value="<?=(isset($keypage['url']) ? $keypage['url'] : null);?>" style="width:20%;" onFocus="this.style.width = '43%';" onBlur="this.style.width = '20%';" />
					<input name="<?=$GLOBALS['personal_box_var'];?>[boxes]<?=$box_id;?>[key_pages][<?=$kp_for_saving;?>][anchor]" type="text" value="<?=($title!=="") ? $title : null;?>" style="width:20%;" onFocus="this.style.width = '43%';" onBlur="this.style.width = '20%';" />
					<input name="<?=$GLOBALS['personal_box_var'];?>[boxes]<?=$box_id;?>[key_pages][<?=$kp_for_saving;?>][hover_text]" type="text" value="<?=(isset($keypage['hover_text']) ? $keypage['hover_text'] : null);?>" style="width:20%;" onFocus="this.style.width = '43%';" onBlur="this.style.width = '20%';" />
					<a href="javascript:add_keypages_fields('<?=$root_id;?>',<?=$kp;?>,'delete');" title="Remove this key page">[&#8211;]</a>
					<a href="javascript:add_keypages_fields('<?=$root_id;?>',<?=$kp;?>);" title="Add a copy of these key page options at the end">[+]</a>
				</div>
				<?php
				$kp_for_saving++; $kp++;
				}
				?>
			</div>
			<?php //end key pages fields ?>
		</div><!-- End key pages block -->
	</div><!-- End entire Context Messaging block #<?=$i+1;?> -->
	<?php
}


function frn_personal_dropdown($id,$fieldvar="type",$selected="",$options="",$i="",$help="",$box_type="") {
	/*
		selected = the value stored in the db
		options = all available options in the db
		i = the ID for saving data
	*/
	
	if($selected=="") $selected="cta";
	
	//defaults if sent to the function blank
	//if($type=="") $type="cta";
	if(is_array($id)) $id="";
		elseif($id!=="") $id="[boxes]".$id;
	if($fieldvar!=="") $fieldvar_var="[".$fieldvar."]";
	$field_id = 'id="frn_personal_'.$fieldvar.'_'.$i.'"';
	$style=""; $javascript="";
	if($fieldvar=="position") { 
		//if($box_type!=="kps") $default="35%";
		//else $default="75%";
		//if(trim($box_type)=="") $box_type="msg";
		//$default_text = strtoupper($box_type)." Default (".$default.")";
		$style = 'style="width: 170px;"';
	}
	elseif($fieldvar=="type") {
		$position_defaults = ",'35%','75%','35%'";
		
		//this controls the dropdowns we want to switch out the box type labels in the default settings options
		$dd_switches = "position,sections"; 
		// if adding switch, make sure to change defaults in javascript for regex
		
		$javascript = 'onChange="javascript:remove_blocks(this.value,'.($i).',\''.$selected.'\',\''.$dd_switches.'\''.$position_defaults.')" ';
	}
	
	
	
	if($options=="") $options = array(
		'' => '',
		'cta' => 'Call to Action',
		'kps' => 'Key Pages',
		'msg' => 'Message Only',
	);
	
	
	?>
	<select <?=$field_id;?> name="<?=$GLOBALS['personal_box_var'].$id.$fieldvar_var;?>" <?=$style;?> <?=$javascript; ?>>
		<?php
		foreach($options as $option => $name) {
		?>
		<option value="<?=$option;?>"<?=($selected==$option && $selected!=="")? ' selected="selected" ' : null;?>><?=$name;?></option>
		<?php 
		} ?>
	</select>
	<div id="frn_plugin_personal_<?=$fieldvar;?>_help_<?=$i;?>" class="frn_help_boxes">
		<?=$help;?>
	</div>
	<?php
}
function frn_personal_text($id,$fieldvar="bckgd",$setting="",$i="",$help="") {
	//used for any one-line text fields (e.g. background, font, exclude IDs, etc.)
	//echo "<h2>".$GLOBALS['personal_box_var']."</h2>";
	//defaults
	if(is_array($id)) $id="";
		elseif($id!=="") $id="[boxes]".$id;
	$settings_var = "";
	if($fieldvar=="") $fieldvar="bckgd";
	if($i!=="") {$i_id="_".$i; $i="[boxes][".$i."]"; }
		else $i_id="";
	//if($box_type!=="") $settings_var="[".$box_type."]";
	if($fieldvar=="bckgd" || $fieldvar=="font") $div_id = "frn_personal_style";
		else {
			$div_id = "frn_personal_".$fieldvar;
			//$settings_var = "[settings]";
		}
	
	echo '
		<input id="'.$div_id.$i_id.'" name="'.$GLOBALS['personal_box_var'].$id.$settings_var.'['.$fieldvar.']" size="20" type="text" value="'.$setting.'" />
		';
	if($fieldvar=="font") echo '
		<div id="frn_plugin_personal_font_help" class="frn_help_boxes">
			You can change the font styles if you\'d like. Just enter the portion you\'d normally have between the quotes for an inline style (e.g. color:black;text-decoration:none; ). 
			This will override any styles in your style sheet or the default one that comes with this plugin.
		</div>
		';
	elseif($fieldvar=="bckgd") echo '
		<div id="frn_plugin_personal_bckgd_help" class="frn_help_boxes">
			The default background color for this box is none. 
			You can override that here if you\'d like. Just enter the portion you\'d normally have between the quotes for an inline style (e.g. background:#efefef;border:1px solid gray; ).
		</div>
		';
	else echo '
		<div id="frn_plugin_personal_'.$fieldvar.'_help'.$i_id.'" class="frn_help_boxes">
			'.$help.'
		</div>
		';

}
function frn_personal_textarea($id,$fieldvar="triggers",$setting="",$i="",$help="") {
	//used to create a multi-line textbox
	//defaults
	if(is_array($id)) $id="";
		elseif($id!=="") $id="[boxes]".$id;
	if($fieldvar=="") $fieldvar="triggers";
	if($i!=="") {$i_id="_".$i; $i="[".$i."]"; }
		else $i_id="";
	$div_id = "frn_personal_".$fieldvar;
	?>
		<textarea id="<?=$div_id.$i_id;?>" name="<?=$GLOBALS['personal_box_var'].$id;?>[<?=$fieldvar;?>]" rows="3" /><?=$setting;?></textarea>
		<div id="frn_plugin_personal_<?=$fieldvar;?>_help<?=$i_id;?>" class="frn_help_boxes">
			<?=$help;?>
		</div>
	<?php

}
function frn_personal_checkbox($box_id="",$fieldvar="",$value="yes",$setting="",$label="",$i="",$help="",$help_btn="") {
	//used to create a checkbox for a simple yes or no option
	//defaults
	if(is_array($box_id)) $box_id="";
		elseif($box_id!=="") $box_id="[boxes]".$box_id;
	if($fieldvar=="") $fieldvar="dominant";
	if($i!=="") {$i_id="_".$i; }
		else $i_id="";
	$div_id = "frn_personal_".$fieldvar;
	if($value=="") $value="yes";
	?>
		<input id="<?=$div_id.$i_id;?>" name="<?=$GLOBALS['personal_box_var'];?><?=$box_id;?>[<?=$fieldvar;?>]" type="checkbox" value="<?=$value;?>" <?=($setting==$value) ? "checked=\"checked\"" : null ;?> /> <?=$label;?> <?=$help_btn;?>
		<div id="frn_plugin_personal_<?=$fieldvar;?>_help<?=$i_id;?>" class="frn_help_boxes">
			<?=$help;?>
		</div>
	<?php
}







/////////
// SAVE THE BOXES
// cleans up data before stored
function plugin_options_personalboxes($input) {
	
	$newinput['activation'] = trim($input['activation']);
	$newinput['bckgd'] = trim($input['bckgd']);
	$newinput['font'] = trim($input['font']);
	
	
	/*
	//hidden since it just seems to be overkill
	//The following makes sure that nothing is saved if the "type" is left blank
	//It blanks out any boxes with that case
	$update_box_var=false;
	if(is_array($input['boxes'])) {
		$i=0; //$count=count($input['boxes']);
		foreach($input['boxes'] as $box) {
			if($box['type']!=="") $update_box_var=true;
			else $input['boxes'][$i]=""; //clear the box settings
		$i++;
		}
	}
	if($update_box_var) */
	
	
	
	////////
	/// Set unique ID for any new boxes
	//get comma delimited list of current ids
	$comma="";
	if(isset($input['boxes'])) {
		$newinput['boxes'] = $input['boxes'];
		$ids=""; $i=0;
		foreach($newinput['boxes'] as $box) {
			if(isset($box['id'])) {
				if($i>0) $comma=",";
				if(trim($box['id'])!=="") {
					if($box['id']!=="TBD") {
						// checks for TBD just to be safe but unlikely will ever be a scenario. If $box['id'] is not explicitely set, then it's likely blank and the array key for it is TBD#.
						$ids .= $comma.$box['id'];
						$i++;
					}
				}
			}
		}
		//Get the highest number in the array
		$max_id=1;
		if(trim($ids)!=="") {
			$ids=explode(",",$ids); //creates the actual array even if there are no values
			$max_id = max($ids);
		}
		//then as we cycle through each one for default values, we can assign the next highest ID
		
		
		$default_box_activate = "Activate";
		$i=0; $new_id=$max_id+1; //increment 1 above max_id to assign to new box
		foreach($newinput['boxes'] as $box_id => $box) {
			
			//Basically, if a box's key has TBD in it then create a new key that is fully numeric using the max number
			// then save all data for the TBD box into the new ID and remove the TBD version from the array
			if(!is_numeric($box_id)) {
				$newinput['boxes'][$new_id] = $newinput['boxes'][$box_id];
				unset($newinput['boxes'][$box_id]);
				$new_id++; //since this is blank, increment up 1 in case there is more than one new box
			}
			
			//If a field is set to a default, remove the value so we can still control it with FRN updates
			if($box['activation']==$default_box_activate) $newinput['boxes'][$i]['activation']="";
			if($box['autoinsert']=='words') $newinput['boxes'][$i]['autoinsert']="";
			if($box['type']!=='kps' && $box['position']=='.35') $newinput['boxes'][$i]['position']="";
				elseif($box['type']=='kps' && $box['position']=='.75') $newinput['boxes'][$i]['position']="";
			if($box['type']!=='kps') $newinput['boxes'][$i]['key_pages']="";
			if($box['type']!=='cta') {$newinput['boxes'][$i]['chat_id']=""; $newinput['boxes'][$i]['email_image']=""; }
			
			$i++;
		}
	}

	return $newinput;
	
}