//Compression tool: http://closure-compiler.appspot.com/ (recommended by Google's pagespeed evaluator)
//coded by daxon edwards (daxon.me) 2013-2019
/*
//v2.0 - Added Hotjar event tracking capability
//v3.0 - Adapted JS to respond to new PHP triggers
    - Incorporated UA triggers into code 3/10/15 so it now works for new analytics approach and the old
    - Added better hotjar checking
    - Used JS validator
    - Should be using a JS compression tool as well
    - Added Pinterest social tracking
    - Added simpler social sharing tracker
    - Added social tracking for hotjar
    - Made email address clicks more specific to use email address in url\
    - fixed duplicated downloads tracking issue
//v3.1 - fixed external linking and target removing options
//v4.0 - combined all of our plugin js into one file to improve Google page speed scores. Previously, we only loaded files we needed. This affected a ton of things below that relate to the FRN plugin. I tried to make notes as I went. 1/26/17
    - changed all domain discover methods to use one window.location.hostname to avoid potential and likely rare issues. So Analytics and images all use the same discover method now.
    - added custom function for chat window popups that intercepts and reports clicks to Google Analytics events. Previously, when the tab wasn't showing (i.e. Contact pages), email and chat buttons weren't recorded to Analytics.
//v4.2 - created new email function
    - fully tested all button use scenarios and GA tracking and included clear notes for use cases around functions
    - combined outbrain, hotjar, and Analytics reporting into one function to reduce code and improve control
//v4.3 - added "label" option to frn_chat_window (includes automatic default values--not tested yet)
//v4.4 - added infinity js click events
//v4.5 - turned off processing for mobile devices and no number processing happens if LHN DIV not found. See details in the LHN section below.
//v4.6 - changed the directory for the plugin from frn_plugins to frn-plugin per Dev team request.
*/




//#####
//### FRN BASIC DEFAULTS
//#####

//since PHP version created in 2014, if variables present, then JS external link and target elements should be activated.
if (typeof frn_eli_target == 'undefined') var frn_eli_target="";
if (typeof frn_hj_act == 'undefined') var frn_hj_act="";
var root_domain = window.location.hostname;
if (typeof frn_pluginInstall == 'undefined') var frn_pluginInstall="";
if (frn_pluginInstall=="") frn_pluginInstall = "http://"+root_domain+"/wp-content/plugins/frn-plugin"; 

/*
  decided to use window location hostname since it looks at the document loaded into the tab window of the browser. The document.domain would like work as well, but it is one step inside the window. 
  As of 2/27/17, Dax isn't sure exactly if there will be any negative consequences of this choice.
  if (typeof frn_eli_act === 'undefined') var frn_eli_act="";
  if (typeof frn_baseDomain === 'undefined') var frn_baseDomain=document.location.hostname;  //looks at where the site is hosted, not the domain showing in the addressbar
  Removed encodeURIComponent() around domain since we don't need the domain encoded for find and replace later
*/

//alert(  "frn_pluginInstall: " + encodeURIComponent(document.domain) + "; ext_links: " + window.location.hostname  ); //"frn_baseDomain: " + frn_baseDomain + 





//#####
//### ENSURES GA INITIALIZED
//#####

//Ensure global _gaq or ga Google Analytics queue has been initialized.
var _gaq = _gaq || [];
if (typeof ga==='function') var _ga=true; 
  else var _ga=false;
  
if (typeof frn_outbrain_head==='function') var frn_outbrain_js = true;
  else var frn_outbrain_js = false;





////////////////// 
///
/// LIVEHELPNOW DEFAULT SETTINGS
///
//////////////////

lhnVersion = 5.3;



// The following vars are required to ensure there are few errors.
if(typeof frn_customText=='undefined') frn_customText="";
if(typeof lhnInviteEnabled == 'undefined') lhnInviteEnabled = 0;  //1 turns it on. It's on by default in the LHN system settings (via their site). So if this file doesn't load properly, then the auto invite will show.
if(typeof lhnInviteChime == 'undefined') lhnInviteChime = 0;
if(typeof lhnJsHost == 'undefined') lhnJsHost = "https://"; //only affects email button in tab slideout;  old code: (("https:" == document.location.protocol) ? "https://" : "http://"); 
if(typeof lhnCss == 'undefined') lhnCss = ""; 
if(lhnCss=="") {
  lhnCss = frn_pluginInstall+"/styles.css?v=1.12";
  //lhnCss = lhnJsHost + "s3.amazonaws.com/spanning/scripts/lhn_frn_style.css"; //unnecessary since we know where the plugin is installed, but kept here so we have a record of the file's existence
}
lhnTrackingEnabled = 't';
lhnHPPanel = true;
lhnHPChatButton = true;
lhnHPTicketButton = true;

/*
VARIABLES WE USE TO DEFINE AS DEFAULT HERE
  But they sometimes change if more than one button is used on a page
  Asynchronous issues also caused in consistency in settings
  Many of these if left undefined, the LHN code handles just like we would like them to
  Only the most important ones we normally customize for FRN and are different than LHN defaults are included.
  Prior to 2/8/17, these variables represented the defaults. 
  Due to async issues, sometimes they would load after buttons on the page and overwrite the settings next to the buttons.
  Deactivated the ones that were necessary to avoid errors. Keeping them just in case issues are discovered in the future.

//if(typeof lhnAccountN === 'undefined') lhnAccountN = "14160-1";
//if(typeof lhnWindowN === 'undefined') lhnWindowN = 14960; //0=default, changed to 14960 based on pulling code 1/27/17, can customize window per site
//if(typeof lhnDepartmentN === 'undefined') lhnDepartmentN = 15027;  //default=0, but when getting code as of 1/27/17, it uses this since we select "help desk" as the department
//if(typeof lhnChatPosition === 'undefined') lhnChatPosition = 'default';
//if(typeof lhnHPCallbackButton === 'undefined') lhnHPCallbackButton = false;
//if(typeof lhnHPKnowledgeBase === 'undefined') lhnHPKnowledgeBase = false;
//if(typeof lhnHPMoreOptions === 'undefined') lhnHPMoreOptions = false;
//if(typeof lhnInviteN === 'undefined') lhnInviteN = 32167; //default invite window
//if(typeof lhnTheme === 'undefined') lhnTheme = "green";  //default=green
//if(typeof lhnCustomInvitation === 'undefined') lhnCustomInvitation = '';
//var default_chatbutton_id = 8557;
//if(typeof lhnButtonN === 'undefined') lhnButtonN = default_chatbutton_id;  //old defaults 3829 or 4015
//  else if(lhnButtonN === "") lhnButtonN = default_chatbutton_id;
//if(typeof lhnButtonN4HO === 'undefined') lhnButtonN = default_chatbutton_id; //old default -1 //old variable pre-2013
//  else if(lhnButtonN4HO==="") lhnButtonN = default_chatbutton_id; //old default -1
//  else lhnButtonN=lhnButtonN4HO;  //so that the old ID is passed to the new version
//lhnLO_helpPanel_knowledgeBase_find_answers = "Find answers";
//lhnLO_helpPanel_knowledgeBase_please_search = "Please search our knowledge base for answers or click [More options]";
//lhnLO_helpPanel_typeahead_noResults_message = "No results found for";
//lhnLO_helpPanel_typeahead_result_views = "views";

//discontinued in early 2016 since information wasn't helpful to reps
//if(typeof lhnCustom1 === 'undefined') lhnCustom1 = encodeURIComponent(document.domain);
//if(typeof lhnCustom2 === 'undefined') lhnCustom2 = encodeURIComponent(getElementsByTagName("TITLE")[0].innerHTML);
//if(typeof lhnCustom3 === 'undefined') lhnCustom3 = encodeURIComponent(window.location.href);

*/






//////////////
// GOOGLE_ANALYTICS REPORTING FUNCTION for LHN
//////////////

/*
BUTTON SCENARIOS and how they are tracked:
On-page email button:     Via our frn_email_window function below (it calls the frn_analytics function here)
On-page chat button:    Cases:
                No Slideout: jQuery listener below that fires the frn_analytics function here
                With Slideout: jQuery listener below that fires the frn_analytics function here
On-page email text link:  Via our frn_email_window function below (it calls the frn_analytics function here)
On-page chat text link:   Cases:
                No Slideout: our frn_chat_window sees that "type" is not undefined and calls the frn_analytics function here
                With Slideout: our frn_chat_window sees that "type" is not undefined and calls the frn_analytics function here
Slideout email button:    Listeners defined within wireCustomLHNEvents function below
Slideout chat button:     Listeners defined within wireCustomLHNEvents function below
*/


// Checking if it's already defined was causing issues because we used "undefined", but it should be "function". Discontinued on 2/16/17.
// But we can try that approach at another time. This issue would only happen if somehow we messed up and had two of these loading. So the safeguard isn't necessary at this time.

//if (typeof lhnPushAnalytics !== "function") {
  //alert("It's not already defined");
  ga_count=1;
  //old name: lhnPushAnalytics
  function frn_analytics(category, event, label, conv, fake_url) {
    //this function is also called in the default LHN JavaScript code. 
    //Category and action must be first for their system to work. 
    if(typeof conv == 'undefined' || conv == '') var conv='no';
    //if(typeof ia == 'undefined' || ia == '') var ia='n';


    ////////
    // ANALYTICS
    if(typeof category == 'undefined') var category="Undefined Category";
      else if(category=="") category="Undefined Category";
    if(typeof event == 'undefined') var event="Undefined Event";
      else if(event=="") event="Undefined Event";
    if(typeof label == 'undefined') var label="";
    if (_ga) {
      //alert("frn_analytics ga: category: "+category+"; event: "+event+"; label: "+label+"; conv_trig: "+conv+"; conv_url: "+fake_url);
      //in all cases, we send events to GA. Goals are set up in GA to determine what's a conversion and what's not.
      ga('send', 'event', category, event, label);
    }
    else if(_gaq) {
      _gaq.push(['_trackEvent', category, event, label ]);
      //alert("frn_analytics gaq: category: "+category+"; event: "+event+"; label: "+label+"; conv_trig: "+conv+"; conv_url: "+fake_url);
    }
    //else alert("Google Analytics not activated");
    
    var conv_alert = "; Conv: no";


    ////////
    // THIRD PARTY CONVERSION TRACKING

    //if an event should be considered as a conversion and sent to third parties
    if(conv!=='no') {

      conv_alert = "; Conv: ";

      /////
      // Infinity
      if(typeof _ictt != 'undefined') { //if infinity JS is turned on...
        if(conv=="chat" || conv=="email") {
          var trigger;
          //Send chat or email button clicks to Infinity for conversion tracking and goals (phone calls already tracked)
          //For testing:
          //if(frn_infinity_tracked==false) alert("Infinity ia: "+ia);
          //else alert("Infinity conversion function NOT called since already sent");
          //Infinity's system removes duplicate clicks already, no need to check for prior clicks
          if(conv=="chat") trigger="Live Chat";
          else if(conv=="email") trigger="Email";
          _ictt.push(['_customTrigger', trigger]);
          //alert("Infinity conversion sent");
          conv_alert = conv_alert + "Infinity";
        }
      }
      
      /////
      // HotJar
      //cancels hotjar reporting
      //if(typeof hj!=='function') alert("HJ function not present");
      //if(frn_hj_act==="") alert("HJ not activated");
      //activates hotjar reporting
      //if(typeof hj==='function') alert("HJ function present");
      //if(frn_hj_act!=="") alert("HJ activated");
      if(typeof frn_hotjar_tracked == 'undefined') frn_hotjar_tracked=false;
      if(typeof fake_url != 'undefined') {
        if(fake_url!=="") {
            //if function exists and we have turned on the hj reporting function (don't move higher since we should report to Outbrain if HJ is turned off)
          if(typeof hj==='function' && frn_hj_act!=="") {
              //This won't work unless Chartbeat is in test mode or you are not on the company network
                //For testing:
                //if(frn_hotjar_tracked==false) alert("Hotjar fake_url: "+fake_url);
                  //else alert("Hotjar conversion function NOT called since already sent");
              //if a person clicks on more than one converting element on a page, we only want it reported to Hotjar once, so don't run the function again if so
            if(frn_hotjar_tracked==false) hj('vpv', fake_url);
            frn_hotjar_tracked=true;
            conv_alert = conv_alert + ", hotjar";
          }
        }
      }

      /////
      // Bing
      if(typeof frn_bing_tracked == 'undefined') frn_bing_tracked=false;
      if(conv=="chat" || conv=="email") {
        //calls already reported to Bing via Infinity
        if(frn_bing_tracked==false && typeof bing_id != 'undefined') {
          window.uetq = window.uetq || [];
          window.uetq.push ('event', event, {event_category: category}); 
          frn_bing_tracked=true; 
          if(window.uetq) conv_alert = conv_alert + ", bing";
        }
      }
      
      /////
      // Yahoo
      if(typeof frn_yahoo_tracked == 'undefined') frn_yahoo_tracked=false;
      if(conv=="chat" || conv=="email") {
        //calls already reported to Yahoo via Infinity
        //ids set within main yahoo tag using our custom additions
        if(frn_yahoo_tracked==false && typeof yahoo_id != 'undefined') {
          window.dotq = window.dotq || [];
      window.dotq.push(
      { 
       'projectId': yahoo_id,
       'properties': {
         'pixelId': yahoo_pixel,
         'qstrings': {
           'et': 'custom',
           'ec': category,
           'ea': event,
           'el': label,
           'ev': '',
           'gv': ''
        }
      } } ); 
          frn_yahoo_tracked=true;
          if(window.dotq) conv_alert = conv_alert + ", yahoo ("+yahoo_id+";"+yahoo_pixel+")";
        }
      }

      /////
      // Outbrain
      //If Outbrain JS reporting function is present run it (function will only run if "outbrain" is found in the URLs)
      if(typeof frn_outbrain_conv==='function') {
        if(typeof frn_outbrain_tracked == 'undefined') frn_outbrain_tracked=false;
        if(frn_outbrain_tracked==false) {
          frn_outbrain_tracked=true;

          //original:
            //var OB_ADV_ID= 30792;
            //var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
            //var str = '<script src="'+scheme+'widgets.outbrain.com/obtp.js" type="text/javascript"><\/script>';
            //document.write(str); 

          //Process:
            //Write the outbrain conversion JS file reference to the HTML at the bottom of the BODY. The browser executes the JavaScript once added and OB will track the conversion as if it was a page load. 
            //So the page would show a visit and a conversion having the same page name in their analytics.
          if(location.pathname.indexOf("outbrain")!==-1) {
            //alert("Outbrain conversion JS function added to BODY");
            var script = document.createElement( 'script' );
              script.type = 'text/javascript';
              script.src = scheme+"widgets.outbrain.com/obtp.js"; //"/wp-content/plugins/frn-plugin/test-dax.js"; //file just included an alert showing the JS was triggered.
              script.text = "var OB_ADV_ID= 30792; var scheme = ((\"https:\" == document.location.protocol) ? \"https://\" : \"http://\");";
            jQuery("body").append( script );
            conv_alert = conv_alert + ", outbrain";
          }
        }
        //else alert("Outbrain conversion function NOT called since already sent");
      }
    }

    //console.log("frn_analytics has ran: "+ga_count+" times. Category: "+category+ "; Event: "+event+ "; Label:" +label+ conv_alert);
    //alert("frn_analytics has ran: "+ga_count+" times. Category: "+category+ "; Event: "+event+ "; Label:" +label+ conv_alert);
    //ga_count=ga_count+1;
  }
//}
//else alert("It is already defined");






////////////////// 
///
/// LIVEHELPNOW SLIDEOUT WINDOW - Including adding phone number below buttons
///
//////////////////


//frn version 3.0
//frn version 3.1: added phone number as label in GA event trigger for mobile phone clicks
//frn version 3.2: added Hotjar event tracking capability
//frn version 3.3: removed UA option, and remove any chat Google Events tracking
//frn version 3.4: added infinity classes and data tags for replacing numbers
//frn version 3.5: rebuilt phone processing for desktop slideout - included phone linking for mobile devices although it won't ever be triggered

//The following adds custom text below the two buttons in the slide out
function LHN_HelpPanel_onLoad() {
  //Find the DIV with the class that content needs to be added within
  //The mobile slideout only triggers the chat--there is no window with several options
  if((typeof isTierIphone == 'undefined') || (typeof isTierTablet == 'undefined')) {
    var classObjs = document.getElementsByTagName('div'), i; //store all divs in page
    for (i in classObjs) {
      if(typeof classObjs[i].className === 'undefined') var do_nothing="";
      else {
        if((classObjs[i].className).indexOf('lhn_btn_container') > -1) {
            //alert('lhn_btn_container was found!');
            //if it doesn't find "a div with lhn_btn_container, then it will error out;
            //Create DIV with the content - used for GA event tracking
            var frn_addphone = document.createElement('div');
                frn_addphone.class = 'lhn_live_phone';
                frn_addphone.innerHTML = frn_prepare_phone();
                classObjs[i].appendChild( frn_addphone );
          }
      }
    }
  }
}
function frn_prepare_phone(){
  //If no phone number in <HEAD> area...
  if(typeof frn_phone == 'undefined') frn_phone="";
  if(typeof frn_phoneBasic == 'undefined') frn_phoneBasic="";  //this var created by other javascript found on the page. Only available when JS is used to identify numbers.
  if(frn_phone===null) frn_phone=""; //activate variable
  if(frn_phone==="") {
    //if frn blank, see if Basic is defined. If so, set frn_phone with the number for slideout.
    if(frn_phoneBasic!=="") {
      frn_phoneBasic=frn_phoneBasic.trim();
      if(frn_phoneBasic!=="") frn_phone=frn_phoneBasic;
    }
  }

  if(frn_phone.length>17) frn_phone="";
  var format="", pattern="", linked="";
  //format the phone number if it has no formatting and create link code if mobile device
  if(frn_phone!=="") {
    //get down to letters and digits

    //Although with PHP we are linking for all devices, it's okay to be device specific with JS.
    if((typeof isTierIphone != 'undefined') || (typeof isTierTablet != 'undefined')) {
      var phone_link = frn_phone_switch_letters(frn_phone);
          phone_link = frn_phone_format(phone_link); //There is only one phone format output and it uses dashes, no parenthesis.
      //Old approach that also strips out letters: if(frn_phoneBasic==="") frn_phoneBasic = frn_phone.replace(/\D/g,''); //strips out any non-numbers
      linked = '<a href="tel:'+phone_link+'" class="InfinityNumber call-us visible-xs" data-ict-discovery-number="'+phone_link+'" data-ict-silent-replacements="false" >'+frn_phone+'</a>';
    }
    //most common since the mobile option will not be processed.
    else linked = '<span class="InfinityNumber">'+frn_phone+'</span>';
  }
  
  //Set custom text below phone number
  var frn_customText='<span style="white-space:nowrap;"><p class="flyout-phone">' + linked + '</p><p class="flyout-bottom">Confidential & Private</p></span>'; 
  
  return frn_customText;
    
}
function frn_phone_switch_letters(digits_only) {
  //replace letters with numbers. (allows us to use 1-855-555-HELP)
  var digits_only = frn_phone.replace(/[ \(\)\-\.\+]/g,''); //frn_phone.replace(/\D/g,'');  //removed -- can't remember why it was used at beggin of this line if(frn_phoneBasic!=="") 
  var length = digits_only.length;
  var number = digits_only.toLowerCase();
  if(length>=10&&length<=14) {
    number = number.replace( /a|b|c/  , "2");
    number = number.replace( /d|e|f/  , "3");
    number = number.replace( /g|h|i/  , "4");
    number = number.replace( /j|k|l/  , "5");
    number = number.replace( /m|n|o/  , "6");
    number = number.replace( /p|q|r|s/, "7");
    number = number.replace( /t|u|v/  , "8");
    number = number.replace( /w|x|y|z/, "9");
    number = number.replace( /\+/     , "0");
  }
  return number;
}
function frn_phone_format(digits_only) {
  var frn_phone="";
  if(digits_only.length==11) {
      format  = /^1?\d([a-zA-Z0-9]..)([a-zA-Z0-9]..)([a-zA-Z0-9]...)$/; ///^1?(\d)([0-9]..)([0-9]..)([0-9]...)$/; //starts with 1
      pattern = '1-$1-$2-$3';
    }
    else if(digits_only.length==10) {
      format =  /([a-zA-Z0-9]..)([a-zA-Z0-9]..)([a-zA-Z0-9]...)$/; ///([0-9]..)([0-9]..)([0-9]...)$/; //doesn't start with 1
      pattern = '$1-$2-$3';
    }
    frn_phone = digits_only.replace(format,pattern);
    return frn_phone;
}






////////////////// 
///
/// LIVEHELPNOW SLIDEOUT GA TRACKING AND PHONE NUMBER
///
//////////////////

//These functions only happen when a slideout is present
//IMPORTANT: If we added in-page button tracking here, it would work only until there is no slideout (past mistake)
function wireCustomLHNEvents() {
   //Tracked in GA:
     //slideout tab
     //slideout chat button
     //slideout email button
     //slideout phone number

  //slideout tab clicks
  window.lhn("#lhn_help_btn").click(function(){ 
      frn_analytics('Live Chat', 'Contact Us Slide Out', '', 'no', ''); 
      //alert("slide out tab clicked");
  });
  //slidout chat button
  window.lhn("#lhn_helppanel a#lhn_live_chat_btn").click(function(){ 
    frn_analytics('Live Chat', 'Contact Us Slide Out: Chat Button', 'Chats', 'chat', '/conversion-lhn-slideout-chat-button-click/'); 
    //alert("slide out chat button");
  });
  //slideout email button
  window.lhn("#lhn_helppanel .lhn_ticket_btn").click(function(){ 
     frn_analytics('Live Chat', 'Contact Us Slide Out: Email Button', 'Emails', 'email', '/conversion-lhn-slideout-email-button-click/');
     //alert("slide out email chat button");
  });
  //slideout phone number
  if((typeof isTierIphone != 'undefined') || (typeof isTierTablet != 'undefined')) {
      window.lhn("#lhn_helppanel .flyout-phone").click(function(){  
          frn_analytics('Live Chat', 'Contact Us Slide Out: Phone Number', frn_phoneBasic, 'phone', '/conversion-lhn-slideout-phone-number-click/');
          //alert("slide out phone number");
      });
   }

  //The following we have disabled in our LHN account. No need to have browser keep looking for these things.
  /*
  
  //If the help panel is not present
  //if(document.getElementById("lhn_helppanel")!==null) {}

  //More button
  window.lhn("#lhn_helppanel .lhn_options_btn").click(function(){
    frn_analytics('Live Chat', 'Contact Us Slide Out: More Options', '', 'no', /lhnchat-window-click-more-options/');
  });
  //Close slideout window (if after searching)
  window.lhn("#lhn_helppanel #lhn_help_exit").click(function(){
    frn_analytics('Live Chat', 'Contact Us Slide Out: Close Button', '', 'no', /lhnchat-window-click-close-button/');
  }); 
  //search box
  window.lhn("#lhn_helppanel #lhn_search_box").blur(function(){
    if(window.lhn(this).val() !== ""){
     lhnCurVal = window.lhn(this).val();
     if(typeof(lhnOldVal) === 'undefined' || lhnOldVal != lhnCurVal){
      frn_analytics('Live Chat', 'Contact Us Slide Out: Search', encodeURIComponent(window.lhn(this).val()), 'no', '');
      lhnOldVal = lhnCurVal;
     }
    }
  });
  //callback request button
  window.lhn("#lhn_helppanel .lhn_callback_btn").click(function(){
    frn_analytics('Live Chat', 'Contact Us Slide Out: Callback Button', '', 'no', '/lhnchat-window-click-for-callback/');
   });
  */
}





///////////////////
// LHN On-Page CHAT Buttons:
   //This jQuery runs upon page load, but it has a listener waiting for the chat button to be printed to the page
   //in-page email image button (covered in new email launch function below)
   //in-page chat image button (covered below)
   //in-page email text button (covered by the new email launch function below)
   //in-page chat text button (covered by the frn_chat_window)
   //IMPORTANT: 
   // Do not use the typical email or chat image IDs (used in jQueries here) for our custom text buttons or the action labels will be incorrect

/*
//Email GA tracking is now handled by the email JS open below as of 3/9/2017
//Moved to that function to get it closer to the actual window open action
//Keeping here for reference
jQuery("#lhnEmailButton").ready(function($) {
  $("#lhnEmailButton").click(function(){  //Email button only
    frn_analytics('Live Chat', 'In-Page Buttons: Email Button Clicks/Touches', 'Email Image', 'email', '/lhnchat-onpage-click-on-email-button/');
    if(frn_hj_act!=="" && hj) hj('vpv');
    alert("on page LHN system EMAIL button image");
  });
});
*/



//In-page chat image button ONLY 
  //delivered by LHN's system only
  //as a result, we can't affect the open window function directly
  //we can report to analytics easily when the tab is on the page, but our in-page buttons are usually only on Contact pages for facilities--where a contact tab is unnecessary
  //without the tab, our only way to report to Analytics is via the method below
  //aLHNBTN is the same nested item as the onClick that opens the window but due to return false on the onClick, this listener never fires.
  //To get it to fire, we have to listen for clicks on the outer DIV ID lhnChatButton
jQuery("#lhnChatButton").ready(function($) {
  $("#lhnChatButton").click(function(){
    //alert("on page LHN system CHAT button image");
    frn_analytics('Live Chat', 'In-Page Buttons: Chat Button Clicks/Touches', 'Chats', 'chat', '/conversion-lhn-onpage-chat-button-click/');
  });
});






////////////////// 
/// LIVEHELPNOW CUSTOM BUTTON Pre-chat window launch
//////////////////
/// However, for our own text buttons, we use this function via our error checking function in the HEAD (defined in our part_live_chat.php).
/// There is no verification that a window actually opened (some popup blockers may block this feature--that was always the case since we've used LHN)
/// added 1/31/17

function frn_chat_window( category, action, label){
  //previously CustomOpenLHNChat: 
  //  but the function only fired when the slideout was present and then it affected both the in-page chat button AND slideout. 
  //  When slideout wasn't there, system used the default openLHNchat, which did use our Event labels.
  //  Using that function name overwrote their function -- it replaced it instead of both running
  
  //Makes sure the window opens on top of the current browser window
  if (typeof lhnWin !== 'undefined' && lhnWin){
    if (!lhnWin.closed){
      lhnWin.blur();
      setTimeout(lhnWin.focus(), 10);
      return false;
    }
  }

  var wleft = (screen.width - 580-32) / 2;
  var wtop = (screen.height - 520-96) / 2;


  /////
  // GOOGLE ANALYTICS
  if(typeof category=='undefined') category='Live Chat';
  if(category=="") category='Live Chat';
    if(typeof action!=='undefined') action='In-Page Buttons: Chat Button Clicks/Touches';
  if(action=="") action='In-Page Buttons: Chat Button Clicks/Touches';
  if(label=="") label='Chats';
    //alert("type: text/custom in-page button; category: "+category+"; action: "+action);
    
    //Hotjar no longer executed here. Instead in frn_analytics.
    //if(frn_hj_act!=="" && hj) hj('vpv', '/conversion-lhn-onpage-chat-button-click/');
  frn_analytics(category, action, label, 'chat', '/conversion-lhn-onpage-chat-button-click/'); 


  //// OPEN WINDOW
  //alert("Custom Open LHN Chat function executed"); //use if you don't want test URLs to show in LHN for reps
  lhnWin=window.open('https://www.livehelpnow.net/lhn/livechatvisitor.aspx?lhnid='+lhnAccountN+'&d='+lhnDepartmentN+'&zzwindow='+lhnWindowN, 'lhnchat', 'left=' + wleft + ',top=' + wtop +',width=580, height=435,toolbar=no,location=no,directories=no, status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes');
  return false;

}




////////////////// 
/// LIVEHELPNOW EMAIL - Email custom window JS
//////////////////
/// This funciton is called via a function in the HEAD section. We added an error reporting try/catch in case this JS file doesn't load in time
/// It is called for BOTH text and image button versions (it gives us more control over the window.open function and GA reporting)
/// In some tests, the reporting to GA had to heppen prior to window loading or it didn't fire. 
/// There is no verification that a window actually opened (some popup blockers may block this feature--that was always the case since we've used LHN)
/// Dax created this on his own and took some elements from the chat function. LHN was never consulted for this solution.
/// added 3/4/17

function frn_email_window(category,action){

  //makes sure that the popup shows on top of the current browser window -- most of the time it opens behind the browser
  if (typeof lhnWin !== 'undefined' && lhnWin){
    if (!lhnWin.closed){
      lhnWin.blur();
      setTimeout(lhnWin.focus(), 10);
      return false;
    }
  }

  var wleft = (screen.width - 450-32) / 2;
  var wtop = (screen.height - 600) / 2;
  
  //// REPORT TO ANALYTICS
  //defaults
  if(typeof category=='undefined') category='Live Chat';
  if(category=="") category='Live Chat';
    if(typeof action!=='undefined') action='In-Page Buttons: Email Button Clicks/Touches';
  if(action=="") action='In-Page Buttons: Email Button Clicks/Touches';
  
  //alert("Email Window custom open function called // category: "+category+"; action: "+action);
    frn_analytics(category, action, 'Emails', 'email', '/conversion-lhn-onpage-email-click-text-buttons/'); 
  
  //// OPEN WINDOW
  lhnWin=window.open('https://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid='+lhnAccountN+'&d='+lhnDepartmentN+'&zzwindow='+lhnWindowN,'Ticket','left=' +wleft+ ',top='+wtop+',scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');
  return false;

}









//#####
//### ANCILLARY GA TRACKING (links, emails, downloads, icons, etc.)
//#####
//Some of this based on: http://jsfiddle.net/shamel67/VzbPw/1/
/*
  External links
  Email addresses
  Downloads
  Image opens (via clicking on them or via a link)
  Social sharing links

  NOTES: 
  It has been a couple years (prior to 3/9/17) that the social sharing and email features have been tested (all other features regularly show in GA)
  We often mask email addresses now, so it's unlikely that any email address clicks will be reported
*/


//if (window.jQuery) { //removed this check since we activate it via wordpress PHP
jQuery("body").ready(function($) {
//used to use "document" ready instead of "body"
  
  if ((_ga) || (_gaq) || frn_hj_act!=="") {
    


    ///////////////////
    //Email addresses
    $('a[href^="mailto"]').on('click', function(e) {
      if (frn_hj_act!=="" && hj) {
        var email_clean = this.href.replace(/^mailto:/i, '');
        email_clean = email_clean.replace(/@\./i, ''); //strips out @ and periods to simplify url
      }
      frn_analytics('Email Address Clicks', this.href.replace(/^mailto:/i ,'Emails' , 'no', '')); 
      //alert("email reported");
    });
    



    ///////////////////
    //regular links
    var dom_pattern= new RegExp(root_domain.replace('www.', ''),"i");
    
    //sees if the domain of the page is different than the domain in the href
    $('a[href^="http"]').filter(function() {
       if ((dom_pattern).test($(this).attr('href'))) return false;
       else return true;
    }).on('click', function(e) {
    
    //Old case sensitive version: $('a[href^="http"]:not([href*="//' + location.host + '"])').on('click', function(e) {
      //Sees if external link is a download and track if so
      var download_tracked = false;
      regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
      if (regex_pttn.test(this.href)) { 
        download_tracked = true;
        frn_analytics('Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "Off-Site Image/File: "+this.href.replace(/^.*\/\//), 'no', '');
        //alert("Outbound File Click Tracked: (" + regex_pttn.exec(this.href)[1].toUpperCase() + ") " + this.href.replace(/^.*\/\//, ''));
      }
      
      //Checks for non-iframe and non-DIV versions of sharing buttons/links
      var social_type = "";
      var social_action = "";
      if ($(this).attr('href').indexOf("pinterest.com/pin/create/button/") >=0 ) {
        social_type = "Pinterest";
        social_action = "pinned";
      }
      else if ($(this).attr('href').indexOf("linkedin.com/shareArticle") >=0 ) {
        social_type = "LinkedIn";
        social_action = "share";
      }
      else if ($(this).attr('href').indexOf("facebook.com/sharer/sharer.php") >=0 ) {
        social_type = "Facebook";
        social_action = "share";
      }
      else if ($(this).attr('href').indexOf("twitter.com/home?status") >=0 ) {
        social_type = "Twitter";
        social_action = "tweet";
      }
      
      //Tracks outbound link whether download or not.  Creates duplicate events if link is an offsite download, but this data will be tagged differently.
      if (social_type!=="") {
        frn_analytics(social_type, social_action, this.href.match(/\/\/([^\/]+)/)[1], 'no', ''); 
        //alert("social link click sent to Google: "+social_type);
      }
      else {
        frn_analytics('Outbound Links', "Outbound: "+this.href.match(/\/\/([^\/]+)/)[1], '', 'no', ''); 
        //alert("external link click sent to Google");
      }
      social_type="";
    });
  



    ///////////////////
    // _trackDownloads/Image opens
    //If download has already been tracked since it's an external link, then skip this
    if(typeof download_tracked == 'undefined') {
      
      // helper function - allow regex as jQuery selector
      $.expr[':'].regex = function(e, i, m) {
        var mP = m[3].split(','),
          l = /^(data|css):/,
          a = {
            method: mP[0].match(l) ? mP[0].split(':')[0] : 'attr',
            property: mP.shift().replace(l, '')
          },
          r = new RegExp(mP.join('').replace(/^\s+|\s+$/g, ''), 'ig');
        return r.test($(e)[a.method](a.property));
      };

      $('a:regex(href,\\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*))').on('click', function(e) {
        //Make sure it's on this website
        var download_error = "";
        if (this.href.indexOf(frn_pluginInstall)>0) {
          regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
          frn_analytics('Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "On-Site Image/File: "+this.href.replace(/^.*\/\//, 'no', ''), '', ''); 
          //alert("On-Site Download Tracked: (" + regex_pttn.exec(this.href)[1].toUpperCase() + ") " + this.href.replace(/^.*\/\//, ''));
        }
      });
    }


  }
  



  ///////////////////
  // _trackError: track 404 - Page not found Errors
  //The following is not tracked in hotjar, so the following only happens if any Google Analytics code is activated
  if ((_ga) || (_gaq)) {
    if (typeof error_404_title == 'undefined') var error_404_title="Page Not Found";
    else if (error_404_title==="") error_404_title="Page Not Found";
    if (document.referrer==="") $site_referrer = "; Problem_on=[direct_visit/offsite]";
    else {
      if (document.referrer=="http://"+root_domain) $site_referrer = "; Problem_on=" + document.referrer.replace("http://"+root_domain+"/","[homepage]");
      else $site_referrer = "; Problem_on=" + document.referrer.replace("http://"+root_domain,"");
      //removed location.host from all of these and replaced with root_domain. Noting in case there are issues in the future.
    }
    if (document.title.search(error_404_title) >=0) {
      regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
      if (regex_pttn.test(location.pathname)) {error_label = "On-Site Download (" + regex_pttn.exec(location.pathname)[1].toUpperCase() + ")"; error_type = "Download Error";}
      else {error_label = "Page Not Found"; error_type = "Page Not Found";}

      frn_analytics('404 Errors', error_type + "; Broken_URL=" + location.pathname + $site_referrer, error_label, 'no', ''); 
      //alert("404 tracked: " + error_404_title + "; " + document.title);
      //alert(error_type + "; Broken_URL=" + location.pathname + $site_referrer + "; Label:" + error_label);
    }
  }
  


  
  ///////////////////
  //Adds external link icon
  if (typeof frn_eli_deact != 'undefined' ) { //both frn_eli_deact and frn_eli_target are defined at the same time if the JS version of these are selected in the plugin settings, so we only need to check one of them.
    var currStyle="", hasbgimg="", styleBg="", img_check="";
    var ext_icon = "background-image:url('"+frn_pluginInstall+"/images/icon_ext_links.png'); background-repeat:no-repeat; background-position:right center; padding-right:14px; margin-right: 5px;";
      
    //making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
    if (frn_eli_target!="new" || (frn_eli_deact=="A")) {
      $('a[href^="http"]').filter(function() {
           if ((dom_pattern).test($(this).attr('href'))) return false;
           else return true;
        }).each(function(e) {
          //Old case sensitive version: $('a[href^="http"]:not([href*="//' + location.host + '"])').each(function(e) {
          //alert($(this).attr("href"));
          
          //making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
          if (frn_eli_deact=="A" && !$(this).attr("data-exticon") && ext_icon!=="") { 
          //deactivates icon for external links if variable = A or if the link has data-exticon attribute defined
            //alert(currStyle);
            //check if inline styles present so we can just append to them
            currStyle=$(this).attr("style");
            if (typeof currStyle!="undefined") { //if there is a style attribute manually added to the link, add a semicolon
              currStyle=currStyle.trim(); //removes end spaces
              if (currStyle.substr(currStyle.length - 1)!=";") currStyle=currStyle+"; "; //adds a semicolon if inline style doesn't have one
            }
            else currStyle="";
            
            //Check if image is linked or if css uses a background image for linking
            img_check = $(this).html();
            styleBg=$(this).css('background-image');
            if (img_check.indexOf("img")==-1 && styleBg=="none") 
              $(this).attr("style", currStyle+ext_icon);
          }
          //if there is a target set, check if we don't want to remove it.
          if((this).attr("target")!='undefined') {
            //if the whole site setting is to open external links in the same window, check if we specifically wanted to open that one into a new window
            if (frn_eli_target=="same" && ($(this).attr("data-target")=="new" || $(this).attr("keep_target")=="yes")) var do_nothing=""; //do nothing and keep the target (easier than testing for the opposite situations)
            else $(this).removeAttr( "target" );
          }
      });
    }
    //deactivates icon for email addresses if variable is not A
    //making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
    if (frn_eli_deact=="A" && !$(this).attr("data-exticon" && ext_icon!=="")) { 
      $('a[href^="mailto"]').each(function(e) {
        currStyle=$(this).attr("style");
        if (typeof currStyle != 'undefined') {
          currStyle=currStyle.trim(); //removes end spaces
          if (currStyle.substr(currStyle.length - 1)!=";") currStyle=currStyle+"; "; //adds a semi colon if inline style doesn't have one
        }
        else currStyle="";
        
        img_check = $(this).html();
        styleBg=$(this).css('background-image');
        if (img_check.indexOf("img")==-1 && styleBg=="none") 
          $(this).attr("style", currStyle+ext_icon);
      });
    }
  }
  
  
});






//#####
//### SOCIAL SHARING TRACKING
//#####

// ONLY tracks Twitter and Facebook. Google+ automatically tracked in Anaytics. LinkedIn tracked as an external link above but reported as a social share.
// authored by bravedick on GitHub, last updated Jan 26,2015
// modified by Daxon Edwards 5/11/2015
// https://github.com/bravedick/Google-Analytics-Social-Tracking

  /**
   * https://developers.google.com/analytics/devguides/collection/analyticsjs/social-interactions
   */

  var GASocialTracking = function() {

  };

  GASocialTracking.prototype = {
    /*
     * Docs: https://dev.twitter.com/web/javascript/events
     * */
    trackTwitter: function() {
      var _this = this,
        opt_target,
        network = 'twitter';

      try {
        // tweet
        twttr.events.bind('tweet', function(event) {
          if (event.target.nodeName.toUpperCase() == 'IFRAME') {
            opt_target = _this.getParamFromUrl(event.target.src, 'url');
          }
          _this.push(network, event.type, opt_target);
        });

        // follow
        twttr.events.bind('follow', function(event) {
          opt_target = event.data.user_id + ' (' + event.data.screen_name + ')';
          _this.push(network, event.type, opt_target);
        });

        // retweet
        twttr.events.bind('retweet', function(event) {
          opt_target = event.data.source_tweet_id;
          _this.push(network, event.type, opt_target);
        });

        // favorite
        twttr.events.bind('favorite', function(event) {
          opt_target = event.data.tweet_id;
          _this.push(network, event.type, opt_target);
        });
      } catch (e) {
        //if (console) console.log('GA Twitter Tracking error:', e);
      }
    },



    /*
     * Docs: https://developers.facebook.com/docs/reference/javascript/FB.Event.subscribe/v2.2?locale=en_GB
     * */
    trackFacebook: function() {
      var  _this = this,
        network = 'facebook';
      try {
        FB.Event.subscribe('edge.create', function(url) {
          _this.push(network, 'like', url);
        });
        FB.Event.subscribe('edge.remove', function(url) {
          _this.push(network, 'unlike', url);
        });
        FB.Event.subscribe('message.send', function(url) {
          _this.push(network, 'message', url);
        });
        FB.Event.subscribe('comment.create', function(obj) {
          _this.push(network, 'comment.create', obj.href);
        });
        FB.Event.subscribe('comment.remove', function(obj) {
          _this.push(network, 'comment.remove', obj.href);
        });
      } catch (e) {
        //if (console) console.log('GA Facebook Tracking error:', e);
      }
    },
    push: function(network, socialAction, opt_target) {
      frn_analytics(network, socialAction, opt_target, 'no', ''); 
    },
    getParamFromUrl: function(url, param) {
      var pairs = url.split('&');
      for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        if (pair[0] == param) {
          return decodeURIComponent(pair[1]);
        }
      }
      return false;
    }
  };

  var gaSocialTracking = new GASocialTracking();

/*
              $(document).ready(function() {
                $("h2").each(function(i) {
                  var heading = $(this);
                  var headingId = "topic" + (i + 1);
                  heading.attr("id", headingId);
                });
              });

               $(document).ready(function (){
                      $("#frn_toc a").click(function (){
                          $("html, body").animate({
                              scrollTop: $("#div1").offset().top
                          }, 2000);
                      });
                  });
*/
function toc_scroll(index) {
  //alert(index);
  if(typeof index=='undefined') index=1; //Starts with 1 to skip the TOC h2
  //alert(index);
  if(index==='') index=1;
  //alert(index);
  jQuery("html, body").animate({
    scrollTop: jQuery("h2:eq( "+index+" )").offset().top-100
  }, 1000);

  /*
  //history.pushState(null, null, null); //can be used to store the prior page location. The following code is only example from https://stackoverflow.com/questions/16731271/history-pushstate-and-scroll-position
  $('#link').click(function (e) {

        //overwrite current entry in history to store the scroll position:
        stateData = {
            path: window.location.href,
            scrollTop: $(window).scrollTop()
        };
        window.history.replaceState(stateData, 'title', 'page2.html');

        //load new page:
        stateData = {
            path: window.location.href,
            scrollTop: 0
        };
        window.history.pushState(stateData, 'title', 'page2.html');
        e.preventDefault();

    });

  */

}