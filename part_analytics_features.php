<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

	/* NOTES:
		"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
		Although this file is seperate, most of the other files depend or reference something from this file. 
		This file only includes frontend changes for users. 
		Admin features are still in the admin php.
		Move to it's own file on 3/7/17
		v 2.3 - 10/3/17
		v 2.4 - 10/12/17 - added new Analytics custom dimension groups based on word counts
		v 2.5 - 10/25/17 - moved Optimizely higher in the header
		v 2.6 - 9/18/18 - added content groups for buyers journey and promoted content date dimensions
		v 2.7 - 6/23/19 - changed the jQuery dependancy for our scripts.js to no longer consider rehabandtreatment.com
		v 2.7.1 - 8/2/19 - fixed the error caused by changing to a "jQuery" call in the array instead of all lowercase. Also added in a better detection method using wp_script_is that hopefully 
		v 2.7.2 - 8/8/19 - updated the company's IP address regex
		v 2.7.3 - 12/12/19 - remove jQuery dependency. FRN.com deregisters theirs so WP doesn't think that it exists and doesn't load our scripts.


	INCLUDED:
		Company IPs
		Google Analytics
		Chartbeat
		Hotjar
		PPC: 
			Yahoo
			Bing
			Facebook
		Contently
		Outbrain (discontinued 9/2017)
		Optimizely code 10/3/17
	*/


add_action( 'wp_enqueue_scripts', 'frn_plugin_js', 201 );
function frn_plugin_js(){
	//R&T.com Exception///
	//FYI: RehabAndTreatment.com relies on this function name to determine if FRN Plugin is activated.
	//Pagespeed improvement puts jQuery in the footer, but WP doesn't do that natively.
	//R&T loads its own jQuery as a result, so it needs to be deactivated here just for that domain.
	//jQuery dependency removed 12/12/19 due to FRN.com calling it's own version and deregistering WP's
	//$jquery_dep = array();
	//if(wp_script_is('jquery')) {
	//    $jquery_dep = array('jquery');
	//}
	//R&T option discontinued due to new wp_script_is approach 8/2/19
	//$frn_domain = str_replace("http://","",str_replace("https://","",str_replace("www.","",home_url())));
	//if(stripos($frn_domain,"rehabandtreatmentcom")===false) $jquery_dep = array('jquery');
	//END R&T.com Exception///
	
	wp_register_script( 'frn_plugin_scripts', plugin_dir_url( __FILE__ ).'scripts.js', null, '1.3' , true ); //true = footer
	wp_enqueue_script( 'frn_plugin_scripts' );
}


function frn_ga_click_event_js($set_number="",$atts) {
	$ga="";
	$defaults=array(
		//updated 2016 since other versions hard to remember
		'category' => 'Phone Numbers',
		'action' => 'Phone Clicks [General]',
		'label' => '',

		//old vars - likely no longer used
		'ga_phone_category' => '',
		'ga_phone_location' => '',
		'ga_phone_label' => '',

		//Device overrides
		'mobile_category' => '', 
		'mobile_action' => '', 
		'desktop_category' => '', 
		'desktop_action' => '', 
	);
	//combine arrays, but ignore defaults if previously defined
	$atts=wp_parse_args($atts,$defaults); 
	extract($atts); //turn keys into vars

	$ga_phone_location=trim($ga_phone_location);
	$ga_phone_category=trim($ga_phone_category);
	$ga_phone_label=trim($ga_phone_label);
	if($ga_phone_category!=="") $category=$ga_phone_category;
	if($ga_phone_location!=="") $action=$ga_phone_location;
	if($ga_phone_label!=="") $label=$ga_phone_label;

	/// Device-Specific Overrides
	global $frn_mobile;
	if($frn_mobile) {
		$mobile_category=trim($mobile_category);
		$mobile_action=trim($mobile_action);
		if($mobile_category!=="") $category=$mobile_category;
		if($mobile_action!=="") $action=$mobile_action;
	}
	else {
		$desktop_category=trim($desktop_category);
		$desktop_action=trim($desktop_action);
		if($desktop_category!=="") $category=$desktop_category;
		if($desktop_action!=="") $action=$desktop_action;
	}
	$category=trim($category);
	$action=trim($action);
	$label=trim($label);

	//When phone number used as the label...
	if($label=="" && strpos(strtolower($category),"global")===false && strpos(strtolower($category),"desktop")===false) {
		//"global" is used for global contact floating buttons (URL used for desktop instead of number) - e.g. Talbott and MH.com
		//"desktop" is included just in case it's used in the category
		$label=$set_number;
	}

	/* Deactivated 2/17/19 due to the JS file now simply detecting if Infinity's function exists.
	$ia=""; //adds trigger for infinity conversion tracking
	if(function_exists('frn_infinity_click_events')) {
		if(frn_infinity_click_events()=="yes") {
			$ia = ",'y'";
		}
	}
	*/

	//escape single and double quotes to avoid issues
	$category = frn_escape_quotes($category); 
	$action   = frn_escape_quotes($action); 
	$label    = frn_escape_quotes($label); 
	$ga=" onClick=\"frn_reporting('','".$category."','".$action."','".$label."','no');\"";

	return $ga;
}
function frn_escape_quotes($text="") {
	$text = str_replace("'","\'",$text); //single
	$text = str_replace('"','\"',$text); //double
	return $text;
}



	////////////////////////
	// COMPANY_IPs TEST   //
	////////////////////////

	function frn_company_ips() {

		//set global value for other functions
		global $frn_company_ip;
		if(!isset($frn_company_ip)) $frn_company_ip="";

		/*
		// OLD FRN APPROACH:
		//loop through IP addresses to see if there is a match
		
		$frn_company_ips = array(
			'12.230.217.13',
			'12.230.217.14',
			'73.104.49.76',
			'69.47.164.149',
			'75.22.103.153',
			'173.164.20.37',
			'50.204.247.218',
			'255.255.252.0',
			'108.65.222.48'
		);

		$ch_i=0; $frn_company_ip=="";
		$frn_total_ips=count($frn_company_ips); 
		$frn_user_ip=$_SERVER['REMOTE_ADDR']; 
		while ($ch_i<>$frn_total_ips && $frn_company_ip=="") {
			if($frn_user_ip==$frn_company_ips[$ch_i]) {
				$frn_company_ip=$frn_company_ips[$ch_i];
				//echo "<h1>".$frn_company_ip."</h1>";
			}
			$ch_i++;
		}
		*/


		//If this function has already ran and set the global value, just pass it through
		if($frn_company_ip=="") {
			//UHS IP Pattern (provided February 2017)
			// Keeps chartbeat/hotjar from tracking any FRN and UHS employee
			// See Google Sheet for readable list, dates, and purposes: 
			//    https://docs.google.com/spreadsheets/d/1ok74iWV4QL0j99KDBr1T0uGcu53eC8fYmJC2Brwdl8E/edit#gid=368443453 
			// Old before 10/25/17: $uhs_ip_pattern = "/12\.230\.217\.13|12\.230\.217\.14|73\.104\.49\.76|69\.47\.164\.149|75\.22\.103\.153|173\.164\.20\.37|50\.204\.247\.218|12.230.217.[0-2][0-35]?[0-7]?|255\.255\.252\.0|108\.65\.222\.48|50\.76\.171\.85/";
			// Old before 8/07/19: $uhs_ip_pattern = "/108\.65\.222\.48|12.230.217.[0-2][0-35]?[0-7]?|255\.255\.252\.0|50\.204\.247\.218|69\.47\.164\.149|73\.104\.49\.76|75\.22\.103\.153|50\.234\.149\.124/";

			$uhs_ip_pattern = "/50.238.182.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|50\.204\.247\.218|65\.60\.128\.139|50\.234\.149\.124/";
			$frn_user_ip=$_SERVER['REMOTE_ADDR']; 
			$ip_regex=preg_match($uhs_ip_pattern,$frn_user_ip,$matches);
			if($ip_regex) $frn_company_ip=$matches[0];
			else $frn_company_ip="";
		}
		//echo "<h1>Dax IP: ".$frn_user_ip."; found: ".$matches[0]."</h1>";
		return $frn_company_ip;
		
	}






	//////////////////////////////////
	// GOOGLE_ANALYTICS FEATURES    //
	//////////////////////////////////
	if(!is_admin()) {add_action('wp_head', 'frn_addtoheader', 100);}
	if(!function_exists('frn_addtoheader')) {
	function frn_addtoheader() {
	    //frn GA version 2.3 10/15/2013
	    //frn GA version 2.4 6/15/16
		
		$frn_ga = "
		
	<!-- #####
	FRN Google Analytics
	##### -->";

		$options_head = get_option('site_head_code');
		
		//Turns on JavaScript processing of external links if the frn_eli_... variables defined in the HEAD code
		$ga_act_eli=""; $ga_exttarget="";
		if(isset($options_head['ext_link_icon_type'])) {
			if($options_head['ext_link_icon_type']=="js") {
				if(!isset($options_head['ext_link_target'])) $options_head['ext_link_target']="";
				//Deactivates processing of external links if this is set to "deactivate"
				if($options_head['ext_link_target']!=="deactivate") $ga_exttarget = '
			frn_eli_target="'.$options_head['ext_link_target'].'";';
				//Deactivates adding icon to external links
				$ga_act_eli = '
			frn_eli_deact="'.(isset($options_head['ext_link_icon']) ? $options_head['ext_link_icon'] : null).'";';
			}
		}
		

		//Prepare the Analytics code
		$launch_GA=true; $site_ga_test=""; $ea_ua_js=""; $site_ga_id_value=""; $error_404_title=""; $demo_js_activate=""; $ga_test=""; $demo_js=""; //$ea_js = ''; 
		
		//Initial Triggers:
		//If test mode isn't initiated, then see if user is logged in. We don't want our updating activity to mess with analytics.
		if(isset($options_head['site_ga_test'])) $site_ga_test=$options_head['site_ga_test'];
		//echo "<h1>".$site_ga_test."</h1>";
		if ( $site_ga_test!=="Test" ) {
			if (is_user_logged_in()) $launch_GA = false; 
		}

		//Prepare the code:
		if ($launch_GA) { 

			/*  //Now a default in all Google Analytics accounts.
			$ga_ua = ""; 
			if(isset($options_head['frn_ga_ua'])) {
				if($options_head['frn_ga_ua']=="Activate") $ga_ua = "_ua"; //Used to refer to other JS files that have the latest GA event and social tracking codes
			}
			*/

			if(isset($options_head['frn_ga_dgrphx'])) {
				if($options_head['frn_ga_dgrphx']=="Activate") $demo_js_activate = "y"; //Used to refer to other JS files that have the latest GA event and social tracking codes
			}

			//decided to continue tracking company persons since many sites employees use and we want to see how
			if(isset($options_head['site_ga_id_value'])) $site_ga_id_value=$options_head['site_ga_id_value'];
			
			if($site_ga_id_value!=="") {  //if no GA site id is not included, then don't put GA code on site

				if(!isset($options_head['frn_ga_dgrphx'])) $options_head['frn_ga_ea']="";
				if(!isset($options_head['frn_ga_ea'])) $options_head['frn_ga_ea']="";

				//Adds the info to our GA code that turns on our in-page per link click tracking
				if($options_head['frn_ga_ea']=="Activate") {
					//old version discontinued 6/15/16
					//$ea_js = "var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
					//_gaq.push(['_require', 'inpage_linkid', pluginUrl]);";
					$ea_ua_js = "
		ga('require', 'linkid');";  //updated 7/20/16 removed: , 'linkid.js'
				}
				
				if(isset($options_head['frn_ga_404'])) {
					if(trim($options_head['frn_ga_404'])!=="") 
						$error_404_title = "
		error_404_title = '".$options_head['frn_ga_404']."';";
				}
				
				//Used for including Double-Clicks' assumed demographics data
				if($demo_js_activate=="y") 
					$demo_js = "
		ga('require', 'displayfeatures');";
				
				if ( $site_ga_test=="Test" ) $ga_test="
		//Test mode active";

				$frn_cross_domain = ");";  //default end after GA ID
				if(!isset($options_head['cross_trigger'])) $options_head['cross_trigger']="";
				if(!isset($options_head['cross_sites'])) $options_head['cross_sites']="";
				if($options_head['cross_trigger']!=="" && trim($options_head['cross_sites'])!=="") {
					$frn_cross_domain = ", 'auto', {'allowLinker': true});
		ga('require', 'linker');
		ga('linker:autoLink', ['".$options_head['cross_sites']."'] ); //cross domain tracking active";
				}



				$ga_post_types=""; //Current post type value
				$ga_custom_go="no"; //the following metrics will not be reported by default
				$dimensions=""; 
				$content_grps=""; 
				
				//PAGE IS POST? : Since dimensions and groups only apply to pages or posts, make sure current page is one
				$page_is_post="no"; if(frn_check_if_post()=="yes") $page_is_post="yes";
				if($page_is_post=="yes") {
					//echo "page_is_post: " . get_field('frn_ga_dem_disable');
					
					//Page Override: Disable dates and length reporting vars (ACF)
					$ga_custom_activate="yes"; //default
					if(function_exists('get_field')) {
						$ga_custom_activate = strtolower(get_field('frn_ga_dem_disable'));
						if(!$ga_custom_activate) $ga_custom_activate="yes"; //many pages until saved won't have this field
						if($ga_custom_activate=="") $ga_custom_activate="yes"; //although "No" is the default ACF option, it still may return something blank
					}
					//echo "page_is_post: " . $ga_custom_activate;
					
					if($ga_custom_activate=="yes") {
						//echo "ga_custom_activate";
				

						//POST TYPE LIMITATION: Check if current post is in the list of accepted post types
						//Since buyers journey/promotions aren't reported to GA unles something is entered into ACF fields, post type limitations would only apply to the remaining group types
						//FRN Plugin Post Types Check:
							//get list of post types selected
							//if list is blank or if "any" is selected, then run function anyway.
							//get current post type
							//See if that post type is in the list of those selected

						//first, do the easiest--no array check
						if(!isset($options_head['ga_custom_posts'])) $options_head['ga_custom_posts']="";
						$ga_post_types = $options_head['ga_custom_posts'];
						if($ga_post_types=="any" || $ga_post_types=="") {$ga_custom_go="yes";}
						//else, see if current type is in the list
						else {
							if(!is_array($ga_post_types)) $ga_post_types=explode(",",$ga_post_types);
							//echo "<h1>Before: ";
							//print_r($ga_post_types);
							//echo "</h1>";
							if(is_array($ga_post_types)) {
								if(in_array("any",$ga_post_types)) {
									$any_id=count($ga_post_types)-1;
									unset($ga_post_types[$any_id]); //assumes "any" is always at the end of any array -- because of checkbox order in settings
								}
								foreach($ga_post_types as $post_type) {
									if($post_type) {
										//echo "<h1>Current type: ".get_post_type()."; Selected Type: ".$post_type."</h1>";
										if(get_post_type()==$post_type) {
											$ga_custom_go="yes";
										}
									}
								}
							}
							//echo "<h1>After: ";
							//print_r($ga_post_types);
							//echo "</h1>";
						}
				

					
						
						if($ga_custom_go=="yes") {
							/////
							/// DIMENSIONS
							/////

							//Global and post override settings can disable this.
							//PUBLISHED DATE: 
							// 9-11-17: dashes would be easier to see the segments, but then the number can't be sorted or filtered using greater than or less than. We will test if data is a number and if Analytics lets us sort by that. If not, we'll add dashes in.
							$pub_date=trim(get_the_date( 'Y-m-d' ));
							$p_id="1";
							if( $pub_date ) {
								if(!isset($options_head['ga_pub_id'])) $options_head['ga_pub_id']="";
								if($options_head['ga_pub_id']!=="") $p_id=$options_head['ga_pub_id'];
								//$pub_date=trim(get_the_date( 'Y-m-d' ));
							}
							if($pub_date!=="") {
								$dimensions.="
		ga('set', 'dimension".$p_id."', '".$pub_date."');";
							}

							//MODIFIED DATE
							$mod_date=trim(get_the_modified_date( 'Y-m-d' ));
							$m_id="2";
							if( $mod_date ) {
								if(!isset($options_head['ga_mod_id'])) $options_head['ga_mod_id']="";
								if($options_head['ga_mod_id']!=="") $m_id=$options_head['ga_mod_id'];
							}
							if($mod_date!=="") {
								$dimensions.="
		ga('set', 'dimension".$m_id."', '".$mod_date."');";
							}

						}


						//PROMOTED CONTENT DATES (to measure if promotions help content performance)
						$promoted=false; 
						if(function_exists('get_field')) {
							$promoted = get_field('frn_prmtd');
						}
						if($promoted) { //if active, it's an array
							/* //Checkbox option
							$promoted_flag="Not Promoted";
							foreach( $promoted as $flag ) {
								if(trim($flag)!=="" && $promoted_flag=="Not Promoted") $promoted_flag=ucfirst($flag); //mostly a back up in case we make a mistake in the ACF settings.
							}
							*/
							//echo "Test promoted";
							//print_r($promoted);
							if(!$promoted['start']) $promoted['start']="";
							if(!$promoted['end']) $promoted['end']="";

							if($promoted['start']!=="") {
								//Avoid dimensions from be reported until on/after start date (to put "hits" happening during the correct promotional period)
								if(date("Y-m-d") >= date("Y-m-d", strtotime($promoted['start']))) {
									//echo "Today's date is the same or after the date entered";
									$p_s_id="3";
									if(!isset($options_head['ga_prmtd_s_id'])) $options_head['ga_prmtd_s_id']="";
									if($options_head['ga_prmtd_s_id']!=="") $p_s_id=$options_head['ga_prmtd_s_id'];
									$dimensions.="
		ga('set', 'dimension".$p_s_id."', '".$promoted['start']."');";
								
									if($promoted['end']!=="") {
										$p_e_id="4";
										if(!isset($options_head['ga_prmtd_e_id'])) $options_head['ga_prmtd_e_id']="";
										if($options_head['ga_prmtd_e_id']!=="") $p_e_id=$options_head['ga_prmtd_e_id'];
										$dimensions.="
		ga('set', 'dimension".$p_e_id."', '".$promoted['end']."');";
									}
								} //requires hits only be reported after promotion start date
							} //requires start to be set
						}



						/////
						/// CONTENT GROUPS
						/////

						if($ga_custom_go=="yes") {
							/////////
							//Wordcount for contact pages
							if(stripos(get_the_title(),"contact")===false) {

							// Word count - Content Length
								//Short 1  <500
								//Short 2  500-849
								//Medium   850-1199
								//Long 1   1200-1499
								//Long 2   1500-1999
								//Long 3   >=2000
								$id="1";
								if(!isset($options_head['ga_group_id'])) $options_head['ga_group_id']="";
								if($options_head['ga_group_id']!=="") $id=$options_head['ga_group_id'];
								$word_count = str_word_count(
												apply_filters('the_content',get_the_content() ) //makes sure all shortcodes are processed when counting the words
											);
								$content_length="";
								if($word_count>=10) {
									if($word_count<500) 							$content_length = "Short 1 (<500)";
									elseif($word_count>=500 && $word_count<850) 	$content_length = "Short 2 (500-849)";
									elseif($word_count>=850 && $word_count<1200) 	$content_length = "Medium (850-1199)";
									elseif($word_count>=1200 && $word_count<1500) 	$content_length = "Long 1 (1200-1499)";
									elseif($word_count>=1500 && $word_count<2000) 	$content_length = "Long 2 (1500-1999)";
									elseif($word_count>2000) 						$content_length = "Long 3 (>2000)";
									if($content_length!=="") {
										$content_grps.="
		ga('set', 'contentGroup".$id."', '".$content_length."');";
									}
								} //word_count > 10
							} //contact page exception
						} //end check if current post type is in global types limitations list
					} //end check if post-specific custom metrics override.


					// REHAB DECISION JOURNEY (RDJ) STAGE/SUB-TOPIC (or buyers journey or patient acquisition journey or whatever else we agreed on over time)
					//These options must be set manually for each individual page or nothing will be sent.

					if(function_exists('get_field')) {
						//ACF function
						// another test option for ACF, but didn't seem as reliable due to the sequence of plugin loading: 
						//   detect a plugin based on checking if their main PHP filename is in WP's list of activated plugins
						//   frn_test_plugin_activation('advanced-custom-fields-pro/acf.php'); //boolean
						
						//Patient Acquisition Stage
						$paj=false;
						$paj = get_field('frn_paj');
						if($paj) { //if active, it's a text field.
							if($paj!=="") { //if there is a value set
								$ga_paj_id="2"; //default
								if(!isset($options_head['ga_paj_id'])) $options_head['ga_paj_id']="";
								if($options_head['ga_paj_id']!=="") $ga_paj_id=$options_head['ga_paj_id'];
								$content_grps.="
		ga('set', 'contentGroup".$ga_paj_id."', '".$paj."');";
							}
						}
					
						//Patient Acquisition Stage Sub-Topic
						$pajt=false;
						$pajt = get_field('frn_pajt');
						if($pajt) { //if active, it's a text field.
							if($pajt!=="") { //if there is a value set
								$ga_pajt_id="3"; //default
								if(!isset($options_head['ga_pajt_id'])) $options_head['ga_pajt_id']="";
								if($options_head['ga_pajt_id']!=="") $ga_pajt_id=$options_head['ga_pajt_id'];
								$content_grps.="
		ga('set', 'contentGroup".$ga_pajt_id."', '".$pajt."');";
							}
						}


					} //end ACF requirement


				} //end requirement that current page is a post/page
				


				// removed from script and added into extensions JS: 
				//baseDomain = location.hostname;
				//"pluginInstall='".plugin_dir_url( __FILE__ )."';".
				$frn_ga .= "
	<script>".
		$ga_test.
		$error_404_title.
		$ga_act_eli.
		$ga_exttarget."
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', '".$site_ga_id_value."'".$frn_cross_domain.
		$ea_ua_js.
		$demo_js.
		$dimensions.
		$content_grps."
		ga('send', 'pageview');
	</script>";
				
				/*
				//Once a GA account is converted to a Universal Analytics account, this is the code that will replace the standard GA code.
				//if($ga_ua=="_ua") {
					//this is where the UA code above used to be
				//}
				else { //If UA is not activated, then install the old script by default
				
					//Used for including Double-Clicks' assumed demographics data
					if($demo_js_activate=="y") $demo_js = "'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js'";
					else $demo_js = "'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'";

					$frn_ga .= '
		<script type=\'text/javascript\'>
			pluginInstall="'.plugin_dir_url( __FILE__ ).'";'.
			$error_404_title.$ga_act_eli.'
			baseDomain = location.hostname;
			var _gaq = _gaq || [];
			'.$ea_js.'
			_gaq.push([\'_setAccount\', \''.$site_ga_id_value.'\']);
			_gaq.push([\'_trackPageview\']);
			(function() {
				var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
				ga.src = (\'https:\' == document.location.protocol ? '.$demo_js.';
				var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>';
				}
				*/
				
			} //ends if site_ga_id_value!==""
			

			elseif($ga_exttarget!=="" || $ga_act_eli!=="") {
				//removed due to new reliance on JS for this: pluginInstall="'.plugin_dir_url( __FILE__ ).'";
				$frn_ga .= "

		<!-- FRN Plugin: No GA UAID provided (tracking deactivated)  -->
	
		<script type='text/javascript'>".$ga_exttarget.$ga_act_eli."</script>
		";
			
			}
			
			
			else $frn_ga .= "
		
		<!-- FRN Plugin: No GA UAID provided (tracking deactivated) -->
		";

		} //if launch_ga (user logged in)
		

		elseif($ga_exttarget!=="" || $ga_act_eli!=="") {
			//removed due to new reliance on JS for this: pluginInstall="'.plugin_dir_url( __FILE__ ).'";
			 $frn_ga .= "

		<!--   YOU ARE NOT BEING TRACKED: Either...   -->
		<!-- 		1) You are logged in or           -->
		<!-- 		2) No GA UAID is provided.        -->
	
		<script type='text/javascript'>".$ga_exttarget.$ga_act_eli."</script>
		";
		
		}
		

		else {
			//removed this line due to new reliance on JS for this: pluginInstall="'.plugin_dir_url( __FILE__ ).'";
			 $frn_ga .= "

		<!--   YOU ARE NOT BEING TRACKED: Either...   -->
		<!-- 		1) You are logged in or           -->
		<!-- 		2) No GA UAID is provided.        -->
		";
		}

		$frn_ga .= "
	<!-- #####
	FRN Google Analytics Ends
	##### -->
		

";

		//Removed since combinging all JS into one file
		//<script async type="text/javascript" src="'.plugin_dir_url( __FILE__ ).'frn_ga_extensions.js?v=3.1"></script>
		
		echo $frn_ga;
		
	} //end function
	} //end function exists test for header







	//////////////////////////////////
	// FRN_GA Reporting
	//////////////////////////////////
	if(!is_admin()) {add_action('wp_head', 'frn_reporting_head', 101);}
	if(!function_exists('frn_reporting_head')) {
		//this is temporary until we see no errors being reported to GA
	function frn_reporting_head() { ?>

	<!-- #####
	FRN Google Analytics Error Checking
	##### -->
	<script type="text/javascript">
		function frn_reporting (type,category,action,label,conv) {
			if(conv===undefined || conv=="") conv='no';
			if(type=="phone") {
				if(category===undefined || category=="") category='Phone Numbers';
				if(action===undefined || action=="") action='Phone Clicks [General]';
				if(label===undefined || label=="") label='Calls';
				var conv_url='/conversion-phone-number-click-touch/';
				if(isTierIphone) conv='phone';
			}
			else { 
				if(category===undefined || category=="") category='Unknown';
				if(action===undefined || action=="") action='Unknown';
				if(label===undefined || label=="") label='';
				conv_url='';
			}
			try{ frn_analytics( category, action, label, conv, conv_url );  }
			catch(err) { if (typeof ga==='function') ga('send', 'event', category, 'FRN_Analytics Function Not Loaded', err); }
<?php
			//track Facebook conversions
			$options_ppc = get_option('site_head_code');
			if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']="";
			if(isset($options_ppc['fb'])) {
				if($options_ppc['fb']=="A" && $options_ppc['fb_conv_type']!=="pages" ) {
					if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="Lead";
					if($options_ppc['fb_conv_label']=="") $options_ppc['fb_conv_label']="Lead";
					//this only works if fbq function is on the page
?>
			if(type=="phone") { 
				if(isTierIphone && (typeof fbq==='function')) fbq('track', '<?=$options_ppc['fb_conv_label'];?>', {
				    content_name: location.hostname+' Page',
				    content_category: 'Regular Webpage',
				    content_type: 'Phone Clicks'
				}); 
			}
<?php 		
				}
			} 
?>
		}
	</script>
	<!-- #####
	FRN Google Analytics Error Checking
	##### -->
		
		<?php
	}
	}





	///////////////////////////////////
	// CHARTBEAT_HOTJAR, CONTENTLY  //
	///////////////////////////////////

	///////////
	/// HEAD_COMPONENTS
	if(!is_admin()) {add_action('wp_head', 'frn_addtoheadchrbt', 102	); }
	if(!function_exists('frn_addtoheadchrbt')) {
	function frn_addtoheadchrbt() {
		if(!is_admin()) {
			//This should only run for the front end of the site
			$options_data_tools = get_option('site_footer_code');
			$chartbeat = '';

			//check test_mode
			$options_hdr= get_option('site_head_code');
			$test_mode="";
			if(!isset($options_hdr['test_mode'])) $options_hdr['test_mode']="";
			if($options_hdr['test_mode']!=="") {
				$test_mode="yes";
			}

			/*
				IPs:
				FRN: [various, see Google Spreadsheet for Analytics referral spam]
				Matt: 75.118.35.203
				//RankLab: 173.55.187.31
			*/

			$frn_company_ip=frn_company_ips();




			/////////////////////
			/// CHARTBEAT PART 1 ///
			/////////////////////

			//First see if chartbeat isn't disabled altogether
			$launch_chartbeat="no"; $ftr_chrbt_checkbox=""; $test_notice="";
			if(isset($options_data_tools['ftr_chrbt_checkbox'])) $ftr_chrbt_checkbox=$options_data_tools['ftr_chrbt_checkbox'];
			
			if($ftr_chrbt_checkbox=="Activate") {

				//Makes sure the person isn't an admin or company employee
				if(!is_user_logged_in() && $frn_company_ip=="") $launch_chartbeat="yes";

				//test mode only overrides:
					// if it's an employee visiting the site
					// if an admin is logged in
					// test mode won't affect normal viewers or settings
				elseif((is_user_logged_in() || $frn_company_ip!=="") && $test_mode=="yes") {
					$launch_chartbeat="yes";
					$test_notice=" (Test Mode Engaged)";
				}
				
				// Display the header notification
				if($launch_chartbeat=="yes") { 
					$chartbeat = "\n	<script type='text/javascript'>_sf_startpt=(new Date()).getTime();</script>";
				}
				elseif(is_user_logged_in()) $chartbeat = "\n	<!--\n		Chartbeat: You are either logged in or accessing the site from a company location. As a result you are not being tracked in Chartbeat: Your IP address: ".$_SERVER['REMOTE_ADDR'].". See the footer for details.\n	-->\n\n";
			}
			elseif(is_user_logged_in()) {
				//displayed only for logged in users on the frontend of the site
				$chartbeat = "\n	<!--\n		Chartbeat: Chartbeat is disabled in the FRN Plugin (this message only displays for admins). See the footer for details. \n	-->\n\n";
			}


			

			/////////////////////
			/// HOTJAR PART 1 ///
			/////////////////////

			$ftr_hotjar_id=""; $hotjar_hdr="\n\n"; $launch_hotjar="no";
			if(isset($options_data_tools['ftr_hotjar_radio'])) { 
				
				if(isset($options_data_tools['ftr_hotjar_id'])) $ftr_hotjar_id=trim($options_data_tools['ftr_hotjar_id']);

				//test mode only overrides:
					// if it's an employee visiting the site
					// if an admin is logged in
					// test mode won't affect normal viewers or settings
				if($options_data_tools['ftr_hotjar_radio']=="Activate" && $ftr_hotjar_id!=="") {
					if($frn_company_ip=="") $launch_hotjar="yes"; //keep it off for company IPs
					elseif( ($frn_company_ip!=="" || is_user_logged_in()) && $test_mode=="yes") $launch_hotjar="yes"; //keep off unless it's an admin logged in
				}

				if($launch_hotjar=="yes") {
					$hotjar_hdr = "\n	<script type='text/javascript'>frn_hj_act='Y'; //Hotjar trigger</script>";
				}
				elseif(is_user_logged_in()) {
					$hotjar_hdr="\n	<!-- Hotjar not activated (due to missing ID or for the same reasons as Chartbeat; See footer code for details; this only shows for admins)	-->";
				}
			}
			

			///
			// PRINT ALL TO PAGE
			///

			$header_analytics = $chartbeat.$hotjar_hdr;

			$label_c=""; $label_h=""; $label_o="";
			if(trim($header_analytics)!=="") {
				if(trim($chartbeat)!=="") 	$label_c="Chartbeat-Part 1,";
				if(trim($hotjar_hdr)!=="")	$label_h=" Hotjar-Part 1,";
				echo "\n	<!-- FRN ".$label_c."".$label_h."".$label_o.$test_notice." -->";
				echo $header_analytics."\n\n\n";
			}
		} //Ends requirement that it only runs in the frontend of the site
	}
	}








	//////////
	/// FOOTER_COMPONENTS
	if(!is_admin()) {add_action('wp_footer', 'frn_data_tools_ftr', 2);}
	if(!function_exists('frn_data_tools_ftr')) {
	function frn_data_tools_ftr() {
		//makes sure function doesn't execute for the admin area
		if(!is_admin()) {

			//check test_mode
			$options_hdr= get_option('site_head_code');
			$test_mode=""; $test_mode_mssg="";
			if(!isset($options_hdr['test_mode'])) $options_hdr['test_mode']="";
			if($options_hdr['test_mode']!=="") {
				$test_mode="yes";
			}
			if($test_mode=="yes") $test_mode_mssg = "(Test Mode Activated) ";

			$frn_company_ip=frn_company_ips();
			
			$options_data_tools = get_option('site_footer_code');
			

			/////////////////
			/// CHARTBEAT ///
			/////////////////
			$frn_chartbeat=''; $launch_all="no"; $launch_chartbeat="no"; $ftr_chrbt_checkbox="";

			//basic activation (blank IP means not an employee surfing)
			if(!is_user_logged_in() && $frn_company_ip=="") $launch_all="yes";

			//Test mode override automatic settings
			if($test_mode=="yes") $launch_all="yes";
			
			//chartbeat main activation setting
			if(isset($options_data_tools['ftr_chrbt_checkbox'])) $ftr_chrbt_checkbox=$options_data_tools['ftr_chrbt_checkbox'];
			if($launch_all=="yes" && $ftr_chrbt_checkbox!=="Deactivate") $launch_chartbeat="yes";
			
			if($launch_chartbeat=="yes")
			{
				if($ftr_chrbt_checkbox=="Activate" or $ftr_chrbt_checkbox=="Test") { //if chartbeat is activated
					
					//defaults
					$category="Domain";
					$homepage = home_url();

					$frn_domains = array(	
						"TalbottCampus.com",
						"BlackBearRehab.com",
						"SkywoodRecovery.com",
						"TheCanyonMalibu.com",
						"TheOaksTreatment.com",
						"MichaelsHouse.com",
						"FoundationsRecoveryNetwork.com",
						"DualDiagnosis.org",
						"RehabAndTreatment.com",
						"SoberHero.com",
						"FoundationsEvents.com",
						"IIR.FoundationsEvents.com",
						"MOC.FoundationsEvents.com",
						"HeroesInRecovery.com",
						"LCAccepted.com"
					);

					foreach($frn_domains as $domain_test) {
						//find, replace
						if(stripos($homepage,$domain_test)>0) {
							$domain=$domain_test;
						}
					}

					if($domain=="") $domain = $homepage;

					/* //using list of domains above for word capitalization instead 6/1/18
						$pagehttp = 'http';
						if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") {$pagehttp .= "s";}}
						$pagehttp .= "://";
						$domain = ucfirst(str_replace($pagehttp,"",str_replace("http://","",str_replace("https://","",str_replace("www.","",home_url())))));
						$domain = str_replace(".Com",".com",str_replace(".Net",".net",str_replace(".Org",".org",$domain))); //fixes capitalization after period
					*/


					//Settings prep
					if(!isset($post)) $post="";
					if(!isset($path)) $path="";
					if($post!="") $path = get_permalink( $post->ID ); //not effective for listing pages
					if($path=="") $path = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]".explode('?', $_SERVER['REQUEST_URI'], 2)[0];

					if(isset($options_data_tools['ftr_chrbt_category'])) {
						if($options_data_tools['ftr_chrbt_category']!=="") {
							if($options_data_tools['ftr_chrbt_category']=="Domain") {
								$category=$domain;
								$sections = "";
							}
							elseif($options_data_tools['ftr_chrbt_category']!=="none") {
								$category=$options_data_tools['ftr_chrbt_category'];
								$sections = '
			_sf_async_config.sections = "'.$domain.'";"';
							}
							else $category="";
						}
					}
					
					
					//goes in footer
					$frn_company = $options_data_tools['ftr_chrbt_company'];
					if($frn_company=="UHS") $frn_company = "uhssites.com";
					else  $frn_company = "allwebsites.com";
					
					$frn_chartbeat =  "
			
			
		<!-- #####
		FRN Chartbeat Code ".$test_mode_mssg."
		##### -->";
					
					if($ftr_chrbt_checkbox=="Test") { 
						$frn_chartbeat .= '
			
		<!--Chartbeat: Testing Mode Engaged, all messages below confirm typical situations (Your IP address: '.$_SERVER['REMOTE_ADDR'].'; Company IP='.$frn_company_ip.'; launch='.$launch_chartbeat.' )-->
		';
				//if(is_admin()) echo " <!--It's admin -->"; else echo " <!--Not an admin -->";
						if(is_user_logged_in() and $frn_company_ip!=="") $frn_chartbeat .= "
		<!--Chartbeat: As a tester, you are logged in and on the company network and therefore not being tracked in Chartbeat. Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
		";
						else if(is_user_logged_in()) $frn_chartbeat .= "
		<!--Chartbeat: As a tester, you are logged in and therefore not being tracked in Chartbeat-->\n
		";
						else if($frn_company_ip!=="") $frn_chartbeat .= "
		<!--Chartbeat: As a tester, you are accessing the site from a company location and therefore not being tracked in Chartbeat: Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
		";
						else $frn_chartbeat .= "
		<!--Chartbeat: Not sure why you are seeing this.-->
		";
					}


			///NOTES:
					//path below is used to help with campaign situations. It'll loop campaigns in with normal visits to improve the exposure of the page in Chartbeat. 
					//this applies to Outbrain, facebook, and other campaign
			
			//_sf_async_config.title = "Story Title";
					$frn_chartbeat .= "
		<script type='text/javascript'>
			var _sf_async_config={};
			/** CONFIGURATION START **/
			_sf_async_config.uid = 37107;
			_sf_async_config.domain = '".$frn_company."';".$sections."
			_sf_async_config.path = '".$path."';
			_sf_async_config.authors = '".$category."';
			/** CONFIGURATION END **/
			(function(){
			  function loadChartbeat() {
				window._sf_endpt=(new Date()).getTime();
				var e = document.createElement('script');
				e.setAttribute('language', 'javascript');
				e.setAttribute('type', 'text/javascript');
				e.setAttribute('src',
				   (('https:' == document.location.protocol) ? 'https://a248.e.akamai.net/chartbeat.download.akamai.com/102508/' : 'http://static.chartbeat.com/') +
				   'js/chartbeat.js');
				document.body.appendChild(e);
			  }
			  var oldonload = window.onload;
			  window.onload = (typeof window.onload != 'function') ?
				 loadChartbeat : function() { oldonload(); loadChartbeat(); };
			})();
		</script>
		<!-- #####
		End FRN Chartbeat Code
		##### -->
		
		";
				
				}
				
			}
			else {
			
				if($ftr_chrbt_checkbox=="Deactivate") $frn_chartbeat .=  "
		<!--Chartbeat: Chartbeat is deactivated-->\n
		";
				else if(is_user_logged_in() and $frn_company_ip!=="") $frn_chartbeat .=   "
		<!--Chartbeat: You are logged in and on the company network and therefore not being tracked in Chartbeat. Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
		";
				else if(is_user_logged_in()) $frn_chartbeat .=   "
		<!--Chartbeat: You are logged in and therefore not being tracked in Chartbeat-->\n
		";
				else if($frn_company_ip!=="") $frn_chartbeat .=   "
		<!--Chartbeat: You are accessing the site from a company location and therefore not being tracked in Chartbeat: Your IP address: ".$_SERVER['REMOTE_ADDR']."-->\n
		";
				else $frn_chartbeat .=   "
		<!--Chartbeat: You are looking at the admin area-->
		";
				
			}



			/////////////////
			/// HOTJAR ///
			/////////////////
			
			$frn_hotjar=""; $ftr_hotjar_id=""; $launch_hotjar="no"; 
			
			if(!isset($options_data_tools['ftr_hotjar_radio'])) $options_data_tools['ftr_hotjar_radio']="";
			if($options_data_tools['ftr_hotjar_radio']!=="") {
				$ftr_hotjar_id = trim($options_data_tools['ftr_hotjar_id']);
			}
			
			if(
				$launch_all=="yes" && 
				$options_data_tools['ftr_hotjar_radio']=="Activate" &&
				$ftr_hotjar_id!==""
			) 
				$launch_hotjar="yes";
				
				
			if($launch_hotjar=="yes") {
				$frn_hotjar = "
	<!-- #####
	FRN Hotjar Tracking ".$test_mode_mssg."
	##### -->";
				$frn_hotjar .= '
	<script>
		(function(f,b){
			var c;
			f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
			f._hjSettings={hjid:'.$ftr_hotjar_id.', hjsv:3};
			c=b.createElement("script");c.async=1;
			c.src="//static.hotjar.com/c/hotjar-'.$ftr_hotjar_id.'.js?sv=3";
			b.getElementsByTagName("head")[0].appendChild(c); 
		})(window,document);
	</script>';
				$frn_hotjar .= "
	<!-- #####
	FRN Hotjar Tracking Ends
	##### -->
	
		";
			}


			elseif(is_user_logged_in()) {
				//means this is only shown for admins
				
				$frn_hotjar =  "
		<!--
			FRN Hotjar is deactivated.
			The following cases are true: \n";
				
				if($options_data_tools['ftr_hotjar_radio']=="Deactivate" && is_user_logged_in()) $frn_hotjar .=  "			Hotjar is manually deactivated in settings. \n"; //added logged in state so that this message only applies for admins
				if($options_data_tools['ftr_hotjar_radio']=="" && is_user_logged_in()) $frn_hotjar .=  "			Hotjar settings have never been saved. \n"; //added logged in state so that this message only applies for admins
				if($ftr_hotjar_id==="" && is_user_logged_in()) $frn_hotjar .=  "			The ID field is empty in settings. \n"; //added logged in state so that this message only applies for admins
				if($frn_company_ip!=="") $frn_hotjar .=   "			You are on a company network. \n";
				if(is_user_logged_in()) $frn_hotjar .=   "			You are logged in. \n";
				if(is_admin()) $frn_hotjar .=   "			You are looking at the admin area. \n";
				
				$frn_hotjar .=  "	--> \n\n";
			
			}


			/*
			//Fully discontinued 1/28/19
			/////////////////
			/// CONTENTLY ///
			/////////////////
			
			$frn_contently = ""; $ftr_contently_id=""; $launch_contently="";

			if(!isset($options_data_tools['ftr_contently_radio'])) {
				$options_data_tools['ftr_contently_radio']="";
			}

			if(!isset($options_data_tools['ftr_contently_id'])) {
				$options_data_tools['ftr_contently_id']="";
			}
			if($options_data_tools['ftr_contently_id']!=="") {
				$ftr_contently_id = $options_data_tools['ftr_contently_id'];
			}

			if(
				$launch_all=="yes" && 
				$options_data_tools['ftr_contently_radio']=="Activate" &&
				$ftr_contently_id!==""
			) 
				$launch_contently="yes";


			if($launch_contently=="yes") { 
					
				$frn_contently = "
		<!-- #####
		FRN Contently Tracking ".$test_mode_mssg."
		##### -->";
				
				$frn_contently .= "
			<script type=\"text/javascript\">
				(function(){
				  function initInsights() {
				    if (!window._contently) {
				      var _contently = { siteId: \"".$ftr_contently_id."\" }
				      _contently.insights = new ContentlyInsights({site_id: _contently.siteId});
				      window._contently = _contently
				    }
				  }

				  var s = document.createElement('script');
				  s.setAttribute('type', 'text/javascript');
				  s.setAttribute('src', '//assets.contently.com/insights/insights.js')
				  s.onreadystatechange = function() {
				    if (this.readyState == 'complete' || this.readyState == 'loaded') {
				      initInsights();
				    }
				  };
				  s.onload = initInsights;
				  document.body.appendChild(s);
				})();
			</script>";
				
				$frn_contently .= "
		<!-- #####
		FRN Contently Tracking Ends
		##### -->
		
		";
			}
			
			elseif(is_user_logged_in()) {
				//Means the following only shows for admins
				$frn_contently =  "
		<!--
			FRN Contently is deactivated.
			The following cases are true: \n";
				
					if($options_data_tools['ftr_hotjar_radio']=="Deactivate" && is_user_logged_in()) $frn_contently .=  "			Contently is manually deactivated in settings. \n"; //added logged in state so that this message only applies for admins
					if($options_data_tools['ftr_hotjar_radio']=="" && is_user_logged_in()) $frn_contently .=  "			Contently settings have never been saved. \n"; //added logged in state so that this message only applies for admins
					if($ftr_hotjar_id==="" && is_user_logged_in()) $frn_contently .=  "			The ID field is empty in settings. \n"; //added logged in state so that this message only applies for admins
					if($frn_company_ip!=="") $frn_contently .=   "			You are on a company network. \n";
					if(is_user_logged_in()) $frn_contently .=   "			You are logged in. \n";
					if(is_admin()) $frn_contently .=   "			You are looking at the admin area. \n";
				$frn_contently .=  "	--> \n\n";
			}
		*/

			
			echo $frn_chartbeat.$frn_hotjar;  //.$frn_contently
		} //Ends restriction that this only executes on the frontend
	}
	}




	////////////////////////////////
	/// VISUAL WEBSITE OPTIMIZER ///
	////////////////////////////////
	add_action('wp_head', 'frn_analytics_vwo', 1);
	function frn_analytics_vwo() {

		$options_hdr = get_option('site_head_code');
		$launch_vwo="no";	
		if(!isset($options_hdr['vwo'])) $options_hdr['vwo']="";
		if($options_hdr['vwo']=="A") {
			if(!isset($options_hdr['vwo_id'])) $options_hdr['vwo_id']="";
			if($options_hdr['vwo_id']=="") {
				$options_hdr['vwo_id']="341821";
			}
			//test mode only overrides:
				// if it's an employee visiting the site
				// if an admin is logged in
				// test mode won't affect normal viewers or settings
			$test_mode=""; $test_mode_mssg="";
			if(!isset($options_hdr['test_mode'])) $options_hdr['test_mode']="";
			if($options_hdr['test_mode']!=="") $test_mode="yes";

			$frn_company_ip=frn_company_ips(); //already utilizes the global variable, no need to prep for it
			if($frn_company_ip=="" && !is_user_logged_in() && $options_hdr['vwo_id']!=="") $launch_vwo="yes"; //no employee, not admin, ID provided
			if($test_mode=="yes") {
				$launch_vwo	="yes"; //keep off unless it's an admin logged in
				$test_mode_mssg 	=" (Test Mode Activated) ";
			}

			if($launch_vwo=="yes") { ?>

	<!-- #####
	Visual Website Optimizer
	<?php
				if(is_user_logged_in()) {
					if($test_mode=="yes") echo "	- Secondary analytics tools are in testmode (overriding everything else).\n	";
					echo "	- VWO is activated.\n	";
					if($options_hdr['vwo_id']!=="") echo "	- Account ID: ".$options_hdr['vwo_id']."\n	";
					else echo "	- Account ID is not provided"."\n	";
					echo "	- Normally, this would be deactivated because you are logged in.\n	";
					if($frn_company_ip!=="") echo "	- Your IP: ".$frn_company_ip." (VWO would normally be deactivated)\n	";
					else echo "	- You are not visiting the within the company network.\n	";
				}


	?>
##### -->

		<script type='text/javascript'>
		var _vis_opt_account_id = <?=$options_hdr['vwo_id'];?>;
		var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
		document.write('<s' + 'cript src="' + _vis_opt_protocol +
		'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
		+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
		</script>

		<script type='text/javascript'>
		if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol +
		'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
		/* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
		</script>

		<script type='text/javascript'>
		if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
		        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
		}
		</script>
	
	<!-- #####
	End Visual Website Optimizer Synchronous Code
	##### -->

				<?php 
				//echo "	<script src=\"https://cdn.optimizely.com/js/1033590360.js\"></script>\n\n";
			}
			elseif(is_user_logged_in()) {
				echo "\n	<!--FRN Visual Website Optimizer:".$options_hdr['test_mode'];
				if(is_user_logged_in()) {
					echo "\n		- VWO has been deactivated.";
					echo "\n			- Because you are logged in.";
					echo "\n			- Because the system is not in secondary test mode.\n	";
					if($frn_company_ip!=="") echo "		- Because your IP is ".$frn_company_ip."\n	";
					else echo "	- You are not visiting the within the company network (normally VWO would be activated).\n	";
				}
				echo "-->\n\n";
			}
		}
		elseif(is_user_logged_in()) {
			echo "\n	<!--VWO: deactivated manually-->\n\n";
		}
	}




	//////////////////////////////////
	// Yahoo, Bing, Facebook Reporting
	//////////////////////////////////
	if(!is_admin()) {add_action('wp_head', 'frn_analytics_ppc', 103);}
	if(!function_exists('frn_analytics_ppc')) {
	function frn_analytics_ppc() {
		$options_ppc = get_option('site_head_code');
		$frn_company_ip=frn_company_ips();

		//check test_mode
		$test_mode="";
		if(!isset($options_ppc['test_mode'])) $options_ppc['test_mode']="";
		if($options_ppc['test_mode']!=="") {
			$test_mode="yes";
		}

		if($test_mode=="yes" || (!is_user_logged_in() && $frn_company_ip=="")) {



			//////
			// YAHOO
			if(isset($options_ppc['yahoo'])) {
				if($options_ppc['yahoo']=="A") {
					if(!isset($options_ppc['yahoo_id'])) $options_ppc['yahoo_id']=""; //Old ID: 10027931
		?>

	<!-- #####
	Yahoo Tracking <?=($test_mode=="yes") ? "(Test Mode Activated) " : null ;?>
	##### -->
	<script type="application/javascript">yahoo_id='<?=$options_ppc['yahoo_id'];?>';yahoo_pixel='<?=$options_ppc['yahoo_pixel'];?>';</script>
	<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'<?=$options_ppc['yahoo_id'];?>','properties':{'pixelId':'<?=$options_ppc['yahoo_pixel'];?>'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
	<!-- #####
	End Yahoo Tracking
	##### -->

	<?php
				}
			}



			//////
			// BING
			if(isset($options_ppc['bing'])) {
				if($options_ppc['bing']=="A") {
					if(!isset($options_ppc['bing_id'])) $options_ppc['bing_id']=""; //Old ID: 5625888
		?>
	
	<!-- #####
	Bing Tracking <?=($test_mode=="yes") ? "(Test Mode Activated) " : null ;?>
	##### -->
	<script type="application/javascript">bing_id='<?=$options_ppc['bing_id'];?>';</script>
	<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"<?=$options_ppc['bing_id'];?>"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=<?=$options_ppc['bing_id'];?>=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
	<!-- #####
	End Bing Tracking
	##### -->

	<?php
				}
			}

		}  //ends check if logged in, company ip, or test mode
		elseif(is_user_logged_in()) { 
		?>

	<!--
		Bing and Yahoo tracking are disabled. 
		One of the following cases are true:
			You are logged in
			You are accessing the site from a UHS network (UHS IP If Applies: <?=$frn_company_ip;?>)
			Test mode is NOT activated
	-->
			
	<?php
		} //ends displaying comment if auto deactivated




		//////
		// FACEBOOK ANALYTICS

		//declare variables
		if(!isset($options_ppc['fb_init'])) $options_ppc['fb_init']="";
		if(!isset($options_ppc['fb_empl'])) $options_ppc['fb_empl']="";
		
		//check if activated
		if(isset($options_ppc['fb'])) {
			if($test_mode=="yes" || ($options_ppc['fb_empl']=="" && !is_user_logged_in() && $frn_company_ip=="")) {
			if($options_ppc['fb']=="A" && $options_ppc['fb_init']!=="") {
		
				//deploy the script
		?>
		
	<!-- #####
	Facebook Pixel <?=($test_mode=="yes") ? "(Test Mode Activated) " : null ;?>
	##### -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '<?=$options_ppc['fb_init'];?>');
		fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?=$options_ppc['fb_init'];?>&ev=PageView&noscript=1" /></noscript>
	<?php
				/////
				//FB CONVERSION TRACKING
				//set up conversion defaults
				if(!isset($options_ppc['fb_pages'])) $options_ppc['fb_pages']="";
				if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="Lead";
				if($options_ppc['fb_conv_label']=="") $options_ppc['fb_conv_label']="Lead";
				if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']="";
				if($options_ppc['fb_pages']!=="" && ($options_ppc['fb_conv_type']=="pages" || $options_ppc['fb_conv_type']=="both")) {
					$fb_pages=explode(",",$options_ppc['fb_pages']);
					//echo "; Post ID: ".get_the_id();
				foreach($fb_pages as $page_id) {
					if($page_id==get_the_id()) {
						?>
						fbq('track', '<?=$options_ppc['fb_conv_label'];?>', {
						    content_name: 'Page: <?=get_the_title();?>',
						    content_category: 'Regular Webpage',
						    content_type: 'Phone Clicks'
						}); <?php
					} //end if
				} //end foreach
				} //end conv type options
		?><!-- #####
	END Facebook Pixel
	##### -->


		<?php

		/* 
		//removed 6/23/17 since at this point, we don't have key pages defined upfront. May simply add this per site as necessary.
		<script>// ViewContent 
		// Track key page views (ex: product page, landing page or article)
		fbq('track', 'ViewContent'); 
		</script> 
		*/
			} //end fb activation
			} //end default exclusions (company employees, etc.)
			elseif(is_user_logged_in()) { ?>

		<!-- For Admins Only:
			Facebook tracking is disabled. 
			One of the following cases are true:
				You are logged in
				You are accessing the site from a UHS network (UHS IP If Applies: <?=$frn_company_ip;?>) AND
					The setting for tracking employees is NOT checked
				Test mode is NOT activated
		-->
			
			<?php }
		} // end fb isset


		
	} //ends function
	} //ends check for duplicate function






	/*
	//////////////////////////////////
	// FRN_PHONE_OUTBRAIN Reporting
	//////////////////////////////////
	//removed 9/2017 since Contently contract discontinued by August 2017
	//This is triggered in both the phone and live chat part php files
	//removed 6/16/17 since it's written only when users click on a conversion element.
	//script below is in the scripts.js file
	
	add_action('wp_head', 'frn_outbrain_head', 102);
	if(!function_exists('frn_outbrain_head')) {
	function frn_outbrain_head() { ?>

		<!-- #####
		Outbrain Conversion Reporting
		##### -->
			<script language='JavaScript'>
	                var OB_ADV_ID= 30792;
	                var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
	                var str = '<script src="'+scheme+'widgets.outbrain.com/obtp.js" type="text/javascript"><\/script>';
	                document.write(str);
			</script>
		<!-- #####
		Outbrain Conversion Reporting
		##### -->
		
		<?php
	}
	}
	*/

//Although it's not recommended by WP, keep this closing tag as long as this file ends with commented out code.
//Removing it will disable the sitemaps through Yoast. It adds a tabbed space at the top of the file, making it unreadable to crawlers and such.
//I.E. don't end PHP files with comments.
?>