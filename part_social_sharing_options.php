<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );


/* NOTES:
	"part" php files are in the works in an attempt to make it easier to add to any theme without requiring the massive main plugin PHP file.
	Unlike most "parts", nothing else depends on these features
	This file references other elements like analytics, phone numbers, etc.
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17


	//version 2.2 - 10/23/17 
	//version 2.3 - 10/23/17 - fixes for Twitter URL issues and added bookends feature to apply to text instead of just images.

*/





//////////////////////////////
// SOCIAL_IMAGE OPENGRAPH   //
//////////////////////////////

if(!function_exists('frn_opengraph_images')) {
function frn_opengraph_images () {
    $options_social=get_option('frn_social_options');
    if(!isset($options_social['opengraph_trigger'])) $options_social['opengraph_trigger']="";
    $metas="";
    if($options_social['opengraph_trigger']=="Activate") {
	    
	    /*

		/*this function allows users to use the first image in their post as their thumbnail
		if ( ! function_exists( 'frn_first_image' ) ){
			function frn_first_image() {
				global $post;
				$img = '';
				$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
				if ( isset($matches[1][0]) ) $img = $matches[1][0];

				return trim($img);
			}
		}


	    //Removed: 5/9/17 - until we can find dynamic way to remove old attached images
	    //For older pages, if we remove images and replace with new ones, the old ones will still be in the list of attachments even though we don't think they should be. That means they'll still be pulled into opengraph list of images and certainly won't make sense.
	    //this also applies to cases when we duplicate pages and import pages
	    if(is_single() || is_page()) {
	        $images = get_attached_media( 'image' );
	        
	        if(is_array($images)) {
	            foreach($images as $image) {
	                if($image->guid!=="") $metas .= '     <meta property="og:image" content="'.$image->guid.'" />'."\n";
	            }
	        }
	    }
	    else (category went here)
	    else {
	        $images = get_attached_media( 'image' );
	        if(is_array($images)) {
	            foreach($images as $image) {
	                if($image->guid!=="") $metas .= '     <meta property="og:image" content="'.$image->guid.'" />'."\n";
	            }
	        }
	    }
		*/

	    if(is_category() || is_tag()) {
	        $category = get_the_category(); 
	        //Gets a list of all posts in the category
	        $args = array( 
	            'post_type' => 'post', 
	            'cat' => $category[0]->cat_ID, 
	        );
	        $all_posts = new WP_Query($args); 
	        if ( $all_posts->have_posts() ) {
	            while ( $all_posts->have_posts() ) {
	                $all_posts->the_post();
	                //try easy method just looking for featured image
	                if(has_post_thumbnail()) {
	                    $metas .= '     <meta property="og:image" content="'.get_the_post_thumbnail_url( null, 'full' ).'" />'."\n";
	                }
	                //If it didn't find one, then pull from the content, which requires a new query
	                else {
	                    $args = array(
	                        'post_type'      => 'attachment',
	                        'post_parent'    => get_the_id(),
	                        'post_mime_type' => 'image',
	                        'post_status'    => null,
	                        'numberposts'    => -1,
	                    );
	                    $attachments = get_posts($args);
	                    if ($attachments) {
	                        foreach ($attachments as $attachment) {
	                            $metas .= '     <meta property="og:image" content="'.wp_get_attachment_url($attachment->ID).'" />'."\n";
	                        }
	                        
	                    }
	                }
	            }
	            
	        } else {
	            // no posts found
	        }
	        wp_reset_postdata();
	    }
	} //end trigger

	//5/9/17 - made this work outside of the main trigger since this can be controlled by the presence of the URL
    //The following applies for all pages on the site.
    //By default, we always add the default image as an option in case people prefer it, but it needs to be last so it's last in the Facebook image options.
    if(isset($options_social['default_img'])) {
    	$options_social['default_img'] = trim($options_social['default_img']); //removes spaces (important for URLs)
    	if($options_social['default_img']!=="") {
    	
	    	//We intend for us to manually enter a relative domain, but in older versions, they include the domain already.
	    	//If http isn't there, then add the home_url to the front of it.
	    	if(strpos($options_social['default_img'],"http://")===FALSE && strpos($options_social['default_img'],"https://")===FALSE) {
	    		$options_social['default_img'] = get_home_url().$options_social['default_img'];
	    	}

        	$metas.='     <meta property="og:image" content="'.$options_social['default_img'].'" />'."\n";
        	if($options_social['default_img_w']!=="") $metas.='     <meta property="og:image:width" content="'.$options_social['default_img_w'].'" />'."\n";
        	if($options_social['default_img_h']!=="") $metas.='     <meta property="og:image:height" content="'.$options_social['default_img_h'].'" />'."\n";
        	$metas.='     <meta name="twitter:image" content="'.$options_social['default_img'].'" />'."\n";
        }
    }

    if($metas!=="") {
        echo " \n \n <!-- FRN Custom Opengraph --> \n ";
        echo $metas;
        echo " <!-- END FRN Custom Opengraph --> \n \n ";
    }
	
}
add_action('wp_head', 'frn_opengraph_images');
}







//////////////////////////////////
// SOCIAL_SHARING SHORTCODE	    //
//////////////////////////////////

add_shortcode( 'twitter', 'frn_twitter_funct' );
if(!function_exists('frn_twitter_funct')) {
function frn_twitter_funct($atts, $content = null){
	
	//defaults
	$atts = shortcode_atts( array(
		'type' => 'twitter',
		'version' => 'button',
		'float' => '',
		'align' => 'right',
		'div_align' => '',
		'pretext' => '', //best used with float=inline
		'size' => 'medium',
		'width' => '',
		'place' => '',
		'order' => '',
		'counter' => 'none',
		'tweet' => '',
		'hashtag' => '',
		'link_text' => 'Tweet This',
		'url' => '',
		'account' => '',
		'related' => '',
		'related_tagline' => '',
		'image' => '',
		'bold' => '',
		'summary' => '',
		'like' => '',
		'youtube_url' => '',
		'plusone' => '',
		'google' => '',
		'facebook' => '',
		'twitter' => '',
		'linkedin' => '',
		'email' => '',
		'pinterest' => '',
		'style' => '',
		'css' => ''
	), $atts, 'frn_twitter' );

	return frn_social_funct($atts, $content);
}
}

add_shortcode( 'frn_social', 'frn_social_shortcode' );
if(!function_exists('frn_social_shortcode')) {
function frn_social_shortcode($atts, $content = null){

	//defaults
	$atts = shortcode_atts( array(
		'type' => '',
		'version' => 'icon',
		'float' => '',
		'align' => '',
		'div_align' => '',
		'pretext' => '', //best used with float=inline
		'size' => 'medium',
		'width' => '',
		'place' => '',
		'order' => '',
		'counter' => 'none',
		'tweet' => '',
		'hashtag' => '',
		'link_text' => 'Tweet It',
		'url' => '',
		'account' => '',
		'related' => '',
		'related_tagline' => '',
		'image' => '',
		'bold' => '',
		'summary' => '',
		'like' => '',
		'youtube_url' => '',
		'plusone' => '',
		'google' => '',
		'facebook' => '',
		'twitter' => '',
		'linkedin' => '',
		'email' => '',
		'pinterest' => '',
		'style' => '',
		'css' => ''
	), $atts, 'frn_social' );

	return frn_social_funct($atts, $content);
}
}

//shortcode requires "return" not "echo"
function frn_social_funct($atts, $content = null){
	$options_shortcodes=get_option('frn_social_options');
	$frn_social="";
	
	/*
	Social networks included:
	1. Twitter
	2. Pinterest
	3. Facebook
	4. ShareThis
	5. LinkedIn
	6. Google+
	7. YouTube (icon only)
	8. Email (icon only)
	*/
	if(isset($options_shortcodes['frn_social'])) {
	if($options_shortcodes['frn_social']=="A") {

		extract( $atts ); 
		
		//uses the key in the shortcode attributes array to make each one a variable
		//this is a security risk if we didn't control our own content
		//we should consider modifying it an utilizing a typical array reference ($key[$value])
		
		//clean up key vars
		$float=trim($float);
		$align=trim($align);
		$type=trim($type);
		$pretext=trim($pretext);
		$content=trim($content);

		if($float=="" && $align!=="") $float=strtolower($align);
		elseif($float!=="") $float=strtolower($float);

		if($float=="") $float="center"; //the most common situation as of 10/16/17 -- needs full testing, but makes it easier for people to just use the plain shortcode [frn_social]

		//Adds inline feature to wrapping DIV while maintaining the individual social icon floating into one line. (added 10/16/17)
		//when we want to display something inline:
			//the wrapping DIV needs to be inline
			//each icon needs to be inline
		$display=""; $display_icons="";
		$display_icons_style="";
		if($float=="inline") {
			$display="inline";
			$float="none"; //keeps typical left/right options from occuring
		}
		if($div_align=="inline") {
			$display="inline";
			$float="none"; //keeps typical left/right options from occuring
			$div_align=""; //keeps things clean so we don't have to rewrite prior code
		}

		if($display=="inline") {
			$display_icons=" display:".$display.'-block;'; //used in cases below when float is provided
			$display_icons_style=' style=" display:'.$display.'-block;"'; //used in cases below when float not provided
		}

		if($style=="" && $css!=="") $style=$css;
		
		$counter=strtolower(trim($counter));
		$image=trim($image);
		$type=strtolower(trim($type));
		$version=strtolower(trim($version));
		//if($version=="") $version="icon";
		$url=trim($url);
		$tweet=trim($tweet);
		$order=trim($order);
		
		//order letters for each network
		$o_t="t";
		$o_f="f";
		$o_p="p";
		$o_l="l";
		$o_g="g";
		$o_y="y";
		$o_e="e";
		$o_s="s";
		
		//Note: Icon versions typically cannot share a specific image on the page. Many can't be customized at all. But some like Google+ will look at the block the share button is in and choose the image within that DIV block.
		//Note: Since all buttons use off-site links, they will be tracked as events in Analytics.



		//////////
		//SHORTCODE BOOKENDS OPTION (Part 1)
		
		//Part 2 is just before returning results below
		//if using shortcode bookends approach, transfer the image alignment to the DIV wrap
		//At this position since we need to appropriately apply a margin to the sharing DIV if an image is within the bookends since the alignment options already do
		//this process won't work when captions are used since WP removes the alignment from the IMG code and uses it as a CAPTION shortcode attribute instead
		//Adapted from WP's caption shortcode 8/24/17
		$img_content=""; $div_margin=""; //$inner_content=""; 
		//Look for image that is wrapped with a shortcode (pattern adapted from WP's caption code)
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			//echo "\n\n<!-- CONTENT VAR \n\n";
			//echo $content;
			//echo "\n\n<!-- MATCHES VAR \n\n";
			//print_r($matches);
			//echo "\n\n<!-- END BOTH VARS -->\n\n";
			//$inner_content = $content; //returned by shortcode
			//$img_full = trim($matches[0]); //sometimes grabs closing DIVs -- don't use (FYI--index 0 includes everything after the first bookend and includes the last bookend)
			$img_content = trim($matches[1]); //grabs only the image code between bookends, no text 
		}
		//if div_align provided as shortcode attribute
		if($div_align!=="") {
			//wp-caption has WP predefined spacing when using image alignment classes
			if($div_align=="left") {
				$div_align="alignleft";
				$div_margin= ' margin-right: 22px;'; //WP default for image
			}
			elseif($div_align=="right") {
				$div_align="alignright";
				$div_margin= ' margin-left: 22px;'; //WP default for image
			}
		}
		if($img_content!=="") {
			if($div_align=="") {
				//Grab from image CSS align setting
				//echo "<h1>Image Code: ".$matches[0]."</h1>";
				if(stripos($img_content,"alignright")) {
					$div_align="alignright";
					$div_margin= ' margin-left: 22px;'; //WP default for image
				}
				elseif(stripos($img_content,"alignleft")) {
					$div_align="alignleft";
					$div_margin= ' margin-right: 22px;'; //WP default for image
				}
			}
		}
		//sets default alignment for most use cases when we want more than one button to show
		if($float=="" && ($type=="" || $type=="all")) {
			if($img_content!=="") {
				$float="center"; 
			}
			else {
				$float="right";
			}
		}

		//if image URL not provided and using bookends, look for src in code and grab url that way
		$site_url = get_site_url();
		if($image=="") {
			if($img_content!=="") {
				if(preg_match('/ src="(.+(jpg|png|gif|jpeg))" /is',$img_content,$img_src)) {
					$image = $img_src[1];
					if(stripos($image,"http")===false) {
						$image = $site_url.$image;
					}
					$image=trim($image);
				}
			}
		}

		
		if($url!=="") {
			if(is_home() || is_front_page()) $url = get_site_url();
			else $url = get_permalink();
		}
		if($url!=="") $url = urlencode($url);

		$post_title = get_the_title();





		//////////
		// PREPARE BUTTONS
		//////////

		if($type=="sharethis" || $type=="all" || strpos($order,$o_s)===TRUE) {
			//documentation: http://www.sharethis.com/get-sharing-tools/
			$sharethis = "";
			add_action('wp_footer', 'frn_sharethis_js', 3); //Adds the script into the footer in order to run it only once no matter how many buttons are on the page //Adds the buttons.js file to the footer since HEAD already built before shortcodes processed.
			if($version=="buttons") {
				//if(strtolower(trim($plusone))!="no") $sharethis.="<span class='st_plusone_large' displayText='Google +1'></span>";
				if(strtolower(trim($like))!="no") $sharethis.="<span class='st_fblike_large' displayText='Like' style='float:left; margin-top:3px;'></span>";
				if(strtolower(trim($facebook))!="no") $sharethis.="<span class='st_facebook_large' displayText='ShareThis'></span>";
				//if(strtolower(trim($google))!="no") $sharethis.="<span class='st_googleplus_large' displayText='ShareThis'></span>";
				if(strtolower(trim($twitter))!="no") $sharethis.="<span class='st_twitter_large' displayText='ShareThis'></span>";
				if(strtolower(trim($linkedin))!="no") $sharethis.="<span class='st_linkedin_large' displayText='ShareThis'></span>";
				if(strtolower(trim($pinterest))!="no") $sharethis.="<span class='st_pinterest_large' displayText='ShareThis'></span>";
				if(strtolower(trim($email))!="no") $sharethis.="<span class='st_email_large' displayText='ShareThis'></span>";
				if(strtolower(trim($sharethis))!="no") $sharethis.="<span class='st_sharethis_large' displayText='ShareThis'></span>";
			}
			elseif($version=="floating") {
				$sharethis.='
					<script>
						var options={ "publisher": "edec65cb-2ba0-420b-90af-5194e9f67e79", "logo": { "visible": false, "url": "", "img": "", "height": 45}, "ad": { "visible": false, "openDelay": "5", "closeDelay": "0"}, "livestream": { "domain": "", "type": "sharethis"}, "ticker": { "visible": false, "domain": "", "title": "", "type": "sharethis"}, "facebook": { "visible": false, "profile": "sharethis"}, "fblike": { "visible": false, "url": ""}, "twitter": { "visible": false, "user": "sharethis"}, "twfollow": { "visible": false}, "custom": [{ "visible": false, "title": "Custom 1", "url": "", "img": "", "popup": false, "popupCustom": { "width": 300, "height": 250}}, { "visible": false, "title": "Custom 2", "url": "", "img": "", "popup": false, "popupCustom": { "width": 300, "height": 250}}, { "visible": false, "title": "Custom 3", "url": "", "img": "", "popup": false, "popupCustom": { "width": 300, "height": 250}}], "chicklets": { "items": [facebook", "twitter", "linkedin", "pinterest", "email", "sharethis"]}};
						var st_bar_widget = new sharethis.widgets.sharebar(options);
					</script>
				';
				//<script type="text/javascript">var switchTo5x=true;</script>
				//<script type="text/javascript">stLight.options({publisher: "edec65cb-2ba0-420b-90af-5194e9f67e79", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
				add_action('wp_footer', 'frn_sharethis_js', 3); //Adds the script into the footer in order to run it only once no matter how many buttons are on the page
				wp_register_script( 'frn_sharethis_loader', 'https://s.sharethis.com/loader.js', array(), '1.0.0', true ); //will only output to the footer since HEAD already built before shortcodes ran
				wp_enqueue_script( 'frn_sharethis_loader' );
				
			}
			else {
				//Note: you can't use add_action in the wp_head section since shortcode are ran after the HEAD section has already been built.
				add_action('wp_footer', 'frn_sharethis_js', 3); //Adds the script into the footer in order to run it only once no matter how many buttons are on the page
				if($image!=="") $image_st="st_image=\"".$image."\" ";
				if(trim($bold)!=="") $bold="st_title=\"".$bold."\" ";
				if(trim($summary)!=="") $summary_st="st_summary=\"".$summary."\" ";
				if($link_text=="Tweet It") $link_text="Share This ";
				//url to page is automatic
				//once a person clicks/rolls over a sharethis link, that information is cashed for all other sharethis links on a page. The page must be refreshed.
				//For this link to work on the page, the sharethis plugin must be installed: https://wordpress.org/plugins/share-this/
				if($float!="none" && $float!=="") {
					if($float=="left") {$sharethis.="<div class=\"frn_social_s\" style=\"float:".$float."; margin:0 5px 0 0;\">"; $float_end="</div>";}
					elseif($float=="right") {$sharethis.="<div class=\"frn_social_s\" style=\"float:".$float."; margin:0 0 0 5px;\">"; $float_end="</div>";}
					elseif($float=="center") {$sharethis.="<div class=\"frn_social_s\" style=\"margin:0 2px 0 2px; display: inline-block;\">"; $float_end="</div>";}
				}
				$sharethis.='<span class="st_sharethis" style="cursor:pointer;" '.$image_st.$bold.$summary_st.'>'.$link_text.' </span>';
				$sharethis.=$float_end;
				$sharethis='<div class="sharethis-inline-share-buttons"></div>';
				/* $sharethis.='
					<script type="text/javascript">var switchTo5x=true;</script>
					<script type="text/javascript">stLight.options({publisher: "edec65cb-2ba0-420b-90af-5194e9f67e79", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>'; */
			}
		}
		if($type=="pinterest" || $type=="all" || $type=="" || strpos($order,$o_p)===TRUE) {
			//documentation: https://business.pinterest.com/en/widget-builder#do_pin_it_button
			
			//Add the pinterest script into the footer in order to run it only once no matter how many buttons are on the page
			add_action('wp_footer', 'frn_pinterest_js', 3);
			
			$pinterest="";
			if($float=="left" || $display=="inline") $float_start="<div class=\"frn_social_p\" style=\"float:".$float."; margin-right:5px;".$display_icons."\">";
				elseif($float=="right") $float_start="<div class=\"frn_social_p\" style=\"float:".$float."; margin-left:5px;".$display_icons."\">";
				elseif($float=="center") $float_start="<div class=\"frn_social_p\" style=\"margin:0 2px 0 2px; display:inline-block;\">";
				else $float_start='<div class="frn_social_p">';
			$float_end="</div>";
			
			//Build the buttons
			$pinterest.=$float_start;

			$all_or_one = '
					data-pin-do="buttonPin"';

			$image_p=""; $all_or_one="";
			if($image!=="") {
				$image_p = '
					data-pin-media="'.urlencode( $image ).'"';
			}
			else {
				$all_or_one = '
					data-pin-do="buttonBookmark"';
			}

			$url_p=$url;

			if(trim($summary)!=="") {
				$summary=urlencode($summary);
			}
			else {
				if(is_home() || is_front_page()) $summary=urlencode("Drug addiction is a real problem in the lives of many loved ones. This image comes from a site that endeavors to help us learn what our loved ones are going through, how to deal with the challenges they bring to our lives, and what we can do to help.");
				//else $summary=urlencode("Drug addiction is a real problem in the lives of many loved ones. This image is part of the article titled: ".get_the_title().".");
			}
			if($summary!=="") {
				$summary='
					data-pin-description="'.$summary.'"';
			}
				

			//// PINTEREST REGULAR STYLES
			if($version!=="icon" && $version!=='') {
				
				$version_p="";
				if($version=="round") {
					$version_p='
					data-pin-shape="round"';
				}
				elseif($version=="rectangular") {
					$version_p='
					data-pin-shape="rectangular"';
				}

				$size_p='';
				if($size=="large") {
					if($version_p=="round") $size_p='32px';
					else $size_p='28px';
				}
				elseif($size=="medium") {
					if($version_p=="round") $size_p='16px';
					else $size_p='20px';
				}
				elseif($size=="small") {
					$size_p='12px';
				}
				if($size_p!=="") $size_p='
					data-pin-tall="'.$size_p.'"';
				
				$counter=trim($counter);
				if($counter!=="") $counter_p = '
					data-pin-count="'.$counter.'"';

				$pinterest.='
				<a href="https://www.pinterest.com/pin/create/button/"'
					.$all_or_one
					.$image_p
					.$size_p
					.$counter_p
					.'
					data-pin-url="'.$url_p.'"'
					.$summary
					.'>
				</a>
				';
			}

			//// OUR PINTEREST ICON (default)
			else {
				
				if($size=="large") $size_p="";
				elseif($size=="small") $size_p=' height="12px" style="height:12px;"';
				else $size_p=' height="30px" style="height:30px; width:auto;"';
				
				$pinterest.='
				<a href="https://www.pinterest.com/pin/create/button/"
					data-pin-custom="true"'
					.$image_p.'
					data-pin-url="'.$url_p.'"'
					.$summary
					.'>
					<img 
						src="'.plugin_dir_url( __FILE__ ).'images/btn_pinterest.png"'.$size_p.'
						alt="Save on Pinterest" 
						title="Save on Pinterest" 
					/>
				</a>
				';

			}
			$pinterest.=$float_end;

		}

		$twitter_script="";
		if($type=="twitter" || $type=="all" || $type=="" || strpos($order,$o_t)===TRUE) {
			$twitter = ""; 
			//assume it's twitter
			if($tweet=="" && $content!=="") $tweet=strip_tags($content);
			if($tweet=="") {
				//if tweet is blank, use a default message.
				if(is_home() || is_front_page()) $tweet = "Drug addiction is a real problem in the lives of many loved ones. This site helps show what's possible & how to help.";
				else {
					$period=".";
					if(strpos($post_title,"?")===TRUE || strpos($post_title,"!")===TRUE) $period="";
					$tweet = $post_title.$period;
				}
			}
			$url_t=urldecode($url);
			if(trim($related)=="") $related="HeroesNRecovery";
			if(trim($related_tagline)=="") $related_tagline="Challenging the Stigma";
			if($version!=="text" && $version!=="icon" && $version!=="") {
				////
				//Traditional Twitter Button
				//example: <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://thesite.com" data-text="Check this out" data-related="HeroesNRecovery" data-count="none" data-hashtags="addiction">Tweet</a>
				if($url_t!=="") $url_t=" ".urldecode($url)." ";
				$tweet=str_replace('"', "%22", $tweet); //makes sure this doesn't cause client-side JS errors
				$tweet=str_replace("'", "%27", $tweet); //makes sure this doesn't cause client-side JS errors
				if(trim($link_text)=="Tweet It") $link_text=="Tweet"; 
				if($display=="inline") $display_t=' style="'.$display_icons.'"';
				if($float=="left" || $display=="inline") $float_t=" style=\"float:".$float."; margin-right:5px; line-height:70%; ".$display_icons."\"";
					elseif($float=="center") $float_t=" style=\"margin: 0 -2px 0 2px; line-height:70%; display: inline-block;\"";
					elseif($float=="right") $float_t=" style=\"float:".$float."; margin-left:5px; line-height:70%;\"";
					else $float_t="";
				if(trim($related)!=="") $related = trim($related); //.":".trim($related_tagline);
				if(trim($related)!=="") $related=" data-related=\"".$related."\" ";
				//if($url!=="") $url_t=' data-url="'.$url.'"'; //discontinued 1/30/18 due to Twitters updated button parameters
				if(trim($account)!=="") $account=" data-via=\"".$account."\" ";
				if(trim($hashtag)!=="") $hashtag=" data-hashtags=\"".$hashtag."\" ";
					else $hashtag=' data-hashtags="addiction"';
				if($size!="large") $size_t="";
					else $size_t=' data-size="large"';
				if($counter=="") $counter_t="";
					else $counter_t=' data-count="'.$counter.'"';
				$twitter.='<div class="frn_social_t"'.$float_t.$display_icons_style.'><a href="https://twitter.com/share" class="twitter-share-button" data-text="'.$tweet.$url_t.' "'.$hashtag.$account.$related.$size_t.$counter_t.' data-dnt="true">'.$link_text."</a></div>";
				$twitter_script="<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
			}
			else {
				//Text or Icon
				//convert text to URL friendly vars
				$tweet = urlencode ( $tweet );
				if($version=="icon" || $version=="") {
					if($size=="large") $size_t="";
						elseif($size=="small") $size_t=' style="height:12px;"';
						else $size_t=' style="height:30px; width:auto;"';
					$link_text='<img src="'.plugin_dir_url( __FILE__ ).'images/btn_twitter.png"'.$size_t.'  alt="Tweet This" title="Tweet This"/>';
					if($float=="left" || $display=="inline") $twitter.="<div class=\"frn_social_t\" style=\"float:".$float."; margin-right:5px;".$display_icons."\">";
						elseif($float=="center") {$twitter.="<div class=\"frn_social_t\" style=\"margin:0 2px 0 2px; display: inline-block;\">";}
						elseif($float=="right") $twitter.="<div class=\"frn_social_t\" style=\"float:".$float."; margin-left:5px;\">";
				}
				else { 
					$link_text=str_replace("Share","Tweet",$link_text);
					if($float=="left" || $display=="inline") $twitter.="<div class=\"frn_social_t\" style=\"float:".$float."; margin-right:5px; line-height:70%;".$display_icons."\">";
						elseif($float=="center") {$twitter.="<div class=\"frn_social_t\" style=\"margin:0 2px 0 2px; line-height:70%; display: inline-block;\">";}
						elseif($float=="right") $twitter.="<div class=\"frn_social_t\" style=\"float:".$float."; margin-left:5px; line-height:70%;\">";
				}
				if(trim($related)!=="") $related=$related.":".$related_tagline;
				if(trim($related)!=="") $related = "&related=".urlencode($related);
				//uses page's url if not specified.
				if($url_t=="") $url_t = " ".get_permalink()." ";  //updated 1/30/18 due to Twitters updated button parameters
				if(trim($account)!=="") $account="%20%40".$account;
				if(trim($hashtag)!=="") $hashtag="%20%23".$hashtag;
				if($tweet!=="" || $hashtag!=="" || $url_t!=="" || $account!=="" || $related!=="") $tweet_start = "text=";
				
				$twitter.='<a href="https://twitter.com/intent/tweet?'.$tweet_start.$tweet.$hashtag.$account.$related.'" target="_blank">'.$link_text.'</a>';
				$twitter_script="<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
				if($float=="left" || $float=="right" || $float=="center" || $display=="inline") $twitter.="</div>";
			}
		}

		if($type=="facebook" || $type=="all" || $type=="" || strpos($order,$o_f)===TRUE) {
			add_action('wp_footer', 'frn_fbk_popup_js', 3);
			if($size=="large") $size_f="";
				elseif($size=="small") $size_f=' height:12px;';
				else 
					{
						if($version=="icon" || $version=="") $size_f=' height:30px; width:auto;';
						else $size_f=' height:20px;';
					}
				if($size_f!=="") $size_f_img = ' style="'.$size_f.'"'; else $size_f_img="";
			if($version!=="icon") {
				if($float=="left" || $display=="inline") $float_f=" style=\"float:".$float."; margin-right:5px; line-height:70%;".$size_f.$display_icons."\"";
					elseif($float=="center") $float_f=" style=\"margin:0 2px 0 2px; line-height:70%; display: inline-block;".$size_f."\"";
					elseif($float=="right") $float_f=" style=\"float:".$float."; margin-left:5px; line-height:70%;".$size_f."\"";
			}
			if($link_text=="Tweet It" || $link_text=="") $link_text="Share this page on Facebook";
			$url_f = $url;
			if($version=="icon" || $version=="") {
				if($float=="left" || $display=="inline") $float_f=" style=\"float:".$float."; margin-right:5px;".$size_f.$display_icons."\"";
					elseif($float=="center") $float_f=" style=\"margin:0 2px 0 2px; display: inline-block;".$size_f."\"";
					elseif($float=="right") $float_f=" style=\"float:".$float."; margin-left:5px;".$size_f."\"";
				$button_image="btn_facebook_sm.png";
				/*if($size=="large") $size_f="";
					elseif($size=="small") $size_f=' height="12px" style="height:12px;"';
					else $size_f=' height="20px" style="height:20px;"';
				*/
			}
			else {
				$button_image="btn_facebook_share.png";  ///plugins/like.php?href=  //class="popup" - removed from a href since someone used this class for another kind of popup. Not sure why such a generic version was used anyway. Removing it didn't seem to hurt things on FRN.com.
			}
			$facebook = '<div class="frn_social_f"'.$float_f.'><a href="http://www.facebook.com/sharer.php?u='.$url_f.'&width&layout=standard&action=like&show_faces=true&share=true&height=80&appId=721022864576316"><img src="'.plugin_dir_url( __FILE__ ).'images/'.$button_image.'"'.$size_f_img.' alt="Share on Facebook" title="Share on Facebook"></a></div>';
		}

		if(($type=="linkedin" || $type=="all" || $type=="" || strpos($order,$o_l)===TRUE)) {
			//documentation: https://developer.linkedin.com/plugins/share-plugin-generator
			$url_l=$url;
			if($size=="large") $size_l="";
				elseif($size=="small") $size_l=' style="height:12px;"';
				else $size_l=' style="height:30px; width:auto;"';
			if($float=="left" || $display=="inline") $float_l=" style=\"float:".$float."; margin-right:5px;".$display_icons."\"";
				elseif($float=="right") $float_l=" style=\"float:".$float."; margin-left:5px;\"";
				elseif($float=="center") $float_l=" style=\"margin:0 2px 0 2px; display: inline-block;\"";
				else $float_l="";
			if($version=="icon" || $version=="") {
				if($link_text=="Tweet It" || $link_text=="") $link_text="Share this page on LinkedIn";
				//class="popup" - removed from a href since someone used this class for another kind of popup. Not sure why such a generic version was used anyway. Removing it didn't seem to hurt things on FRN.com.
				$linkedin = '<div class="frn_social_l"'.$float_l.'><a href="http://www.linkedin.com/shareArticle?mini=true&url='.$url_l.'"><img src="'.plugin_dir_url( __FILE__ ).'images/btn_linkedin.png"'.$size_l.' alt="Share on LinkedIn" title="Share on LinkedIn"></a></div>';
			}
			else {
				add_action('wp_footer', 'frn_linkedin_js', 3); //Adds the script into the footer in order to run it only once no matter how many buttons are on the page
				//wp_register_script( 'linkedin_script', '//platform.linkedin.com/in.js', array(), false, true);
				//wp_enqueue_script( 'linkedin_script');
				$linkedin_script = '<script type="IN/Share" data-url="'.$url_l.'"></script>';
				$linkedin = '<div class="frn_social_l"'.$float_l.'>'.$linkedin_script.'</div>';
			}
		}

		if($type=="gplus" || $type=="all" || $type=="" || strpos($order,$o_g)===TRUE) {
			//See parameter options on https://developers.google.com/+/web/share/
			$url_g = $url; $gplus = '';
			/*
			//Due to Google's announcement of completely closing their social network, removing sharing option. 2/16/19
			if($version!=="icon" && $version!=="" ) {
				if($float=="left" || $display=="inline") $float_g=" style=\"float:".$float."; margin-right:5px; line-height:70%;".$display_icons."\"";
					elseif($float=="right") $float_g=" style=\"float:".$float."; margin-left:5px; line-height:70%;\"";
					elseif($float=="center") $float_g=" style=\"margin:0 2px 0 2px; line-height:70%; display: inline-block;\"";
			}
			if($version=="button") {
				add_action('wp_footer', 'frn_gplus_js', 3);
				if($url_g!=="") $url_g = ' data-href="'.$url_g.'"';
				if($size=="small") $size_g=' data-height="15"';
				elseif ($size=="large") $size_g=' data-height="24"';
				$gplus = '<div class="frn_social_g"'.$float_g.'><div class="g-plus" data-action="share" data-annotation="none"'.$url_g.'></div></div>';
			}
			elseif($version=="icon" || $version=="") {
				if($size=="large") $size_g="";
					elseif($size=="small") $size_g=' height="12px" style="height:12px;"';
					else $size_g=' height="30px" style="height:30px; width:auto;"';
				if($float=="left" || $display=="inline") $float_g=" style=\"float:".$float."; margin-right:5px;".$display_icons."\"";
					elseif($float=="right") $float_g=" style=\"float:".$float."; margin-left:5px;\"";
					elseif($float=="center") $float_g=" style=\"margin:0 2px 0 2px; display: inline-block;\"";
				$gplus = '<div class="frn_social_g"'.$float_g.'><a href="https://plus.google.com/share?url='.$url_g.'" onClick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><img src="'.plugin_dir_url( __FILE__ ).'images/btn_googleplus.png"'.$size_g.' alt="Share on Google+" title="Share on Google+"></a></div>';
			}
			else {
				//defaults to the small version of the google+ icon, no text.
				//if($size=="large") $size_g="32";
				//else $size_g="16";
				//Google has a 64 size for button, too, but it's not considered here.
				$gplus = <div class=\"frn_social_g\"".$float_g."><a href=\"https://plus.google.com/share?url=".$url_g."\" onClick=\"javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\"><img src=\"https://www.gstatic.com/images/icons/gplus-".$size_g.".png\" style=\"height:".size_g."px;\" alt=\"Share on Google+\" /></a></div>";
			}
			*/
		}

		if(($type=="email" || $type=="all" || strpos($order,$o_e)===TRUE) && ($version=="icon" || $version=="") ) { //limiting processing only for icon since email button not yet designed
			$url_e = $url;
			/* Discontinued. May finish later. Was basically creating our own share by email program with customized email design. Got complicated though. Tabling until ready to pick up again.
			if(is_home() || is_front_page()) $email_title=get_bloginfo( 'name' );
				else $email_title=get_the_title();
			if($summary!=="") $email_desc=$summary;
				elseif(!is_home() && !is_front_page()) {
					$email_desc = apply_filters( 'the_content', get_the_content('READ MORE') );
					$email_desc = str_replace( ']]>', ']]&gt;', $email_desc );
					$email_desc = strip_tags($email_desc);
					$email_desc = substr($email_desc,0,strpos($email_desc," ",300))."...";
					//strip HTML
					//find the next space after the character cut off
					//Add "..." to end
					}
				elseif(is_home() || is_front_page()) {if (class_exists('WPSEO_Frontend')) $email_desc = WPSEO_Frontend::metadesc(false);}
				
			if($image!=="") $email_img=$image;
				elseif(!is_home() && !is_front_page()) {
					// Get URL of first image in a post
					global $post, $posts;
					ob_start();
					ob_end_clean();
					$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
					$email_img = $matches [1] [0];
				}
			*/
			if($float=="left" || $display=="inline") $float_e=" style=\"float:".$float."; margin-right:5px;".$display_icons."\"";
				elseif($float=="right") $float_e=" style=\"float:".$float."; margin-left:5px;\"";
				elseif($float=="center") $float_e=" style=\"margin:0 2px 0 2px;display: inline-block;\"";
			if($size=="large") $size_e="";
				elseif($size=="small") $size_e=' style="height:12px;"';
				else $size_e=' style="height:30px; width:auto;"';
			
			$excerpt = wp_strip_all_tags(strip_shortcodes(get_the_content()));
			$excerpt_length = apply_filters( 'excerpt_length', 55 );
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[…]' );
			$excerpt = wp_trim_words( $excerpt , $excerpt_length, $excerpt_more );
			$excerpt = apply_filters( 'get_the_excerpt', $excerpt );

			$email_subject="I thought this might help";
			$email_body="I found this and thought it might be helpful.\n\n\n\n";
			$email_body.="- - - - - - - - - - - - - - -\n\n";
			$email_body.=strtoupper($post_title)."\n\n";
			$email_body.=$excerpt ."\n\n";
			$email_body.="Continue Reading: ".$url_e."\n\n";
			$email_body.="- - - - - - - - - - - - - - -\n\n";
			$email='<div class="frn_social_e"'.$float_e.'><a href="mailto:?Subject='.$email_subject.'&Body='.$email_body.'"><img src="'.plugin_dir_url( __FILE__ ).'images/btn_email.png" alt="Email This"'.$size_e.' alt="Email This" /></a></div>'; //'.plugin_dir_url( __FILE__ ).'frn_share_email.php?post_id='.$post->ID.'
		}

		if(($type=="youtube" || strpos($order,$o_y)===TRUE) && trim($youtube_url)!=="") {
			//since you can't share a specific item, this should only display when specifically wanted or if "y" is in the order and a URL is provided
			if($size=="large") $size_y="";
				elseif($size=="small") $size_y=' style="height:12px;"';
				else $size_y=' style="height:30px; width:auto;"';
			if($float=="left" || $display=="inline") $float_y=" style=\"float:".$float."; margin-right:5px;".$display_icons."\"";
				elseif($float=="right") $float_y=" style=\"float:".$float."; margin-left:5px;\"";
				elseif($float=="center") $float_y=" style=\"margin:0 2px 0 2px; display: inline-block;\"";
			$youtube = '<div class="frn_social_y"'.$float_y.'><a href="'.$youtube_url.'" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'images/btn_youtube.png"'.$size_y.' alt="View our Video Channel" title="View our Video Channel"></a></div>';
		}
	
		if($order!=="") {
			//ordering examples for the five social sharing networks (tp, pstf, stpf, tl, tgl, gftp, etc.)
			/*
				find position of letter
				cycle through characters and check for each letter
					if position 1 is t, print twitter
					if position 1 is f, print facebook
					etc
					$x = position number being cycled through
			*/
			for ($x=0; $x<=strlen($order); $x++) {
				if(substr($order,$x,1)==$o_p) $frn_social.=$pinterest;
				elseif(substr($order,$x,1)==$o_t) $frn_social.=$twitter;
				elseif(substr($order,$x,1)==$o_f) $frn_social.=$facebook;
				elseif(substr($order,$x,1)==$o_l) $frn_social.=$linkedin;
				//elseif(substr($order,$x,1)==$o_g) $frn_social.=$gplus;
				elseif(substr($order,$x,1)==$o_s) $frn_social.=$sharethis;
				elseif(substr($order,$x,1)==$o_e) $frn_social.=$email;
				elseif(substr($order,$x,1)==$o_y) $frn_social.=$youtube;
			}
		}
		elseif($type!=="" && $type!="all") $frn_social=$$type; //used when specific buttons are called
		elseif($float=="right") {
			if($type!="all") $frn_social.=$linkedin.$twitter.$facebook.$pinterest; 
			else $frn_social.=$email.$linkedin.$gplus.$twitter.$facebook.$pinterest;
		}
		else { //basically anything but right
			if($type!="all") $frn_social.=$pinterest.$facebook.$twitter.$linkedin;  //default since most images too narrow to include all buttons
			else $frn_social.=$pinterest.$gplus.$facebook.$twitter.$linkedin.$email;  //all buttons
		}
		



		
		if($div_align!=="") {
			$div_align="wp-caption ".$div_align;
		}

		if($pretext!=="") $pretext=sanitize_text_field($pretext)." "; //makes sure code can't be used https://developer.wordpress.org/reference/functions/sanitize_text_field/


		//wrap all in DIV for styling ease
		if($float!=="") {
			$float_wrap=""; //If img_content filled out, we are using bookends and the float messes things up. Removing it if bookends are used.
			if($img_content=="") {
				$float_wrap="float:".$float."; "; 
			}
			//Determine if we want the buttons to show on top of images
			//This feature does not work as of 8/24/17
			$place=trim($place);
			if($place!=="") {
				if($place=="bottom") $place_pos="top";
					else $place_pos="bottom";
				if($float=="right") $place=' position: relative; '.$place_pos.': -35px; right: 10px;';
				elseif($float=="left") $place=' position: relative; '.$place_pos.': -35px; left: 10px;';
				elseif($float=="center") $place=' position: relative; '.$place_pos.': -35px;';
			}
			//directions were too complicated. Just removing.
			$width==trim($width);
			//if($width!=="") $width=" max-width:".$width.";";
			if($display=="inline" && $div_align="center") {
				$float="center"; //for text-align
			}
			elseif($display=="inline") {
				$float="left"; //for text-align
			}
			global $frn_mobile;
			if($frn_mobile) $div_margin="";
			if($float=="center") $frn_social = '<div class="frn_social" style="text-align:center; '.$style.$place.$width.$div_margin.' margin-bottom:10px;">'.$pretext.$frn_social."</div>";
			else $frn_social = '<div class="frn_social" style="text-align:'.$float.'; '.$float_wrap.$display_icons.$style.$place.$width.$div_margin.' margin-bottom:10px;">'.$pretext.$frn_social."</div>";
		}
		$frn_social.=$twitter_script;
		//$frn_social.=$linkedin_script;
		



		//////////
		//SHORTCODE BOOKENDS OPTION (Part 2)
		if($img_content!=="") {
			//if img_content is filled out, then we know we're using bookends. As result, using this approach will wrap the image and social sharing options together.
			$frn_social = '<div class="frn_social_wrap '.$div_align.'">'
				. $content //do_shortcode( $content )
				. $pretext
				. $frn_social
				. '</div>';
		}
		elseif($content!=="") {
			//content var should only be filled out when the bookend approach is used
			//when no image in content, no need to wrap it to associate the buttons to it 1/30/18
			$frn_social = $content.$pretext.$frn_social;
		}
		


	}
	} //ends isset() test


	
	return trim($frn_social); //."<!--".$float."-->";  //if frn_social is not "A", then this will just return blank and the shortcodes will be removed
	
}





if(!function_exists('frn_sharethis_js')) {
function frn_sharethis_js() {
	//This approach used instead of wp_enqueue_script in order for the scripts to be asynchronous
	/*
	//Old version that was broken on 2/16/19
	<script type="text/javascript">var switchTo5x=true;</script>
	<script  type="text/javascript" src="https://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "edec65cb-2ba0-420b-90af-5194e9f67e79", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	*/
	//TalbottCampus.com: property ID: 5c68effd7056550011c4a4da
	?>
	<!--FRN ShareThis Buttons JS-->
	<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c68effd7056550011c4a4da&product=social-ab' async='async'></script>
	<?php
	//wp_register_script( 'frn_sharethis', 'http://w.sharethis.com/button/buttons.js', array(), '1.0.0', false ); //last variable being false is supposed to put this in the HEAD section but it isn't. Not sure why.
	//wp_enqueue_script( 'frn_sharethis' );
}
}
if(!function_exists('frn_pinterest_js')) {
function frn_pinterest_js() {
	//This approach used instead of wp_enqueue_script in order for the scripts to be asynchronous
	echo '
	<!--FRN Pinterest Buttons JS-->
	<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
	';
	//wp_register_script( 'frn_pinterest', '//assets.pinterest.com/js/pinit.js', false, '1.0.0', true ); //will only be added to wp_footer since shortcode are processed after HEAD section built
	//wp_enqueue_script( 'frn_pinterest' );
}
}
if(!function_exists('frn_fbk_popup_js')) {
function frn_fbk_popup_js() {
	echo "
	<!--FRN Facebook Popup JS-->
	<script type=\"text/javascript\">
		jQuery(document).ready(function($) {
		   $('.popup').click(function() {
			 var NWin = window.open($(this).prop('href'), '', 'scrollbars=1,height=400,width=400');
			 if (window.focus)
			 {
			   NWin.focus();
			 }
			 return false;
			});
		});
	</script>
	
	";
}
}
function frn_linkedin_js() {
	//This approach used instead of wp_enqueue_script in order for the scripts to be asynchronous
	echo '
	<!--FRN LinkedIn Buttons JS-->
	<script src="//platform.linkedin.com/in.js" async=\'async\' type="text/javascript">
	  lang: en_US
	</script>
	
	';
	//wp_register_script( 'frn_pinterest', '//assets.pinterest.com/js/pinit.js', false, '1.0.0', true ); //will only be added to wp_footer since shortcode are processed after HEAD section built
	//wp_enqueue_script( 'frn_pinterest' );
}
function frn_gplus_js() {
	//This approach used instead of wp_enqueue_script in order for the scripts to be asynchronous
	echo '';
	/*
	<!--FRN Google+ Buttons JS-->
	<script src="https://apis.google.com/js/platform.js" async=\'async\' defer></script>
	
	';
	*/
	//wp_register_script( 'gplus_script', 'https://apis.google.com/js/platform.js', array(), false, true);
	//wp_enqueue_script( 'gplus_script');
}