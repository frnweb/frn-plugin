<?php

function frn_activate_edit_buttons( $buttons ) {	
	/**
	 * Adds in core toolbar buttons we want that's simply disabled by default
	 */
	$buttons[] = 'superscript';
	$buttons[] = 'subscript';
	$buttons[] = 'fontsizeselect';
	$buttons[] = 'anchor';

	return $buttons;
}
add_filter( 'mce_buttons_2', 'frn_activate_edit_buttons' );


////////////
//// Initiate Shortcode Dropdown JS for TinyMCE Editing Toolbar

add_action('init', 'frn_shortcodes_add_button');
function frn_shortcodes_add_button() {  
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )  
   {  
     add_filter('mce_external_plugins', 'frn_shortcodes_add_plugin');  
     add_filter('mce_buttons_3', 'frn_shortcodes_register_button');  
   }  
}   

function frn_shortcodes_register_button($buttons) {
   array_push($buttons, "frn_phones", "frn_sms_shortcodes", "frn_listshortcodes", "prism");  //not needed: "phoneButton", 
   return $buttons;
}  

function frn_shortcodes_add_plugin($plugin_array) {  
   //$plugin_array['phoneButton'] = plugin_dir_url( __FILE__ ) .'admin_toolbar_shortcodes.js.php'; //not needed, kept as example
   $plugin_array['frn_phones'] = plugin_dir_url( __FILE__ ) .'admin_toolbar_shortcodes.js.php';
   $plugin_array['frn_sms_shortcodes'] = plugin_dir_url( __FILE__ ) .'admin_toolbar_shortcodes.js.php';
   $plugin_array['frn_listshortcodes'] = plugin_dir_url( __FILE__ ) .'admin_toolbar_shortcodes.js.php';
   //$plugin_array['prism'] = plugins_url( 'admin_toolbar_shortcodes.js.php', __FILE__ );
   return $plugin_array; 
}

// Add Custom Quicktags to Text Editor
add_action( 'admin_print_footer_scripts', 'frn_shortcodes_quicktags' );
function frn_shortcodes_quicktags() {
	//function taken from: https://stackoverflow.com/questions/42200158/shortcode-dropdown-wordpress-text-editor-only
    if ( wp_script_is( 'quicktags' ) ) {
        wp_enqueue_script('jquery');
        ?>
        <script>
        /* Button name and callback will be replaced */
        QTags.addButton( 'frn_phone_holder', 'FRN Phones', function(){} );

        jQuery(window).load( function() {
            jQ = jQuery;

            /* Build dropdowns - http://stackoverflow.com/a/4814600 */

            //Facility Phone Shortcodes
            var ph_sc = {
                '-': 'Phone Shortcodes',
                <?php
                global $infinity_pools;
                if(isset($infinity_pools)) {
				foreach($infinity_pools as $pool){
					if($pool['sc']!=="") {
						echo "'[".$pool['sc']."_phone]' : '[".$pool['sc']."_phone]',\n";
					}
				}}
                ?>
            }
            var ph_sc_dd = jQ('<select />');
            var ph_sc_id = "frn_ph_shortcodes";
            ph_sc_dd.attr('id',ph_sc_id);
            for(var val in ph_sc) {
                jQ('<option />', {value: val, text: ph_sc[val]}).appendTo(ph_sc_dd);
            }
            



            //Facility SMS Number Shortcodes
            var sms_sc = {
                '-': 'SMS Shortcodes',
                <?php
                if(isset($infinity_pools)) {
				foreach($infinity_pools as $pool){
					if($pool['sc']!=="" && $pool['sms']!=="") {
						echo "'[".$pool['sc']."_sms]' : '[".$pool['sc']."_sms]',\n";
					}
				}}
                ?>
            }
            var sms_sc_dd = jQ('<select />');
            sms_sc_id = "frn_sms_shortcodes";
            sms_sc_dd.attr('id',sms_sc_id);
            for(var val in sms_sc) {
                jQ('<option />', {value: val, text: sms_sc[val]}).appendTo(sms_sc_dd);
            }
           


            //All Registered Shortcodes except for phone and sms shortcodes
            var all_sc = {
                '': 'Theme Shortcodes',
                <?php
                global $shortcode_tags;
			    $ordered_sct = array_keys($shortcode_tags);
			    sort($ordered_sct);
			    //print_r($ordered_sct);
                $sc_array=""; $sc_i=0;
                foreach($ordered_sct as $sct):
                    if($sct!=="") {
                        $comma=",";
                        if($sc_i==0) $comma="";
                        
                        //by this point the plain shortcode already printed (i.e. #1). We're just adding versions with popular attributes.
                        if($sct=="frn_phone") {
                            $attr=""; $i=1; 
                            while($i<2) {
                                //there is really only one version, but just keeping approach similar to the normal loop
                                if($i==1) $attr=" number=\'\'";
                                echo "'[".$sct.$attr."]' : '[".$sct."]',\n";
                                $i++;
                            }
                            $sc_i++;
                        }
                        //by this point the plain shortcode already printed (i.e. #1). We're just adding versions with popular attributes.
                        elseif($sct=="callout") {
                            $attr=""; $i=1; 
                            while($i<5) {
                                if($i==2) $attr="=\'border\'";
                                elseif($i==3) $attr="=\'inverse\'";
                                elseif($i==4) $attr="=\'secondary\'";
                                echo "'[".$sct.$attr."]' : '[".$sct."]',\n";
                                $i++;
                            }
                            $sc_i++;
                        }
                        //skip listing FRN shortcodes that are not meant for content
                        elseif($sct!=="frn_related_list" 
                            && $sct!=="frn_footer" 
                            && $sct!=="frn_privacy_url"
                            && $sct!=="lhn_inpage"
                            && stripos($sct,"_phone")===false
                            && stripos($sct,"_sms")===false
                        ) { $sc_i++;
                            echo "'[".$sct."]' : '[".$sct."]',\n";
                        }
                    }
                endforeach;
                ?>
            }
            var all_sc_dd = jQ('<select />');
            var all_sc_id = "frn_all_shortcodes";
            all_sc_dd.attr('id',all_sc_id);
            var val_display="";
            for(var val in all_sc) {
            	val_display=val;
            	if(val=="") val_display=all_sc[val];
	            jQ('<option />', {value: val, text: val_display}).appendTo(all_sc_dd);
            }
            /* Change 'Dummy button' for dropdown */
            jQ('#qt_content_frn_phone_holder')[0].outerHTML = "<br />"+ph_sc_dd[0].outerHTML+sms_sc_dd[0].outerHTML+all_sc_dd[0].outerHTML;
            

            /* 
            Listeners for dropdown selection
            Adds the "value" into the HTML textarea
            */
            jQ('#'+ph_sc_id).on('change', function(){
                var ph_sc_code = '' + jQ(this).val() + '';
                QTags.insertContent(ph_sc_code);
            });

            jQ('#'+sms_sc_id).on('change', function(){
                var sms_sc_code = '' + jQ(this).val() + '';
                QTags.insertContent(sms_sc_code);
            });

            jQ('#'+all_sc_id).on('change', function(){
                var all_sc_code = '' + jQ(this).val() + '';
                QTags.insertContent(all_sc_code);
            });


        });
        </script>
        <?php
    }
}


?>