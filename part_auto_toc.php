<?php


/////
// Auto Table of Contents Builder
/////

// v1.0 - 7/5/17
// v2.0 - 7/27/17 - Requires PHP >7 (earlier PHP will cause sites to 502 when adding the anchors to headers)
// v2.5 - 8/21/17 - post-specific options
// v2.6 - 9/14/17 - made class setting respond regardless of whether or not we are using the default CSS
// v2.7 - 6/21/18 - added class for aligning right when specific page overrides are set with ACF
// v2.8 - 6/27/18 
//                 - added class to page anchors to more specifically affect anchor spacing.
//				   - added menu height settings (global only) to compensate for menus
//				   - Fixed issue that only affected The Canyon in excerpts displayed on listing pages. It adds a [...] to the end of excerpts and somehow that affected the extraction of [frn_toc] in the preg_replace regular expression. It was modifed and limited to only work for frn_toc with or without spaces in front or back. Previously, it was written to allow for variables after frn_toc. This change means if we add variables, the shortcode will stop being removed from listing pages.
// v2.9 - 11/5/19 - Fixed: Extra space above paragraphs; Added: full-width selection, device specific, list type, list-only, after first paragraph defaul, new ACF


/*
KNOWN ISSUE: Empty Space Above Paragraphs with Images

In Octdober 2019, we discovered empty paragraphs above paragraphs that had images in the P tags.
The PHP DOM (loadHTML) doesn't like when it analyzes a node and it encounters a child node before the parent node is closed. 
As a result, it closes the node after the image. But since the images are right/left aligned and the <p>, usually has a margin defined in CSS, the blank line shows to the visitors.

There is no tested solution for this problem. Instead, we just bandaided things.
1) When the TOC shows at the top of content, we don't process the page in DOM. We just get the h2s, build the TOC, and insert it before content. We rely on jQuery to scroll down the page.
2) We only have to process with DOM when we are inserting the TOC after the first paragraph or if we have to move an image. 
    3) In the latter situation, it leaves blank <p></p>, so we just strip them out. 
    4) But when we insert it after the first paragraph, there is really nothing we can do about the system closing a p tag after images in the text.

*/



//////
/// FAKE SHORTCODE
//////


/*

Reason for Feature:
//Primary benefit is that Google will add these as sub-links in search results when a page ranks. That increases attention and provides more keywords that relate to what someone really wants to know.
//The second benefit is for mobile users. It keeps them from having to scroll as much and reduces reader fatique.

THE PLAN:
* This should be a content filter so that it's a sitewide option easily activated for all pages and post types on the site
* Shortcode: 
	* Technically, this is not possible using the traditional shortcode approach since the content that has the shortcode needs to be scanned prior to delivering the code.
	* So we fake it. If a shortcode is included, then that becomes the position for the TOC.
	* Typical users won't know the difference.
	* IMPORTANT: To make sure our shortcode is removed if the function should be disabled, we need to remove the shortcode from the content...
	* Which still requires processing even if turned off, so activate comes with a third option ("remove shortcodes")

Admin Options:
* Activation options:
	* Activate
	* Deactivate (default)
	* Remove shortcodes
* Exclusion page IDs
* Inclusion page IDs
* List of post types to choose from
* Header levels to consider (e.g. h1, h2, h3 )
* TOC title
* Styling
* Related posts exclusion
* Caching


Trigger Checks Function:
* Make sure feature is activated
* Make sure the page is not a front page or home page and only a singe or page
* Make sure post type activation options in FRN Settings includes all post types
* Make sure current page is not in excluded page IDs array
* Make sure included pages array is blank
* Make sure custom page options don't have the feature turned off
* Make sure "TOC" isn't used in a class or ID in the content

Content Filter Process:
* If trigger function returns TRUE, activate function

Shortcode Process:
* Included in the content, place TOC in that location

Function Process:
* Activate DOM
* Gather processing-friendly version of content after all other filters have processed
* Create array of each header content
* If there is an image in the first 500 characters, move it to the second header to leave room for the TOC table or whatever is selected in the admin settings
* As it processes content looking for headers:
	* check for page anchors before and after headers
	* if not present, add page anchors (a name="") before headers
	* if present, return content without changes
* Turn header array into a table:
	* Consider responsive needs
	* Make sure there is a default CSS style for designers to customize
	* Make sure FRN Settings allows for customizing classes and IDs
	* Use "title" attribute to h2's to customize the toc text
	* Make sure custom class and IDs can be passed specifically via a shortcode
* See if shortcode is in page content
	* if not, then execute as normal by adding it to the very front of the content
	* If so, use a find/replace to position the TOC in that location of the content

*/


if(!function_exists('frn_auto_toc_trigger')) {
function frn_auto_toc_trigger($content) {
	//Used to quickly test if Auto TOC is possible
	//there are quicker ways to do this, but harder to follow. 
	//The following doesn't require a large resource .

	//TESTING: The following return overrides all other things below for testing on ALL pages and listing pages
	//return TRUE;


	//the_content hook affects excerpts, meta descriptions, etc. 
	//By adding these two checks, it'll make sure that this feature only changes primary content displays and nothing else.
	if( in_the_loop() && is_main_query() ) {

		$toc = get_option('site_toc');
		if(!$toc) return FALSE;
		else {

			if(!isset($toc['activation'])) $toc['activation']="";
			if($toc['activation']!=="yes" && $toc['activation']!=="sc") return FALSE;
			else {

				//Check if ACF field is present and customized for current post
				//This will override everything else below. 
				//This option will be only available for posts, so there is no need to be more specific.
				if(function_exists('get_field')) {
					$custom_activate = get_field('frn_toc_activate');
					if($custom_activate) {
						if($custom_activate!=="") {
							if($custom_activate=="no") return FALSE;
							elseif($custom_activate=="yes") return TRUE;
							//We don't want to overwrite the main settings because we want the rest of this considered
						}
					}
				}
				

				//Since it's activated, go through normal checks
				//In order of most common
				if(is_single() || is_page()) {
					$curr_type=get_post_type();
					//echo "<h1>".$curr_type."</h1>";
					//echo "<h1>Activated Types:</h1>";
					//print_r($toc['post_types']);
					if(!is_front_page() && !is_home() && $curr_type!=="attachment" && $curr_type!=="revision" && $curr_type!=="nav_menu_item" && !stripos(get_the_title(),"Contact") ) {

						//see if shortcode is already in the text, if so, process the page.
						//there is no reason to process the rest since the presence of the shortcode indicates a manual intention to override all other automatic settings
						//using stripos instead of regular expression exec since lower resources needed (case insensitive since we can't sure sure what the user would do)
						if(stripos($content,"[frn_toc")) {
							//echo "<h1>Shortcode Found at character #".stripos($content,"[frn_toc")."</h1>";
							return TRUE;
						}

						/*
						NOTES:
						if post types selected
							use exclude IDs

						if post types not selected
							use include IDs
						*/

						$post_type_match=false; 
						$post_id=get_the_ID();
						if(!isset($toc['post_types'])) $toc['post_types']="";
						if(!isset($toc['include'])) $is_included="";
							else $is_included=trim($toc['include']); //since we are testing it, need to clean it
						if(is_array($toc['post_types'])) {
							foreach($toc['post_types'] as $post_type) {
								//echo "<h1>".$is_included."</h1>";
								if($post_type!=="") {if($curr_type==$post_type) $post_type_match=true;}
							}
							if($post_type_match) {
								if(!isset($toc['exclude'])) $is_excluded="";
									else $is_excluded=trim($toc['exclude']); //since we are testing it, need to clean it
								if($is_excluded!=="") {
									$is_excluded=explode(",",$is_excluded);
									foreach($is_excluded as $excl_post){
										if($post_id==trim($excl_post)) return FALSE;
									}
								}
								//////
								// Empty Content (later in process since it's a rare situation)
								//by this point, the current post's type is one of the selected, but its content may be blank or simply images.
								//some pages, due to ACF content fields and other reasons have nothing in the typical WP content field. 
								//To save TOC processing for those pages, we strip HTML and clean and see if it's blank and return false if so.
								if(trim(htmlentities($content, ENT_DISALLOWED))=="") return FALSE;
								
								return TRUE; //if not excluded or blank, then execute

							}
							else {
								//if the current post type is not selected, check the include IDs array for the post ID.
								if($is_included!=="") {
									$is_included=explode(",",$is_included);
									foreach($is_included as $incl_post){
										if($post_id==trim($incl_post)) return TRUE;
									}
								}
							}

							
						}
						else {
							//post_types is not an array, which means no post types selected
							//still check includes field if current post ID is in the array
							if($is_included!=="") {
								$is_included=explode(",",$is_included);
								foreach($is_included as $incl_post){
									if($post_id==trim($incl_post)) return TRUE;
								}
							}
						}

					} //end if front page
				} //end if page/post
			} //toc activations
		}
	}
	return FALSE; //final stage: if not "return" prior to this, then it means the current post didn't meet any requirements
}
}

add_filter( 'the_content', 'frn_auto_toc', 100 );
if(!function_exists('frn_auto_toc')) {
function frn_auto_toc($content) {

	$toc = get_option('site_toc');

	//Check if shortcode removal specifically requested
	if(!isset($toc['activation'])) $toc['activation']="";
	if(!isset($toc['sc_only'])) $toc['sc_only']="";

	//See if shortcode in content already
	//   If so, set var for later
	//	 And remove shortcode from Meta description after plugins are loaded
	$sc_in_content="no";
	if(stripos($content,"[frn_toc")) {
		$sc_in_content="yes";
		//add_action('plugins_loaded', 'frn_meta_description_edits');
		add_filter( 'wpseo_opengraph_desc', 'frn_remove_toc_shortcode_seo' , 10, 1 );
		add_filter( 'wpseo_twitter_description', 'frn_remove_toc_shortcode_seo' , 10, 1 );
		add_filter( 'wpseo_metadesc', 'frn_remove_toc_shortcode_seo', 10, 1 ); //yoast
		add_filter( 'aioseop_description', 'frn_remove_toc_shortcode_seo'); //all in one seo
	}

	if($toc['activation']=="sc" && $sc_in_content=="yes") {
		//just remove the frn_toc shortcode from the content and return it
		return preg_replace('/\[ ?frn_toc.*\]/', "", $content);
	}

	//trigger function includes basic TOC triggers and post type criteria, not image evaluations
	if(frn_auto_toc_trigger($content)) {

		global $frn_mobile;

		//If shortode only mode activated, keep in mind that by this point, we've already detected that the toc shortcode is in the content.

		///////
		// Initial image check and FRN Settings option selected
		//////
		//  The following avoids further processing
		//  If in shortcode only version, then we dont' need to look for images.
		if( $toc['sc_only']=="" && $sc_in_content=="no" ) {
			//Image cancelling of TOC
			//Only applies in automatic mode
			$image_loc=stripos($content,"img "); 
			//sets default character threshold for moving an image
			if(!isset($toc['chars'])) $toc['chars']=400; 
				elseif($toc['chars']=="") $toc['chars']=400;
			

			//CANCEL TOC IF IMAGE PRESENT
			//If (1) frn setting is to remove toc if image is present, (2) for desktops only, (3) and an image is present within 400 chars, remove TOC
			if($toc['image']=="present" && $frn_mobile!=="Smartphone" && ($image_loc<=$toc['chars'])) { 
				return $content; 
			}

			//CANCEL TOC IF FEATURED IMAGES INSERTED BEFORE CONTENT
			//If nothing selected in settings, then this will still run by default since in most cases, it's safer not to include the TOC with images at the front of content
			elseif($frn_mobile!=="Smartphone" && $toc['image']=="featured") {  
				// Removed this 6/28/18 - $toc['image']=="" || $toc['image']=="both"
				// with niche sites discontinued, most facility sites we never used featured images to show in a situation where content would wrap around them. This shutdown was unnecessary.
				// Most of the time, featured images are used in banners or on listing pages. But the plugin cannot detect those cases. If a person chooses "featured" as what adds images to the content, then this is disabled for desktops.
				// For desktops and tablets, there is no way to avoid competing with the featured image in content. So TOCs are disabled for those cases when "featured" is selected as what adds images to content.
				// But when the featured image function is used to add a banner image, then we should NOT choose "featured" and this will function per usual.

				// Since we are not adding a TOC, simply return the content and nothing else in this function will continue
				//Keep in mind that smartphones will still get the TOC, just not tablets or desktops
				if(has_post_thumbnail()) return $content; 
			}
		}

		

		
		



		/////////////
		//// Step 1: Build TOC and Anchors
		////////////


		//Why offer header level options:
			//Ideally, we'd only have one type to scan--most common is h2
			//However, some sites may have long pages where h3s are also important or just someone coded a theme incorrectly. 
			//Some themes use h1's for section headers incorrectly, but we have to account for that.
		if(!isset($toc['h_level'])) $toc['h_level']="h2";
		elseif($toc['h_level']=="") $toc['h_level']="h2";
		
		/*
		/// WP Transient Caching to store TOC var for quick recovery

		NOTE: 
		// Tried this. We could store the TOC code as a transient, but we still need to add page anchors next to headers. 
		// And we can't do that without activating DOM and processing the content.
		// Deactivated, but keeping code in case there is a new epiphany that will make all of it work.
		// Idea: Store section titles in a transient and then loop through to build TOC and do a preg_replace of the headers to add anchors (not sure if it's lighter due to preg_replace)

		//See if caching var already set to avoid scanning the h2s on every page load since content rarely changes
		//if caching turned off, don't look for var. But we will still save the variable down below for once they reactivate caching.
		if(!isset($toc['cache'])) $toc['cache']="";
		if($toc['cache']=="") $toc_code_transient=get_transient( 'toc_code_'.get_the_id() );
		else $toc_code_transient=false;
		*/
		
		
		//if($toc_code_transient===false) {
		
			
			///////////
			//Are there even headers in the content???
			//Use low-resource heavy approach to finding headers in the content
			//make sure there are at least two section headers on the page
			$first_h2=stripos($content,"<".$toc['h_level']);
			$second_h2=stripos($content,"<".$toc['h_level'],$first_h2+10);
			$toc_code="";
			

			/////////////
			//If it looks like there are two section headers, do more resource heavy work
			if($second_h2) {

				//////////
				//ACTIVATE DIRECT OBJECT MODELING (DOM)

				//IMPORTANT: Keep in mind that in-context personalize boxes and external link processing also use DOM
				//could this be turned into a function where DOMs are only activated on content filters once???
				libxml_use_internal_errors(true);
				$dom = new DOMDocument('1.0', 'UTF-8');
				$dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );  
				libxml_clear_errors(); //keeps DOM errors from appearing on page
				
				//The following is for if used in a PHP template directly (vs. a plugin hook) -- kept for reference (LGBTDrugRehab.com uses this approach)
					//$dom->loadHTML( mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8') );  //"<wp_content>".$content."</wp_content>"
					/* USE IF USING XML VERSION: @$doc->loadHTML('<?xml version="1.0" encoding="UTF-8"?>' . "\n" . $toc);  */
					
				//xpathing is usually for XML and similar to searching a document by parent/child relationships -- it's more intensive than using DOM elements
					//$xpath = new DomXPath($dom); 
					//$parent = $xpath->query("/root");  //kept as a reminder of the feature
				

				///////////
				//Build header array
				$headers = $dom->getElementsByTagName($toc['h_level']);
				//print_r($headers);
				//if less than two h2, cancel function
				//$items=count($headers->h2->sections);
				//echo "<h1>".$headers->length."</h2>";
				
				///////
				//As long as there are at least 2 headers on the page, build out the TOC
				//Although we already checked for this, it verifies that it wasn't just an error in the HTML
				if($headers->length > 1) {

					//Get FRN Settings overall CSS customizations first


					//Check for post-specific overrides using ACF
					$styling=FALSE; $title_overrides=FALSE; $first_anchor=FALSE; $last_anchor=FALSE; $list_only="no"; $after_first_p="no"; $device="all"; $device_css="";
					if(function_exists('get_field')) {
						// another option: 
						//   using the plugin's detection of activated plugins based on their main PHP in WP's list of activated plugins
						//   frn_test_plugin_activation('advanced-custom-fields-pro/acf.php'); //boolean
						$styling = get_field('frn_toc_styling'); //put here to not require looking for the function again later
						$title_overrides=get_field('frn_toc_title');
						$first_anchor=get_field('anchor_first');
						$last_anchor=get_field('anchor_last');
						$list_only=get_field('list_only'); // true/false; strips styling and DIV wrapper; new 10/29/19
						$after_first_p=get_field('after_p'); // true/false; positions TOC after the first </p> tag; new 10/29/19
						$device=get_field('device_only'); // string; adds css style that show/hides TOC by device; new 10/29/19
						if(!$device) $device="all"; //default setting
						
					}

					if(!isset($list_only)) $list_only="no"; //required if the field has not been saved for a post
					if(!$list_only) $list_only="no";

					//Determine if TOC will show for the specific post on the page
					if($device!=="all") { //if device=all, that means we don't want to add a device-specific CSS to the TOC div wrapper
						if($device=="desktab") {
							if(!$toc['desktab']) $device_css="show-for-medium";
							elseif($toc['desktab']=="") $device_css="show-for-medium";
							else $device_css=$toc['desktab'];
						}
						elseif($device=="desktop") {
							if(!$toc['desktop']) $device_css="show-for-large";
							elseif($toc['desktop']=="") $device_css="show-for-large";
							else $device_css=$toc['desktop'];
						}
						elseif($device=="tablet") {
							if(!$toc['tablet']) $device_css="show-for-medium-only"; 
							elseif($toc['tablet']=="") $device_css="show-for-medium-only";
							else $device_css=$toc['tablet'];
						}
						elseif($device=="mobile") {
							if(!$toc['mobile']) $device_css="show-for-small-only";
							elseif($toc['mobile']=="") $device_css="show-for-small-only";
							else $device_css=$toc['mobile'];
						}
						else $device_css=$device; //backup

						$device_css=" ".$device_css; //adds space since likely other CSS classes are there
					}

					
					//Build the TOC table
					$class=""; $div_id=""; $inline_styles=""; $ul_type="ul"; $right=""; $list_type="ul"; $list_style=""; 

					//Set class names
					//Check page-specific settings
					if($styling) {
						$class=trim($styling['class']);
						$div_id=trim($styling['id']);
						
						//styling and list-only common features
						//li bullet style:
						$list_style=trim($toc['list_style']); //circle, disc, numbers CSS (only globally)
						if($list_style=="") $list_style="none";

						if(!isset($styling['list_type'])) $styling['list_type']="";
						if($styling['list_type']!=="" && !is_array($styling['list_type'])) $list_type = $styling['list_type'];
						
						if($list_style=="ol") {
							$ul_type="ol"; //when "numbers" is selected in main settings
						}
						elseif($list_type!=="") $ul_type=$list_type;

					}
					if($list_only=="no") {
						if($class=="") {
							//If there is no custom post override, check main settings and use default
							if(!isset($toc['class'])) $toc['class']="";
							if(trim($toc['class'])!=="") $class = trim($toc['class']);
						}
						//if still nothing set, then use the default
						if($class=="") $class="frn_toc";
					

						//if using default styles (vs. relying on the CSS style sheet)
						if(!isset($toc['styles'])) $toc['styles']="";
						if($toc['styles']=="yes") {

							//introduct vars, but leave blank for testing custom values following
							$float=""; $margin=""; $padding=""; $width=""; $maxwidth="";  
							$border=""; $border_radius=""; $ul_margin=""; $ul_padding="";
							
							if(!isset($toc['menu_top_size'])) $toc['menu_top_size']="";
							$menu_top_size="60px";

							if($styling) {

								//Declare variables in case they aren't defined
								if(!isset($styling['float'])) $styling['float']="";
								if(!isset($styling['margin'])) $styling['margin']="";
								if(!isset($styling['padding'])) $styling['padding']="";
								if(!isset($styling['width'])) $styling['width']="";
								if(!isset($styling['maxwidth'])) $styling['maxwidth']="";
								if(!isset($styling['border'])) $styling['border']="";
								if(!isset($styling['border_radius'])) $styling['border_radius']="";
								if(!isset($styling['background'])) $styling['background']="";
								if(!isset($styling['ul_margin'])) $styling['ul_margin']="";
								if(!isset($styling['ul_padding'])) $styling['ul_padding']="";

								$float=$styling['float'];
								$margin=trim($styling['margin']);
								$padding=trim($styling['padding']);
								$width=trim($styling['width']);
								$maxwidth=trim($styling['maxwidth']);
								$border=trim($styling['border']);
								$border_radius=trim($styling['border_radius']);
								$background=trim($styling['background']);
								$ul_margin=trim($styling['ul_margin']);
								$ul_padding=trim($styling['ul_padding']);
							}

							//load overall FRN Settings if custom settings not set
							if($float=="") $float=$toc['float'];
							if($margin=="") $margin=trim($toc['margin']);
							if($padding=="") $padding=trim($toc['padding']);
							if($width=="") {
								if($float=="full") {
									$width="100%";
									$maxwidth="100%";
								}
								else {
									$width=trim($toc['width']);
								}
							}
							if($maxwidth=="") $maxwidth=trim($toc['maxwidth']);
							if($border=="") $border=trim($toc['border']);
							if($border_radius=="") $border_radius=trim($toc['border_radius']);
							if($background=="") $background=trim($toc['background']);
							if($ul_margin=="") $ul_margin=trim($toc['ul_margin']);
							if($ul_padding=="") $ul_padding=trim($toc['ul_padding']);
							$toc['menu_top_size']=trim($toc['menu_top_size']);
							if($toc['menu_top_size']!=="") $menu_top_size=$toc['menu_top_size'];

							//load defaults
							if($margin=="") $margin="0 15px 20px 0"; //overwritten below depending on float
							if($padding=="") $padding="0";
							if($maxwidth=="") $maxwidth="50%";
							if($border=="") $border="1px solid #ccc";
							if($border_radius=="") $border_radius="10px";
							if($ul_margin=="") $ul_margin="0";
							if($ul_padding=="") $ul_padding="0";

							if($margin=="0 15px 20px 0") { //left default
								if($float=="right") {
									$margin="0 0 20px 15px";
								}
								elseif($float=="none") {
									$margin="15px 0";
								}
								elseif($float=="full") {
									$margin="0 0 20px 0";
								}
							}
							
							if($ul_type=="ol") {
								$ul_margin="0 0 0 1em"; //makes up for the width of numbers on the left side
							}

							//reset float if = full or set left if blank
							if($float=="full") $float="";
							elseif($float=="") $float="left";
							
							//maxwidth fix
							$width_test = str_replace("%","",$width);
							$maxwidth_test = str_replace("%","",$maxwidth);
							$width_test = str_replace("px","",$width);
							$maxwidth_test = str_replace("px","",$maxwidth);
							if(trim($width_test) > trim($maxwidth_test)) {
								$maxwidth = $width;
							}
							if($width!=="") {
								$width="
									width: ".$width.";";
							}


							if($background!=="") $background = "\nbackground: ".$background.";";

							$inline_styles = "
							<style>
								.".$class." {
								    border: ".$border.";
								    border-radius: ".$border_radius.";".$background."
								    padding: ".$padding.";
								    font-size: 1em;
								    margin: ".$margin.";".$width."
								    max-width: ".$maxwidth.";
								    float: ".$float.";
								}
								.".$class."_inner {
								    margin: .8em 1em .3em 1em;
								    padding: .3em .1em .3em .1em !important;
								}
								.".$class." h1, .".$class." h2, .".$class." h3 {
									font-size: 1.3em;
									margin: 0 0 5px 0;
									padding: 0;
								}
								.".$class." ".$ul_type." {
									padding: ".$ul_padding.";
									margin:".$ul_margin.";
									line-height: 1.2em;
								}
								.".$class." .".$class."_inner ul li {
									list-style:".$list_style.";
									margin: .5em 0 .7em 0;
								}
								a[name].frn_toc_anchor:before {
								    content: \"\";
								    display: block;
								    height: ".$menu_top_size.";
								    margin: -".$menu_top_size." 0 0 0;
								}
								@media (max-width: 767px) {
									.".$class." {
									    border: ".$border.";
									    border-radius: ".$border_radius.";
									    padding: 0;
									    font-size: 1em;
									    margin: 0 0 20px 0;
									    max-width: 100%;
									    width: 100%;
									    float: none;
									}
									.".$class." .".$class."_inner {
									    margin: 8px 10px;
									}
									.".$class." ".$ul_type." {
										
									}
									.".$class." .".$class."_inner ul li {
									    padding: 5px 5px 5px 0;
									}
									.".$class." .".$class."_inner ul li:active {
									    background: #eee;
									    padding: 5px 5px;
									    border-radius: 5px;
									}
								}
							</style>
							";
						}
						elseif ($styling) {
							$float=$styling['float'];
							if($float=="right") $right=" frn_toc_right";
							elseif($float=="none") $right=" frn_toc_full";
						}
					}


					///////////
					//Set title of TOC
					$title="";
					if($frn_mobile=="Smartphone") {
						//Mobile title

						//Custom field check first (created by ACF Plugin options)
						if($title_overrides) {
							if(trim($title_overrides['smartphones'])!=="") $title=$title_overrides['smartphones'];
						}
						if($title=="") {
							//if title not overwritten for current post, check main settings and use defaults.
							if(!isset($toc['title_mobile'])) $toc['title_mobile']="";
							if($toc['title_mobile']=="") $toc['title_mobile']="Jump to Section:";
							$title=$toc['title_mobile'];
						}
					}
					else {
						//Desktop title
						//Custom field check first (created by ACF Plugin options)
						if($title_overrides) {
							if(trim($title_overrides['desktops'])!=="") $title=$title_overrides['desktops'];
						}
						if($title=="") {
							//if title not overwritten for current post, check main settings and use defaults.
							if(!isset($toc['title_desktop'])) $toc['title_desktop']="";
							if($toc['title_desktop']=="") $toc['title_desktop']="On This Page:";
							$title=$toc['title_desktop'];
						}
					}

					//System responds to empty field as trigger. So if we want to remove the title altogether, we have to be specific.
					if(strtolower($title)=="empty") $title=""; 
					elseif(strtolower($title)=="remove") $title=""; 



					//////
					// Prepare DIV ID
					if($list_only=="no") { //as long as it's not just a list (i.e. needs DIV wrapper)
						if($div_id=="") {
							//If there is no custom post override, check main settings and use default
							if(!isset($toc['id'])) $toc['id']="";
							if(trim($toc['id'])!=="") $div_id = trim($toc['id']);
						}
					}
					if($div_id!=="") $div_id = 'id="'.$div_id.'" ';

					$ul_class=""; $ul_id="";
					if($list_only=="yes") { //if displaying a list, we may still want to apply sitewide class and ids
						if($div_id!="") $ul_id   = ' '.$div_id;
						if($class!="" ) $ul_class= ' class="'.$class.'"' ;
					}


					$toc_code = "\n							<".$ul_type.$ul_id.$ul_class.">\n";
								


					////////
					//Post-specific additional anchor at the beginning of the TOC using ACF plugin options
					$anchor_cnt=0;
					if($list_only=="no") $anchor_cnt++;
					$addl_link="";
					if($first_anchor) {
						if(trim($first_anchor['text'])!=="") {
							$title=$first_anchor['text'];
							$first_link = $first_anchor['anchor'];   
							$toc_code .= '									<li><a href="#'.$first_link.'" id="toc_first" onClick="toc_scroll('.$anchor_cnt.');frn_reporting(\'\',\'Content Interactions\', \'Table of Contents\', \''.$title.'\',\'no\');" >'.$title.'</a></li>
								';
							$anchor_cnt++; //prepping for next anchor
						}
					}




					//////
					// Move the first image if exists (only if shortcode not present)
					//   NOTE: There may be a better approach, but chose a less resource heavy option for now
					//   Find the first image and determine header # placement before looping through them

					//if a TOC shortcode is in the content, then images have already been taken care of
					//otherwise we need to dynamically take care of it
					$image="";
					if($toc['sc_only']=="" && $sc_in_content=="no" ) {
						//Move image only when shortcode only mode isn't activated
						$image_h_level=0;
						if($toc['image']!=="disabled") {
							if(!stripos($content,"[frn_toc")) {

								//If smartphone, we don't need to move an image since they and the TOC would be 100% likely anyway
								if($frn_mobile!=="Smartphone") {
									//The "present" image option is earlier in the code
									
									// IMAGES MANUALLY INSERTED
									if($toc['image']=="" || $toc['image']=="content" || $toc['image']=="both") {

										//This just gets the location of any img code in content (could be at the front or end)
										$image_loc=stripos($content,"img "); 

										//it's easy to see if an image is within a character threshold
										//the hard part is repositioning. You don't want to put an image inside an element--or worse--the HTML tags.

										//See if the image is within our threshold + some margin for additional HTML code not seen on the page
										//if there is image code within our threshold, then use the more process-heavy option to grab the whole element
										//IMG code can be as many as 200 characters or more with long URLs, ALT, etc.
										if($image_loc<=($toc['chars']+200)) 
											$image = $dom->getElementsByTagName('img')->item(0); //0=first image found in content

										//$toc_height=$headers->length * 16; //16px per line
										
										//now determine where we'll move it
										if($image!="") {
											$image_code = $dom->saveHTML($image); //was saveXML
											$image_chars=strlen($image_code);
											
											//echo $image."Daxtest";

											//we need to get an idea of where within our threshold the image is
											//TESTING for H2 location: echo stripos($content,"<".$toc['h_level']);
											//check if the image and first header is within the area where the TOC would be
											//If so, then move the image to just before the second header
											//Otherwise, continue with the default action that moves the image above the first header
											if($image_loc<=($toc['chars']+$image_chars)) {
												if($first_h2<=($toc['chars']+$image_chars)) $image_h_level=1;
											}
										}
										
									} //end of verifying image settings that should trigger checking for manually added images
								} // end of desktop requirement
							} //end of check if shortcode is in content
						} //end of image setting for disabled
					}
					
					/*
						//For Testing
						else if($image_loc<200 && $loop==1) $header->appendChild($image);
						echo "<h1>It worked".$loop."</h1>";
						
						//Used to verify if an image is found by showing all of it's attributes within the img code.
						if ($image->hasAttributes()) {
						  foreach ($first_image->attributes as $attr) {
							$name = $attr->nodeName;
							$value = $attr->nodeValue;
							echo "Attribute '$name' :: '$value'<br />";
						  }
						}
					*/
					



					//////
					// Loop through headers

					//cycle through each h2 in array
					//Set the anchor var and add to content right before each h2
					//Build the list of anchors
					$loop = 0; $img_in_header=0; $image_moved="no";
					if(!isset($toc['rp_frn'])) $toc['rp_frn']=""; //trigger to ignore rp block
					foreach ($headers as $header) {
						//add anchor to content right before each h2

						//Ignore the FRN related posts section
						//Only works when the automatic version is activated
						//The shortcode is built after the content has gone through this filter, so it won't see the RP h2 header.
						$toc_hdr_id=""; $toc_label="";
						if ($header->hasAttributes()) {
							$toc_hdr_id=trim($header->getAttribute('id')); //only activated if we need to find the related post DIV; returns blank if no attribute
							
							//See if custom TOC link included
							$toc_link="yes"; $title_label="yes"; $toc_title_label="yes";
							$toc_label=trim($header->getAttribute('toc_link')); //Older version, likely not on any sites
							if($toc_label=="") {
								$toc_label=trim($header->getAttribute('title')); //example use on AddictionHelpCenter.com
								$toc_link="no";
							}
							if($toc_label=="") {
								$toc_label=trim($header->getAttribute('toc_title')); //as of 08/2017, not use, but this was created in 07
								$title_label="no";
							}
							if($toc_label=="") {
								$toc_title="no";
							}
							//Idea: if($toc_label=="") $toc_label=$header->getAttribute('link_text'); //just in idea -- need feedback from team
							//echo "<h1>Header #".$loop.": ".$toc_hdr_id."</h1>";
							
						}

						//If title="remove", then skip creating the link in the TOC for that header
						$toc_label_test=strtolower($toc_label);
						if($toc_label_test!=="remove" && $toc_label_test!=="delete" && $toc_label_test!=="skip") {
							//setting is set to ignore
								//often id is not frn rp
								//id=frnrp
							//setting is set to not ignore
								//often id is not frn rp
								//id=frnrp
							//$toc['rp_frn'] is blank when not checked -- i.e. ok to process frn rp header
							if( $toc['rp_frn']=="" || ( $toc['rp_frn']!=="" && $toc_hdr_id!=="frn_related_hdr" ) ) {

								$hdrtext=$header->nodeValue;
								//remove all HTML in header (sometimes images are in h2 tags on old sites)
								$hdrtext_cleaned=trim(htmlentities($hdrtext, ENT_DISALLOWED));

								//If there is text in the header tag, then continue processing
								if($hdrtext_cleaned!=="") {
									
									//see if header has a toc friendly term in h2 attribute, use it, and remove it from header
									if($toc_label=="") {
										//Use HTML cleaned version
										$hdrtext=trim($hdrtext_cleaned);
										//see if ENDS with colon and remove it
										$colon=substr($hdrtext,strlen($hdrtext)-1,1);
										if($colon==":") {
											$toc_label = str_replace(":", "", $hdrtext);
										}
										else $toc_label = $hdrtext;
									}
									else {
										//Remove the custom attributes (in case there could ever be a problem)
										if($toc_link=="yes") {
											$header->removeAttribute ( 'toc_link' );
										}
										elseif($title_label=="yes") {
											$header->removeAttribute ( 'title' );
										}
										elseif($toc_title_label=="yes") {
											$header->removeAttribute ( 'toc_title' );
										}
									}
									
									//Prepare anchor link based on toc label text (whether customized or taken from H2)
									//If current h2 in the loop is for our related posts, then use it's default anchor instead of creating our own
									if($toc_hdr_id=="frn_related_hdr") $anchor_link="frn_related_posts";
									else {
										$anchor_link=str_replace(" ", "-", strtolower(trim($toc_label)));
										$anchor_link=preg_replace('/[^A-Za-z0-9\-]/', '', $anchor_link);
										//Unlikely someone will use a pound symbol, but if it's there, it will be problem
										if(substr($anchor_link,0,1)=="#") $anchor_link=str_replace("#","",$anchor_link);
									}

									 //jQuery option: toc_scroll('.$anchor_cnt.');
									$toc_label_ga = str_replace("'", "-", strtolower(trim($toc_label))); //wp should already be converting quotes and single quotes to HTML codes, but in case they are turned off, single quotes would affect reporting the most. 
									$toc_code .= '							<li><a href="#'.$anchor_link.'" id="toc_'.$loop.'" onClick="toc_scroll('.$anchor_cnt.');frn_reporting(\'\',\'Content Interactions\', \'Table of Contents\', \''.$toc_label_ga.'\',\'no\');" >'.$toc_label.'</a></li>'."\n";
									$anchor_cnt++; //prepping for next anchor
									//echo $header->getNodePath() . "; \n";  //Gets relationship tree for a particular code block
									//$insertBefore = $xpath->query('/html/body/h2')->item($loop);
									
									//Move the first image at the very front of the text to the appropriate header depending on how much text is infront of the selected header_loc
									if($image!="" && $frn_mobile!=="Smartphone" && $sc_in_content=="no") {
										//don't move image for smartphones since they and the TOC are typically 100% wide
										//if image found, insert the image right before the header determined $image_h_level
										//OLD METHOD: if($loop==$image_h_level) $header->appendChild($image); //adds image to within the h2, but just before the closing </h2>
										if($loop==$image_h_level) {
											$header->parentNode->insertBefore($image, $header);
											$image_moved="yes";
										}
									}

									//create anchor -- 
									// unless the current H2 in the loop is for our related posts (it already has an anchor)
									
									//Around 11/4/19, due to a problem with empty paragraphs next to images, we determined we could use a jQuery method that scrolled the page into view.
									//We only process the page with DOM when an image needs to be moved or we put the TOC after the first paragraph.
									if($toc_hdr_id!=="frn_related_hdr" && ($image_moved=="yes" || $after_first_p=="yes")) {
										$domElement = $dom->createElement('a','');
										$anchor_name = $dom->createAttribute('name');
										$anchor_name->value = $anchor_link;
										$domElement->appendChild($anchor_name);
										$anchor_class = $dom->createAttribute('class');
										$anchor_class->value = "frn_toc_anchor";
										$domElement->appendChild($anchor_class);
										//add anchor before h2
										$header->parentNode->insertBefore($domElement, $header);
									}
									

									$loop++;
								}
								else $img_in_header++;

							}
						} //end remove
						elseif($toc_label_test=="remove" || $toc_label_test=="delete" || $toc_label_test=="skip") {
							$header->removeAttribute ( 'title' ); //make sure title attribute is still removed
						}
					}
					
					////////
					// Additional Anchor
					$addl_link=""; $main_deactivate="";

					//Post-specific additional anchor at the end using ACF plugin options
					if($last_anchor) {
						if(trim($last_anchor['text'])!=="") {
							$title=$last_anchor['text'];
							$last_link = $last_anchor['anchor'];  
							$addl_link .= '
								<li><a href="#'.$last_link.'" id="toc_last1" onClick="toc_scroll('.$anchor_cnt.');frn_reporting(\'\',\'Content Interactions\', \'Table of Contents\', \''.$title.'\',\'no\');" >'.$title.'</a></li>
								';
							$anchor_cnt++; //prepping for final anchor
						}
						$main_deactivate = $last_anchor['main_anchor_deactivate']; //yes or no text
					}

					//// Additional anchor from main FRN Settings Table of Contents
					if($main_deactivate!=="yes") {
						if(!isset($toc['addl_anchor'])) $toc['addl_anchor']="";
						if(!isset($toc['addl_title'])) $toc['addl_title']="";  
						if(trim($toc['addl_title'])!=="") {
							$addl_link .= '
									<li><a href="#'.$toc['addl_anchor'].'" id="toc_last2" onClick="toc_scroll('.$anchor_cnt.');frn_reporting(\'\',\'Content Interactions\', \'Table of Contents\', \''.$toc['addl_title'].'\',\'no\');" >'.$toc['addl_title'].'</a></li>
									';
						}
					}

					$toc_code .= $addl_link."\n							</".$ul_type.">";



					//////
					// Prepare TOC Wrapper
					$toc_wrapper="";
					if($list_only=="no") { //as long as it's not just a list (i.e. needs DIV wrapper)
						$inline_styles = trim(preg_replace( "/\r|\n|(\s\s\s\s)/", "", $inline_styles )); //removes line breaks to avoid WP from putting the styles into it's own paragraph 10/29/19
						$toc_wrapper = $inline_styles.'<div '.$div_id.'class="'.$class.$right.$device_css.'">
						<div class="'.$class.'_inner">
							';

						if($title!=="") $toc_wrapper .= '<'.$toc['h_level'].'>'.$title.'</'.$toc['h_level'].'>';
					
						$toc_wrapper .= "							";
						$toc_wrapper .= "%%toc%%"; //this will be replaced with the TOC list
						$toc_wrapper .= "
						</div>
					</div>\n					";

						$toc_code = str_replace("%%toc%%", $toc_code, $toc_wrapper);
					}


				
					//Ends the creation of the TOC




					// WP Transient Caching
					// Discontinued since we still need to add page anchors. If not deactivating DOM, then it's not that much processing savings.
					//set caching var to avoid scanning the DOM for every load when page content doesn't change often.
					//Even if caching is disabled, we want to still save the variable for once they reactivate caching.
					//set_transient( "toc_code_".get_the_id(), $toc_code, 60*60*24 ); //last var is cache timeout in seconds (sec x min x hours) -- refresh is once a day
					


						
				} //end at least 2 headers
			} //end at least 2 headers
		//} //end transient check

		/*
		//see transient notes above for why this is disabled.
		if($toc_code_transient!==false) {
			if(!isset($dom)) { //this check unlikely but safer
				libxml_use_internal_errors(true);
				$dom = new DOMDocument;
				$dom->loadHTML( $content );
				libxml_clear_errors(); //keeps DOM errors from appearing on page
			}
			$toc_code = $toc_code_transient;
		}
		*/

		//echo "<h1>Print to page</h1>";

		/////////////
		//// Step 2: Add to Content
		////////////
		//First, look for shortcode to determine position of TOC
		//If no shortcode, then place it at the top
		if($loop>1) { // || $toc_code_transient!==false
			//Loop count may change if an h2 has no content in it. Double checking to be sure we still have at least 2 in the array
			//if it even found 2 headers, then determine where it'll go in the content


			//whether or not we're in shortcode only mode, if the shortcode is in the content, replace it with the TOC.
			if($sc_in_content=="yes") {
				//use a regex to get full shortcode pattern and replace it with the saved HTML
				//DOM PHP automatically adds doctype and HTML/BODY wrap when one isn't present like with our centent. We have to remove it or content won't display.
				return preg_replace('/\[ ?frn_toc.*\]/', $toc_code, preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $content )); // $dom->saveHTML()
			}
			//If no shortcode in content, put it at the front of the content automatically
			else {

				if($after_first_p=="yes") { //if we want to position the TOC after the first paragraph (i.e. <p></p>) in the post
					
					$first_p = $dom->getElementsByTagName('p')->item(0); //0=first p found in content
					$divWrap = $dom->createElement('div','');
					$class_attr = $dom->createAttribute('class');
					$class_attr->value = "frn_toc_anchor";
					$divWrap->appendChild($class_attr);
					appendHTML($divWrap, $toc_code); //this strips out the default <style>
					//add anchor before first p
					$first_p->parentNode->insertBefore( $divWrap, $first_p->nextSibling );
					//$second_p->parentNode->insertBefore($divWrap, $second_p); //taken from image above, but wouldn't work if header is after a p

					//DOM PHP automatically adds doctype and HTML/BODY wrap when one isn't present like with our centent. We have to remove it or content won't display.
					$dom_saved = preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $dom->saveHTML());

					//Remove empty paragraphs created when we moved the image.
					$remove_empty_p = str_replace("<p></p>","",$dom_saved); 

					return $inline_styles.$remove_empty_p; //inline styles has to be added back in. appendHTML() removes it.
					
				}
			}

			//If image is moved, then we are forced to save the DOM. But moving the image often leaves empty paragraphs, so stripping them out keeps the page clean.
			if($image_moved=="yes") {
				//DOM PHP automatically adds doctype and HTML/BODY wrap when one isn't present like with our centent. We have to remove it or content won't display.
				$dom_saved = preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $dom->saveHTML());

				//Remove empty paragraphs :: Around Octdober 2019, we discovered empty paragraphs above paragraphs that started with images. 
				//The PHP DOM doesn't like when it analyzes a node and it encounters a child node before the parent node is closed. 
				//As a result, it closes the node after the image. But since they are right/left aligned, it adds a blank paragraph. That <p>, usually has a margin in CSS, so the blank line shows.
				$remove_empty_p = str_replace("<p></p>","",$dom_saved);

				return $toc_code.$remove_empty_p;
			}
			else return $toc_code.$content;

		}



	} //ends activation function
	return $content;

} //end of function
}


function appendHTML(DOMNode $parent, $source) {
    $tmpDoc = new DOMDocument();
    $tmpDoc->loadHTML($source);
    foreach ($tmpDoc->getElementsByTagName('body')->item(0)->childNodes as $node) {
        $node = $parent->ownerDocument->importNode($node, true);
        $parent->appendChild($node);
    }
}



///////////
/// META DESCRIPTIONS - Incl. Opengraph and Twitter Cards

//Remove toc shortcode from any content sent to this function
//This function should not run until all plugins are loaded
add_filter( 'the_excerpt', 'frn_remove_toc_shortcode', 10, 1 );
function frn_remove_toc_shortcode_seo( $md ) {
	//check if Yoast plugin installed. If so, add the "yoast" ending to the preg_replace
	//may not need "yoast" portion (was in Yoast's function example on their site)
	$yoast="";
	if(frn_test_plugin_activation ( 'wordpress-seo/wp-seo.php' )) {
		//this function found in frn_plugin.php
		$yoast=",yoast";
	}
	return preg_replace('/\[ ?frn_toc.*\]/', '', $md).$yoast;
}
function frn_remove_toc_shortcode( $md ) {
	return preg_replace('/\[ ?frn_toc ?\]/', '', $md);
}