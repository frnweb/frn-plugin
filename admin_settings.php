<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );



global $frn_version;
$frn_version = "3.3.1";



add_action('admin_head', 'frn_hide_footer');
function frn_hide_footer() {
	?>
	<style>
	  /*This page's footer shows in the middle of content. Can't fiture out why so, just hiding it.*/
	  #wpfooter {
		  display: none;
	  }
	</style>
	<?php
}




/////////////////////////////////////
// Create FRN Settings area        //
/////////////////////////////////////

// Creates the main menu item
add_action('admin_menu', 'frn_plugin_admin');
function frn_plugin_admin() {
	global $frn_settings_page; //variable used for page identification in admin to make sure certain functions don't execute unless it's on our own page
	$frn_settings_page = add_menu_page( 'FRN Common Features', 'FRN Settings', 'manage_options', 'frn_features', 'frn_plugin_settings', plugin_dir_url( __FILE__ ).'images/frn_icon_menu.png' );
	add_action('admin_init', 'frn_plugin_vars_and_sections');
}

//HELP ICON 
$GLOBALS['help_image'] = plugin_dir_url( __FILE__ )."images/icon_help.png";
$GLOBALS['frn_acf_start'] = plugin_dir_url( __FILE__ ).'part_acf_settings_2019-11-05_MH.json';


// SUBPAGES
include("admin_submenu_phone.php");
include("admin_submenu_toc.php");
include("admin_submenu_related_posts.php");
//include("admin_submenu_boxes.php");
include("admin_submenu_dev.php"); //features moved here 2/1/18

function frn_rename_settings() {
	//needs to be called in the first sub-page code (phone number subpage)
	add_submenu_page('frn_features', 'FRN Common Features', 'Main Settings', 'manage_options', 'frn_features' );
}



/*
	/////////////////////////
	/// TABLE OF CONTENTS: //
	/////////////////////////
	
	///////
	//TIP: if there's an underscore, use that to search by to jump to the exact section.
	///////

	PAGE_LAYOUT (line: )
	REGISTER_SETTINGS (line: )
	PHONE_NUMBERS (line: )
	Analytics & DATA_TOOLS (line: )
	EXTERNAL_LINKS Processing (line: )
	LIVE_HELP_NOW (line: )
	FOOTER_OPTIONS (line: )
	MOBILE_OPTIONS (line: )
	SOCIAL_SHARING (line: )
	OPENGRAPH_OPTIONS (line: )
	SHORTCODE_ACTIVATIONS (line: )
	URL_Base - NOT USED (line: )
	
*/





//////////
//PAGE_LAYOUT
//////////
function frn_plugin_settings() {
global $frn_version;
?>
<div class="wrap">
<h2>FRN Plugin Settings <small>v. <?=$frn_version;?></small></h2>
<div class="intro_text" style="margin-top:50px;">
	<p>
	<h3>Quick Links:</h3>
		<ol style="font-weight:bold;font-size:14px;">
		<li><a href="#analytics">Analytics & Data Tools</a></li>
		<li><a href="#links">External Link Handling</a></li>
		<li><a href="#lhn">LiveHelpNow Options</a></li>
		<li><a href="#footer">Footer / Privacy Policy</a></li>
		<li><a href="#mobile">Mobile Options</a></li>
		<li><a href="#social">Social Sharing Options</a></li>
		<li><a href="#sc_fields">Shortcode Field Activations</a></li>
		
		</ol>

		<br />
		<h3>FRN Automated Content Features</h3>
		<ol>
			<li><a href="admin.php?page=frn_phones">Phone Settings</a> (shortcode and automated)</li>
			<li><a href="admin.php?page=frn_toc">Table of Contents</a> (shortcode and automated)</li>
			<li><a href="admin.php?page=frn_related_section">Related Posts</a> (shortcode and automated)</li>
			<li><a href="admin.php?page=frn_boxes">In-Content Boxes for Rehab Decision Journey</a> (shortcode and automated)</li>
			<li><a href="admin.php?page=frn_dev">Shortcode Report / Notes for Developers</a></li>
		</ol>

	</p>
</div>


<form action="options.php" method="post" class="frn_styles">
	<?php $hide="none";
		settings_fields('frn_plugin_data'); // must match register_setting first variable
		if(!function_exists('frn_admin_infinity_main')) $hide="block";
			//As of 1/24/19, decided to just hide this section to continue to preserve original the settings. 
			//We could consider once the plugin has fully rolled out, we can remove this section and the original parent var for phone settings will be erased
			//Keeping this allows us to do a more full-system revert, but we'd need to make sure facility shortcodes remain active and the infinity pool array remain active
		?>
		<div style="display:<?=$hide;?>;">
			<?php do_settings_sections('frn_phone_section'); ?>
			<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
			<br />
			<br /> 
			<br />
		</div>
		<?php 
		
	?>
	<?php do_settings_sections('frn_head_section'); //Google analytics ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_extlinks_section'); //still google analtyics?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_lhn_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_footer_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_mobile_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_social_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br />
	<?php do_settings_sections('frn_sc_fields_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" /></div>
	<br /><!--Hidden 10/26/18 Since niche sites discontinued
	<?php //do_settings_sections('frn_sitebase_section'); ?>
	<div class="intro_text"><input name="Submit" type="submit" class="button button-primary" value="<?php //esc_attr_e('Save Changes'); ?>" /></div>
	<br />-->
	<br />
	<?php //do_settings_sections('frn_shortcode_report_section'); ?>
	<br />
</form>
</div>

<?php
}


////////
// Initiate vars and settings page layout
////////

function frn_plugin_vars_and_sections(){
	
	//site_footer_code = houses all footer fields data in an array
	//site_head_code = houses all head fields data in an array

	//initiates groups of data (id of settings_fields, field name storing data, function setting up array)
	register_setting( 'frn_plugin_data', 'site_phone_number','plugin_options_validate'  );
	register_setting( 'frn_plugin_data', 'site_head_code','plugin_options_head'  );
	register_setting( 'frn_plugin_data', 'site_lhn_code','plugin_options_lhn'  );
	register_setting( 'frn_plugin_data', 'site_footer_code','plugin_options_footer'  ); 
	register_setting( 'frn_plugin_data', 'site_mobile_code','plugin_options_mobile'  ); 
	register_setting( 'frn_plugin_data', 'frn_social_options','plugin_store_social'  ); 
	register_setting( 'frn_plugin_data', 'shortcode_fields','plugin_options_sc_fields_save'  ); 
	register_setting( 'frn_plugin_data', 'site_sitebase_code','plugin_options_sitebase'  ); 
	
	
	//displays the data in Admin and sets up DB
	//PHONE shortcode
	add_settings_section('site_phone_number', '<a name="phone"></a><br />Shortcode for Phone Numbers', '', 'frn_phone_section');
		add_settings_field('frn_plugin_phone', "Phone <span style='white-space:nowrap;'>Number <a href='javascript:showhide(\"frn_plugin_phone_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_phone_field', 'frn_phone_section', 'site_phone_number');
		add_settings_field('frn_phone_mobile', "Mobile Number Linking <span style='white-space:nowrap;'>Options <a href='javascript:showhide(\"frn_plugin_scantype_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_phone_mobile', 'frn_phone_section', 'site_phone_number');
		
	//Analytics settings field:(unique id, field label, function to add form field, menu page, page section)
	add_settings_section('site_head_code', '<a name="analytics"></a>Analytics & Data Tools', 'text_before_head_form', 'frn_head_section');
		add_settings_field('frn_plugin_ga', "Google Analytics", 'ga_id_field', 'frn_head_section', 'site_head_code');
		add_settings_field('frn_plugin_hdr_test', "Secondary Test <span style='white-space:nowrap;'>Mode <a href='javascript:showhide(\"frn_plugin_test_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_test_funct', 'frn_head_section', 'site_head_code');
		add_settings_field('frn_plugin_ftr_chbt', "Chartbeat <span style='white-space:nowrap;'>Tracking <a href='javascript:showhide(\"frn_plugin_cht_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'chrbt_chkbx_funct', 'frn_head_section', 'site_head_code');
		add_settings_field('frn_plugin_ftr_hotjar', "Hotjar <span style='white-space:nowrap;'>Tracking <a href='javascript:showhide(\"frn_plugin_hotjar_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'hotjar_function', 'frn_head_section', 'site_head_code');
		//Removed 1/28/19
		//add_settings_field('frn_plugin_ftr_contently', "<span style='white-space:nowrap;'>Contently Analytics <a href='javascript:showhide(\"frn_plugin_contently_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'contently_function', 'frn_head_section', 'site_head_code');
		add_settings_field('frn_plugin_opt', "<span style='white-space:nowrap;'>Visual Website Optimizer <a href='javascript:showhide(\"frn_plugin_opt_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_vwo_funct', 'frn_head_section', 'site_head_code');
		add_settings_field('frn_plugin_ftr_ppc', "PPC <span style='white-space:nowrap;'>Reporting <a href='javascript:showhide(\"frn_plugin_ppc_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'ppc_tracking_funct', 'frn_head_section', 'site_head_code');
	
	//External Links Section
	add_settings_section('site_extlinks_code', '<a name="links"></a><br />How to Handle External Links', '', 'frn_extlinks_section');
		add_settings_field('frn_plugin_ext_link_icon', "Add Icon after External <span style='white-space:nowrap;'>Links <a href='javascript:showhide(\"frn_ext_link_icon\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_extlinkicon_field', 'frn_extlinks_section', 'site_extlinks_code');
		add_settings_field('frn_plugin_target', "Open in New <span style='white-space:nowrap;'>Windows? <a href='javascript:showhide(\"frn_ext_link_target\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_extlinktarget_field', 'frn_extlinks_section', 'site_extlinks_code');

	//LHN section
	add_settings_section('site_lhn_code', '<a name="lhn"></a><br />LiveHelpNow Settings', 'text_before_lhn_form', 'frn_lhn_section');
		add_settings_field('frn_plugin_lhn_general', "General <span style='white-space:nowrap;'>Settings <a href='javascript:showhide(\"frn_plugin_lhn_general_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'lhn_general_options', 'frn_lhn_section', 'site_lhn_code');
		add_settings_field('frn_plugin_lhn_activate', "<span style='white-space:nowrap;'>Slideout <a href='javascript:showhide(\"frn_plugin_lhn_activate_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'lhn_activation', 'frn_lhn_section', 'site_lhn_code');
		add_settings_field('frn_plugin_lhn_autoinvite', "<span style='white-space:nowrap;'>Auto Invite <a href='javascript:showhide(\"frn_plugin_lhn_autoinvite\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'lhn_auto_invite', 'frn_lhn_section', 'site_lhn_code');
		add_settings_field('lhn_inpage_chat_btnid', "In-Page <span style='white-space:nowrap;'>Buttons <a href='javascript:showhide(\"frn_plugin_lhn_inpage_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'lhn_inpage', 'frn_lhn_section', 'site_lhn_code');
	
	//FOOTER settings field:(unique id, field label, function to add form field, menu page, page section)
	add_settings_section('site_footer_code', '<a name="footer"></a><br />Privacy Policy and Tracking', 'text_before_ftr_form', 'frn_footer_section');
		add_settings_field('frn_plugin_ftr_ppurl', "Privacy Policy Shortcode <span style='white-space:nowrap;'>URL <a href='javascript:showhide(\"frn_plugin_lhn_priv_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'ftr_priv_url_funct', 'frn_footer_section', 'site_footer_code');
		add_settings_field('frn_plugin_ftr', "Copyright & Privacy <span style='white-space:nowrap;'>Code Override <a href='javascript:showhide(\"frn_plugin_lhn_ftr_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'ftr_funct', 'frn_footer_section', 'site_footer_code');		
	
	//MOBILE footer options
	add_settings_section('site_mobile_code', '<a name="mobile"></a><br />Mobile Footer Options', 'text_before_mobile_form', 'frn_mobile_section');
		add_settings_field('frn_plugin_mob_cpyrt', "Mobile <span style='white-space:nowrap;'>Copyright <a href='javascript:showhide(\"frn_plugin_mobile_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'ftr_mobile_function', 'frn_mobile_section', 'site_mobile_code');
		add_settings_field('frn_plugin_ftr_bar', "Footer <span style='white-space:nowrap;'>Bar <a href='javascript:showhide(\"frn_plugin_ftrbar_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'ftr_footerbar_function', 'frn_mobile_section', 'site_mobile_code');
	
	//Social shortcode (no settings form, just info)
	add_settings_section('site_social', '<a name="social"></a><br />Social Network Features', '', 'frn_social_section');
		add_settings_field('frn_social', "Shortcode Options <a href='javascript:showhide(\"frn_plugin_social_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'frn_social_shortcode', 'frn_social_section', 'site_social');
		add_settings_field('frn_social_defimg', "Opengraph Images <a href='javascript:showhide(\"frn_plugin_defimg_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'frn_social_opengraph', 'frn_social_section', 'site_social');
	
	//Shortcode Fields Activations Checkboxes
	add_settings_section('shortcode_fields', '<a name="sc_fields"></a><br />Activate Shortcodes in Additional WP Fields', '', 'frn_sc_fields_section');
		add_settings_field('sc_fields', "Website Fields <a href='javascript:showhide(\"frn_plugin_sc_fields_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'plugin_options_sc_fields', 'frn_sc_fields_section', 'shortcode_fields');
	
	//Site Base shortcode - Rarely used, but can't remove since old sites still use it
	add_settings_section('site_sitebase_code', '<a name="urls"></a><br />(Old) Shortcode for Links and Images (Used on Old Sites)', 'frn_before_sitebase_info', 'frn_sitebase_section');
		add_settings_field('frn_site_base_URL', "Link Base URL <span style='white-space:nowrap;'>Override<a href='javascript:showhide(\"frn_plugin_sitebase_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'sitebase_sc_info', 'frn_sitebase_section', 'site_sitebase_code');
		add_settings_field('frn_image_base_URL', "Image Base URL <span style='white-space:nowrap;'>Override<a href='javascript:showhide(\"frn_plugin_sitebase_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'imagebase_sc_info', 'frn_sitebase_section', 'site_sitebase_code');
	
	//REPORT activation
	//add_settings_section('site_shortcode_report', '<a name="report"></a><br />Developer Resources', 'frn_shortcode_report_section_text', 'frn_shortcode_report_section');  //initially just shortcodes report -- changed it in admin, but to speed up dev left all functions shortcode related
		//add_settings_field('frn_shortcode_report_button', "Activation <span style='white-space:nowrap;'><a href='javascript:showhide(\"frn_shortcode_report_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_shortcode_report_field', 'frn_shortcode_report_section', 'site_shortcode_report');
		
}




/////
// PHONE_NUMBERS: FRN_PHONE displays the settings for the phone shortcode

// Creates the field for the phone number shortcode
if(!function_exists('frn_phone_field')) {
function frn_phone_field() {
	$phones = get_option('site_phone_number');
	
	//for copy text script
	$frn_shortcode='[frn_phone number="" action="Phone Clicks in ##page location##"]';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$frn_shortcode2='[frn_phone action="Phone Clicks in ##page location##" id="" class="" text="" desktop="" desktop_url="" image_url="" alt="" title="" css_style="" ]';
	$php_shortcode2 = '&lt;?php echo do_shortcode(\''.$frn_shortcode2.'\'); ?&gt;';
	$section_id = 'frn_plugin_phone';
	$shortcode_box = $section_id.'_box';

	if(!isset($phones['dialogtech'])) $phones_dt="";
		else $phones_dt=$phones['dialogtech'];
	if(!isset($phones['dialogtech_id'])) $phones_dt_id="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
		elseif($phones['dialogtech_id']=="") $phones_dt_id="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
		else $phones_dt_id=trim($phones['dialogtech_id']);
	$dt_activate_chkd="";
	if($phones_dt!=="") $dt_activate_chkd=" checked";

	if(!isset($phones['site_phone'])) $site_phone="";
		else $site_phone=trim($phones['site_phone']);
	
	echo "<input id='".$section_id."' name='site_phone_number[site_phone]' size='60' type='text' value='".(($site_phone!=="") ? $site_phone : null)."' /><br /> \n";
	echo '<br /><input type="checkbox" name="site_phone_number[dialogtech]" value="Activate" '.$dt_activate_chkd.' onClick="showhide(\''.$section_id.'_dtid\');" /> Activate DialogTech Service<br />';
	echo "<div id='".$section_id."_dtid' style='display:".(($dt_activate_chkd!=="") ? "block" : "none" ).";' >ID: <input name='site_phone_number[dialogtech_id]' size='50' type='text' value='".$phones_dt_id."' /></div> \n";
	
	?>
	<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="top">Shortcode for Text: </td>
			<td valign="top"><b><span class="frn_shortcode_sel"><?=$frn_shortcode;?></span></b> <font size="1">(remove location if going in content; add number="" if it\'s a unique number)</font></td>
		</tr><tr>
			<td valign="top">PHP: </td>
			<td valign="top"><b><span class="frn_shortcode_sel"><?=$php_shortcode;?></span></b> <font size="1">(most common for use in header.php)</font></td>
	</tr><tr>
			<td valign="top" style="padding-top:5px;border-top:1px solid gray;">Shortcode for Images: </td>
			<td valign="top" style="padding-top:5px;border-top:1px solid gray;"><b><span class="frn_shortcode_sel"><?=$frn_shortcode2;?></span></b></td>
	</tr><tr>
			<td valign="top">Image PHP: </td>
			<td valign="top"><b><span class="frn_shortcode_sel"><?=$php_shortcode2;?></span></b></td>
	</tr></table></div><font size="1">See FRN HELP tab at top-right for more customizing and tracking options</font>
	'."
	<div id="frn_plugin_phone_help" class="frn_help_boxes">
		<li><strong>Number Custom:  </strong>Add <b>number=""</b> to the shortcode if you want to use a number other than what's defined in the settings above. It'll then apply the normal linking for mobile devices and Analytics tracking.</li>
		<li><strong>Formatting:  </strong>Do not use letters in phone number or they will be stripped out in the link. The number format will display on the page just like you enter it here but will be formatted differently in the link itself (a friendly version for smartphones).</li>
		<li><strong>Styling:     </strong>You can use IDs by inserting id="" and CLASSes by adding class="".</li>
		<li><strong>Text:        </strong>You can link any text to a phone number call option. Just add the text="" variable to the shortcode and whatever you have inside it will be linked instead. Use mobile_text or desktop_text to be specific for device type.</li>
		<li><strong>Remove:     </strong>If you want to remove the item for a particular device, just use "remove" for the text value (e.g. desktop_text="remove"). It will completely remove the code and nothing will display.</li>
		<li><strong>No Text, Only CSS: </strong>If you want to solely rely on CSS, such as utilizing only an icon. Use text="empty" and no text will be inserted--only the A HREF code. On mobile, the link would show. On Desktop, only the SPAN tags would show as well as any ID or CSS you included. <b>IMPORTANT:</b> Keep in mind, that IDs and CLASSes are used in the link on mobile and in the SPAN tag for desktops.</li>
		<li><strong>Images with Numbers: </strong>The shortcode for images with phone numbers actually builds the image HTML so it can be wrapped in a SPAN for mobile linking. See the FRN Help tab above for details and more options.</li>
		<li><strong>Analytics:   </strong>In Analytics, all phone numbers that don't have an "action" set with the shortcode will use the event label "Phone Clicks (General)". The only time an action should be set is when the phone is in a prominent location on a page you wanted tracked separately. If the number is in the body of an article, you can just use the plain "[frn_plugin]" shortcode.</li>
		<li><strong>Activate Custom Fields: </strong>By default, this phone shortcode only works in frn_plugin fields and shortcodes, widget content (default), widget titles (activate at bottom), Yoast SEO fields (activate at bottom), and content (always). You can put them in other places by <a href="https://developer.wordpress.org/reference/functions/do_shortcode/" target="_blank">filtering the content through the shortcode activation</a>.</li>
		<li style="margin-top:15px;">
			<strong>Options for All Devices: </strong>
			<ul class="frn_level_2" >
				<li><b>text:</b> What you put here will be the text used for all devices.</li>
				<li><b>text="remove":</b> Using text="remove" will ONLY deactivate the shortcode for desktops. The initial concept is that we want to use the same text for all devices and just change it for desktops. That became confusing, but we had to maintain the original intent for sites that still use expect that.</li>
				<li><b>text="empty":</b> On desktops, this leaves the SPAN HTML with any defined ID or CLASS and no text. For mobile, the phone number wouldn't be displayed but the link for it would remain.</li>
				<li><b>id:</b> IMPORTANT: This is added to the link for mobile devices and the SPAN for desktops. </li>
				<li><b>class:</b> IMPORTANT: This is added to the link for mobile devices and the SPAN for desktops. </li>
				<li><b>category:</b> Don't use this unless necessary. The default is "Phone Numbers". But if you're analyzing a global contact options feature, you may want phone number clicks to be grouped with other contact clicks.</li>
				<li><b>action:</b> You MUST include "Phone Clicks" into whatever action you determine. That text is what makes sure phone number clicks are tracked as conversions in Analytics. Customize this only if you want to track a phone number position on a page seperate from other phone numbers. It's really helpful when testing a design position across many pages on a site if single pages may not have enough traffic to be conclusive.</li>
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" attribute will be ignored. For desktops only, if you use text="remove", the image will also be removed. This is used frequently on niche sites. Essentially, images are links on phones and not linked on desktop unless "desktop_url" is used.</li>
			</ul>
		</li>
		<li style="margin-top:15px;">
			<strong>Desktop Customizations: </strong>
			<ul class="frn_level_2" >
				<li><b>desktop_text:</b> Customize text by including just the "desktop" attribute. It will override the "text" and "mobile_text" attributes.</li>
				<li><b>desktop_text="remove":</b> Using desktop="remove" or text="remove" essentially deactivates the shortcode for desktops. You can use this if you just want a phone number option to disappear for desktop users.</li>
				<li><b>desktop_text="empty":</b> Leaves the SPAN HTML with any defined ID or CLASS but doesn't display text.</li>
				<li><b>desktop_id:</b> Overrides the normal "id" and "mobile_id" to customize any effects. Since phone numbers aren't linked on desktops, this is used in the SPAN tag.</li>
				<li><b>desktop_class:</b> Overrides the normal "class" and "mobile_class" to customize any effects. Since phone numbers aren't linked on desktops, this is used in the SPAN tag.</li>
				<li><b>desktop_category:</b> Overrides the normal "category" and "mobile_category" to customize what's reported to Analytics.</li>
				<li><b>desktop_action:</b> Overrides the normal "action" and "mobile_action" to customize what's reported to Analytics. If using desktop_url, the default action is "Desktop Switch with [url]" unless you fill this out.</li>
				<li><b>desktop_url:</b> If you'd like to switch out the number with a link to the contact page, for example, use both "desktop" and "desktop_url" attributes (e.g. <span class="frn_shortcode_sel">'desktop="Contact" desktop_url="/contact/"</span>). Mobile devices will still get a linked phone number, but it'll switch to just a "/contact/" link for desktop users. Only "desktop_url" is required. "Contact" is the default text unless you want something else. The TEXT attribute becomes a mobile-only attribute at that point, so you'll likely want both attributes if you want to use a text version for mobile.</li>
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" attribute will be ignored. For desktops only, if you use text="remove", the image will also be removed. This is used frequently on niche sites. For desktops, if you use an ID or CLASS, they will apply to the image, not the link.</li>
			</ul>
		</li>
		<li style="margin-top:15px;">
			<strong>Mobile Customizations: </strong>
			<ul class="frn_level_2" >
				<li><b>mobile_text:</b> Customize text by including just the "mobile" attribute. It will override the "text" and "desktop_text" attributes.</li>
				<li><b>mobile_text="remove":</b> Using mobile="remove" essentially deactivates the shortcode for smartphones and tablets (text="remove" will not affect mobile in order to maintain the shortcode's initial use concept). You can use this if you just want a phone number option to disappear for mobile users.</li>
				<li><b>mobile_text="empty":</b> Leaves the SPAN HTML with any defined ID or CLASS but doesn't display text. Useful if relying on CSS to provide an icon or other formatting.</li>
				<li><b>mobile_id:</b> Overrides the normal "id" and "mobile_id" to customize any effects.</li>
				<li><b>mobile_class:</b> Overrides the normal "class" and "mobile_class" to customize any effects.</li>
				<li><b>mobile_category:</b> Overrides the normal "category" and "mobile_category" to customize what's reported to Analytics. Default is "Phone Numbers".</li>
				<li><b>mobile_action:</b> Overrides the normal "action" and "mobile_action" to customize what's reported to Analytics. The default is "Phone Clicks [General]" unless specified.</li>	
				<li><b>Images:</b> If you put anything in the "image_url" attribute, the "text" and "mobile_text" attributes will be ignored. This is used frequently on niche sites.</li>
			</ul>
		</li>
		<li style="margin-top:15px;">
			<strong>DialogTech:</strong>
			<ul class="frn_level_2"  >
				<li>Activate this if this site has been set up in DialogTech's services. Zander is our typical contact for this vendor. 
				<li>This service allows us to track all phone calls in Analytics. Activating the service here adds their JavaScript code into the footer of all pages. Their JavaScript looks for the phone number you added above on all pages and replaces it with a reserved phone number for this site. 
				<li>As of Fall 2016, we try to reserve enough phone numbers to account for the peak of concurrent users on a site multiplied by our typical conversion rate. This makes sure if the expected number of people called around the same time, their individual activities would be appropriately recorded in Analytics.
				<li>If you change the phone number above, it will deactivate DialogTech tracking. Be sure to change the phone number in their settings and above ASAP to avoid any disruption.
				<li>Employees need to know that the phone numbers showing on the site are not FRN's. So they can't plan to use the number they find anywhere else. Furthermore, if people save the number in their phone, someday the number will stop working. But before that, their activity will inappropriately be reported to Analytics as a conversion.
				<li><a href="https://secure.dialogtech.com/login.php" target="_blank">Link to DialogTech login</a>
			</ul>
	</div>
	<?php
}}
if(!function_exists('frn_phone_mobile')) {
function frn_phone_mobile() {
	$options_phone = get_option('site_phone_number');
	//phone_linking = processing method
	if(isset($options_phone['phone_linking'])) $phone_linking=$options_phone['phone_linking'];
		else $phone_linking="";
	?>
	<strong>Shortcode Processor:</strong> 
	<select id='frnphone_linking' name='site_phone_number[phone_linking]'>
	  <option value="PHP" <?=($phone_linking=="PHP" || $phone_linking==="") ? "selected " : "" ; ?>>PHP (Most Efficient)</option>
	  <option value="JS" <?=($phone_linking=="JS") ? "selected " : "" ; ?>>JavaScript (not working as of v2.7)</option>
	  <option value="N" <?=($phone_linking=="N") ? "selected " : "" ; ?>>Just Display Numbers (no linking)</option>
	  <option value="R" <?=($phone_linking=="R") ? "selected " : "" ; ?>>Remove Numbers</option>
	</select> 
	
	<?php
	$auto_scan="C";  //default setting if not already set
	if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
	?>
	<br />
	<br />
	<strong>Auto Scan Patterns:</strong> 
	<select id='frnphone_auto_scan' name='site_phone_number[auto_scan]'>
	  <option value="DA" <?=($auto_scan=="D" || $auto_scan==="") ? "selected " : "" ; ?>>Disabled</option>
	  <option value="WC" <?=($auto_scan=="WC" || $auto_scan=="A") ? "selected " : "" ; ?>>Widgets & Content</option>
	  <option value="C" <?=($auto_scan=="C") ? "selected " : "" ; ?>>Only Main Content</option>
	  <option value="W" <?=$auto_scan=="W" ? "selected " : "" ; ?>>Only Widgets</option>
	  <!--<option value="A" <?=$auto_scan=="A" ? "selected " : "" ; ?>>Entire Page (JavaScript)</option> //disabling since issues with JavaScript autoscan-->
	</select> <br />
	<small>This looks for phone number patterns to add tracking to (no shortcode required)</small>
	
	<div id="frn_plugin_scantype_help" class="frn_help_boxes">
		<ul>
			<li>PHP is the more efficient and default option. It adds links around phone numbers before the page is loaded in the browser.</li>
			<li>The JavaScript processor adds SPAN tags around phone numbers for mobile phones only. Then when mobile devices visit a page, that triggers the JavaScript code to scan a page for SPANs using a specific ID and then turns those into linked numbers as the page loads. Page loading is a little slower but it works when PHP can't.</li>
			<li>When phone numbers using the shortcode are in fields other than content and widgets, JavaScript is the better option if you can\'t program the PHP shortcode in those areas. JavaScript looks for those within everything between the BODY tags.</li>
			<li>If the script for a JavaScript phone number program (like IfByPhone) is added to the phone field instead of a typical phone number, this system automatically switches to the JavaScript method of adding Analytics tracking and linking. You can turn the linking off all together by selecting "none" for the processing.</li>
			<li>Auto Scan: This scans the selected content area for phone number patterns. To be found, phone numbers must have a period, bracket, &lt; or &gt;, or space before and/or after the number. See the FRN Help tab above for all details.</li>
		</ul>
	</div>
	<?php
}}
//validate function--not really validating just trimming for storage
if(!function_exists('plugin_options_validate')) {
function plugin_options_validate($input) {
	//sets up an array in case more variables should be added
	$newinput['site_phone'] = trim($input['site_phone']);
	
	if(isset($input['dialogtech'])) {
		$newinput['dialogtech'] = $input['dialogtech'];
		$newinput['dialogtech_id'] = trim($input['dialogtech_id']);
	}
	else {
		$newinput['dialogtech'] = "";
		$newinput['dialogtech_id'] = "";
	}
	
	$newinput['auto_scan'] = $input['auto_scan'];
	$newinput['phone_linking'] = $input['phone_linking'];
	//$newinput['js_content_id'] = $input['js_content_id'];
	
	return $newinput;
}}
// Text between the section H2 and the settings field for shortcode
// must have been removed before 10/22/18. Disabled function call in site_phone_number add settings section function.
function frn_shortcode_section_text() { 
	echo '
	<script type="text/javascript">
		var change_help = document.getElementById("contextual-help-link");
		change_help.innerHTML = "<img src=\''.plugin_dir_url( __FILE__ ).'images/frn_icon_menu.png\' /> FRN Help";
	</script>
	';
}
//function frn_admin_head() {
//}
//if ( is_admin() ) add_action( 'admin_head', 'frn_admin_head' );

function load_custom_wp_admin_stuff() {
	wp_register_script( 'frn_wp_admin_script', plugin_dir_url( __FILE__ ). 'admin_scripts.js', false, '2.0.0' );
	wp_enqueue_script( 'frn_wp_admin_script' );
	wp_register_style( 'frn_wp_admin_css', plugin_dir_url( __FILE__ ). 'admin_styles.css', false, '2.1.0' );
	wp_enqueue_style( 'frn_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_stuff' );




//////
// DATA_TOOLS: Settings fields for HEAD Components

//Used for HEAD section form to add text between h2 and form fields
function text_before_head_form() {
	?>
	<p><span style="font-weight:normal">
		Install <a href="https://www.advancedcustomfields.com/my-account/" target="_blank">Advanced Custom Fields PRO plugin</a> for post-specific overrides. 
		You will need Jonny Stovall's login to gain access to the zip file you'll install. <a href="<?=$GLOBALS['frn_acf_start'];?>">Download and import this file</a> to install the custom fields used by this plugin.
	</span></p>
	<?php
}
// field for Google analytics id
function ga_id_field() {
	$options = get_option('site_head_code');
	
	
	
	?>
	<span style="white-space:nowrap;">ID: <input id='frn_plugin_ga' name='site_head_code[site_ga_id_value]' size='40' type='text' value='<?=(isset($options['site_ga_id_value']) ? $options['site_ga_id_value'] : null);?>' /> <a href="javascript:showhide('frn_plugin_ga_help')" ><img src="<?=$GLOBALS['help_image']; ?>" /></a></span><br /> 
	
	<?php //if($ga_testmode=="") { ?>
	<!--<input type='hidden' name='site_head_code[site_ga_test]' value=''>-->
	<?php //} ?>
	<div id='frn_plugin_ga_help' class='frn_help_boxes'>
		Leave blank if you don't want the tracking code to be on the site. If you want to override the defaults and force the Google code to show for everyone visiting the site, place a checkmark in the box for "Test Mode".
	</div>
	<?php //ga_404_field(); //Discontinued by Dax 11/22/16 ?>
	<?php //ga_ua_field();  //Discontinued by Dax 6/15/16?>
	<?php ga_dgrphx_field(); ?>
	<?php ga_ea_field(); ?>
	<?php 
}
//field for 404 page title
//Tried to report 404 errors to Analytics to more easily get 404 error totals in Analytics, but it's not working too well.
//Disabled this option since 9 of 10 times it was the default and the section is already too long. Can re-enable if we find the feature is needed. Discontinued by Dax 11/22/16
function ga_404_field() {
	$options_404 = get_option('site_head_code');
	//if($options_404['frn_ga_404']=="") $options_404['frn_ga_404']="Page Not Found";  //disabled since javascript handles this if it's blank. default = Page Not Found
	echo "<input id='frn_plugin_ga_404' name='site_head_code[frn_ga_404]' size='60' type='text' value='".(isset($options_404['frn_ga_404']) ? $options_404['frn_ga_404'] : null)."' />
	<div id='frn_plugin_ga_404_help' class='frn_help_boxes'>Leaving blank will use the default \"Page Not Found\" in tracking 404 events. Enter the beginning words of the page title for the 404 page--the words between the &lt;title&gt; tags in the HTML, also used in browser tabs. </div>
	";
}
//field for Universal Analytics (Discontinued 6/15/16)
function ga_ua_field() {
	$options_ua = get_option('site_head_code');
	
	//defaults
	$activate_chkd=" checked"; $deactivate_chkd=""; $frn_ga_ua="";
	if(isset($options_ua['frn_ga_ua'])) $frn_ga_ua=$options_ua['frn_ga_ua'];
	
	//if not defaults
	if($frn_ga_ua=="Deactivate") {$activate_chkd=""; $deactivate_chkd=" checked";}
	
	//onClick="disable_ga_features(\'frn_plugin_frn_ga_dgrphx\',\'frn_plugin_frn_ga_dgrphx2\')"; //used to deactivate form fields
	//onClick="enable_ga_features(\'frn_plugin_frn_ga_dgrphx\',\'frn_plugin_frn_ga_dgrphx2\')";
	echo '
	<input id="frn_plugin_frn_ga_ua" type="radio" name="site_head_code[frn_ga_ua]" value="Activate" '.$activate_chkd.' /> Activate <br />'."\n".'
	<input id="frn_plugin_frn_ga_ua" type="radio" name="site_head_code[frn_ga_ua]" value="Deactivate" '.$deactivate_chkd.' /> Deactivate'."\n".'
	<div id="frn_plugin_frn_ga_ua_help" class="frn_help_boxes">This is Google\'s new way of integrating Analytics into a site. You must convert this site\'s Analytics account to <a href="https://developers.google.com/analytics/devguides/collection/upgrade/guide" target="_blank" >Universal Analytics</a> via the account\'s admin and Property settings. Once Google lets you know it\'s complete, you can then select this option in the plugin below. Activating this will switch the GA code to this new version. For us, there are no additional features we\'ll use when compared to the classic version other than maybe a lighter page. It mostly helps with Adwords and if we provide logins on our sites (as of 4/15/14).</div>
	';
}
//field for DoubleClick Demographic data
function ga_dgrphx_field() {
	$options_demo = get_option('site_head_code');
	
	//defaults
	$activate_chkd=""; $deactivate_chkd=" checked"; $frn_ga_dgrphx="";
	if(isset($options_demo['frn_ga_dgrphx'])) $frn_ga_dgrphx = $options_demo['frn_ga_dgrphx'];
	
	//if not default
	if($frn_ga_dgrphx=="Activate") {$activate_chkd=" checked"; $deactivate_chkd="";}
	
	echo '
	<input id="frn_plugin_frn_ga_dgrphx" type="checkbox" name="site_head_code[frn_ga_dgrphx]" value="Activate" '.$activate_chkd.' /> Record Demographics <a href="javascript:showhide(\'frn_plugin_frn_ga_dgrphx_hlp\')" ><img src="'.$GLOBALS['help_image'].'" /></a><br />
	<div id="frn_plugin_frn_ga_dgrphx_hlp" class="frn_help_boxes">This affects the privacy policy. You must also activate this option in the site\'s Property settings (<a href="https://www.google.com/analytics/web/?hl=en#management/Settings/a33689825w61035006p62464467/%3Fm.page%3DPropertySettings/" target="_blank">FRN Events example</a>).  This will enable Google to report on the demographics and interests of those visiting the site based on what DoubleClick Advertising assumes about a person as they visit sites on the web. Learn more: <a href="https://support.google.com/analytics/answer/2819948?hl=en" target="_blank" >Activating Demographics</a></div>';

	/* //when combining into one analytics section, we needed to put them on one line, so a checkbox was better.
	echo '<!--
	<input id="frn_plugin_frn_ga_dgrphx" type="radio" name="site_head_code[frn_ga_dgrphx]" value="Activate" '.$activate_chkd.' /> Activate <br />'."\n".'
	<input id="frn_plugin_frn_ga_dgrphx2" type="radio" name="site_head_code[frn_ga_dgrphx]" value="Deactivate" '.$deactivate_chkd.' /> Deactivate'."\n".'
	-->';
	*/
}
//field for Enhanced Link Attribution for in-page link tracking
function ga_ea_field() {
	$options = get_option('site_head_code');
	
	//defaults
	$activate_chkd=""; $deactivate_chkd=" checked"; $frn_ga_ea="";
	if(isset($options['frn_ga_ea'])) $frn_ga_ea = $options['frn_ga_ea'];
	
	//if not default setting
	if($frn_ga_ea=="Activate") {$activate_chkd=" checked"; $deactivate_chkd="";}

	$ga_testmode=""; $site_ga_test="";
	if(isset($options['site_ga_test'])) $site_ga_test = $options['site_ga_test'];
	if($site_ga_test=="Test") $ga_testmode = " checked";

	$ga_cross_domain=""; $cross_trigger=""; $cross_sites="";
	if(isset($options['cross_trigger'])) $cross_trigger = $options['cross_trigger'];
	if($cross_trigger!=="") $ga_cross_domain = " checked";
	if(isset($options['cross_sites'])) $cross_sites = $options['cross_sites'];
	
	//Google provides a way to track more than one site. At the time of this coding, that was not necessary.
	//But if that happens, you'll need to turn the "cross_sites" variable into an array, create multiple input fields, and loop through inputs to create the array (and then loop through to determine the values for each input)
	
	?>
	<input type="checkbox" name="site_head_code[frn_ga_ea]" value="Activate"<?=$activate_chkd;?> /> Enhanced Click Tracking <a href="javascript:showhide('frn_plugin_frn_ga_ea_help')" ><img src="<?=$GLOBALS['help_image'];?>" /></a><br />
	<div id="frn_plugin_frn_ga_ea_help" class="frn_help_boxes">Enhanced link attribution starts tracking where links are clicked on a page. To activate, you must go to the site's Property settings in Analytics in the In-Page Analytics section and select "Use enhanced link attribution" (<a href="https://www.google.com/analytics/web/?hl=en#management/Settings/a33689825w61035006p62464467/%3Fm.page%3DPropertySettings/" target="_blank">FRN Events example</a>). Normally when looking at in-page analytics, if more than one link goes to the same page, they would all have the same numbers (i.e. pageviews). Activating this will tell Google to track how many clicks each individual link on a page actually gets. It\'s best if you are planning on optimizing a webpage on the site. Otherwise, keep it off. Activating this will not apply to data in the past--only going forward. Be sure to make a note in the Analytics charts that you made this change. Learn more: <a href="https://support.google.com/analytics/answer/2558867?hl=en" target="_blank" >enhanced link attribution</a></div>
	
	<input type="checkbox" name="site_head_code[site_ga_test]" value="Test"<?=$ga_testmode;?>> [Test Mode] <a href="javascript:showhide('frn_plugin_frn_ga_test_help')" ><img src="<?=$GLOBALS['help_image'];?>" /></a> <br />
	<div id="frn_plugin_frn_ga_test_help" class="frn_help_boxes">If you want to override the defaults and force the Google code to show for everyone visiting the site, place a checkmark in the box for "Test Mode".</div>
	
	<input type="checkbox" name="site_head_code[cross_trigger]" value="Activate"<?=$ga_cross_domain;?> onClick="showhide('frn_cross_domain_options');"> Cross Domain Tracking <a href="javascript:showhide('frn_plugin_frn_ga_cross_help')" ><img src="<?=$GLOBALS['help_image'];?>" /></a><br />
	<div id="frn_cross_domain_options" style="display:<?=(($cross_trigger!=="") ? "block" : "none" );?>">
		Other Website Domain: <input id="frn_plugin_ga_cross" name="site_head_code[cross_sites]" size="40" type="text" value="<?=$cross_sites;?>" /><br />
		<a href="javascript:showhide('ga_cross_tracking_code');">Show/hide tracking code</a> <br /> <br />
		<div id="ga_cross_tracking_code" style="display:none;">
			<code style="display: block; width: 700px; font-size: 10px;">
				<?php 
					$site_ga_id_value="";
					if(isset($options['site_ga_id_value'])) $site_ga_id_value = $options['site_ga_id_value'];
					if($cross_sites=="") $cross_sites="<span style='color:red; '>   MISSING DOMAIN!!! SAVE SETTINGS BEFORE COPYING THIS!!!!    </span>";
					$frn_domain = str_replace("http://","",str_replace("https://","",site_url())); ?>
				&lt;script&gt;<br />
				&nbsp;&nbsp;  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){<br />
				&nbsp;&nbsp;  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),<br />
				&nbsp;&nbsp;  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)<br />
				&nbsp;&nbsp;  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');<br />
				&nbsp;&nbsp;  <br />
				&nbsp;&nbsp;  ga('create', '<?=$site_ga_id_value;?>', 'auto', {'allowLinker': true});<br />
				&nbsp;&nbsp;  ga('require', 'linker');<br />
				&nbsp;&nbsp;  ga('linker:autoLink', ['<?=$frn_domain;?>'] );<br />
				&nbsp;&nbsp;  ga('set', 'dimension1', '2010-09-12');<!--Published Date date--><br />
				&nbsp;&nbsp;  ga('set', 'dimension2', '2014-10-27');<!--last modified date--><br />
				&nbsp;&nbsp;  ga('set', 'contentGroup1', 'Long 2 (1500-1999)');<!--word count buckets--><br />
				&nbsp;&nbsp;  ga('send', 'pageview');<br />
				&lt;/script&gt;
			</code>
		</div>
	</div>
	<div id="frn_plugin_frn_ga_cross_help" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li>Activate this only if you want to track users from this site to another. </li>
			<li>IMPORTANT: You must put the code below on the other website or this feature won't work. Our analytics will still be reported correctly; activating this won't affect our site's data. </li>
			<li>It is additional code to what they already have, and it goes only on the pages we want tracked in our own Analytics. It doesn't have to be on every page on their site.</li>
			<li>Our event websites are good sites to use this feature on. Often we use a third party to handle registrations, but we also want to know how well our ads perform. Turning on this feature will allow us to set up Goals in Analytics that reports on true conversions instead of just seeing how many people click on a registration button.</li>
			<li>Click the show/hide link above to show the code to place on the other website. You MUST provide the code below to the next website as well or the visitor activity on those sites won't be reported in Analytics. At the least, share this site's Google Analytics ID (<?=$site_ga_id_value;?>) with them so they can report to our account.</li>
			<li>It's also possible to use <a href="http://tagmanager.google.com/">Google Tag Manager</a> to set this up on our sites and theirs. We previously used Tag Manager to setup HeroesInRecovery.com registrations with Active.com. Check out that account if you want to know how to set it up. It's not easy to understand, but it'll give you a start. Dax, Trisha, Cherie, and other folks have access to the account.</li>
			<li>If you need to track visitors across more than one site, let Dax or Ben Wright (or other developer) know and he'll have to add the feature. A quick work around would be adding into the field the syntax Google requires for more than one site: <code>example1.com','example2.com</code></li>
			<li>To turn this feature off, you have to remove everything in the "other website domain" field.</li>
			<li><a href="https://support.google.com/analytics/answer/1033876?hl=en" target="_blank">Google's information on cross domain tracking</a></li>
		</ul>
	</div>
	<small style="margin-top:8px;display: block;"><a href="javascript:showhide('frn_custom_dimensions');">> Set dimension/group IDs</a></small>
	<div id="frn_custom_dimensions" class="frn_options_table" style="display: none; margin: 20px 0 0px 0px; float: left; padding: 10px;">
		<?php 


		/// CUSTOM DIMENTIONS FOR DATES & BUYERS JOURNEY
		$ga_dimensions="";
		$ga_pub_id=1; 	   if(!isset($options['ga_pub_id']))  	  $options['ga_pub_id']=$ga_pub_id;  	   if($options['ga_pub_id']!=="")     $ga_pub_id=$options['ga_pub_id'];
		$ga_mod_id=2; 	   if(!isset($options['ga_mod_id']))  	  $options['ga_mod_id']=$ga_mod_id;  	   if($options['ga_mod_id']!=="")     $ga_mod_id=$options['ga_mod_id'];
		$ga_prmtd_s_id=3;  if(!isset($options['ga_prmtd_s_id']))  $options['ga_prmtd_s_id']=$ga_prmtd_s_id;  if($options['ga_prmtd_s_id']!=="") $ga_prmtd_s_id=$options['ga_prmtd_s_id'];
		$ga_prmtd_e_id=4;  if(!isset($options['ga_prmtd_e_id']))  $options['ga_prmtd_e_id']=$ga_prmtd_e_id;  if($options['ga_prmtd_e_id']!=="") $ga_prmtd_e_id=$options['ga_prmtd_e_id'];
		?>
			<div style="width:50%;float:left;">
			<b>Custom Dimension IDs:</b> <a href="javascript:showhide('frn_plugin_ga_dates')" ><img src="<?=$GLOBALS['help_image'];?>" /></a>
			<table class="frn_options_table">
				<tr>
					<td>Pub Date:</td>
					<td><select id='ga_content_dim1' name='site_head_code[ga_pub_id]'>
						  <?php $i=1; while($i<=7) { //default=1?>
						  <option value="<?=$i;?>" <?=($ga_pub_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
						  <?php $i++;} ?>
						</select> 
					</td>
				</tr>
				<tr>
					<td>Mod Date:</td>
					<td><select id='ga_content_dim2' name='site_head_code[ga_mod_id]'>
						  <?php $i=1; while($i<=7) { //default=2?>
						  <option value="<?=$i;?>" <?=($ga_mod_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
						  <?php $i++;} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Promoted Start:</td>
					<td><select id='ga_content_dim3' name='site_head_code[ga_prmtd_s_id]'>
						  <?php $i=1; while($i<=7) { //default=3?>
						  <option value="<?=$i;?>" <?=($ga_prmtd_s_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
						  <?php $i++;} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Promoted End:</td>
					<td><select id='ga_content_dim4' name='site_head_code[ga_prmtd_e_id]'>
						  <?php $i=1; while($i<=7) { //default=4?>
						  <option value="<?=$i;?>" <?=($ga_prmtd_e_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
						  <?php $i++;} ?>
						</select>
					</td>
				</tr>
			</table>
			</div>
		<?php


		/// CONTENT GROUPING
		$ga_group_id=1;  if(!isset($options['ga_group_id']))  $options['ga_group_id']=$ga_group_id;  if($options['ga_group_id']!=="") $ga_group_id=$options['ga_group_id'];
		$ga_paj_id=2;    if(!isset($options['ga_paj_id']))    $options['ga_paj_id']=$ga_paj_id;      if($options['ga_paj_id']!=="")   $ga_paj_id=$options['ga_paj_id'];
		$ga_pajt_id=3;   if(!isset($options['ga_pajt_id']))   $options['ga_pajt_id']=$ga_pajt_id;    if($options['ga_pajt_id']!=="")  $ga_pajt_id=$options['ga_pajt_id'];
		?>
			<div style="width:50%;float:left;">
			<b>Content Group IDs:</b> <a href="javascript:showhide('frn_plugin_ga_groups')" ><img src="<?=$GLOBALS['help_image'];?>" /></a>
			<table class="frn_options_table">
			<tr>
				<td>Content Length:</td>
				<td><select id='ga_content_group1' name='site_head_code[ga_group_id]'>
					  <?php $i=1; while($i<=5) { //default=1?>
					  <option value="<?=$i;?>" <?=($ga_group_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
					  <?php $i++;} ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Patient Journey:</td>
				<td><select id='ga_content_group2' name='site_head_code[ga_paj_id]'>
					  <?php $i=1; while($i<=5) { //default=2?>
					  <option value="<?=$i;?>" <?=($ga_paj_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
					  <?php $i++;} ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Sub-Topic:</td>
				<td><select id='ga_content_group3' name='site_head_code[ga_pajt_id]'>
					  <?php $i=1; while($i<=5) { //default=3?>
					  <option value="<?=$i;?>" <?=($ga_pajt_id==$i) ? "selected " : "" ; ?>><?=$i;?></option>
					  <?php $i++;} ?>
					</select>
				</td>
			</tr>
			</table>
			</div>
			<div class="frn_ga_post_types" style="margin-top:10px;float: left;width: 100%;">
				<div><b>Limit Pub/Mod and Content Length to Post Types:</b></div>
				<?php
				//Due to the way we plan to analyze dates and content length, it's mostly an SEO feature and having unnecessary pages in the list isn't helpful. This allows us to exclude large bulks of unnecessary pages.
				//The buyers journey and promotions only report if something is set in the FRN Plugin Options for the specific pages. They don't need a global deactivation feature.
				//Global custom definitions/groups deactivation options (helps if a site has a lot of publicly visible custom post types we'd never want measured--keeps our data cleaner.)
				//This approach is similar to the related posts feature.
				$ga_custom_posts="";
				if(isset($options['ga_custom_posts'])) $ga_custom_posts = $options['ga_custom_posts'];
				$args=array(
					'public'   => true,
				);
				$post_types = get_post_types( $args, 'names' ); //get list of post types. 
				$at_least_one="";
				if($post_types) {
					$sel_count=0;
					foreach($post_types as $post_type) {
						if($post_type!=="attachment" && $post_type!=="revision" && $post_type!=="nav_menu_item") { 
							$selected="";
							if(is_array($ga_custom_posts)) {
								foreach($ga_custom_posts as $selected_type) {
									if($selected=="") {
										if($post_type==$selected_type) {
											$selected="checked"; $sel_count++;
											$at_least_one = "yes";
										}
									}
								}
							}
				?>
				<div style="float:left;width: 29%;min-width: 100px;overflow: hidden;white-space: nowrap;margin-left: 10px;" ><input type="checkbox" name="site_head_code[ga_custom_posts][]" value="<?=$post_type; ?>" <?=$selected;?> /> <?=$post_type; ?></div>
				<?php
						}
					}
					///////
					/// ANY option consideration
					//"Any" is NOT a post type, but it is a WP_query option. 
					//But we don't want "any" showing as a post type when doing a wp_query. The frontend of this feature removes "any" from an array when building the wp_query args
					//The wp_query default type searched is post, so having nothing checked is okay.
					//Since "any" is not a typical post type, we need to manually add it as an option
					//this is outside the loop since it would be duplicated if added to the ga_custom_posts check above
					if($at_least_one!=="yes") {
						if($ga_custom_posts=="") $any_select="checked";
						else $any_select="";
						if(is_array($ga_custom_posts)) {
							if(in_array("any",$ga_custom_posts)) { $any_select="checked"; }
						}
					}
				?>
				<div style="float:left;width: 29%;min-width: 100px;overflow: hidden;white-space: nowrap;margin-left: 10px;" ><input type="checkbox" name="site_head_code[ga_custom_posts][]" value="any" <?=$any_select;?> /> [All Types]</div>
				<?php
				} //ends requirement that wp post_types are returned

				//The following will likely never happen unless there is an miracle error
				else echo "
				<div><p>No \"Public\" Post Types Found</p></div>
					";
				?>
			</div>
		
			<div class="frn_help_boxes" id="frn_plugin_ga_dates" style="display: none;float: left;width: 100%;">
				<b>Custom Dimensions (Pub/Mod Dates)</b>
				<ul class="frn_level_1">
					<li>Custom dimensions are only available to use in custom reports and advanced segments.</li>
					<li>By default, the dimensions we list here are already reported to GA. But GA has to be setup to start storing them. In that process, you'll give it an appropriate label in GA. That's what will actually show in reports.</li>
					<li>Only the dates for page types of "page" or "post" are reported to Analytics.</li>
					<li><b>Pubication Date:</b> By default, this is reported to custom dimension 1. There is typically only one date reported to Analytics for each post --UNLESS-- someone goes in and manually changes the publish date after someone has visited the page. At that point, there would be more than one date reported for the post.</li>
					<li><b>Modified Date:</b> By default, this is reported to custom dimension 2. Because it's a modified date, this means that each post could have multiple dates reported to Analytics. But this enables us to test after which modification traffic improved (or declined). Prior to 9/18/18, Dax assumed this would be reported everytime, but a new page doesn't have a mod date, yet GA would get blank data sent. We stopped sending data if none present, but reports prior to that date may have many "not set" visits.</li>
					<li><b>Promotion Dates:</b>
						<ul class="frn_level_2">
							<li>Studies prove that promoted content performs better overall. But does our promotion methods produce positive results? This group will help us compare performance. </li>
							<li>You can change these dates as much as you'd like. So, if we promote for just one day and then a week another time or for another month again, the dates can change. Once a person visits the page and the promotion begins, we can't change the date in GA. But a page can have multiple date values. So, it's ok if this continually changes.</li>
							<li><b>Promotion Start Date:</b> The most helpful is the start date, but we can also compare end date. ACF custom field options for posts control the format reported to GA. It should be yyyy-mm-dd, to help with alphabetical sorting in reports.</li>
							<li><b>Promotion End Date</b> If you can enter a date for this, it could help when analyzing performance. One question it could answer is if the longer something is promoted, does it produce better results.</li>
						</ul>
					</li>
					<li><b>ACF Plugin Required: </b> Promotion dates can only be set if the ACF plugin and FRN Plugin Options are set up for the site. <a href="<?=$GLOBALS['frn_acf_start'];?>">If not already set up, download and import this JSON file</a>. Via the Custom Fields "Tools" menu, import the JSON file. If ACF is not already installed, use the <a href="https://www.advancedcustomfields.com/my-account/" target="_blank">Advanced Custom Fields PRO plugin</a> to enable the FRN Plugin's post-specific overrides. You will need Jonny Stovall's login to gain access to the zip file you'll install.</li>
					<li>We started sending custom dimensions October 2017 (August 2017 for just a couple of sites).</li>
					<li>If a UHS site already has a custom dimension set up with one of the two IDs we are using, this will overwrite those. You will need to select a different ID from the dropdowns to avoid a conflict.</li>
					<li><b>Turning On Custom Dimensions in Google Analytics:</b>
						<ul class="frn_level_2">
							<li><i>Here are the Steps: </i>
								<ol class="frn_level_2">
									<li>Go to the Analytics Admin.</li>
									<li>In the PROPERTY column, at the bottom click "Custom Definitions" and then click "Custom Dimensions". </li>
									<li>Click the red button to create a new one. You'll do this for each dimension/metric below.</li>
									<li>IMPORTANT: 
										<ul class="frn_level_2">
											<li>You can't repurpose the dimension ID after you start sending data to GA using that dimension.</li>
											<li>If an ID is aleady being used in the site's Analytics, be sure set up another one in GA and change it here so that the label in GA matches the ID you select here. Sites like Talbott already had something using ID 1, so all of ours are shifted up 1.</li>
											<li>Like most GA data, you can't delete or change data that's already recorded. So, thinking thoroughly through why and how you'll use the data, will help reduce changes and future confusion.</li>
											<li>If the ID or label changes for one site while all the rest are the same, any custom reports or advanced segments for the rest of the sites won't work for that one. So if data is blank or there is an error, double-check the IDs used in the GA Property settings.</li>
										</ul>
									</li>
									<li>For <b>dimension1:</b> "Published Date".</li>
									<li>For <b>dimension2:</b> "Modified Date". </li>
									<li>For <b>dimension3:</b> "Promotion Start Date".</li>
									<li>For <b>dimension4:</b> "Promotion End Date".</li>
									<li>Make sure "Hit" is selected and it's activated.</li>
									<li>Click Save.</li>
									<li>Click Done. The plugin has already added the JavaScript.</li>
								</ol>
							</li>
						</ul>
					</li>
					<li><a href="https://support.google.com/analytics/answer/2709829?hl=en&ref_topic=2709827" target="_blank">Here are Google's directions on how to set them up</a>.</li>
					<li><a href="https://support.google.com/analytics/answer/1033861?hl=en&ref_topic=2709827" target="_blank">Google's overview on custom dimensions</a></li>
				</ul>
			</div>
			<div class="frn_help_boxes" id="frn_plugin_ga_groups" style="display: none;float: left;width: 100%;">
				<b>Content Groups (Length and Buyers Journey)</b>
				<ul class="frn_level_1">
					<li>
						Content grouping allows us to report on our own groups of content. We chose to evaluate the performance of content length, buyers journey stage, and stage topic. 
						It allows us to compare performance of content with similar features or styles. 
						It's also easier in some of Google's reports to see pages before and after a group. 
						There is no way to create our own funnel. Plus people move all around in our journey, so we wouldn't be able to read a report correctly anyway.
						Listing pages and the homepage is not necessarily "content" and group settings will not be reported to Analytics.
					</li>
					<li><b>IMPORTANT:</b> 
						<ul class="frn_level_2">
							<li>The plugin automatically sends length to the site's GA property. But the journey stage and topic must be manually set on each page's or post's FRN Plugin Settings. </li>
							<li>Although the plugin reports these groups to GA, they will not be seen in Analtyics until you set up the groups in each "View" for each site.</li>
							<li>When setting up in GA, if you choose a different group ID or name for a site, different from others, and create an advanced segment you expect to work across all sites, you won't be able to apply that segment to ones that use something different.</li>
						</ul>
					</li>
					<li>We started sending content length October 2017.</li>
					<li>You cannot disable content group reporting with this plugin. You can only change the group ID it's reported to.</li>
					<li><b>CONTENT LENGTH:</b> Here are the groups you can expect to see in reports:
						<div style="margin-left:15px;font-weight:bold;">
							Short 1 &nbsp; (<500)<br />
							Short 2 &nbsp;(500-849)<br />
							Medium (850-1199)<br />
							Long 1 &nbsp; (1200-1499)<br />
							Long 2 &nbsp; (1500-1999)<br />
							Long 3 &nbsp; (>2000)
						</div>
					</li>
					<li><b>Buyers Journey Content Groups (started 9/18/18):</b><br />
						<ul class="frn_level_2">
							<li>The stages/topics available to be selected for our journey are controlled in the <a href="/wp-admin/edit.php?post_type=acf-field-group" target="_blank">Advanced Custom Fields FRN Plugin Options</a> form setup. </li>
							<li>IMPORTANT: Whatever you put here will be reported EXACTLY to Google Analytics. </li>
							<li>You'll never be able to change data already sent. So carefully think through the label convensions you provide. These will be used when trying to filter or build advanced segments off of.</li>
							<li>To select the journey stage or topic, go to a page/post in the admin and edit it. You'll see the dropdown in the FRN Plugin Options below Content.</li>
							<li>The journey topic is to help divide up a primary journey stage and get a little closer to the specific barrier or treatment stage.</li>
						</ul>
					</li>
					<li><b>ACF Plugin Required: </b> Post-specific settings can only be set if the ACF plugin and FRN Plugin Options are set up for the site. <a href="<?=$GLOBALS['frn_acf_start'];?>">If not already set up, download and import this JSON file</a>. Via the Custom Fields "Tools" menu, import the JSON file. If ACF is not already installed, use the <a href="https://www.advancedcustomfields.com/my-account/" target="_blank">Advanced Custom Fields PRO plugin</a> to enable the FRN Plugin's post-specific overrides. You will need Jonny Stovall's login to gain access to the zip file you'll install.</li>
					<li><b>To Activate Content Groups:</b>
						<ol class="frn_level_2">
							<li>Go to the Admin settings.</li>
							<li>You'll set these groups up for EVERY view you have access to.</li>
							<li>In that column click "Content Grouping".</li>
							<li>Click the red "New Content Grouping" button.</li>
							<li>Enter the name. As of 9/15/18, we have three groups:
								<ul class="frn_level_2">
									<li>Content Length (ID: 1)</li>
									<li>Patient Acquisition Journey/Buyers Journey (ID: 2)</li>
									<li>Acquisition Journey Topic (ID: 3)</li>
								</ul>
							</li>						
							<li>Choose group by tracking code. We are using the Universal Analytics approach as of Oct 2017, the Global Site Tag doesn't offer all of the things we use to report to Analytics.</li>
							<li>If our preferred ID is already taken in GA (unlikely) and if no data has been reported there for many months, then just change the label. But if it's actively getting data, then set up a new one and change the ID in the options above. Remember that this also means you'll need to set up a custom advanced segment for the site then.</li>
							<li>Click "done" and "save".</li>
							<li>Come back here and set the correct ID above.</li>
							<li>To read what Google says about setting them up, see the "Create a Content Grouping" section after clicking the Google link below.</li>
						</ol>
					</li>
					<li><a href="https://support.google.com/analytics/answer/2853546?hl=en&ref_topic=1727167#code" target="_blank">Google's resource on content groupings</a>. Read the "Create a Content Grouping" section.</li>
					<li><a href="http://cutroni.com/blog/2014/01/24/use-google-analytics-content-groups/" target="_blank">A non-Google resource from 2014</a></li>
				</ul>
			</div>
	</div><!--end custom dimensions DIV-->
	<?php



	/*  //when combining into one analytics section, we needed to put them on one line, so a checkbox was better.
	echo '
	<input id="frn_plugin_frn_ga_ea" type="radio" name="site_head_code[frn_ga_ea]" value="Activate" '.$activate_chkd.' /> Activate <br />'."\n".'
	<input id="frn_plugin_frn_ga_ea" type="radio" name="site_head_code[frn_ga_ea]" value="Deactivate" '.$deactivate_chkd.' /> Deactivate'."\n".'
	';
	*/
}

function hotjar_function() {
	$options_ftr = get_option('site_footer_code');  //must match second section of register_setting
	$activate_chkd=""; $deactivate_chkd="";
	if(isset($options_ftr['ftr_hotjar_radio'])) {
		if($options_ftr['ftr_hotjar_radio']=="Activate") { $activate_chkd=" checked"; }
		if($options_ftr['ftr_hotjar_radio']=="Deactivate" or $options_ftr['ftr_hotjar_radio']==="") { $deactivate_chkd=" checked"; }
	}
	else $deactivate_chkd=" checked";
	if(!isset($options_ftr['ftr_hotjar_id'])) $options_ftr['ftr_hotjar_id'] = "172468";
	if($options_ftr['ftr_hotjar_id']=="")     $options_ftr['ftr_hotjar_id'] = "172468";
	?>
	<input type="checkbox" name="site_footer_code[ftr_hotjar_radio]" value="Activate"<?=$activate_chkd;?> onClick="showhide('frn_hotjar');" /> Activate Hotjar
	<?php
	/* //switched to checkbox method
	echo '
	<input type="radio" name="site_footer_code[ftr_hotjar_radio]" value="Activate"'.$activate_chkd.' onClick="showhide(\'frn_hotjar\',\'block\');" />Activate <br />
	<input type="radio" name="site_footer_code[ftr_hotjar_radio]" value="Deactivate"'.$deactivate_chkd.' onClick="showhide(\'frn_hotjar\',\'none\');" />Deactivate <br />';
	*/
	?>
	<div id="frn_hotjar" style="display:<?=($deactivate_chkd!=="") ? "none" : "block";?>;" >Account ID: <input name="site_footer_code[ftr_hotjar_id]" size="10" type="text" value="<?=$options_ftr['ftr_hotjar_id'];?>" /></div>
	<div id="frn_plugin_hotjar_hlp" class="frn_help_boxes">
		Place a checkmark to record sessions in Hotjar and add the JavaScript to every page on the site. 
		Each site has a unique ID. Enter it into the ID field. Leaving it empty will also remove the Hotjar code. 
		Company employees and those logged in are automatically not tracked.
	</div>
	<?php
}

function frn_vwo_funct() {
	//ADDS VWO SCRIPT TO HEAD
	//Requires us to setup an account on the VWO site to work
	$options = get_option('site_head_code');
	$activate_chkd="";;
	if(!isset($options['vwo'])) $options['vwo']="";
	if($options['vwo']=="A") {
		$activate_chkd=" checked";
	}
	if(!isset($options['vwo_id'])) $options['vwo_id']="";
	if($options['vwo_id']=="") {
		$options['vwo_id']="341821";
	}
	?>
	<input type="checkbox" name="site_head_code[vwo]" value="A" onClick="showhide('frn_vwo_id');"<?=$activate_chkd;?> /> Activate VWO <br />
	<div id="frn_vwo_id" style="display:<?=($activate_chkd=="") ? "none" : "block";?>;" >
		Account ID: <input type="text" name="site_head_code[vwo_id]" value="<?=$options['vwo_id'];?>" size="10" />
	</div>
	<div id="frn_plugin_opt_hlp" class="frn_help_boxes">
		<li>This only adds the script to the HEAD section of code. You must set up testing scenarios in Visual Website Optimizer's platform for this to work.</li>
		<li>VWO is automatically deactivated for company employees unless Chartbeat is in "test" mode.</li>
		<!--<li>This script previously used Zander's PPC Optimizely account (ID: 1033590360). If we set up a separate account, we'll need to change that in the PHP part_analytics_features.php file.</li>
		<li><a href="https://app.optimizely.com/signin" target="_blank">Optimizely Login</a>: zander.jones@frnmail.com. You will need to get the password from Zander.</li>-->
	</div>
	<?php
}

function frn_test_funct() {
	//Adds a primary test mode setting that affects all analytics but GA.
	$options = get_option('site_head_code');
	$test_mode_checked=""; $test_mode="";
	if(isset($options['test_mode'])) $test_mode = $options['test_mode'];
	if($test_mode!=="") {
		$test_mode_checked=" checked";
	}
	?>
	<input type="checkbox" name="site_head_code[test_mode]" value="activate" <?=$test_mode_checked;?> /> Activate Test Mode
	<div id="frn_plugin_test_hlp" class="frn_help_boxes">
		<li>This feature only affects secondary analytic tools. Google Analytics has a seperate test mode (mostly due to company IP detection).</li>
		<li>Use this feature to ignore company IP and login exclusions.</li>
		<li>The only element that overrides the test mode is the activations for each tool. i.e. If the tool isn't activated, it won't show anyway.</li>
	</div>
<?php
}

function chrbt_chkbx_funct() {
	$options_ftr = get_option('site_footer_code');  //must match second section of register_setting
	
	//defaults
	$activate_chkd=" checked"; $deactivate_chkd=""; 
	if(!isset($options_ftr['ftr_chrbt_checkbox'])) $options_ftr['ftr_chrbt_checkbox']="";
	
	//if other than defaults
	if($options_ftr['ftr_chrbt_checkbox']=="Deactivate") {$activate_chkd=""; $deactivate_chkd=" checked";}
	?>
	<input type="radio" name="site_footer_code[ftr_chrbt_checkbox]" value="Activate" <?=$activate_chkd;?> onClick="showhide('frn_plugin_chrbt_options','block');" />Activate <br />
	<input type="radio" name="site_footer_code[ftr_chrbt_checkbox]" value="Deactivate" <?=$deactivate_chkd;?> onClick="showhide('frn_plugin_chrbt_options','none');" />Deactivate <br />
	<div id='frn_plugin_cht_hlp' class='frn_help_boxes'>
		Click "test" to turn on the code so that you as an admin can see it when viewing the source code. Choose "activate" to keep employee activity from being tracked. 
		Click "deactivate" to remove the chartbeat code for everyone visiting the site so that no pages in the site show in our real-time reporting monitors.
	</div>
	<div id="frn_plugin_chrbt_options" <?=(($deactivate_chkd!=="") ? ' style="display:none;" ' : null);?> >
		<br /> <b>Company:</b>
		<?php if(isset($options_ftr['ftr_chrbt_company'])) $ftr_chrbt_company=$options_ftr['ftr_chrbt_company'];
			else $ftr_chrbt_company=""; 
		?>
		<select id='frn_plugin_chrbt_comp' name='site_footer_code[ftr_chrbt_company]'>
		  <option value="none" <?=($ftr_chrbt_company=="none" || $ftr_chrbt_company==="") ? "selected " : "" ; ?>></option>
		  <option value="FRN" <?=($ftr_chrbt_company=="FRN" || $ftr_chrbt_company==="") ? "selected " : "" ; ?>>FRN</option>
		  <option value="UHS" <?=$ftr_chrbt_company=="UHS" ? "selected " : "" ; ?>>UHS</option>
		</select> 

		<br /> <br /><b>Category:</b>
		<?php if(isset($options_ftr['ftr_chrbt_category'])) $ftr_chrbt_category=$options_ftr['ftr_chrbt_category'];
			else $ftr_chrbt_category=""; 
		?>
		<select id='frn_plugin_chrbt_cat' name='site_footer_code[ftr_chrbt_category]'>
		  <option value="none" <?=$ftr_chrbt_category=="none" ? "selected " : "" ; ?>></option>
		  <option value="Domain" <?=($ftr_chrbt_category=="Domain" || $ftr_chrbt_category==="") ? "selected " : "" ; ?>>Domain (Default)</option>
		  <option value="Facility" <?=$ftr_chrbt_category=="Facility" ? "selected " : "" ; ?>>Facility</option>
		  <option value="Primary" <?=$ftr_chrbt_category=="Primary" ? "selected " : "" ; ?>>Primary</option>
		  <option value="Secondary" <?=$ftr_chrbt_category=="Secondary" ? "selected " : "" ; ?>>Secondary</option>
		  <option value="Niche" <?=($ftr_chrbt_category=="Niche") ? "selected " : "" ; ?>>Niche</option>
		  <option value="PPC" <?=$ftr_chrbt_category=="PPC" ? "selected " : "" ; ?>>PPC</option>
		</select>
	</div>
	<div id='frn_plugin_chtcat_hlp' class='frn_help_boxes'>
		The actual settings for FRN and UHS in Chartbeat is UHS=uhssites.com and FRN=allwebsites.com. 
		Categories are ordered by FRN priority. Select the category you want the site to fall within. In some Chartbeat reports, we can report on these seperately. We can't compare them side by side, but we can at least get a little more information. The default category is niche.
	</div>
		
<?php
}



function contently_function() {
	$options_ftr = get_option('site_footer_code');  //must match second section of register_setting
	$activate_chkd=""; $deactivate_chkd="";
	if(isset($options_ftr['ftr_contently_radio'])) {
		if($options_ftr['ftr_contently_radio']=="Activate") $activate_chkd=" checked";
		if($options_ftr['ftr_contently_radio']=="Deactivate" or $options_ftr['ftr_contently_radio']==="") $deactivate_chkd=" checked";
	}
	else $deactivate_chkd=" checked";
	
	?>
	<input type="checkbox" name="site_footer_code[ftr_contently_radio]" value="Activate" <?=$activate_chkd;?> onClick="showhide('frn_contently');" /> Activate Contently <small>(Ended Aug 1, 2017)</small>
	<?php
	/*
	echo '
	<input type="radio" name="site_footer_code[ftr_contently_radio]" value="Activate"'.$activate_chkd.' onClick="showhide(\'frn_contently\',\'block\');" />Activate <br />
	<input type="radio" name="site_footer_code[ftr_contently_radio]" value="Deactivate"'.$deactivate_chkd.' onClick="showhide(\'frn_contently\',\'none\');" />Deactivate <br />';
	*/
	?>
	<div id="frn_contently" <?=(($deactivate_chkd!=="") ? ' style="display:none;" ' : null );?> >siteID: <input name="site_footer_code[ftr_contently_id]" size="40" type="text" value="<?=(isset($options_ftr['ftr_contently_id']) ? $options_ftr['ftr_contently_id'] : null);?>" /></div>
	<div id="frn_plugin_contently_hlp" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li><b>DISCONTINUED:</b> Our Contently agreement ended August 1, 2017. It's likely we no longer even access to data. Disable at your convenience.</li>
			<li>The "siteID" can be found in Contently Analytics Installation Instructions (<a href="https://contently.com/analytics/2159/dashboard/install_tracker" target="_blank">example</a>). Place a checkmark to activate and add Contently JavaScript to the site. This is only beneficial for sites we have Contently writing for. Otherwise, it'll just mess up the Contently analytics. 
			<li>Each site has a unique ID. Enter it into the ID field. Leaving it empty will automatically remove the Contently code as well. 
			<li>Company employees and those logged in are automatically not tracked.
			<li><b>CONTENTLY PLUGIN:</b> 
				<ul class="frn_level_2">
					<li>Contently has a seperate plugin that does not control analytic data. Instead, it integrates with the WP backend to automatically publish content to our Posts section based on settings in the Contently platform. 
					<li><a href="http://integrations.contently.com/wordpress/contently.zip">Download the plugin here</a>
					<li>It has to be manually installed. 
					<li>The API key in the plugin needs to be provided by our Contently account rep.
				</ul>
			</li>
		</ul>
	</div>
	<?php
}

function ppc_tracking_funct() {
	$options_ppc = get_option('site_head_code');
	$activate_yahoo=""; 
	if(isset($options_ppc['yahoo'])) {
		if($options_ppc['yahoo']=="A") $activate_yahoo=" checked";
	}
	if(!isset($options_ppc['yahoo_id'])) $options_ppc['yahoo_id']="";
	if(!isset($options_ppc['yahoo_pixel'])) $options_ppc['yahoo_pixel']="";

	$activate_bing="";
	if(isset($options_ppc['bing'])) {
		if($options_ppc['bing']=="A") $activate_bing=" checked";
	}
	if(!isset($options_ppc['bing_id'])) $options_ppc['bing_id']="";

	$fb_activate=""; 
	if(isset($options_ppc['fb'])) {
		if($options_ppc['fb']=="A") $fb_activate=" checked";
	}
	$fb_empl=""; 
	if(isset($options_ppc['fb_empl'])) {
		if($options_ppc['fb_empl']=="D") $fb_empl=" checked";
	}
	 
	if(!isset($options_ppc['fb_init'])) $options_ppc['fb_init']=""; //default Facebook account
	if($options_ppc['fb_init']=="") $options_ppc['fb_init']="402262266824056";
	if(!isset($options_ppc['fb_pages'])) $options_ppc['fb_pages']=""; 
	if(!isset($options_ppc['fb_conv_type'])) $options_ppc['fb_conv_type']=""; 
	if(!isset($options_ppc['fb_conv_label'])) $options_ppc['fb_conv_label']="";
	if($options_ppc['fb_conv_label']=="") $options_ppc['fb_conv_label']="Lead";
	$fb_conv_type=$options_ppc['fb_conv_type'];
	?>
	<input type="checkbox" name="site_head_code[bing]" value="A" <?=$activate_bing; ?> onchange="showhide('bing_input','inline-block');" /> Activate Bing 
	<span id="bing_input" style="display:<?=($activate_bing=="") ? "none" : "inline-block" ;?>" >&nbsp; [ID: <input name="site_head_code[bing_id]" size="20" type="text" value="<?=$options_ppc['bing_id'];?>" />]</span><br />
	<input type="checkbox" name="site_head_code[yahoo]" value="A" <?=$activate_yahoo; ?> onchange="showhide('yahoo_input','inline-block');" /> Activate Yahoo 
	<span id="yahoo_input" style="display:<?=($activate_yahoo=="") ? "none" : "inline-block" ;?>" >&nbsp; [
		Project ID: <input name="site_head_code[yahoo_id]" size="20" type="text" value="<?=$options_ppc['yahoo_id'];?>" /> 
		Pixel ID:   <input name="site_head_code[yahoo_pixel]" size="10" type="text" value="<?=$options_ppc['yahoo_pixel'];?>" />
		]</span><br />
	<input type="checkbox" name="site_head_code[fb]" value="A" <?=$fb_activate; ?> onchange="showhide('frn_fb_analytics_options');" /> Activate Facebook  
	<div id="frn_fb_analytics_options" style="display:<?=($fb_activate=="") ? "none" : "block" ;?>" >
		<table class="frn_options_table">
		<tr>
			<td align="left" style="padding: 12px 5px 0 8px;">Init ID: </td>
			<td align="left"><input name="site_head_code[fb_init]" size="40" type="text" value="<?=($options_ppc['fb_init']!=="") ? $options_ppc['fb_init'] : null ;?>" /></td>
		</tr>
		<tr>
			<td align="left" style="padding: 8px 5px 0 8px;">Track Employees: </td>
			<td align="left"><input type="checkbox" name="site_head_code[fb_empl]" value="D" <?=$fb_empl; ?> /></td>
		</tr>
		<tr>
			<td align="left" style="padding: 12px 5px 0 8px;">Conv Type: </td>
			<td align="left"><select id="frn_plugin_fb_conv" name="site_head_code[fb_conv_type]" onchange="$type=document.getElementById('frn_plugin_fb_conv').value; if($type=='both' || $type=='pages') {showhide('frn_plugin_fb_page','block');showhide('frn_plugin_fb_page2','block');} else {showhide('frn_plugin_fb_page','none');showhide('frn_plugin_fb_page2','none');}">
			  <option value="click_events" <?=($fb_conv_type=="click_events" || $fb_conv_type=="") ? "selected " : null ; ?>>Click Events Only (default)</option>
			  <option value="both" <?=($fb_conv_type=="both") ? "selected " : null ; ?> >Click Events & Pages</option>
			  <option value="pages" <?=($fb_conv_type=="pages") ? "selected " : null ; ?> >Pages Only</option>
			</select></td>
		</tr>
		<tr>
			<td align="left" style="padding: 12px 5px 0 8px;">Conv Label: </td>
			<td align="left"><input name="site_head_code[fb_conv_label]" size="20" type="text" value="<?=($options_ppc['fb_conv_label']!=="") ? $options_ppc['fb_conv_label'] : "CompleteRegistration" ;?>" /></td>
		</tr>
		<tr>
			<td align="left" style="padding: 12px 5px 0 8px;"><span id="frn_plugin_fb_page" style="display:<?=($fb_conv_type=="both" || $fb_conv_type=="pages") ? "block" : "none" ; ?>;" >Conv URLs: </span></td>
			<td><input id="frn_plugin_fb_page2" name="site_head_code[fb_pages]" size="20" type="text" value="<?=($options_ppc['fb_pages']!=="") ? trim($options_ppc['fb_pages']) : null ;?>"  style="display:<?=($fb_conv_type=="both" || $fb_conv_type=="pages") ? "block" : "none" ; ?>;" /></td>
		</tr>
		</table>
	</div>
	<div id='frn_plugin_ppc_hlp' class='frn_help_boxes'>
		<b>Facebook Analytics Options:</b>
		<ul>
			<li><b>Init ID:</b> This is the ID Facebook assigns when you setup an advertising account.</li>
			<li><b>Track Employees:</b> By default, employees are not tracked. But if you are utilizing a third party and Google's Tag Manager, that feature isn't available. It's better do deactivate employee tracking so that data is consistent as people pass from our site to the third party registration sites.</li>
			<li><b>Conv Pages: </b>
			<ul class="frn_level_2"> 
				<li>One of the benefits to using the plugin's version for this instead of just manually inserting the code: conversion reporting is added to smartphone clicks and chat/email</li>
				<li>IMPORTANT: If conversions happen on another site than this one, you'll have to modify the code on that site to track conversions. Any conversions settings you put here will have no effect since this FRN Plugin isn't installed on the other site.</li>
				<li>If you want to track one or more pages as conversions, put the page's ID from this site in the Conv Pages field above. You can find that by going to edit the post and looking in the web address. You'll see "post=". The number after that is the ID.</li>
				<li>If you want to tag more than one page as a converting page, seperate the page IDs with commas (e.g. 1234,1235,1236).</li>
			</ul>
		</ul>
	</div>
	<?php
}



///////
// EXTERNAL_LINKS PROCESSING
///////

//field for removing external link icon (affects javascript)
function frn_extlinkicon_field() {
	$options_extlink = get_option('site_head_code');
	
	//defaults
	$activate_chkd=" checked"; $deactivate_chkd="";
	if(!isset($options_extlink['ext_link_icon'])) $options_extlink['ext_link_icon']="";
	
	//if not default
	if($options_extlink['ext_link_icon']=="D") {$activate_chkd=""; $deactivate_chkd=" checked";}
	
	?>
	<div style="float:right;width:60%;"><small>You can disable an icon on a specific link by adding <b><span class="frn_shortcode_sel">data-exticon="no"</span></b> to the &lt;a href&gt; portion.</small></div>
	<input id="frn_ext_link_input" type="radio" name="site_head_code[ext_link_icon]" value="A" <?=$activate_chkd;?> onClick="showhide('frn_ext_link_processing','block');" /> Activate Icon: <img src="<?=plugin_dir_url( __FILE__ );?>images/icon_ext_links.png" /><br />
	<input id="frn_ext_link_input" type="radio" name="site_head_code[ext_link_icon]" value="D" <?=$deactivate_chkd;?> /> Deactivate
	<div id="frn_ext_link_icon" class="frn_help_boxes">
		By default, an icon will show to the right of any external links found. 
		If you don't want that on the site, select deactivate above and it will remove the icons from the site.
	</div>
	<?php
}
//field for removing external link icon (affects javascript)
function frn_extlinktarget_field() {
	$options_exttarget = get_option('site_head_code');
	
	//defaults (external links never open in new windows)
	$new_window=""; $same_window=" checked"; $target_deact="";
	if(!isset($options_exttarget['ext_link_target'])) $options_exttarget['ext_link_target']="";

	//Start thinking...
	if($options_exttarget['ext_link_target']=="new") {$new_window=" checked"; $same_window=""; $target_deact="";}
	elseif($options_exttarget['ext_link_target']=="deactivate") {$new_window=""; $same_window=""; $target_deact=" checked";}
	else {$new_window=""; $same_window=" checked"; $target_deact="";} //default
	//FYI--"same" is never used as a trigger. All targets are removed unless "new" is set for ext_link_target
	
	if($target_deact!=="" && $options_exttarget['ext_link_icon']=="D" ) $frn_ext_link_proc_display="display:none;"; else $frn_ext_link_proc_display="display:block";

	?>
	<div style="float:right;width:60%;">
		<small>
			<b>Default:</b> new window for desktops; same window for mobile. Targets using "new" will work as if "_blank" were used. Any other target name will not be overwritten.
			<!--
			<b><span class="frn_shortcode_sel">keep_target="yes"</span></b> for PHP and 
			<b><span class="frn_shortcode_sel">data-target="yes"</span></b> for JavaScript to the &lt;a href&gt; portion.
			-->
		</small>
	</div>
	<input id="frn_ext_target_input" type="radio" name="site_head_code[ext_link_target]" value="new"<?=$new_window;?> onClick="showhide('frn_ext_link_processing','block');" /> New Windows<br />
	<input id="frn_ext_target_input" type="radio" name="site_head_code[ext_link_target]" value="same"<?=$same_window;?> onClick="showhide('frn_ext_link_processing','block');" /> Default<br />
	<input id="frn_ext_target_input" type="radio" name="site_head_code[ext_link_target]" value="deactivate"<?=$target_deact;?> /> Disable
	<div id="frn_ext_link_target" class="frn_help_boxes">
		<li>For mobile, no external links will open in a new window unless they use a specific name other than "new" or "_blank".
		<li>For desktop devices, the options above apply. The default is that all links should not open in a new window unless they use a target name other than "new" or "_blank". 
		<li>By selecting "New Window" it will make all external links open in a new window for desktops. Internal links will open in the same window.
		<li>If you want to keep some link targets, either select "new windows" above or add <b><span class="frn_shortcode_sel">keep_target="yes"</span></b> to A HREF code (e.g. &lt;a href="/link/" keep_target="yes"&gt;). For JavaScript, you'll need to use the data-target option. Data-target will work for both JS and PHP, although it's harder to remember.
		<li>If PHP is selected above, then this will only apply to external links in content or widgets. 
		<li>If you select JavaScript, then target codes will be removed for all external links, no matter where they are (e.g. even in menus). It unlikely this option will be helpful. It was just the first attempt and controlling these features.
		<li>To deactivate this feature altogether, select Disable.
	</div>
	<br />
	<br />
	<div id="frn_ext_link_processing" style="<?=$frn_ext_link_proc_display;?>">
		<?php
		$php_chkd=" selected "; $js_chkd="";
		if(isset($options_exttarget['ext_link_icon_type'])) {
			if($options_exttarget['ext_link_icon_type']=="js") {$js_chkd=" selected "; $php_chkd="";}
		}
		
		?>
		<b>Processing Technology: </b><a href='javascript:showhide("frn_ext_link_type")'><img src="<?=$GLOBALS['help_image'];?>"></a><br />
		<select id="frn_ext_link_input" name="site_head_code[ext_link_icon_type]">
			<option value="php"<?=$php_chkd;?>>PHP (Fastest)</option>
			<option value="js"<?=$js_chkd;?>>JavaScript (Currently Not Active)</option>
		</select>
		<div id="frn_ext_link_type" class="frn_help_boxes">The PHP version only reads content and widgets and does the thinking prior to sending the page, so it's faster for users. 
			<p>The JavaScript version is supposed to scan the entire page before adding the icon, but at last test Fall of 2016, it was not working. If this is an important element, please request someone from the DEV team to troubleshoot the feature.</p>
			<p>You'd only use this option if you have an external style sheet you can't change and it controls the size and background image of an A HREF. 
			If you can and the situation happens in various locations in the site, remove the background-image portion of the style and make it an in-line style. 
			Or add our <b>data-exticon="no"</b> to the A HREF and it will automatically disable the icon.</p>
		</div>
		<?php
		
		//defaults (external links will be added to content links only)
		$content_chkd=" selected"; $widgets_chkd=""; $both_chkd=""; 
		if(!isset($options_exttarget['ext_link_icon_scan'])) $options_exttarget['ext_link_icon_scan']="";
		
		//if not defaults (widgets or both)
		if($options_exttarget['ext_link_icon_scan']=="widgets") {$widgets_chkd=" selected"; $content_chkd="";}
			elseif($options_exttarget['ext_link_icon_scan']=="both") {$both_chkd=" selected"; $content_chkd="";}
		
		?>
		<br />
		<br />
		<b>Scanned Areas: </b><a href='javascript:showhide("frn_ext_link_area")'><img src="<?=$GLOBALS['help_image'];?>"></a><br />
		<select id="frn_ext_link_input" name="site_head_code[ext_link_icon_scan]">
			<option value="content"<?=$content_chkd;?>>Content Only</option>
			<option value="widgets"<?=$widgets_chkd;?>>Widgets Only</option>
			<option value="both"<?=$both_chkd;?>>Both</option>
		</select> <small>(only works when PHP technology option chosen)</small>
		<div id="frn_ext_link_area" class="frn_help_boxes">
			Settings in this dropdown only apply if PHP selected above. 
			The content setting represents only the body of any post. Not the title, Meta data or any other field in the admin. 
			Widgets is as it sounds. It scans all widget locations in the site for external links.
		</div>
	</div>
	<?php
}
//Chooses whether php adds the icon or if javascript does (PHP faster for user and only looks in content and widgets but can't read external CSS files for background-image)
function frn_extlinkicon_type() {
	
}
function frn_extlinkicon_area() {
	
}




// cleans up data before stored using plugin_admin_init
function plugin_options_head($input) {

	$input['site_ga_id_value'] 	= 	trim($input['site_ga_id_value']);
	$input['frn_ga_404'] 		= 	trim($input['frn_ga_404']);
	$input['fb_init']			= 	trim($input['fb_init']);
	$input['fb_pages'] 			= 	trim($input['fb_pages']);
	
	//clearing default IDs to allow default overwrites later
	if($input['ga_pub_id']=="1")	$input['ga_pub_id']=""; //dimension
	if($input['ga_mod_id']=="2")	$input['ga_mod_id']=""; //dimension
	if($input['ga_group_id']=="1")	$input['ga_group_id']=""; //content length group
	if($input['ga_paj_id']=="2")	$input['ga_paj_id']=""; //buyers journey stage or rehab decision journey or patient acquisition journey group
	if($input['ga_pajt_id']=="3")	$input['ga_pajt_id']=""; //sub-journey topic group
	if($input['ga_prmtd_id']=="4")	$input['ga_prmtd_id']=""; // promoted content group

	$input['fb_conv_label'] 	= 	trim($input['fb_conv_label']);
	if($input['fb_conv_label']=="CompleteRegistration") 
									$input['fb_conv_label']=""; //to allow default overwrite later
	else $input['fb_conv_label']=	$input['fb_conv_label'];

	$input['cross_sites']		= 	trim($input['cross_sites']);
	if(stripos($input['cross_sites'],"http")>=0) {
		$input['cross_sites'] = 	str_replace("http://","",str_replace("https://","",$input['cross_sites']));
	}
	
	return $input;
}





///////
// LIVE_HELP_NOW Options


// Text before LHN form
function text_before_lhn_form() {
	//return "";
}

// General options all features use
function lhn_general_options() {
	$options_lhn = get_option('site_lhn_code');
	if(!isset($options_lhn['contactpg_remove'])) $options_lhn['contactpg_remove']="";
	if(!isset($options_lhn['lhn_dept_id'])) $options_lhn['lhn_dept_id']="";

	//load result from external DB
	//function is in the part_live_chat.php file
	$lhn_db_phonecode=trim(frn_external_db()); //this function on lines after 1800
	//echo "test: " . $lhn_db_phonecode;
	if(isset($options_lhn['lhn_phonecode'])) $lhn_wp_phonecode=trim($options_lhn['lhn_phonecode']);
		else $lhn_wp_phonecode="";	
	
	$frn_central_dba=""; $frn_central_db=""; $frn_save_domain="";
	//check if both are blank
	if($lhn_db_phonecode=="[none]" && $lhn_wp_phonecode==="") $frn_central_db=""; //means that everything is okay when they are both blank -- we don't intend on the site having any code
	elseif($lhn_db_phonecode==="") $frn_central_db="<small>Nothing was returned when trying to connect to the database. This shouldn't happen. Contact Dax.</small>"; //the wordpress value may still be okay
	elseif($lhn_db_phonecode=="[error]") $frn_central_db="<small>There was an error trying to connect to the central database. Contact Matt.</small>"; //the wordpress value is likely still ok
	elseif($lhn_db_phonecode!=$lhn_wp_phonecode) {
		//since no error, and they are both not blank at the same time, then we'll need to tell the values are different
		if($lhn_wp_phonecode==="") {
			$frn_central_db="<small>*WP is blank; code pulled from the FRN Plugin code</small>";
			$frn_central_dba="*";
			//since WP blank and ext db is not, define it with what the ext DB has as long as it's not returning "none"
			if($lhn_db_phonecode!="[none]") $lhn_wp_phonecode=$lhn_db_phonecode;
			//if it does equal none, then we'll let the wp variable stay blank
		}
		else {
			//by this point, wp has a value (i.e. not blank), and the ext db isn't blank and doesn't have an error
			//since different, we'll default to the WP version since it's more likely where we'll go to change the values
			
			if($lhn_db_phonecode=="[none]") $frn_central_db="<small> The domain was found in the external database, but the value is blank. ".$lhn_db_phonecode."</small>";
				else $frn_central_db="<small> The display code is different than what's in the FRN Plugin code: \"".$lhn_db_phonecode."\".</small>";
			
			/*
				//give an option to save the wp setting to the external DB
				$activate_dchkd=""; $deactivate_dchkd="";
				if(isset($options_lhn['lhn_domain_add'])) {
					if($options_lhn['lhn_domain_add']=="Save") $activate_dchkd=" checked";
						else if($options_lhn['lhn_domain_add']=="Not" or $options_lhn['lhn_domain_add']=="") $deactivate_dchkd=" checked";
				}
				else $deactivate_dchkd=" checked";
				
				
				//Disabled the following since the external DB was more trouble than it's worth activating for all sites due to IPs and security. Keeping in case we ever need the code to activate for smaller site groupings.
				$frn_save_domain = "\n".'
		<div class="frn_options_table" style="width:300px;"><table class="frn_options_table"><tr>
		<td style="white-space:nowrap; padding-bottom:8px;">
			<input id="frn_plugin_lhn_dsave" type="radio" name="site_lhn_code[lhn_domain_add]" value="Save"'.$activate_dchkd.' /> Yes<br />
			<input id="frn_plugin_lhn_dsave" type="radio" name="site_lhn_code[lhn_domain_add]" value="Not"'.$deactivate_dchkd.' /> No
		</td>
		<td style="border-left:1px gray solid;padding-left:5px;">Add this domain to the external/central FRN display codes database?</td>
		</tr>
		</table></div>'."\n";*/
		}
	}
	//else $frn_central_db="<small>This shouldn't be showing. Here are the values: WP: ".$lhn_wp_phonecode."; Ext DB: ".$lhn_db_phonecode."</small>";

	$default_dept = "15027";
	$frn_domain = str_replace("http://","",str_replace("https://","",str_replace("www.","",home_url())));
	if($frn_domain=="rehabandtreatment.com") $default_dept = "17982";


	echo '
	<table class="frn_options_table"><tr>
			<td align="left">Window ID:</td>
			<td><input id="frn_plugin_lhn_window" type="text" name="site_lhn_code[lhn_window]" value="'.(isset($options_lhn['lhn_window']) ? $options_lhn['lhn_window'] : null).'" />'."\n".'</td>
	</tr><tr>
			<td align="left">Department ID:</td>
			<td><input id="frn_plugin_lhn_dept" type="text" name="site_lhn_code[lhn_dept_id]" value="'.$options_lhn['lhn_dept_id'].'" /> <small>(default: '.$default_dept.')</small>
	</tr><tr>
			<td align="left">FRN Call-In Code'.$frn_central_dba.':</td>
			<td><input id="frn_plugin_lhn_display" type="text" name="site_lhn_code[lhn_phonecode]" value="'.$lhn_wp_phonecode.'" /><div style="width:300px;">'.$frn_central_db."</div>\n".$frn_save_domain.'</td>
	</tr></table>
	<div id="frn_plugin_lhn_general_hlp" class="frn_help_boxes">
		<ul>
			<li>Go into LiveHelpNow settings for Chat Window. You\'ll see a list of custom windows starting with their "Window ID".</li>
			<li>You can find the Department ID by going into LiveHelpNow "My Account" settings and clicking "Departments". The Call Center will define a department a rep is assigned to in order to spread out the load. Our default is "Help Desk" with ID "15027". PPC is "17982". But we can have as many "departments" as the Call Center would like. First they need to be added to LHN settings and then the ID needs to be entered here for the appropriate site.</li>
			<li>The call-in code here should show in the agent\'s chat window in order to better give credit to the appropriate site for the contact. Within this plugin, we have a list of all the call-in codes for every domain. If the field above is blank, that means we don\'t have a call-in code for the domain and no code will show for the agent. That\'s not a big deal. It\'s merely to help reduce agent errors.</li>
		</ul>
	</div>
	';


}

//field for lhn slideout activation radio button
function lhn_activation() {
	$options_lhn = get_option('site_lhn_code');
	
	//defaults
	$activate_chkd=" checked"; $deactivate_chkd=""; 
	$frn_cp_activate=" checked"; $frn_cp_deactivate="";	
	if(!isset($options_lhn['lhn_activation'])) $options_lhn['lhn_activation']="";
	if($options_lhn['contactpg_remove']=="No") {$frn_cp_activate=""; $frn_cp_deactivate=" checked";}
	
	//if set to anything but defaults
	if($options_lhn['lhn_activation']=="Deactivate") {$activate_chkd=""; $deactivate_chkd=" checked";}
	
	
	//if(!isset($options_lhn['lhn_ph_slideout'])) $options_lhn['lhn_ph_slideout']="";  //Javascript only trigger discontinued
	//if($options_lhn['lhn_ph_slideout']=="Include") {$phoneinclude_chkd=" checked";} //Javascript only trigger discontinued
	
	
	if($deactivate_chkd!=="") $lhn_slideout_display="display:none;"; else $lhn_slideout_display="display:block";
	
	echo '
	<input id="frn_plugin_lhn_activate" type="radio" name="site_lhn_code[lhn_activation]" value="Activate" '.$activate_chkd.' onClick=\'showhide("lhn_slideout_options","block");\' /> Activate <br />'."\n".'
	<input id="frn_plugin_lhn_activate" type="radio" name="site_lhn_code[lhn_activation]" value="Deactivate" '.$deactivate_chkd.' onClick=\'showhide("lhn_slideout_options","none");\' /> Deactivate'."\n".'<br />
	<div id="frn_plugin_lhn_activate_hlp" class="frn_help_boxes">This feature only activates the floating LiveHelpNow slideout button on the right side of the page. Slideout is automatically disabled on pages with "Contact" in the page\'s title field unless "No" is selected below. You can get page IDs by going to edit a page or hovering over a page\'s edit link. It\'s the number after "post".</div>
	<table id="lhn_slideout_options" class="frn_options_table" style="'.$lhn_slideout_display.'"><tr>
			<td align="left">Remove on Contact Page: </td>
			<td><input id="frn_plugin_lhn_contactpg" type="radio" name="site_lhn_code[contactpg_remove]" value="Yes" '.$frn_cp_activate.' /> Yes &nbsp; '."\n".'
			<input id="frn_plugin_lhn_contactpg" type="radio" name="site_lhn_code[contactpg_remove]" value="No" '.$frn_cp_deactivate.' /> No'."\n".'</td>
	</tr><tr>
			<td align="left">Remove On Page IDs: </td>
			<td><input id="frn_plugin_lhn_contactpg" type="text" name="site_lhn_code[tabdisable_pgID]" value="'.(isset($options_lhn['tabdisable_pgID']) ? $options_lhn['tabdisable_pgID'] : null).'" /><br />'."
			<font size='1'>Separate page IDs by a comma.</font></td>
	</tr></table>";

}
//fields for lhn auto-invite window
function lhn_auto_invite() {
	$options_lhn_auto = get_option('site_lhn_code');
	
	//defaults
	$auto_activate_chkd=""; $auto_deactivate_chkd=" checked";
	$chime_activate_chkd=""; $chime_deactivate_chkd=" checked";
	
	//make sure variables defined at the least
	if(!isset($options_lhn_auto['lhn_autoinvite'])) $options_lhn_auto['lhn_autoinvite']="";
	if(!isset($options_lhn_auto['lhn_autoinvite_chime'])) $options_lhn_auto['lhn_autoinvite_chime']="";
	
	//if other than defaults
	if($options_lhn_auto['lhn_autoinvite']=="Activate") {$auto_activate_chkd=" checked"; $auto_deactivate_chkd="";}	
	if($options_lhn_auto['lhn_autoinvite_chime']=="Activate") {$chime_activate_chkd=" checked"; $chime_deactivate_chkd="";}
	if($auto_deactivate_chkd!=="") $autoinvite_display="display:none;"; else $autoinvite_display="display:block";
	
	echo '
	<div id="frn_plugin_lhn_autoinvite" class="frn_help_boxes">These settings will activate and customize the auto-invite pop-up for this site. Adding keywords (or anything into that box) will disable the auto-invite on all pages except for post or page titles that have those keywords in them.</div>
	<input id="frn_plugin_lhn_autoinv" type="radio" name="site_lhn_code[lhn_autoinvite]" value="Activate" '.$auto_activate_chkd.' onClick=\'showhide("auto_invite_options","block");\' /> Activate <br />'."\n".'
	<input id="frn_plugin_lhn_autoinv" type="radio" name="site_lhn_code[lhn_autoinvite]" value="Deactivate" '.$auto_deactivate_chkd.' onClick=\'showhide("auto_invite_options","none");\' /> Deactivate'."\n".'
	<table id="auto_invite_options" class="frn_options_table" style="'.$autoinvite_display.'" >
	<tr>
			<td align="left">Chime:</td>
			<td><input id="frn_plugin_lhn_autoinv_chime" type="radio" name="site_lhn_code[lhn_autoinvite_chime]" value="Activate" '.$chime_activate_chkd.' /> Activate <br />'."\n".'
			<input id="frn_plugin_lhn_autoinv_chime" type="radio" name="site_lhn_code[lhn_autoinvite_chime]" value="Deactivate" '.$chime_deactivate_chkd.' /> Deactivate'."\n".'</td>
	</tr>
	<tr>
			<td align="left">LHN Auto Invite ID:</td>
			<td><input id="frn_plugin_lhn_autoinv_id" type="text" name="site_lhn_code[lhn_autoinvite_id]" value="'.(isset($options_lhn_auto['lhn_autoinvite_id']) ? $options_lhn_auto['lhn_autoinvite_id'] : null).'" />'."\n".'</td>
	</tr>
	<tr>
			<td align="left" style="vertical-align:top;">Custom Message:</td>
			<td><textarea id="frn_plugin_lhn_autoinv_mssg" name="site_lhn_code[lhn_autoinvite_mssg]" cols="37" rows="3" />'.(isset($options_lhn_auto['lhn_autoinvite_mssg']) ? $options_lhn_auto['lhn_autoinvite_mssg'] : null)."</textarea>\n".'</td>
	</tr>
	<tr>
			<td align="left" style="vertical-align:top;">Auto Activate Keywords:</td>
			<td><textarea id="frn_plugin_lhn_autoinv_kw" name="site_lhn_code[lhn_autoinvite_keywords]" cols="37" rows="2" />'.(isset($options_lhn_auto['lhn_autoinvite_keywords']) ? $options_lhn_auto['lhn_autoinvite_keywords'] : null)."</textarea>\n".'
			<br />&nbsp;<small>Separate keywords by a comma.</small></td>
	</tr>
	</table>';
}
//field for lhn in-page shortcode buttons activation
function lhn_inpage() {
	$options_lhn = get_option('site_lhn_code');
	
	//for copy text script
	$frn_shortcode='[lhn_inpage button="[chat or email]" id="[chat button ID; or text ID class]" text="" offline="[chat only]" class="[added to a link only; offline text not affected]" url="[email only]" ]';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$section_id = 'frn_plugin_lhn_inpage';
	$shortcode_box = $section_id.'_box';
	$activate_chkd=" checked"; $deactivate_chkd="";
	if(!isset($options_lhn['lhn_inpageact'])) $options_lhn['lhn_inpageact']="";
	if($options_lhn['lhn_inpageact']=="Deactivate") {$activate_chkd=""; $deactivate_chkd=" checked";}
	if($deactivate_chkd!=="") $in_page_display="display:none;"; else $in_page_display="display:block";
	
	echo '
	<input id="'.$section_id.'" type="radio" name="site_lhn_code[lhn_inpageact]" value="Activate" '.$activate_chkd.' onClick="showhide(\'in_page_options\',\'block\');" /> Activate <br />'."\n".'
	<input id="'.$section_id.'" type="radio" name="site_lhn_code[lhn_inpageact]" value="Deactivate" '.$deactivate_chkd.' onClick="showhide(\'in_page_options\',\'none\');" /> Deactivate<br />'." \n
	<div id=\"in_page_options\" style=\"".$in_page_display."\">
		<table class=\"frn_options_table\" ><tr>
				<td>Chat Button ID:</td>
				<td><input id='frn_plugin_lhn_chat' name='site_lhn_code[lhn_inpage_chat_btnid]' size='10' type='text' value='".(isset($options_lhn['lhn_inpage_chat_btnid']) ? $options_lhn['lhn_inpage_chat_btnid'] : null)."' /></td>
			<tr></tr>
				<td>Email Button URL:</td>
				<td><input id='frn_plugin_lhn_email' name='site_lhn_code[lhn_inpage_email_btnurl]' size='30' type='text' value='".(isset($options_lhn['lhn_inpage_email_btnurl']) ? $options_lhn['lhn_inpage_email_btnurl'] : null)."' /></td>
		</tr></table>".'
		<div class="frn_options_table"><table class="frn_options_table"><tr>
				<td valign="top">Shortcode: <font size=1 style="white-space:nowrap;">(most common)</font></td>
				<td valign="top"><b><span class="frn_shortcode_sel">'.$frn_shortcode.'</span></b></td>
			</tr><tr>
				<td valign="top">PHP: </td>
				<td valign="top"><b><span class="frn_shortcode_sel">'.$php_shortcode.'</span></b></td>
		</tr></table></div>
		
	';
	?>
		<div id="frn_plugin_lhn_inpage_hlp" class="frn_help_boxes">
			<h3>THE BASICS:</h3>
			<ul class="frn_level_1">
				<li><b>TIP: </b>: Leaving the button id and email url fields blank will default to our standard LHN buttons. Selecting to deactivate the buttons here will turn them off everywhere on the site where the shortcode is used.</li>
				<li><b>[lhn_inpage]:</b> Use the <b>[lhn_inpage]</b> shortcode to display the default LHN image buttons on a page side-by-side.</li>
				<li><b>button="":</b> button="email" shows just the email button(<b>[lhn_inpage button="email"]</b>). button="chat" shows just the chat (<b>[lhn_inpage button="chat"]</b>). If you're using the "text" version, as long as you use "chat" or "email" in your text, you won't need this attribute.</li>
				<li><b>email_url="":</b> To use a custom email button, add <b>email_url=""</b> to the shortcode.</li>
				<li><b>TEXT & id="":</b> If you include a "text" attribute, then "id" will switch to being used as a CSS ID for styling.</li>
				<li><b>IMAGE & id="####":</b> To customize a chat button image on just one page, upload your button image to the LHN servers and add <b>id="####"</b> to the shortcode (replacing the #### with the button id). </li>
			</ul>

			<h3>BASIC LINK ATTRIBUTES:</h3>
			<ul class="frn_level_1">
				<li><b>title="":</b> Used only on the A tag of links. Helpful if you want to display a hover message that extends the message. E.g. We're available 24/7.</li>
				<li><b>style="":</b> Used only in the A tag of links. Makes it easier to customize one particular use.</li>
				<li><b>class="":</b> Used only in the A tag of links. </li>
			</ul>
			
			<h3>TEXT/CSS VERSIONS:</h3>
			<ul class="frn_level_1">
				<li><b>text="":</b> Add text="[Your text]" (<span class="frn_shortcode_sel">[lhn_inpage button="email" text="Email Us"]</span>) if you want to rely on CSS to style the buttons.</li>
				<li><b>text="empty":</b> If you want to solely rely on CSS, such as an icon situation. Use <b>text="empty"</b> and no text will be used--only the A HREF code. This is helpful if you want to rely on a background instead.</li>
				<li><b>text="remove":</b> DO NOT USE! Including here to make sure it's seen. Unlike the phone number shortcode, this option isn't available. To remove text, you need to be device specific with mobile_text or desktop_text. To reduce the potential for error, if you use "remove" for text, it'll act the same as if you used "empty".</li>
				<li><b>text="[Email or Chat]":</b> If you use the words "chat" or "email" in your text, you don't need to include a "button" attribute. If you include both in your text (for whatever reason), email will be the dominent result.</li>
				<li><b>offline_text="":</b> Used to provide different text when chat is offline.</li>
				<li><b>offline_text="empty":</b> Use this if you want to rely only on CSS for styling and not have any text.</li>
				<li><b>offline_text="remove":</b> Use to remove the link altogether when chat is offline.</li>
				<li><b>offline_text="Email":</b> Used to switch to an EMAIL option when offline. LHN likely controls this anyway even if you didn't do this, but this allows us to control styling and messaging in cases when we want to.</li>
				<li><b>offline_text="email empty":</b> Used to swith to an EMAIL option when offline, but with no text in order to utilize CSS styling instead (not case sensitive). This is the only attribute that allows two controlling text elements.</li>
				<li><b>offline_id="":</b> Used to customize styling or use in JS code for offline message only. It will override whatever is used as the default ID.</li>
				<li><b>offline_class="":</b> Used to customize styling for offline message only. It will override whatever is used as the default CLASS.</li>
				<li><b>TIP:</b> Text IDs and classes are added to the link. Although it no longer happens as of 12/2016, if the chat were to go offline (i.e. all reps logged out), the link is removed and offline text is used. So, plan your HTML code accordingly.</li>
				<li><b>Call Center Hours:</b> 
						As of December 2016, things changed to 24 hours M-T and 7am - midnight F-Sun. 
						But if the system were to automatically go off when no agents are logged in, this version will still say Chat is online. I think even if someone clicks the button, it'll still show an email form instead. 
						As noted above, if you want the button removed when offline, use "remove" for the offline text and only a commented out message will show in the HTML code.
					</li>
			</ul>
			
			<h3>TEXT LIMITATIONS:</h3>
			<ul class="frn_level_1">
				<li>Use one shortcode per button. Using text without defining the button will just print both version on the page instead and IDs may cause problems. Not defining a "button" has not been tested.</li>
				<li>You can NOT use HTML code in the text option (mostly due to the quotes).</li>
			</ul>

			<h3>EMAIL IMAGE ATTRIBUTES:</h3>
			<ul class="frn_level_1">
				<li><b>url="":</b> This allows you to use a unique email button image on a particular page. If you want to use a default image for all email buttons, instead add the URL for that in the field for it above.</li>
				<li><b>alt="":</b> This is used as the ALT tag for the email image only. The default chat image approach is controlled by LHN JS and can't be modified easily.</li>
			</ul>

			<h3>DEVICE OVERIDES:</h3>
			<ul class="frn_level_1">
				<li><b>MOBILE: </b> 
					<ul class="frn_level_2">
						<li><b>mobile_text="":</b> Overrides anything defined for "text".</li>
						<li><b>mobile_class="":</b> Allows you to use a different class for mobile users. Likely unnecessary since you can rely on mobile screen sizes to change things, but it was easy to incorporate just in case.</li>
						<li><b>mobile_offline_text="":</b> Overrides default offline text for mobile users. All the same features with empty, remove, and email work here just like the defaults above.</li>
						<li><b>mobile_offline_class="":</b> Allows you to use a different CSS style when offline for mobile devices only. Likely unnecessary since you can rely on mobile screen sizes to change things for the main offline_class attribute, but it was easy to incorporate just in case.</li>
					</ul>
				</li>
				<li><b>DESKTOP: </b> 
					<ul class="frn_level_2">
						<li><b>desktop_text="":</b> Overrides anything defined for "text".</li>
						<li><b>desktop_class="":</b> Allows you to use a different class for desktop users.  Likely unnecessary since you can rely on desktop screen sizes to change things, but it was easy to incorporate just in case.</li>
						<li><b>desktop_offline_text="":</b> Overrides default offline text for mobile users. All the same features with empty, remove, and email work here just like the defaults above.</li>
						<li><b>desktop_offline_class="":</b> Allows you to use a different CSS style when offline for desktops only. Likely unnecessary since you can rely on desktop screen sizes to change things for the main offline_class attribute, but it was easy to incorporate just in case.</li>
					</ul>
				</li>
			</ul>

			<h3>ANALYTICS:</h3>
			<ul class="frn_level_1">
				<li><b>!!CONVERSIONS!!:</b> You must include "chat button" or "email button" in your action label for them to be tracked as Goals in GA for landing pages.</li>
				<li><b>category="":</b> Customizes the GA Event "category" shown in Analytics (case sensitive). DO NOT!! use if you are not specifying the button used or both email and chat GA Events will be reported under the same headings.</li>
				<li><b>action="":</b> IMPORTANT!! Customizes the GA Event "action" shown in Analytics (case sensitive). This determines what's tracked as a conversion (i.e. Goal) in Analytics. Read the following notes for details. Older uses of the shortcode will include "where_on_page" instead of action. Either will work. DO NOT!! use if you are not specifying the button used or both email and chat GA Events will be reported under the same headings.</li>
				<li><b>label="":</b> Customizes the GA Event "label" shown in Analytics (case sensitive). This is typically not included by default. But if you need to seperate "actions" out, you can include this to help. DO NOT!! use if you are not specifying the button used or both email and chat GA Events will be reported under the same headings.</li>
				<li><b>Button Images:</b> Default LHN on-page chat buttons or slideout button IMAGES cannot have any GA recording events customized for them. Only the TEXT versions can be customized since they use our own custom versions with the "frn_open_lhnwindow()" JS function.</li>
				<li><b>Email Defaults:</b> The default EMAIL button GA Event Action label is "In-Page Buttons: Email Button Clicks/Touches".</li>
				<li><b>Chat Defaults:</b> The default CHAT button GA Event Action is "In-Page Buttons: Chat Clicks (Slideout or In-Page)". There is no option to change this for now.</li>
			</ul>

			<h3>DEVELOPER NOTES on JS (frn_open_lhnwindow(type)):</h3>
			<ul class="frn_level_1">
				<li>This JS function is used ONLY on text versions and email image buttons. Chat image buttons are controlled via LHN's JavaScript due to the offline/online element.</li>
				<li>The JS function is located in the HEAD tag of every page to avoid any asynchronous issues.</li>
				<li>The shortcode adds an A tag and an onClick attribute that activates this function.</li>
				<li><b>Manual Code: </b>
					<ul class="frn_level_2">
						<li>If you use your own code, it'll be hard to remember where you do so when we switch live chat companies. </li>
						<li>But if you still have to use your own, you can refer to our JS function in the head to control the opening of the windows. </li>
						<li>At a minimum, the first var of "TYPE" needs to be included and used for "chat" or "email". </li>
						<li>Leaving TYPE blank opens the chat window by default and uses the chat's GA Event category and action labels. </li>
						<li>To open an email window and use the default GA event labels, "email" must be used as the type.</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<?php
}
function frn_external_savedb($code) {
	//saves display code changes to DB
	global $frn_settings_page;
	$screen = get_current_screen();
	
	// Makes sure that we are on the FRN Settings Page or it just cancels
	if ( $screen->id != $frn_settings_page ) return;
		
		$frn_dc_db = mysqli_init();
		$frn_dc_db->options( MYSQLI_OPT_CONNECT_TIMEOUT , 4 );
		$frn_dc_db->real_connect("foundations-list.cxa58y0btukq.us-east-1.rds.amazonaws.com", "foundations_list", "ch6aqxTkCQ2gRh", "foundations_list", 3306);
		
		//error message activation
		if(is_user_logged_in()) $frn_debug=true;
			else $frn_debug=false;
		$frn_lhn_dc = "";
		
		if ($frn_dc_db->connect_error) {
			if($frn_debug) die('Failed to connect to ext DB: ' . htmlspecialchars($frn_dc_db->error));
		}
		else {
		
			$code = $frn_dc_db->real_escape_string($code);
			$frn_domain = str_replace("http://","",str_replace("https://","",str_replace("www.","",site_url())));
			$db_phone_code = frn_external_db();
			if($db_phone_code!="[none]" && $db_phone_code!="[error]") {
					//$query = "UPDATE Display_Codes SET display_code='".$code."' WHERE domain='".$frn_domain."'";
				$query = "UPDATE Display_Codes SET display_code=? WHERE domain=?";
				$frn_lhn_dc_prep = $frn_dc_db->prepare($query);
				if(!$frn_lhn_dc_prep) {
					if($frn_debug) die('prepare() failed for ext DB: ' . htmlspecialchars($frn_dc_db->error));
				}
				else {
					//if(!$frn_error) die('Prepare successful. Code: '.$code."; Domain: ".$frn_domain.'; Query: "'.$query.'" ('. $frn_dc_db->errno .') '. $frn_dc_db->error);
					//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$frn_error=false;
					if(!$frn_lhn_dc_prep->bind_param('ss', $code, $frn_domain)) {
						if($frn_debug) die('bind_param() failed for ext DB: ' . htmlspecialchars($frn_dc_db->error));
						$frn_error=true;
					}
					//if(!$frn_error) die('Bind_param was successful. Code: '.$code."; Domain: ".$frn_domain.'; Query: "'.$query.'" ('. $frn_dc_db->errno .') '. $frn_dc_db->error);
					if(!$frn_lhn_dc_prep->execute()) {
						if($frn_debug) die("Error : Execute didn't work; Code: ".$code."; Domain: ".$frn_domain."; Query: \"".$query."\"; Error codes: (".$frn_dc_db->errno .') '. $frn_dc_db->error); 
						$frn_error=true;
					}
					//if(!$frn_error && $frn_debug) die('Execution was successful. Code: '.$code."; Domain: ".$frn_domain.'; Query: "'.$query.'" ('. $frn_dc_db->errno .') '. $frn_dc_db->error);
					$frn_lhn_dc_prep->close();
				}
			}
			else {
			
				//Add new domain to external DB
				//Since domain not found in DB, we'll try adding it to the DB with the display code the admin person added.
				//if($frn_debug) die("Domain not found. Check database. Code: ".$code."; Domain: ".$frn_domain."; Query: \"".$query."\"; Error codes: ".$frn_dc_db->errno .') '. $frn_dc_db->error);
				$options_lhn = get_option('site_lhn_code');
				if(isset($options_lhn['lhn_domain_add'])) {
				if($options_lhn['lhn_domain_add']=="Save") {
					//If admin person manually selects to add the domain to the central DB
					$query = "INSERT INTO Display_Codes (domain, display_code, timestamp) VALUES(?, ?, ?)";
					$frn_lhn_dc_prep = $frn_dc_db->prepare($query);
					
					$date_now = new DateTime();
					//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$frn_lhn_dc_prep->bind_param('sss', $frn_domain, $code, $date_now->format('Y-m-d H:i:s'));
					if($frn_lhn_dc_prep->execute()){
						if($frn_debug) die('Success! New domain added to external DB. ID is : ' .$frn_lhn_dc_prep->insert_id ); 
					}else{
						if($frn_debug) die('Error inserting new domain in external DB: ('. $frn_dc_db->errno .') '. $frn_dc_db->error);
					}
					//if($frn_debug) die("Everything Worked. Check database. Code: ".$code."; Domain: ".$frn_domain."; Query: \"".$query."\"; Error codes: ".$frn_dc_db->errno .') '. $frn_dc_db->error);
					$frn_lhn_dc_prep->close();
				}
				}
			}
			
			$frn_dc_db->close();
		}
		
}
// cleans up data before stored using plugin_admin_init
function plugin_options_lhn($input) {
	//sets up an array when more variables are added
	$input['lhn_window'] = trim($input['lhn_window']);
	$input['lhn_phonecode'] = trim($input['lhn_phonecode']);
	$input['lhn_dept_id'] = trim($input['lhn_dept_id']);
	/*$newinput['lhn_domain_add'] = trim($input['lhn_domain_add']); //see note above for section */
	$input['tabdisable_pgID'] = trim($input['tabdisable_pgID']);
	$input['lhn_autoinvite_mssg'] = trim($input['lhn_autoinvite_mssg']);
	$input['lhn_autoinvite_keywords'] = trim($input['lhn_autoinvite_keywords']);
	$input['lhn_inpage_chat_btnid'] = trim($input['lhn_inpage_chat_btnid']);
	$input['lhn_inpage_email_btnurl'] = trim($input['lhn_inpage_email_btnurl']);
	
	//frn_external_savedb($input['lhn_phonecode']);
	
	return $input;
}









//////////////////////
// FOOTER_OPTIONS
/////////////////////

// Prep Footer form fields
function text_before_ftr_form() {
	//return "";
}
function ftr_priv_url_funct() {
	$options_ftr = get_option('site_footer_code');  //must match second section of register_setting
	
	//for copy text script for privacy url
	$frn_shortcode='[frn_privacy_url]';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$section_id = 'frn_plugin_priv_url';
	$shortcode_box = $section_id.'_box';
	
	echo "<input id='".$section_id."' name='site_footer_code[ftr_priv_url]' size='60' type='text' value='".(isset($options_ftr['ftr_priv_url']) ? $options_ftr['ftr_priv_url'] : null)."' /><br />
	".'
	<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="top">Shortcode: </td>
			<td valign="top"><b><span class="frn_shortcode_sel">'.$frn_shortcode.'</span></b></td>
		</tr><tr>
			<td valign="top">PHP: </td>
			<td valign="top"><font size="1">(PHP version is most common if ever used: add this to footer.php)<br /> </font>
			<b><span class="frn_shortcode_sel">'.$php_shortcode.'</span></b></td>
	</tr></table></div>
	'."
	<div id='frn_plugin_lhn_priv_hlp' class='frn_help_boxes'>Leaving blank will use the default URL for our privacy policy included with the plugin. If you have a custom version for this site, enter the URL for its location here. You can have a special privacy policy just for one page by adding \"url\" to the shortcode and using a full URL to the privacy policy. It will override all defaults (e.g. [frn_privacy_url url=\"http://website.com/privacy.html\"]).<br />
		<b>Shortcode: [frn_privacy_url url=\"[optional]\"']</b><br />
		<b>Header/Footer:</b> ".$php_shortcode.";
	</div>
	";
}
function ftr_funct() {
	$options_ftr = get_option('site_footer_code');  //must match second section of register_setting
	//for copy text script for copyright footer
	$frn_shortcode2='[frn_footer startyear="" sitename="" frn_phone="" frn_privacy_url="" ga_phone_location="" nodiv=""]';
	$php_shortcode2 = '&lt;?php echo do_shortcode(\''.$frn_shortcode2.'\'); ?&gt;';
	$section_id2 = 'frn_plugin_ftr';
	$shortcode_box2 = $section_id2.'_box';
	$activate_autoftr = "";
	if(isset($options_ftr['act_autoftr'])) {
		if(trim($options_ftr['act_autoftr'])!=="") $activate_autoftr = " checked";
	}
	//$options_phone = get_option('site_phone_number'); //Old
	$number=frn_phone_number();
		
	echo "<font size=1>See all customize options in <b>FRN HELP</b> tab at top.</font>
	<textarea id='".$section_id2."' cols='65' rows='5' name='site_footer_code[frn_footer]' >".(isset($options_ftr['frn_footer']) ? $options_ftr['frn_footer'] : null)."</textarea><br />
	<input type='checkbox' name='site_footer_code[act_autoftr]' value='Activate'".$activate_autoftr." style='margin-left:8px;'>&nbsp; Activate Auto Footer <font size=1>(auto added to wp_footer() in footer.php file)</font>
	".'
	<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="top">Shortcode: </td>
			<td valign="top"><b><span class="frn_shortcode_sel">'.$frn_shortcode2.'</span></b> <font size="1">(remove variables you don\'t want to customize)</font></td>
		</tr><tr>
			<td valign="top">PHP: </td>
			<td valign="top"><font size="1">(PHP version most common: add this to footer.php)<br /> </font>
			<b><span class="frn_shortcode_sel">'.$php_shortcode2.'</span></b></td>
	</tr></table></div>
	'."
	<div id='frn_plugin_lhn_ftr_hlp' class='frn_help_boxes'>
		View the FRN HELP tab at the top to see all options and directions. Leaving the textbox above empty will use the default copyright and privacy arrangement wherever you put the shortcode in footer.php or if the Auto Footer is activated, wherever the wp_footer() code is in footer.php. The default footer: <br /> <br />
		<strong>&lt;p style=\"text-align:center;\"&gt;Copyright © 2010-".date('Y')." [Site Name]. All Rights Reserved. | Confidential and Private Call: ".$number." | Privacy Policy &lt;/p&gt;</strong><br /> <br />
		<b>SHORTCODE PARAMETERS:</b> As you can see in the shortcode's parameters above (e.g. startyear=\"\"), you can customize specific pieces to keep them dynamic and make updating in the future easier. You'd leave the box above blank and just add those parameters to the shortcode you add into the footer.php. If you just want to use the default footer, remove all parameters and leave just \"frn_footer\" in the shortcode.<br /> <br />
		<b>CUSTOMIZED FOOTER: </b> If you have a custom version for this site, enter the code in the textbox above. Whatever you put into the textbox above will be used wherever you use the shortcode or the wp_footer() code is in footer.php. If you use both the shortcode and Auto Footer, the copyright information will be placed on the page twice. A good starting place for customizing your footer is:<br /> <br />
		<b><span class=\"frn_shortcode_sel\">&lt;div class=\"frn_footer\"&gt;Copyright 2006-%%year%% %%site_name%% | Confidential and Private Call: %%frn_phone%% | &lt;a href=\"%%frn_privacy_url%%\"&gt;Privacy Policy&lt;/a&gt;&lt;/div&gt;</span></b><br /> <br />
	</div>
	";
}


// cleans up data before stored using plugin_admin_init
function plugin_options_footer($input) {
	//sets up an array when more variables are added
	//if(trim($input['ftr_priv_url'])=='[our default url]') $newinput['ftr_priv_url'] = ""; //resets to blank since default doesn't need to be stored, but should be displayed to admin person
	//else 
	
	if(!isset($input['act_autoftr'])) $input['act_autoftr']="";
	
	$newinput['ftr_priv_url'] = trim($input['ftr_priv_url']);
	$newinput['frn_footer'] = trim($input['frn_footer']);
	$newinput['act_autoftr'] = $input['act_autoftr'];

	$newinput['ftr_chrbt_checkbox'] = $input['ftr_chrbt_checkbox'];
	$newinput['ftr_chrbt_company'] = $input['ftr_chrbt_company'];
	$newinput['ftr_chrbt_category'] = $input['ftr_chrbt_category'];

	$newinput['ftr_hotjar_id'] = trim($input['ftr_hotjar_id']);
	$newinput['ftr_hotjar_radio'] = $input['ftr_hotjar_radio'];

	$newinput['ftr_contently_id'] = trim($input['ftr_contently_id']);
	$newinput['ftr_contently_radio'] = $input['ftr_contently_radio'];

	return $newinput;
}






//////////
// MOBILE_OPTIONS SECTION
//////////


function text_before_mobile_form() {
	//return "";
}
function ftr_mobile_function() {
	$options_mobile = get_option('site_mobile_code');  //must match second section of register_setting
	
	$mftr_activate_chkd=""; $mftr_deactivate_chkd="";
	if(isset($options_mobile['ftr_bar_copyright'])) {
		if($options_mobile['ftr_bar_copyright']=="A") $mftr_activate_chkd=" checked";
		else $mftr_deactivate_chkd=" checked";
	}
	else $mftr_deactivate_chkd=" checked";
	if($mftr_deactivate_chkd!=="") $mftr_display="display:none;"; else $mftr_display="display:block";
	
	echo '
	<div style="margin:0 0 10px 0;">
		<input type="radio" name="site_mobile_code[ftr_bar_copyright]" value="A"'.$mftr_activate_chkd.' onClick=\'showhide("mftr_options","block");\' >Activate <br />
		<input type="radio" name="site_mobile_code[ftr_bar_copyright]" value="D"'.$mftr_deactivate_chkd.' onClick=\'showhide("mftr_options","none");\' >Deactivate <br />
		<small>Note: Use only when main footer isn\'t showing. Main footer is not deactivated.</small>
	</div>
	<div id="mftr_options" style="'.$mftr_display.'">
		Replace Mobile Copyright:<br />
		<textarea id="frn_plugin_ftrbar_mssg" name="site_mobile_code[ftr_bar_copyright_text]" cols="65" rows="3" />'.(isset($options_mobile['ftr_bar_copyright_text']) ? $options_mobile['ftr_bar_copyright_text'] : null).'</textarea><br />
		<small>Use %%frn_phone%% for phone number. Use %%year%% for current year.</small>
	</div>
	<div id="frn_plugin_mobile_hlp" class="frn_help_boxes">The mobile copyright will not deactivate the main site\'s footer set by footer options above. Use this footer only if the main site\'s footer isn\'t showing.</div>
	';
	
}
function ftr_footerbar_function() {
	$options_mobile = get_option('site_mobile_code');  //must match second section of register_setting
	
	//Prepare radio buttons
	$activate_chkd=""; $deactivate_chkd=""; $s_activate_chkd="";
	if(isset($options_mobile['ftr_bar_radio'])) {
		if($options_mobile['ftr_bar_radio']=="A" or $options_mobile['ftr_bar_radio']==="") $activate_chkd=" checked"; //WPTouch only - activated by default to avoid us having to check every niche site
		if($options_mobile['ftr_bar_radio']=="D") $deactivate_chkd=" checked"; //No devices
		if($options_mobile['ftr_bar_radio']=="S") $s_activate_chkd=" checked"; //All devices
	}
	else $activate_chkd=" checked";
	
	$numb_activate_chkd=""; $numb_deactivate_chkd="";
	if(isset($options_mobile['ftr_bar_phone'])) {
		if($options_mobile['ftr_bar_phone']=="A" || $options_mobile['ftr_bar_phone']=="S" || $options_mobile['ftr_bar_phone']==="") $numb_activate_chkd=" checked"; //activated by default
		if($options_mobile['ftr_bar_phone']=="D") $numb_deactivate_chkd=" checked";
	}
	else $numb_activate_chkd=" checked";
	if($deactivate_chkd!=="") $ftr_bar_display="display:none;"; 
		else $ftr_bar_display="display:block";
	
	echo '
	<input type="radio" name="site_mobile_code[ftr_bar_radio]" value="All"'.$s_activate_chkd.' onClick=\'showhide("ftr_bar_options","block");\' >All Devices<br />
	<input type="radio" name="site_mobile_code[ftr_bar_radio]" value="S"'.$s_activate_chkd.' onClick=\'showhide("ftr_bar_options","block");\' >Mobile Devices<br />
	<input type="radio" name="site_mobile_code[ftr_bar_radio]" value="A"'.$activate_chkd.' onClick=\'showhide("ftr_bar_options","block");\' >WPTouch Only<br />
	<input type="radio" name="site_mobile_code[ftr_bar_radio]" value="D"'.$deactivate_chkd.' onClick=\'showhide("ftr_bar_options","none");\' >Deactivate<br />'.'

	<div id="ftr_bar_options" style="'.$ftr_bar_display.'">
		<div style="margin:10px 0 0 0;">
			Background Color:
			<input id="frn_ftr_bckgd" name="site_mobile_code[ftr_bar_bgcolor]" size="10" type="text" value="'.(isset($options_mobile['ftr_bar_bgcolor']) ? $options_mobile['ftr_bar_bgcolor'] : null).'" /> <small>(default: #33BCF5)</small>
		</div>
		<div>
			<table><tr><td valign="top" style="padding-left:0px;">Number in Bar:</td>
			<td><input type="radio" name="site_mobile_code[ftr_bar_phone]" value="A"'.$numb_activate_chkd.' >Activated <br />
			<input type="radio" name="site_mobile_code[ftr_bar_phone]" value="D"'.$numb_deactivate_chkd.'>Deactivated </td>
			</tr></table>
		</div>
		<div>
			Replace Text Prior To Number:<br />
			<textarea id="frn_plugin_ftrbar_intro" name="site_mobile_code[ftr_bar_intro]" cols="65" rows="3" />'.(isset($options_mobile['ftr_bar_intro']) ? $options_mobile['ftr_bar_intro'] : null).'</textarea>
			<small>Use %%frn_phone%% for phone number. Use %%year%% for current year.</small>
		</div>
	</div>
	<div id="frn_plugin_ftrbar_hlp" class="frn_help_boxes">For now, the bar will only activate if WPTouch is installed. If you disable it here, then it won\'t show even if WPTouch installed.</div>
	';
}

// cleans up data before stored using plugin_admin_init
function plugin_options_mobile($input) {
	$newinput['ftr_bar_copyright'] = $input['ftr_bar_copyright'];
	$newinput['ftr_bar_copyright_text'] = trim($input['ftr_bar_copyright_text']);
	$newinput['ftr_bar_radio'] = $input['ftr_bar_radio'];
	$newinput['ftr_bar_bgcolor'] = trim($input['ftr_bar_bgcolor']);
	$newinput['ftr_bar_phone'] = $input['ftr_bar_phone'];
	$newinput['ftr_bar_intro'] = trim($input['ftr_bar_intro']);
	
	return $newinput;
}





/////////
//// SOCIAL_SHARING Shortcode
////////

function frn_social_shortcode($input) {	
	$options_shortcodes=get_option('frn_social_options');
	$activate_chkd=""; $deactivate_chkd="";
	if(isset($options_shortcodes['frn_social'])) {
		if($options_shortcodes['frn_social']=="A") $activate_chkd=" checked";
		if($options_shortcodes['frn_social']=="D" or $options_shortcodes['frn_social']==="") $deactivate_chkd=" checked";
	}
	else $deactivate_chkd=" checked";
	//top four buttons
	$frn_shortcode1top="[frn_social]";
	$shortcode_box1top="frn_social_box_top";
	//top four buttons wrap
	$frn_shortcode1top2="[frn_social version=\"icon\" align=\"center\" div_align=\"right\"]";
	//top buttons but inline with text in front
	$frn_shortcode1top3="[frn_social version=\"icon\" align=\"inline\" div_align=\"center\" pretext=\"Share This: \"]";
	//twitter options
	$frn_shortcode1t="[frn_social type=\"TWITTER\" version=\"icon\" align=\"\" tweet=\"\" link_text=\"\" counter=\"\" url=\"\" account=\"\" hashtag=\"\" related=\"\" related_tagline=\"\"]";
	$shortcode_box1t="frn_social_box1";

	$frn_shortcode1t2="[twitter]";
	$shortcode_box1t2="frn_social_box1t2";
	//twitter button
	//$frn_shortcode2t="[frn_social size=\"\" tweet=\"\" link_text=\"\" account=\"\" related=\"\" related_tagline=\"\"]";
	//$shortcode_box2t="frn_social_box2";
	//pinterest share button
	$frn_shortcode1p="[frn_social type=\"PINTEREST\" version=\"(icon|round|rectangular)\" align=\"\" counter=\"(above|beside)\" image=\"\" summary=\"\" url=\"\"]";
	$shortcode_box1p="frn_social_box4";
	//sharethis - for sharing page (default sharethis)
	$frn_shortcode1s="[frn_social type=\"SHARETHIS\" link_text=\"\" bold=\"\" summary=\"\"]";
	$shortcode_box1s="frn_social_box3";
	//sharethis buttons
	$frn_shortcode2s="[frn_social type=\"SHARETHIS\" version=\"BUTTONS\" plusone=\"\" like=\"\" google=\"\" facebook=\"\" twitter=\"\" linkedin=\"\" email=\"\" pinterest=\"\" sharethis=\"\"]";
	$shortcode_box2s="frn_social_box4";
	//all share options
	$frn_shortcode1a="[frn_social version=\"\" type=\"\" order=\"\" align=\"\" pretext=\"\" css=\"\" link_text=\"\" size=\"\" place=\"top or bottom\" url=\"\" tweet=\"\" account=\"\" related=\"\" related_tagline=\"\" hashtag=\"\" counter=\"\" bold=\"\" summary=\"\" image=\"\" youtube_url=\"\" plusone=\"\" google=\"\" facebook=\"\" twitter=\"\" linkedin=\"\" email=\"\" pinterest=\"\"]";
	$shortcode_box1a="frn_social_box_all";
	
	
	?>
	<input type="radio" name="frn_social_options[frn_social]" value="A"<?=$activate_chkd;?> >Activate: Shortcodes<br />
	<input type="radio" name="frn_social_options[frn_social]" value="D"<?=$deactivate_chkd;?> >Deactivate<br />
	<div style="margin-top:10px;">
		<div class="frn_shortcode_sel"><b><?=$frn_shortcode1top;?>&lt;img&gt;[/frn_social]</b></div>
		<div class="frn_shortcode_sel"><b><?=$frn_shortcode1t2;?></b>tweet text<b>[/twitter]</b></div>
	</div>
	<div id="frn_plugin_social_hlp" class="frn_options_table" style="display:none;">
		<p>See FRN Help tab at top-right for more. For the "version" attribute, you can use icon or text. Leaving blank will use the standard sharing buttons for each network. Use "order" attribute to choose the social networks and thier order.</p>
		<table class="frn_options_table"><tr>
			<td ><h3>EXAMPLE USES: </h3></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">Top Four Networks (Pinterest, Facebook, Twitter, LinkedIn): </h4></td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1top;?></span></b></td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1top;?>&lt;img src="" /&gt;[/frn_social]</span></b><small> * Creates a wrapping DIV</small>
			<br /> <br /> &nbsp; &nbsp; &nbsp;<small>Default Attributes: type="" version="icon" align="center" order="pftl" </small></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">Pre-Text: </td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1top3;?></span></b></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">Twitter Only: </td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1t2;?></span></b></td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1t;?></span></b></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">Pinterest Only: </td>
		</tr><tr>
			<td valign="top"><b><span class="frn_shortcode_sel"><?=$frn_shortcode1p;?></span></b></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">ShareThis (Share Link to Page): </td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1s;?></span></b><br />
			You can still customize the title, image, and summary for a page, but you don't have to. The following shortcode assumes you don't prefer to customize anything.</td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">ShareThis (Share Link to Image): </td>
		</tr><tr>
			<td valign="top"><b><span class="frn_shortcode_sel">[frn_social type="SHARETHIS" link_text="Share This Image " bold="Bold point about object above summary (85 chars in window but FB shows more)" summary="Second key point about the object (FB up to 300 chars)" image="<?=site_url();?>/wp-content/uploads/image.png (image 1600px)"]</span></b></td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">ShareThis (Display their buttons instead): </td>
		</tr><tr>
			<td valign="top"><b><span class="frn_shortcode_sel">[frn_social type="SHARETHIS" version="BUTTONS" ]</span></b><br />
			This option has everything customized in order to work as if a person is sharing an image although the URL being shared goes to the page.</td>
		</tr><tr>
			<td style="font-weight:bold; padding-top: 25px;">All Attributes: </td>
		</tr><tr>
			<td><b><span class="frn_shortcode_sel"><?=$frn_shortcode1a;?></span></b></td>
		</tr></table>
	</div>
	
	<?php
}



//////////
// OPENGRAPH_OPTIONS
//////////
function frn_social_opengraph($input) {	
	$options_social=get_option('frn_social_options');
	$activate_chkd="";
	if(isset($options_social['opengraph_trigger'])) {
		if($options_social['opengraph_trigger']=="Activate") $activate_chkd=" checked";
	}
	//else echo "<h1>Not Set</h1>"; 
	//echo "<h1>Dax: ".$options_social['opengraph_trigger']."</h1>";

	//sets our default social image dimensions
	if(!isset($options_social['default_img_w'])) $options_social['default_img_w']="1200";
	if($options_social['default_img_w']=="") $options_social['default_img_w']="1200";
	if(!isset($options_social['default_img_h'])) $options_social['default_img_h']="717";
	if($options_social['default_img_h']=="") $options_social['default_img_h']="717";

	//as of 4/24/17, we prefer a relative URL. But older rollouts didn't, so we need to allow for both
	$url_start="";
	if(!isset($options_social['default_img'])) $options_social['default_img']="";
	if(!strpos($options_social['default_img'],"http://") && !strpos($options_social['default_img'],"https://")) {
		$url_start=get_home_url();
	}

	?>
	<input id="frn_plugin_frn_ga_dgrphx" type="checkbox" name="frn_social_options[opengraph_trigger]" value="Activate" <?=$activate_chkd; ?> /> Use FRN's Custom Opengraph for Images<br /> <br />
	Default Image: <br />
	<input name="frn_social_options[default_img]" size="60" type="text" value="<?=$options_social['default_img'];?>" /><br />
	w<input name="frn_social_options[default_img_w]" size="10" type="text" value="<?=$options_social['default_img_w'];?>" /> x
	h<input name="frn_social_options[default_img_h]" size="10" type="text" value="<?=$options_social['default_img_h'];?>" /><br />
	<div id="frn_plugin_defimg_hlp" class="frn_options_table" style="display:none;">
	<ul style="list-style: disc; margin-left: 25px;">
		<li>Deactivating this feature only affects the content scanning portion for images. To remove the default social sharing image, you'll have to delete the URL from the field above.</li>
		<li>Yoast's social opengraph features don't select high quality images nor does it consider categories or the homepage. But it still pulls the rest of the OG content needed.</li>
		<li>The default image is always in the Meta for every page on the site so that it can always be selected by the sharer.</li>
		<li>Although it feels tedius, Facebook will not show the default image in some cases if there is no width and height entered. You must include them.</li>
		<li>You must include the entire web address to the default image, including the http://, just like you would if you wanted someone to see it via an email.</li>
		<li>For the homepage, it uses the default image you selected above or nothing if you don't provide it.</li>
		<li>For categories, it scans the first five images on the page and provides all of those post images.</li>
		<li>For pages and posts, it gets the featured images and images in the content and provides those as options--always selecting the quality of the image uploaded to WordPress.</li>
	</ul>
	</div>
	<?php
}
function plugin_store_social($input) {
	$newinput['frn_social'] = $input['frn_social'];
	$newinput['opengraph_trigger'] = $input['opengraph_trigger'];
	$newinput['default_img'] = trim($input['default_img']);
	$newinput['default_img_w'] = trim($input['default_img_w']);
	$newinput['default_img_h'] = trim($input['default_img_h']);
	return $newinput;
}





/////////
//// SHORTCODE_ACTIVATIONS Fields
////////
function plugin_options_sc_fields($input) {	
	/// Since we are using checkboxes, if there is no value in the checkbox, then no variable is passed--they are unset.
	// Because we are introducing this down the line, we need the shortcodes activated by default
	// So, these need to be check to deactivate instead of to activate


	$shortcode_fields=get_option('shortcode_fields');

	//For testing
	//if(isset($shortcode_fields['widget_text_deact'])) {if($shortcode_fields['widget_text_deact']=="") $blank="not set"; echo "widget_text_deact is ".$blank.$shortcode_fields['widget_text_deact']."; ";}
	//if(!isset($shortcode_fields['widget_text_deact'])) echo "widget_text_deact is Not Set; ";
	//if($shortcode_fields['widget_text_deact']=="") echo "widget_text_deact is blank; ";

	/// On by default if Settings not saved yet
	if(isset($shortcode_fields['widget_text'])) {$widget_text=" checked "; $widget_text_deact=""; } elseif(!isset($shortcode_fields['widget_text_deact'])) {$widget_text=" checked "; $widget_text_deact="";} else {$widget_text=""; $widget_text_deact="Y";}
	if(isset($shortcode_fields['widget_title'])) {$widget_title=" checked "; $widget_title_deact=""; } elseif(!isset($shortcode_fields['widget_title_deact'])) {$widget_title=" checked "; $widget_title_deact="";} else {$widget_title=""; $widget_title_deact="Y";}	
	if(isset($shortcode_fields['yoast_seo'])) {$yoast_seo=" checked "; $yoast_seo_deact=""; } else {$yoast_seo=""; $yoast_seo_deact="Y";}
	
	/*
	// THESE DON'T WORK. Disabled for another day of testing
	if(isset($shortcode_fields['single_post_title'])) {$single_post_title=" checked "; $single_post_title_da=""; } else {$single_post_title=""; $single_post_title_da="Y";}
	if(isset($shortcode_fields['wp_title'])) {$wp_title=" checked "; $wp_title_deact=""; } else {$wp_title=""; $wp_title_deact="Y";}
	if(isset($shortcode_fields['the_title'])) {$the_title=" checked "; $the_title_deact=""; } else {$the_title=""; $the_title_deact="Y";}	
	if(isset($shortcode_fields['the_excerpt'])) {$the_excerpt=" checked "; $the_excerpt_deact=""; } else {$the_excerpt=""; $the_excerpt_deact="Y";}
	if(isset($shortcode_fields['bloginfo'])) {$bloginfo=" checked "; $bloginfo_deact=""; } else {$bloginfo=""; $bloginfo_deact="Y";}
	if(isset($shortcode_fields['get_the_excerpt'])) {$get_the_excerpt=" checked "; $get_the_excerpt_deact=""; } elseif(!isset($shortcode_fields['get_the_excerpt_deact'])) {$get_the_excerpt=" checked "; $get_the_excerpt_deact="";} else {$get_the_excerpt=""; $get_the_excerpt_deact="Y";}
	*/

	/// FORM FIELDS:
	?>
	<div>
		<input type="checkbox" name="shortcode_fields[widget_text]" value="Y" <?=$widget_text;?> /> Widget Content
			<input type="hidden" name="shortcode_fields[widget_text_deact]" value="<?=$widget_text_deact;?>" /><br />
		<input type="checkbox" name="shortcode_fields[widget_title]" value="Y" <?=$widget_title;?> /> Widget Titles
			<input type="hidden" name="shortcode_fields[widget_title_deact]" value="<?=$widget_title_deact;?>" /><br />
		<input type="checkbox" name="shortcode_fields[yoast_seo]" value="Y" <?=$yoast_seo;?> /> <span class="frn_shortcode_sel">%%frn_phone%%</span> in Yoast SEO
			<input type="hidden" name="shortcode_fields[yoast_seo_deact]" value="<?=$yoast_seo_deact;?>" /><br />
	</div>
	<!--The following aren't working, so they are hidden for now-->
	<?php /*
		<input type="checkbox" name="shortcode_fields[single_post_title]" value="Y" <?=$single_post_title;?> /> Single_Post_Title() (If Used)
			<input type="hidden" name="shortcode_fields[single_post_title_da]" value="<?=$single_post_title_da;?>" /><br />
		<!-- Not Working wp_title --> 
		<input type="checkbox" name="shortcode_fields[wp_title]" value="Y" <?=$wp_title;?> /> WP_Title() &lt;title&gt; (If Used)
			<input type="hidden" name="shortcode_fields[wp_title_deact]" value="<?=$wp_title_deact;?>" /><br />
		<!-- Not Working the_title --> 
		<input type="checkbox" name="shortcode_fields[the_title]" value="Y" <?=$the_title;?> /> Titles Everywhere (the_title())
			<input type="hidden" name="shortcode_fields[the_title_deact]" value="<?=$the_title_deact;?>" /><br />
		<input type="checkbox" name="shortcode_fields[bloginfo]" value="Y" <?=$bloginfo;?> /> Meta Info (bloginfo())
			<input type="hidden" name="shortcode_fields[bloginfo_deact]" value="<?=$bloginfo_deact;?>" /><br />
		<input type="checkbox" name="shortcode_fields[get_the_excerpt]" value="Y" <?=$get_the_excerpt;?> /> Manual Excerpt (the_excerpt())
			<input type="hidden" name="shortcode_fields[get_the_excerpt_deact]" value="<?=$get_the_excerpt_deact;?>" /><br />
		<input type="checkbox" name="shortcode_fields[the_excerpt]" value="Y" <?=$the_excerpt;?> /> Auto Excerpts (i.e. <--MORE-->) 
			<input type="hidden" name="shortcode_fields[the_excerpt_deact]" value="<?=$the_excerpt_deact;?>" /><br />
	*/ ?>
	<div id='frn_plugin_sc_fields_hlp' class='frn_help_boxes'>
	<ul style="list-style: disc; margin-left: 25px;">
		<li>The widget options apply to all shortcodes, not just this plugins, but all that any plugin or theme has. 
		<li>The Yoast option is only for the phone number. Instead of brackets like a normal shortcode, use the typical Yoast approach of %% before and after "frn_phone". 
		<li>If you prefer to use more variables in Meta fields like page titles and Meta descriptions, make that request. It's not too complicated depending on what you want.
	</ul>
	</div>



<?php
}

function plugin_options_sc_fields_save($input) {
	
	// Widget content (default)
	if(!isset($input['widget_text'])) {
		if(!isset($input['widget_text_deact'])) {$input['widget_text']="Y"; $input['widget_text_deact']="";}
		else $input['widget_text_deact']="Y";
	}
	elseif($input['widget_text']=="") {$input['widget_text']; $input['widget_text_deact']="Y"; }
	else {$input['widget_text']; $input['widget_text_deact']=""; }
	
	// Widget titles (default)
	if(!isset($input['widget_title'])) {
		if(!isset($input['widget_title_deact'])) {$input['widget_title']="Y"; $input['widget_title_deact']="";}
		else $input['widget_title_deact']="Y";
	}
	elseif($input['widget_title']=="") {$input['widget_title']; $input['widget_title_deact']="Y"; }
	else {$input['widget_title']; $input['widget_title_deact']=""; }
	
	//activates %%frn_phone%% variable in Yoast options
	if(!isset($input['yoast_seo'])) $input['yoast_seo_deact']="Y";
		else {$input['yoast_seo']; $input['yoast_seo_deact']=""; }


	///////
	/// THE FOLLOWING DOESN'T WORK, commented out for another day of testing
	//////
	/*


	// Only for bloginfo() uses. Activates in site title and description (and possibly other uses other than URL)
	if(!isset($input['bloginfo'])) $input['bloginfo_deact']="Y";
		else {$input['bloginfo']; $input['bloginfo_deact']=""; }

	// Used for cases when the manual excerpt field is filled out. Haven't tested this.
	if(!isset($input['get_the_excerpt'])) $input['get_the_excerpt_deact']="Y";
		else {$input['get_the_excerpt']; $input['get_the_excerpt_deact']=""; }

	//in post & pages title bar - not h1 titles
	if(!isset($input['single_post_title'])) $input['single_post_title_da']="Y";
		else {$input['single_post_title']; $input['single_post_title_da']=""; }

	//for page & posts title bars (alt method)
	if(!isset($input['wp_title'])) $input['wp_title_deact']="Y";
		else {$input['wp_title']; $input['wp_title_deact']=""; }

	//Automatically generated excerpts in categories, etc. based on content
	if(!isset($input['the_excerpt'])) $input['the_excerpt_deact']="Y";
		else {$input['the_excerpt']; $input['the_excerpt_deact']=""; }

	// Use in all titles that are printed with the_title() function
	if(!isset($input['the_title'])) $input['the_title_deact']="Y";
		else {$input['the_title']; $input['the_title_deact']=""; }
	*/

	return $input;
}









////////
// Settings form for URL_BASE shortcodes
/////////
//These were discointinued officially by 8/29/16 by Dax. These functions were left in main plugins page since older sites still use them (notably Suboxone abuse help.com).
function frn_before_sitebase_info() { 
	//not used
}
function sitebase_sc_info() {
	$options_base=get_option('site_sitebase_code');
	
	//for copy text script
	$frn_shortcode='[ldomain]';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$section_id = 'frn_plugin_sitebase';
	$shortcode_box = $section_id.'_box';
	
	echo "
	<input id='".$section_id."' name='site_sitebase_code[frn_sitebase]' size='60' type='text' value='".(isset($options_base['frn_sitebase']) ? $options_base['frn_sitebase'] : null)."' /><br />";
}
function imagebase_sc_info() {
	$options_base=get_option('site_sitebase_code');
	
	//for copy text script
	$frn_shortcode='idomain';
	$php_shortcode = '&lt;?php echo do_shortcode(\''.$frn_shortcode.'\'); ?&gt;';
	$section_id = 'frn_plugin_imagebase';
	$shortcode_box = $section_id.'_box';
	$frn_shortcode2='ldomain';
	$php_shortcode2 = '&lt;?php echo do_shortcode(\''.$frn_shortcode2.'\'); ?&gt;';
	$section_id2 = 'frn_plugin_sitebase';
	$shortcode_box2 = $section_id2.'_box';
	
	if(trim($options_base["frn_imagebase"])=="") $idomain = site_url();
	else $idomain = $options_base["frn_imagebase"];
	if(trim($options_base["frn_sitebase"])=="") $ldomain = site_url();
	else $ldomain = $options_base["frn_sitebase"];
	
	echo "
	<input id='".$section_id."' name='site_sitebase_code[frn_imagebase]' size='60' type='text' value='".(isset($options_base['frn_imagebase']) ? $options_base['frn_imagebase'] : null)."' /><br />
	".'
	<div id="frn_plugin_sitebase_hlp" class="frn_help_boxes">
		<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="bottom">Shortcodes for Links: </td>
			<td valign="bottom"><b><span class="frn_shortcode_sel">['.$frn_shortcode2.']</span></b></td>
		</tr><tr>
			<td valign="top">&nbsp;&nbsp; <small>For FRN Plugin Fields: </small></td>
			<td valign="top"><b><span class="frn_shortcode_sel">%%'.$frn_shortcode2.'%%</span></b></td>
		</tr><tr>
			<td valign="bottom">Shortcode for Images: </td>
			<td valign="bottom"><b><span class="frn_shortcode_sel">['.$frn_shortcode.']</span></b></td>
		</tr><tr>
			<td valign="top">&nbsp;&nbsp; <small>For FRN Plugin Fields: </small></td>
			<td valign="top"><b><span class="frn_shortcode_sel">%%'.$frn_shortcode.'%%</span></b></td>
		</tr><tr>
			<td valign="top" style="padding-top:5px;">This Replaces the Shortcode if Blank: </td>
			<td valign="top" style="padding-top:5px;"><b><span class="frn_shortcode_sel">'.site_url().'</span></b></td>
		</tr></table></div>
	
		<p>The most important one is <b>ldomain</b> to allow links to work in a sub-directory or sub-domain environment and automatically convert when launching a site. <b>ldomain</b> stands for "link domain". <b>idomain</b> stands for "image domain".</p>
		<p>To keep either shortcode automatic, don\'t enter anything above. As a result, the installed directory in the site\'s General Settings will be used as a default.</p>
		<p>However, if you want to lock down the links but still give you the option to change all the base domains in one fell swoop, you can put something in the fields above and use the shortcodes in the web addresses.</p>
		<p>NOTE: Shortcodes only work in the HEAD area (i.e. meta descriptions), page titles, excerpts, frn_plugin fields on this page and shortcodes, widgets and content. You can\'t use them in menus, theme options, or other plugins.</p>
		<p><br />
		<b>EXAMPLES: </b><br />
			<i>If used in an FRN shortcode like LHN email buttons: </i><b>%%'.$frn_shortcode.'%%</b>/images/image.png<br />
				&nbsp;&nbsp;&nbsp; => <i>Turns into: </i><b>'. $idomain .'/images/image.png</b><br />
			<i style="margin-top:7px;"><i>If not in an FRN shortcode: </i><b>['.$frn_shortcode2.']</b>/contact/<br />
				&nbsp;&nbsp;&nbsp; => <i>Turns into: </i><b>'.$ldomain.'/contact/</b>
		</p>
	</div>
	';
}
function plugin_options_sitebase($input) {
	$newinput['frn_sitebase'] = trim($input['frn_sitebase']);
	$newinput['frn_imagebase'] = trim($input['frn_imagebase']);
	return $newinput;
}

?>