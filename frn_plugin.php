<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );


/**
 * Plugin Name: FRN Settings!
 * Description: This plugin installs the common website elements used on most of our sites. 
 * Version: 3.3.1
 * Author: Daxon Edwards / Foundations Recovery Network
 * Author URI: https://daxon.me
 */
 




/*
	Copyright 2013-2019  Foundations Recovery Network  ( email : frnservices@gmail.com )

	Each field in the FRN Settings page needs the following to work:
	1. Admin
		Define the code for the input field using a function
		Add field to the new section of admin
	2. Saving or storing data in fields
		Create data group
		Create data field(s)
		Use function to filter/qualify data and put multiple information into an array (to save server efficiency)
		Saving arrays is more efficient than using the DB to create a new field for every piece of data.
	3. On-Page
		Create function that will process data stored and create the code that will be added up on page load
		If shortcode, use add_shortcode to tell wp that it's available and reference the function that creates what replaces the shortcode
		Use add_filter to execute a function if going to add data to a section of a page






	/////////////////////////
	/// TABLE_OF CONTENTS: //
	/////////////////////////
	/// Used all caps item to jump "find" and jump to section

	ERROR Notices (line: 90)
	Admin Features (line: 110)
	JQUERY Load (line: 125)
	MOBILE Detection (line: 140)
	ASYCHRONOUS Loading Mod (line: 205)
	FRN_PLUGIN_COMMON Files (line: 230)
	FRN_PLUGIN_PHONE features (line: 250)
	ANALYTICS (GA, Chartbeat, Hotjar)
	EXTERNAL_LINKS Icon
	LIVE_CHAT
	SOCIAL_SHARING shortcode
	Personalization Boxes (CTAs)
	FRN FOOTER_SHORTCODE
		Privacy Policy Shortcode
		MAIN_Footer Shortcode Starting Code
		FRN Footer_Function
	MOBILE_FOOTER
	SOCIAL SHARING AND Open Graph
	Third Party Plugin Activation Test
	FRN_Auto Table of Contents
	Shortcode Activations & Filters
	Shortcode Report
	URL_BASE shortcodes


*/


////////////////////////
// Error Notices      //
////////////////////////
	//error message activation
	//use these settings to turn on PHP error notices change 0 to 1
	/*
	$test_user = "daxon.edwards";
	global $current_user;
	if ( isset($current_user) ) {
		if($current_user->user_login==$test_user) {
			ini_set('display_errors',0);
			ini_set('display_startup_errors',0);
			error_reporting(-1);
			ini_set('error_reporting', E_ALL);
		}
	}
	else error_reporting(0);
	*/
//error_reporting(0);



	
////////////////////////
// ADMIN FEATURES     //
////////////////////////

//so that the server doesn't load the admin code with every front-facing page load
if(is_admin()) {
	//Adds the Settings help tab in admin
	include("admin_helptab.php");

	//Admin Forms and Settings
	include("admin_settings.php");
}





/////////////////////
// jQUERY LOAD - Discontinued
/////////////////////
//disabled 10/27/17 since another approach provided by relying on the dependency when registering scripts.js
/*
if(!function_exists('load_jQuery')) {
function load_jQuery() {
	//loads jQuery if the theme doesn't already. We don't use it in the backend, but WP likely already includes it when needed.
	wp_enqueue_script('jquery');
}
}
*/



//////////////////////////////////
// MOBILE DEVICE CONSIDERATION  //
//////////////////////////////////

//PHP mobile device detection (updated 06-2016)
if (!is_admin()) require_once("part_mdetect.php");

if(!is_admin()) add_action('init', 'frn_mobile_init'); 
function frn_mobile_init() { 
	//turned into a function to move up the initialization on 10/30/17
	if(!is_admin()) {
		global $uagent_obj,$frn_mobile;
		$uagent_obj = new uagent_info();
		$frn_mobile = false;
		if($uagent_obj->DetectTierIphone()) $frn_mobile="Smartphone";
		elseif($uagent_obj->DetectTierTablet()) $frn_mobile="Tablet";
	}

	//FYI--1/22/18
	//Pantheon uses Varnish caching system
	//Due to this plugin, everytime a different device visits the same page, the system re-caches the changes. Sometimes that happens for every visit--making the caching system unhelpful.
	//So, we need to tell Varnish to cache three versions of every page based on device category.
	//Details: https://varnish-cache.org/docs/trunk/users-guide/devicedetection.html
	//Unfortanately we can't modify the Varnish system directly. We have to go through Pantheon for any changes. 
	//As of 1/22/18, Dax opened a help ticket with them to make edits or provide another method to help with caching three different HTML versions of each page, but they could not modify their Varnish system and we would have to look into Cloudflare for a solution instead.

}


//HTML Meta data for mobile considerations
if(!is_admin()) add_action('wp_head', 'frn_mobile_head_js', 1);
function frn_mobile_head_js() {
	/*
	//Now is in the PHP include file
		//global $uagent_obj;
		//$uagent_obj = new uagent_info();
		//if($uagent_obj->DetectTierIphone() || $uagent_obj->DetectTierTablet()) 
	
	//FOUR FEATURES:
		[no longer used] If "script" is found in the main number (used in original DialogTech approach for R&T.com)
		[no longer used] If JavaScript is the preferred method for automatically finding and linking phone numbers (caused problems in navs and other styled areas; just needs more work)
		[Mostly for LHN] DEVICE: Used to pass values to other JS functions - sets a global JS variable for anything to use.
		DEFINATELY USED: Deactivates mobile browser's natural phone linking approach. Sometimes, the browser moves our link to the side and then adds their own--deactivating any tracking we had that relied on a class or ID. It was rare, but enough to add. Likely on older phone models pre-2016. It also just gives us a clean space to work in knowing that the browser won't affect styling or features.
	*/

	
	$phone_linking=""; $frn_phoneBasic=""; $span_processing="";
	
	//$options_phone = get_option('site_phone_number'); //Old before Infinity
	//"SCRIPT" find used in initial DialogTech days for RehabAndTreatment.com when DT's script was used instead of a phone number
	//if(strpos($main_number,"script")!==false) 
		$frn_phoneBasic = 'frn_phoneBasic="'.frn_phone_number().'";';

	/*
	//JS method only (not working as of 12/18) phone_linking = processing method
	if(isset($phones['phone_linking'])) $phone_linking = $phones['phone_linking'];
	if( $phone_linking=="JS" ) $span_processing = '
		js_span_process="'.$phone_linking.'"';
	*/
	
	//identify the device for LHN (if used, it'd be in our custom LHN script likely)
	global $frn_mobile;
	if($frn_mobile=="Smartphone") $device = "isTierIphone = true;\n";
	elseif ($frn_mobile=="Tablet") $device = "isTierTablet = true;\n";
	else $device = "\n";
	
	//disables browser's number auto linking
	?>
	
	<!-- #####
	FRN Browser Phone Linking Deactivation
	Global JS vars for device and number pass-through
	##### -->
	<meta name="format-detection" content="telephone=no">
	<?php
		//print JavaScript settings -- benefits the LHN chat window and JavaScript phone pattern scanning
		if($device!=="" || $frn_phoneBasic!="") 
	?><script type='text/javascript'>
		<?=$frn_phoneBasic;?>
		<?=$device;?>
	</script>
	<!-- #####
	END phone & mobile treatment
	##### -->
	
	<?php
}





/////////////////////
// TRIGGERS ASYCHRONOUS LOADING OF ANY URL
/////////////////////
//this function hooks into the esc_url WP function using the clean_url hook. That function is used already in enqueue scripts and can be called for any function.
//Only URLs that have #async appended to the end will have the async added to the script code.
//Used only for the scripts.js file as of 1/26/17
//core source: https://ikreativ.com/async-with-wordpress-enqueue/ (solution from 2015)
if(function_exists('esc_url') && !function_exists('frn_async_scripts')) { //esc_url is on purpose since it's a WP function and needs to be present. It is a minor function and could change in the future, so protecting this function for the long-term.
function frn_async_scripts($url)
{
    if ( strpos( $url, '#async') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '#async', '', $url );
    else
	return str_replace( '#async', '', $url )."' async='async"; 
    }
add_filter( 'clean_url', 'frn_async_scripts', 11, 1 );
}




//////////////////////////////////
// FRN_PLUGIN_COMMON FILES  //
//////////////////////////////////
function frn_enqueue_script() { //#async

	//analytics js moved to part_analytics_features.php (frn_plugin_js())

	wp_register_style( 'frn_plugin_styles', plugin_dir_url( __FILE__ ).'styles.css', array(), '1.12' ); //LHN, Analytics, and in-context box styles
	wp_enqueue_style( 'frn_plugin_styles', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'frn_enqueue_script', 200 );







//////////////////////////////////
// FRN_PLUGIN_PHONE FEATURES  //
//////////////////////////////////

require_once("part_phone_number_features.php");





//////////////////////////////////
// GOOGLE_ANALYTICS FEATURES  //
//////////////////////////////////

include("part_analytics_features.php");





//////////////////////////////////
// EXTERNAL_LINKS Processing	//
//////////////////////////////////
//icon, new windows
if(!is_admin()) include("part_external_links_processing.php");





////////////////////////////////////
// LIVEHELPNOW Live_Chat FEATURES //
////////////////////////////////////
//including by default since if we deactivate after using shortcodes, we need those removed. 
//Not including this file, will cause the shortcodes to show for users.
//There is a function pulling phone codes for the chat window used in the admin as well (frn_external_db)
include("part_live_chat.php");





//////////////////////////////////
// SOCIAL_SHARING SHORTCODE	    //
//////////////////////////////////
//including by default since if we deactivate after using shortcodes, we need those removed. 
//Not including this file, will cause the shortcodes to show for users.
include("part_social_sharing_options.php");




//////////////////////////////////
// PERSONALIZATION and CONTEXT BOXES  //
//////////////////////////////////
//including by default since if we deactivate after using shortcodes, we need those removed. 
//Not including this file, will cause the shortcodes to show for users.
//Deactivating since feature no longer used on main sites November 2018
//if(!is_admin()) include("part_content_context_boxes.php");




//////////////////////////////
// FRN_Auto Table of Contents
//////////////////////////////
if(!is_admin()) include("part_auto_toc.php");




//////////////////////////////////
// URL Keyword Search Results //
//////////////////////////////////

//Adds functions for to provide a list of posts based on the keywords in the URL
// Most often used in the 404.php template

if(!is_admin()) include("part_related_posts.php");





///////////////////////////////////
// FRN FOOTER_SHORTCODE //
///////////////////////////////////

//Privacy Policy Shortcode
if(!function_exists('frn_privShortcode_funct')) {
function frn_privShortcode_funct($atts) {
	extract( shortcode_atts( array(
		'url' => ''
	), $atts, 'frn_privacy_url' ) );
	
	//assumes array of data is saved.
	$options_priv = get_option('site_footer_code');
	//shortcode simply replaces shortcode with standard url or a custom url
	if(trim($url)!=="") return $url; //if they include a special URL in the privacy shortcode, then it will be used instead of main setting
	else if(trim($options_priv['ftr_priv_url'])!=="") return $options_priv['ftr_priv_url'];  //If there is a custom sitewide privacy url, then it will be used instead of the default
	else return plugin_dir_url( __FILE__ ).'privacy-policy.pdf'; //this is the default URL for the privacy policy that all sites will use
	
}
}

///////////
// MAIN_Footer Shortcode
//shortcode requires "return" not "echo"
// Splitting into two functions enables us to use the same core footer code for two purposes.
function frn_ftrShortcode_funct($atts,$content="",$sc="") {
	$new_atts = shortcode_atts( array(
		'sitename' => get_bloginfo( 'name' ),
		'startyear' => '',
		'align' => 'center',
		'frn_phone' => '', //original
		'number' => '',
		'frn_privacy_url' => '',
		'ga_phone_category' => '',
		'ga_phone_location' => 'Phone Clicks in Footer',
		'ga_phone_label' => '',
		'frn_number_style' => '',
		'paragraph' => '',
		'nodiv' => ''
	), $atts, 'frn_footer' );
	return frn_footer($new_atts,$content,$sc);
}



///////////
//// Footer_Function Shortcode
if(!is_admin()) add_action('wp_footer', 'frn_footer', 1);
function frn_footer($atts="",$content="",$sc="") {

	/* Notes:
		This function is triggered for every page load:
			1) When auto footer is activated in Settings (rare)
			2) When the shortcode is used.
	*/

	$options_ftr = get_option('site_footer_code'); //load all footer settings
	if(!isset($options_ftr['act_autoftr'])) {
			$options_ftr['act_autoftr']="";
	}

	if($options_ftr['act_autoftr']!=="" || $sc=="frn_footer") {
		//if $sc is not blank, then you know it's a shortcode that was used and we'll need to use "return" instead of "echo" and build things a little differently.
		
		$defaults = array(
			'sitename' => get_bloginfo( 'name' ),
			'startyear' => '',
			'align' => 'center',
			'paragraph' => '',
			'frn_phone' => '', //original
			'number' => '', //probably not used, added 11/2018
			'frn_privacy_url' => '',
			'ga_phone_category' => '', //original
			'ga_phone_location' => '', //original
			'ga_phone_label' => '', //original
			'category' => 'Phone Numbers', //probably not used, added 11/2018
			'action' => 'Phone Clicks in Footer', //probably not used, added 11/2018
			'label' => '', //added 11/2018
			'frn_number_style' => '', //original
			'nodiv' => ''
		);
		//combine arrays, but ignore defaults if previously defined
		$atts=shortcode_atts($defaults,$atts); 
		extract($atts); //turn keys into vars
		
		if($number=="") $number=$frn_phone; //consolidate attributess
		//Add the manual number key and value back into the default atts before processing via main phone number function
		if($number!=="") {
			$number_array = array('number' => $number);
			$atts=shortcode_atts($atts,$number_array); 
		}

		if(!isset($options_ftr['frn_footer'])) $options_ftr['frn_footer']="";
		$footer_code = trim($options_ftr['frn_footer']);
		$printed_number="";
		if(function_exists('frn_phone')) { //added to help with troubleshooting
			$printed_number=frn_phone($atts);
		}

		$style="";
		if($frn_number_style=="none") $frn_number_style="";
		else if($frn_number_style!=="") $style=$frn_number_style;
		

		/* OLD NUMBER APPROACH PRIOR TO 12/2018
		//checks if phone is needed in the footer
		if(($footer_code!=="" AND stripos($footer_code,"%%frn_phone%%")>=0) || $footer_code=="") {
			//Cases:
			//  if manual code provided and frn_phone used
			//  if using default footer code (ie. no manual option provided)
			
			if($ga_phone_category!=="") $ga_phone_category=' ga_phone_category="'.$ga_phone_category.'"';
			if($ga_phone_location!=="") $ga_phone_location=' ga_phone_location="'.$ga_phone_location.'"';
			if($ga_phone_label!=="") $ga_phone_label=' ga_phone_label="'.$ga_phone_label.'"';
			
				// add in main frn_phone function
				// search code for site_phone
				if($number=="") {
					$site_phone_options = get_option('site_phone_number');
					$number = $site_phone_options['site_phone']; //strip_tags(stripslashes($site_phone_options['site_phone'])); //cleans out all HTML and quotes
				}
				
				if($number!=="") {
					//JavaScript way: $printed_number = '<span id="frn_phones"'.$ga_phone_category.$ga_phone_location.$ga_phone_label.$frn_number_style.' >'.$site_phone.'</span>';
					$class=""; $lhn_tab_disabled=""; $intl_prefix="";
					$printed_number=frn_phone_prep(array($site_phone,$site_phone,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$lhn_tab_disabled,$intl_prefix));
					else $printed_number=$site_phone;
				}
		//}
		*/
		
		if($frn_privacy_url=="") {
			$frn_privacy_url = plugin_dir_url( __FILE__ ).'privacy-policy.pdf'; //this is the default URL for the privacy policy that all sites will use
			if(isset($options_ftr['ftr_priv_url'])) {
				//If there is a custom sitewide privacy url, then it will be used instead of the default
				if(trim($options_ftr['ftr_priv_url'])!=="") $frn_privacy_url = $options_ftr['ftr_priv_url'];  
			}
		}
		
		//Check if there is a manually entered footer or not
		if($footer_code!=="") {

			$footer_code = str_replace("%%frn_phone%%",$printed_number,$footer_code);
			$footer_code = str_replace("[frn_phone]",$printed_number,$footer_code);
			
			
			$footer_code=str_replace('%%frn_privacy_url%%',$frn_privacy_url,$footer_code);
			$footer_code=str_replace('[frn_privacy_url]',$frn_privacy_url,$footer_code);
			
			//replace site name
			$footer_code=str_replace('%%site_name%%',$sitename,$footer_code);
			$footer_code=str_replace('[site_name]',$sitename,$footer_code);
			$footer_code=str_replace('%%sitename%%',$sitename,$footer_code);
			$footer_code=str_replace('[sitename]',$sitename,$footer_code);
			
			//use dynamic date
			$footer_code=str_replace('%%year%%',date("Y"),$footer_code);
			$footer_code=str_replace('[year]',date("Y"),$footer_code);
			
			$frn_footer = "
			<!--START COPYRIGHT, WEBSITE NAME, PHONE NUMBER, PRIVACY POLICY-->
			".$footer_code."</div>
			<!-- END COPYRIGHT, WEBSITE NAME, PHONE NUMBER, PRIVACY POLICY -->
			";
		 
		}
		else {
			//If there is not a manually entered footer then build our default version
			
			if(trim($startyear)!=="") $startyear.="-";
			if($paragraph!=="") $paragraph = strtolower(trim($paragraph));
			if($nodiv=="") {$nodiv_start="<div class=\"frn_footer\">";$nodiv_end="</div>";}
			else {$nodiv_start="";$nodiv_end="";}
			
			$frn_footer = "
			<!--START COPYRIGHT, WEBSITE NAME, PHONE NUMBER, PRIVACY POLICY-->
			".$nodiv_start;
			if($paragraph=="" || $paragraph=="yes" || $paragraph=="y") $frn_footer .= "<p align=\"".$align."\">";
			$frn_footer .= "Copyright © ".$startyear.date("Y")." ".$sitename.". All Rights Reserved. | Confidential and Private Call: ".$printed_number." | <a href=\"".$frn_privacy_url."\" >Privacy Policy</a>";
			if($paragraph=="" || $paragraph=="yes" || $paragraph=="y") $frn_footer .= "</p>";
			$frn_footer .= "
			".$nodiv_end."
			<!-- END COPYRIGHT, WEBSITE NAME, PHONE NUMBER, PRIVACY POLICY -->
			";
			
		}
		
		if($sc=="frn_footer") return $frn_footer; //shortcode
		else echo $frn_footer;					  //auto footer
	} 
}
/// END FOOTER
///////////








//////////////////////
// MOBILE_FOOTER	//
//////////////////////
if(!function_exists('mobile_stationary_footer')) {
function mobile_stationary_footer() {
	//this footer get automatically added if WP Touch function is active unless manually deactivated in FRN Settings
	//likely used on Black Bear and Old FRN
	global $frn_mobile;
	if(!is_admin() ) {  //Removed mobile requirement 8/14/18: && $frn_mobile
		$options_mobile = get_option('site_mobile_code'); //load all mobile settings
		
		
		//////
		//MOBILE COPYRIGHT
		$copyright="";
		if(isset($options_mobile['ftr_bar_copyright'])) {
			if($options_mobile['ftr_bar_copyright']=="A") {
				//this is the default copyright, defining first to avoid duplication for custom text and since default will be most often
				$copyright = "Copyright © ".date("Y")." ".get_bloginfo( 'name' ).". <a href=\"".$frn_privacy_url."\" >Privacy Policy</a>";
				//Replaces default with custom if true
				if(isset($options_mobile['ftr_bar_copyright_text'])) {
				if(trim($options_mobile['ftr_bar_copyright_text'])!=="") {
					$copyright = $options_mobile['ftr_bar_copyright_text'];
					$copyright = str_replace("%%frn_phone%%",$phone,$copyright);
					$copyright = str_replace("%%year%%",date("Y"),$copyright);
				}
				}
				
		?>
		
		<!-- FRN MOBILE FOOTER -->
		<p class="frn_mobile_copyright" style="margin-bottom:15px;"><?=$copyright;?></p>
		<!-- END MOBILE FOOTER -->
		
		<?php
				}
			}
		// END MOBILE COPYRIGHT
		/////

		
		/* original phone number method prior to 12/2018
			$ga_phone_label="";
			$style="";
			$class="";
			$lhn_tab_disabled="";
			$intl_prefix="";
			$options_mobile = get_option('site_mobile_code'); //load all mobile settings
			$options_phone = get_option('site_phone_number');
			$number_from_db = frn_phone_for_links($options_phone['site_phone']);
			//if not defined in the shortcode, then pull from DB
			$printed_number = trim(stripslashes($options_phone['site_phone']));
			$frn_number = $number_from_db;
			$ga_phone_category = "Phone Numbers";
			$ga_phone_location = "Phone Clicks in Floating Footer Bar";
			//phone_linking = processing method
			if(!isset($options_phone['phone_linking'])) $options_phone['phone_linking']="";
			if($options_phone['phone_linking']!=="N") $phone = frn_phone_prep(array($printed_number,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$lhn_tab_disabled,$intl_prefix));
				else $phone = $printed_number;
		*/
		$atts = array( 'action' => "Phone Clicks in Floating Footer Bar" );
		$phone="";
		if(function_exists('frn_phone_number')) {
			$phone=frn_phone($atts);
		}
		
		//FLOATING FOOTER
		$bar_activate = false;
		if(isset($options_mobile['ftr_bar_radio'])) {
			if($options_mobile['ftr_bar_radio']=="D") $bar_activate = false; //disables no matter what
			elseif($frn_mobile && $options_mobile['ftr_bar_radio']=="S") $bar_activate = true; //all mobile devices (whether wptouch is installed or not)
			elseif(($options_mobile['ftr_bar_radio']=="" || $options_mobile['ftr_bar_radio']=="A") && function_exists('wptouch_the_footer_message')) $bar_activate = true;  //activated only for wp touch (using A since many sites already have that saved for this circumstance)
			elseif($options_mobile['ftr_bar_radio']=="All") $bar_activate = true; //all  devices (whether wptouch is installed or not)
			else $bar_activate = false;
		}
		elseif(function_exists('wptouch_the_footer_message')) $bar_activate = true; //activated only for wp touch by default
		
		if($bar_activate) {
			
			$bg_color="#33BCF5";
			if(isset($options_mobile['ftr_bar_bgcolor'])) {
				if($options_mobile['ftr_bar_bgcolor']!=="") $bg_color=$options_mobile['ftr_bar_bgcolor'];
			}
			
			$frn_privacy_url = plugin_dir_url( __FILE__ ).'privacy-policy.pdf';
			
			$intro="Confidential & Private. We're here to help.";
			if(isset($options_mobile['ftr_bar_intro'])) {
			if($options_mobile['ftr_bar_intro']!=="") {
				$intro = $options_mobile['ftr_bar_intro'];
				$intro = str_replace("%%frn_phone%%",$phone,$intro);
				$intro = str_replace("%%year%%",date("Y"),$intro);
			}
			}
			
			if(isset($options_mobile['ftr_bar_phone'])) {
				if($options_mobile['ftr_bar_phone']=="D") $phone="";
			}
			
			$mobile_limit = "480"; //default for FRN.com
			if(strpos(get_home_url(),"lcaccepted")===true) $mobile_limit = "1024";

			?>
			
			<!-- START FLOATING FOOTER BAR -->
			<style>
				#frn_floating_footer, .frn_mobile_copyright {
					display:none;
				}
				@media only screen and (max-device-width:<?=$mobile_limit;?>px) {
					#frn_floating_footer {
						text-align: center;
					    width: auto;
						left: 0;
						right: 0;
					    display: block;
						color: #333; /*#333; A7D7FB;#AEE0FB*/
						font-weight: bold;
						padding:5px 3px;
						position: fixed;
						bottom: 0;
						z-index: 1000;
						font-size:30px;
						border:1px solid gray;
						background: -moz-linear-gradient(top,  rgba(0,0,0,0) 46%, rgba(0,0,0,0.04) 54%, rgba(0,0,0,0.25) 100%); /* FF3.6+ */
						background: -webkit-gradient(linear, left top, left bottom, color-stop(46%,rgba(0,0,0,0)), color-stop(54%,rgba(0,0,0,0.04)), color-stop(100%,rgba(0,0,0,0.25))); /* Chrome,Safari4+ */
						background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 46%,rgba(0,0,0,0.04) 54%,rgba(0,0,0,0.25) 100%); /* Chrome10+,Safari5.1+ */
						background: -o-linear-gradient(top,  rgba(0,0,0,0) 46%,rgba(0,0,0,0.04) 54%,rgba(0,0,0,0.25) 100%); /* Opera 11.10+ */
						background: -ms-linear-gradient(top,  rgba(0,0,0,0) 46%,rgba(0,0,0,0.04) 54%,rgba(0,0,0,0.25) 100%); /* IE10+ */
						background: linear-gradient(to bottom,  rgba(0,0,0,0) 46%,rgba(0,0,0,0.04) 54%,rgba(0,0,0,0.25) 100%); /* W3C */
						filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#40000000',GradientType=0 ); /* IE6-9 */
					}
					.frn_floating_ftr_intro {
						font-size:12px;
						color:#555;
					}
					.frn_floating_ftr_phone a, .frn_floating_ftr_phone a:visited {
						color:#ECFAFF !important; /*22789B;*/
					}
					.footer {
						margin-bottom:140px;
					}
				}
			</style>
			<div id="frn_floating_footer" style="background-color:<?=$bg_color;?>">
				<div class="frn_floating_ftr_phone">
					<div class="frn_floating_ftr_intro"><?=$intro;?></div>
					<?=$phone;?>
				</div>
			</div>
			<!-- END FLOATING FOOTER BAR -->
			
			
			<?php
		}
		
	}
}
}






/*
//////////////////////////////
// URL_BASE SHORTCODES	    //
//////////////////////////////
//shortcode requires "return" not "echo"
//discontinued Fall 2018 (plugin v3.0 s)
function frn_baseurl_funct($atts){
	if(!is_category() && !is_home() && !is_tag() && !is_author() && !is_date() && !is_search()) {
		$options_basedomain = get_option('site_sitebase_code');
		
		//create site base url
		$site_base = site_url();
		if(isset($options_basedomain['frn_sitebase'])) {
			if(trim($options_basedomain['frn_sitebase'])!=="") $site_base = trim($options_basedomain['frn_sitebase']);
		}
		
		if(isset($_SERVER["HTTPS"])) {if($_SERVER["HTTPS"] == "on") $site_base = str_replace('http://','https://',$site_base);}
			
	}
	return $site_base;
}
function frn_imagebaseurl_funct($atts){
	if(!is_category() && !is_home() && !is_tag() && !is_author() && !is_date() && !is_search()) {
		$options_basedomain = get_option('site_sitebase_code');
		
		//create image base url
		$site_base = site_url();
		if(isset($options_basedomain['frn_imagebase'])) {
			if(trim($options_basedomain['frn_imagebase'])!=="") $site_base =  trim($options_basedomain['frn_imagebase']);
		}
		
		if(isset($_SERVER["HTTPS"])) {if($_SERVER["HTTPS"] == "on") $site_base = str_replace('http://','https://',$site_base);}
	}
	return $site_base;
}
*/




//////////////////////////////////
// ALL SHORTCODE FUNCTION CALLS //
//////////////////////////////////

if ( !is_admin() ){ //if page is not an admin page
	
	//Removed 10/27/17 since jQuery is activated automatically when the scripts.js registration requires it
	//add_action('init', 'load_jQuery'); //loads jQuery if the theme doesn't already (function above)
	
	
	add_action('wp_footer', 'mobile_stationary_footer', 11);
	
	
	//shortcode works in main content areas by default. The following applies them to more areas.
	add_shortcode( 'frn_privacy_url', 'frn_privShortcode_funct' );
	add_shortcode( 'frn_footer', 'frn_ftrShortcode_funct' );
	//add_shortcode( 'frn_sitebase', 'frn_baseurl_funct' );
	//add_shortcode( 'ldomain', 'frn_baseurl_funct' );
	//add_shortcode( 'frn_imagebase', 'frn_imagebaseurl_funct' );
	//add_shortcode( 'idomain', 'frn_imagebaseurl_funct' );
	
	
	////////
	/// FILTERS FOR SHORTCODES

	// activates our shortcodes to be used in specific functions on the site
	// most use the normal do_shortcode option, but some utilize a unique function below to filter the information and replace our shortcodes.
	// Yoast is the only one that activates only one shortcode. All other options activate all shortcodes on that field, not just ours.
	// initial idea from: https://wordpress.org/plugins/jonradio-shortcodes-anywhere-or-everywhere/developers/

	// FRN Plugin shortcode location activation settings var
	$shortcode_fields=get_option('shortcode_fields');

	//////
	// Defaults
	// in widget code (if first time, activated by default)
	if(!isset($shortcode_fields['widget_text'])) { 
		if(isset($shortcode_fields['widget_text_deact'])) {
			if($shortcode_fields['widget_text_deact']=="") add_filter('widget_text', 'do_shortcode'); 
		}
	}
	elseif($shortcode_fields['widget_text']=="Y") add_filter('widget_text', 'do_shortcode');  

	// widget title not working
	//in widget code (if first time, activated by default)
	if(!isset($shortcode_fields['widget_title'])) { 
		if(isset($shortcode_fields['widget_title_deact'])) {
			if($shortcode_fields['widget_title_deact']=="") add_filter('widget_title', 'do_shortcode'); 
		}
	}
	elseif($shortcode_fields['widget_title']=="Y") add_filter('widget_title', 'do_shortcode');  
	
	// Yoast codes
	// Register our own Yoast shortcode (but only if Yoast activated)
	if(isset($shortcode_fields['yoast_seo'])) {
		if($shortcode_fields['yoast_seo']=="Y") {
			//run this only after all activated plugins are loaded
			add_action('plugins_loaded', 'frn_sc_yoast_activated');
		}
	}
	

	/*
	//////
	// THESE DON'T WORK. Saving for another day of testing.
	// for manual excerpts (default) -- if even used in the theme -- often they are used on category pages
	// we are activating by default since less technical users may not think about activating it 
	if(!isset($shortcode_fields['get_the_excerpt'])) { 
		if(isset($shortcode_fields['get_the_excerpt_deact'])) {
			if($shortcode_fields['get_the_excerpt_deact']=="") add_filter('get_the_excerpt', 'do_shortcode'); 
		}
	}
	elseif($shortcode_fields['get_the_excerpt']=="Y") add_filter('get_the_excerpt', 'do_shortcode');  
	if(isset($shortcode_fields['bloginfo'])) { if($shortcode_fields['bloginfo']=="") add_filter('bloginfo ', 'do_shortcode'); } //site title and tagline - all bloginfo options except "url", "directory" and "home"
	if(isset($shortcode_fields['single_post_title'])) { if($shortcode_fields['single_post_title']=="") add_filter('single_post_title ', 'do_shortcode'); } //in post & pages title bar - not h1 titles
	if(isset($shortcode_fields['wp_title'])) { if($shortcode_fields['wp_title']=="") add_filter('wp_title ', 'do_shortcode'); } //for page & posts title bars (alt method)
	if(isset($shortcode_fields['the_title'])) { if($shortcode_fields['the_title']=="") add_filter('the_title ', 'do_shortcode'); } //for page & post titles everywhere, anywhere the_title() is used (cats, tags, blog page, etc.)
	
	// Excerpts: Automatic excerpts like categories, etc.
	if(isset($shortcode_fields['the_excerpt'])) { 
		if($shortcode_fields['the_excerpt']=="") {
			//applies to automatic excerpts only; the_excerpt() (this doesn't apply to manual ones)
			//code taken from: https://wordpress.org/plugins/jonradio-shortcodes-anywhere-or-everywhere/developers/
			$text = get_the_content( '' );
			$text = do_shortcode( $text );
			$text = apply_filters( 'the_content', $text );
			$text = str_replace(']]>', ']]&gt;', $text);
			$excerpt_length = apply_filters( 'excerpt_length', 55 );
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
			$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
			return $text;
		}
	}
	
	/////
	// Custom Fields for Posts & Pages
	if(isset($shortcode_fields['custom_fields'])) {
		if($shortcode_fields['custom_fields']!=="") echo do_shortcode( get_post_meta( get_the_id(), $shortcode_fields['custom_fields'] , true ) );
	}
	*/

}



//////////////////////////////////
// TEST IF CURRENT PAGE IS A PAGE/POST
//////////////////////////////////
function frn_check_if_post() {
	if( (is_single() || is_page())   &&   !is_home() && !is_front_page() && !is_404()) {
			return "yes";
	} 
	else return "no";
}



//////////////////////////////////
// DISABLE TEXTURIZE FOR SOME SHORTCODES
//////////////////////////////////
//avoids transformations of quotes to smart quotes, apostrophes, dashes, ellipses, the trademark symbol, and the multiplication symbol.
//more info: https://codex.wordpress.org/Function_Reference/wptexturize
add_filter ('no_texturize_shortcodes', 'frn_sc_notexturize' );
function frn_sc_notexturize( $shortcodes ) {
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'twitter';
    return $shortcodes;
}
// Fix Problem with WordPress converting line breaks in our printed HTML to paragraphs. 
// Solution: remove and re-prioritize wpautop to prevent auto formatting inside shortcodes
// Disabled since I just removed all line breaks in any code provided by this plugin. This became unnecessary to process and I didn't want to mess up other shortcodes that depended on the natural order.
//remove_filter( 'the_content', 'wpautop' );
//add_filter ( 'the_content', 'wpautop', 99 );
//add_filter ('the_content', 'shortcode_unautop', 100 ); // shortcode_unautop is a core function



//////////////////////////////////
// TEST IF THIRD PARTY PLUGIN ACTIVATED
//////////////////////////////////

// The following delays the function loading until all activated plugins have been loaded (source: http://stackoverflow.com/questions/39293716/detect-yoast-seo-plugin)
// Here just for an example. Include these where you need to delay a function from loading
//add_action('init', 'your_function');
//add_action('plugins_loaded', 'your_function');

//NOTE: when calling this function from within part_related_posts.php, although the $plugin_url was defined, the in_array would not work. Gave up testing why.
function frn_test_plugin_activation ( $plugin_url="" ) {
    if($plugin_url!=="") {
	    //multisite detection
	    $network_active = false;
	    /* if ( is_multisite() ) {
	        $plugins = get_site_option( 'active_sitewide_plugins' );
	        if ( isset( $plugins[$plugin_url] ) ) {
	            $network_active = true;
	        }
	    } */
	    // get_option returns an array. in_array looks for the Yoast plugin URL in that array of active plugins
	    //print_r(get_option( 'active_plugins' ));
	    return in_array( $plugin_url, get_option( 'active_plugins' ) ) || $network_active;
	}
}




/////
// YOAST-SPECIFIC FUNCTIONS
/////
//These are activated above: 
	// test if Yoast activated
	// adds our own variable to Yoast for phone numbers
function frn_sc_yoast_activated() {
	//Test if plugin activated
	//If so, then register our own var
	if(frn_test_plugin_activation ( 'wordpress-seo/wp-seo.php' )) {
		add_action( 'wpseo_register_extra_replacements', 'frn_sc_add_phone' ); 
	}
}
function frn_sc_add_phone() {
    wpseo_register_var_replacement( '%%frn_phone%%', 'frn_phone_for_links', 'advanced', 'You cannot customize the number. This only prints the number stored in the FRN Settings.' );
    // Example from Yoast Help: wpseo_register_var_replacement( 'myvar2', array( 'class', 'method_name' ), 'basic', 'this is a help text for myvar2' );
}